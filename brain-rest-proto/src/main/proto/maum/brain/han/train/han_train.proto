syntax = "proto3";

package maum.brain.han.train;

import "google/protobuf/empty.proto";
import "maum/common/lang.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/duration.proto";
import "maum/brain/han/core/han_core.proto";
import "maum/brain/han/data/han_data.proto";

message Trainer{
  maum.brain.han.data.RawDataLoader raw_data_loader = 1;
  maum.brain.han.data.Nlp nlp = 2;
  maum.brain.han.core.Model model = 3;
}

enum RawDataType{
  ACLIMDB = 0;
  TA = 1;
  TSV = 2;
}

message Aclimdb{
  string dir_path = 1;
  string train_dir = 2;
  string test_dir = 3;
}

message Ta{
  string file_path = 1;

  float test_ratio = 2;
}

message Tsv{
  repeated string col_names = 1;
  string doc_uid_name = 2;
  string sentence_name = 3;

  string data_file_path = 4;
  string label_file_path = 5;

  float test_ratio = 6;
}

message TrainData{
  message Common{
    string encoding = 1;

    int32 max_batch_size = 2;
    int32 train_max_size_prod = 3;
    int32 test_max_size_prod = 4;
    int32 epoch_size = 5;
    int32 log_per_update = 6;

    bool use_graph = 7;
  }
  Common common = 1;
  RawDataType raw_data_type = 2;
  Aclimdb aclimdb = 3;
  Ta ta = 4;
  Tsv tsv = 5;
}

message HanModel {
  string model = 1; /// model name should be alphanum
  maum.common.LangCode lang = 2;

  string pretrained_model_path = 3;
  Trainer option = 4;

  TrainData data = 5;

  // when done or cancelled callback url will be called
  string callback_url = 100;
}

message HanTrainKey {
  string train_id = 1;
}

message HanTrainBinary {
  bytes pt = 1;
}

enum HanTrainResult {
  training = 0;
  success = 1;
  failed = 2;
  cancelled = 3;
  preparing = 4;
}

enum HanTrainStep {
  HAN_TRAIN_START = 0;
  HAN_LOAD_RESOURCES = 1;
  HAN_LOAD_NN_MODEL = 2;
  HAN_LOAD_RAW_DATA = 3;
  HAN_NLP_TRAIN_DATA = 4;
  HAN_NLP_TEST_DATA = 5;
  HAN_TRAIN_RUN = 6;
  HAN_TRAIN_CLEAN = 7;
  HAN_TRAIN_DONE = 8;
}

message HanTrainStatus{
  string key = 1;
  HanTrainStep step = 2;
  string model = 3;
  maum.common.LangCode lang = 4;
  HanTrainResult result = 5;

  // PROGRESS BAR DISPLAY
  int32 maximum = 11;
  int32 value = 12;

  Trainer option = 21;
  TrainData data = 22;
  int32 epoch = 23;

  google.protobuf.Timestamp started = 41;
  google.protobuf.Duration elapsed = 42;
  string logfile_path = 43;
  repeated string logs = 44;

  string error = 101;
}

message HanTrainStatusList {
  repeated HanTrainStatus han_trains = 1;
}

service HanTrainer{
  rpc Open(HanModel) returns (HanTrainKey);
  rpc GetProgress(HanTrainKey) returns(HanTrainStatus);
  rpc GetBinary(HanTrainKey) returns (stream HanTrainBinary);
  rpc Close(HanTrainKey) returns (HanTrainStatus);
  rpc Stop(HanTrainKey) returns (HanTrainStatus);
  rpc GetAllProgress(google.protobuf.Empty) returns (HanTrainStatusList);
  rpc RemoveBinary(HanTrainKey) returns (google.protobuf.Empty);
}

message TrainProcStatus {
  string key = 1; // uuid
  int32 pid = 2;
  HanTrainResult result = 3;
  int32 sig_no = 10;
}

// INTERNAL USE ONLY
// IF REMOTE is not 127.0.0.1, then reject it
service TrainerMonitor {
  rpc Notify(TrainProcStatus) returns (google.protobuf.Empty);
}