package brain.db.mapper;

import brain.api.common.model.usage.UsageDto;
import brain.api.common.model.user.DjangoClientDto;

import java.util.List;
import java.util.Map;

public interface DjangoMapper {
    int insertDjangoClient(DjangoClientDto clientDto);
    List<UsageDto> selectSttusage(Map reqParam);
}
