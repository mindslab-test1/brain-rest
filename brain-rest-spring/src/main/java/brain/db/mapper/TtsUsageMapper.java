package brain.db.mapper;

import brain.api.tts.model.server.TtsResultDto;
import brain.api.tts.model.server.TtsUsageDto;
import brain.api.tts.model.server.TtsUsageSelectDto;

import java.util.List;

public interface TtsUsageMapper {
    int insertTtsUsage(TtsUsageDto usageDto);
    List<TtsUsageDto> selectAll();
    List<TtsUsageDto> selectByDate(TtsUsageSelectDto usageSelectDto);
    int insertTtsResult(TtsResultDto dto);
}
