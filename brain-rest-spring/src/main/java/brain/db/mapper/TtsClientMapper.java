package brain.db.mapper;

import brain.api.tts.model.server.TtsModelDto;

import java.util.Map;

public interface TtsClientMapper {
    public TtsModelDto getTtsModelDto(Map <String, String> ttsInfo);
    public TtsModelDto getPublicTtsModelDto(Map <String, String> modelInfo);
    public TtsModelDto getTtsModelDtoForAdmin(String model);
    public TtsModelDto getOpenModel(Map<String, String> params);
}
