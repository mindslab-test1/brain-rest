package brain.db.mapper;

import brain.api.tts.model.server.TtsAccessMetaDto;

import java.util.List;

public interface TtsAccessMetaMapper {
    int insertTtsAccess(TtsAccessMetaDto accessMetaDto);
    List<TtsAccessMetaDto> readTtsAccess(TtsAccessMetaDto accessMetaDto);
    int updateTtsAccess(TtsAccessMetaDto accessMetaDto);
    int deleteTtsAccess(TtsAccessMetaDto accessMetaDto);
}
