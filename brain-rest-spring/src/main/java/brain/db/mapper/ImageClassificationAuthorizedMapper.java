package brain.db.mapper;

import brain.api.classification.data.model.ImageClassificationAuthorized;

import java.util.Map;

public interface ImageClassificationAuthorizedMapper {
    ImageClassificationAuthorized findByApiIdAndModel(Map<String, String> params);
}
