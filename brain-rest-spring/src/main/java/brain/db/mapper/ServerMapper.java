package brain.db.mapper;

import brain.api.common.model.server.BaselineDto;
import brain.api.common.model.server.ServerDto;

import java.util.List;

public interface ServerMapper {
    // mapper.xml의 Query ID와 동일한 이름의 method 생성, 전달할 Param과 ReturnType도 명시 필요
    List<ServerDto> getServerByService(String service);
    BaselineDto getBaseServer(String service);
    BaselineDto getBaseServerWithOpts(BaselineDto dto);
}
