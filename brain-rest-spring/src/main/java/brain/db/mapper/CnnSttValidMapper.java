package brain.db.mapper;

import brain.api.stt.model.SttModelDto;

import java.util.Map;

public interface CnnSttValidMapper {
    public SttModelDto checkValidSimple(Map<String, String> params);
    public SttModelDto checkValidDetail(Map<String, String> params);
}
