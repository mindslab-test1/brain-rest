package brain.db.mapper;

import brain.api.common.model.server.ChannelDto;

import java.util.Map;

public interface ModelMapper {
    public ChannelDto selectLipsyncModelVal(Map<String, String> params);
}
