package brain.db.mapper;

import brain.api.tts.model.server.TtsModelMetaDto;

import java.util.List;

public interface TtsModelMetaMapper {
    int insertTtsVoicefont(TtsModelMetaDto modelMetaDto);
    List<TtsModelMetaDto> readTtsVoicefont(TtsModelMetaDto modelMetaDto);
    int updateTtsVoicefont(TtsModelMetaDto modelMetaDto);
    int deleteTtsVoicefont(TtsModelMetaDto modelMetaDto);
}
