package brain.db.mapper;

import brain.api.common.model.usage.UsageDto;
import brain.api.common.model.usage.UsageUpdateDto;
import brain.api.common.model.user.ApiClientDto;

import java.util.List;

public interface ApiClientMapper {
    ApiClientDto getApiClientInfo(String apiId);
    List<ApiClientDto> getApiClientList();
    int setNewApiClient(ApiClientDto dto);
    int setApiClientTables(String valTable, String useTable);
    int setApiClientTableDefaults(String service);
    int updateUsage(UsageUpdateDto dto);
    int updateClient(ApiClientDto dto);
    int updateClientKey(ApiClientDto apiClientDto);
}
