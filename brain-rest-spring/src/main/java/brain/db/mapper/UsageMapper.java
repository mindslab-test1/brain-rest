package brain.db.mapper;

import brain.api.common.model.usage.SumUsageDto;
import brain.api.common.model.usage.SumUsageRequestParam;
import brain.api.common.model.usage.UsageDto;

import java.util.List;
import java.util.Map;

public interface UsageMapper {
    int updateUsage(UsageDto dto);
    List<UsageDto> selectUsage(Map reqParam);
    List<SumUsageDto> selectSumUsage(SumUsageRequestParam param);
}
