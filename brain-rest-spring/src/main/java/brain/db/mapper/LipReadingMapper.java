package brain.db.mapper;

import java.util.List;

public interface LipReadingMapper {
    List<String> getActiveLipReadingTextList();
}
