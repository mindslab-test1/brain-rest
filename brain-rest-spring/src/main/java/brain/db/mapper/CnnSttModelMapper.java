package brain.db.mapper;

import brain.api.stt.model.CnnSttAccessDto;
import brain.api.stt.model.CnnSttModelDto;

import java.util.List;

public interface CnnSttModelMapper {
    CnnSttModelDto readCnnSttModel(CnnSttModelDto cnnSttModelDto);
    int readCnnSttAccess(CnnSttAccessDto cnnSttAccessDto);
}
