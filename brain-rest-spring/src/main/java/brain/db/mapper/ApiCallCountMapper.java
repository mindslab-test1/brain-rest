package brain.db.mapper;

import brain.api.common.callcount.data.ApiCallCountDTO;
import brain.api.common.callcount.data.ApiCallCountQuotaDTO;
import brain.api.common.callcount.data.RequestApiCallCountDTO;

import java.util.List;

public interface ApiCallCountMapper {
    int insertApiCallCount(ApiCallCountDTO apiCallCountDTO);
    List<ApiCallCountDTO> getApiCallCountList(RequestApiCallCountDTO requestApiCallCountDTO);
    ApiCallCountQuotaDTO getApiCallCountQuota();
    int updateApiCallCountQuota(ApiCallCountQuotaDTO dto);
}
