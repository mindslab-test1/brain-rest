package brain.db.mapper;

import brain.api.stt.model.SttAccessMetaDao;
import brain.api.stt.model.SttAccessMetaoDto;

public interface CnnSttModelMetaMapper {
    SttAccessMetaDao getModelInfo(SttAccessMetaoDto dto);
}
