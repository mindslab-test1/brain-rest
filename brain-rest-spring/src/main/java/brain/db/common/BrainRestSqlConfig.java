package brain.db.common;

import brain.api.common.log.CloudApiLogger;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.Reader;

public class BrainRestSqlConfig {
    private static final CloudApiLogger logger = new CloudApiLogger("DB CONFIG", "Brain-Rest");
    private static final SqlSessionFactory sessionFactory;
    private static final SqlSession sqlSession;

    private BrainRestSqlConfig(){}

    /*
    SqlSession객체를 얻기 위해 init 시점에서 SqlSessionFactoryBuilder 객체 생성
    생성 시, 사전 설정해둔 설정값들을 활용 (brainRestSqlMapConfig.xml)
     */
    static {
        try {
            String resource = "mybatis/brainRestSqlMapConfig.xml";
            Reader reader = Resources.getResourceAsReader(resource);
            sessionFactory = new SqlSessionFactoryBuilder().build(reader);
            sqlSession = sessionFactory.openSession(true);
        } catch (Exception e) {
            logger.debug(e.getMessage());
            throw new RuntimeException("Error initializing BrainRestSqlConfig class. Cause : " + e.getMessage());
        }
    }

    public static SqlSessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static SqlSession getSqlSessionInstance() {
        return sqlSession;
    }

}
