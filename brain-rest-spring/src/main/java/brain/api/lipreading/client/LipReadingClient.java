package brain.api.lipreading.client;

import brain.api.common.CommonCode;
import brain.api.common.exception.GrpcTimeOutException;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.lipreading.LipReadingCommonCode;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.lipreading.LipReadingGrpc;
import maum.brain.lipreading.Lipreading;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class LipReadingClient {
    private static final CloudApiLogger logger = new CloudApiLogger(LipReadingCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public float lipReading(MultipartFile speech, String text) throws IOException, InterruptedException, MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(LipReadingCommonCode.IP, LipReadingCommonCode.PORT);
        LipReadingGrpc.LipReadingStub stub = LipReadingGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        AtomicReference<Lipreading.EvaluationResult> response = new AtomicReference<>();
        AtomicReference<Throwable> error = new AtomicReference();

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<Lipreading.EvaluationResult> responseObserver = new StreamObserver<Lipreading.EvaluationResult>() {
            @Override
            public void onNext(Lipreading.EvaluationResult evaluationResult) {
                response.set(evaluationResult);
                logger.info("[lipReading] evaluationResult received");
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                error.set(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("[lipReading] server on completed called");
                finishLatch.countDown();
            }
        };

        StreamObserver<Lipreading.Pronunciation> requestObserver = stub.evaluatePronunciation(responseObserver);
        Lipreading.Pronunciation.Builder inputBuilder = Lipreading.Pronunciation.newBuilder();

        if(speech.getBytes() != null) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(speech.getBytes());
            int len = 0;
            byte[] buffer = new byte[1024 * 512];
            while ((len = byteArrayInputStream.read(buffer)) > 0) {
                logger.debug(String.format("speech chunk size: %d", len));
                if (len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(inputBuilder.setSpeech(inputByteString).setText(text).build());
                logger.debug("send speech chunk to server");
            }
        }

        requestObserver.onCompleted();

        if(!finishLatch.await(1, TimeUnit.MINUTES)) {
            logger.error("lipReading connection timeout");
            throw new MindsRestException("connection timeout with internal server");
        }
        if(error.get() != null) {
            logger.error(error.get());
            throw new GrpcTimeOutException();
        }

        return response.get().getScore();
    }
}
