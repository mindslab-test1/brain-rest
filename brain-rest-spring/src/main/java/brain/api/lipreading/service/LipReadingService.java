package brain.api.lipreading.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.lipreading.LipReadingCommonCode;
import brain.api.lipreading.client.LipReadingClient;
import brain.api.lipreading.dao.LipReadingDao;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class LipReadingService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(LipReadingCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);
    private final LipReadingClient client;
    private final LipReadingDao dao;

    private void validateParameter(MultipartFile file, String text) throws MindsRestException {
        Optional.ofNullable(text).orElseThrow(() -> new MindsRestException(
                CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not found requested text resource"));

        if (file.getSize() == 0 || file == null || file.isEmpty()) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "not found requested file resource");
        }

        validateFileMetaData(file);

        List<String> lipReadingList = dao.getActiveLipReadingTextList();
        if (!(lipReadingList.stream().anyMatch(word -> word.equalsIgnoreCase(text)))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "request text(" + text + ") not included in lipReadingList");
        }
    }

    private void validateFileMetaData(MultipartFile file) throws MindsRestException {
        if (!(file.getContentType().contains(CommonCode.WAV))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported media type. (" + file.getContentType() + ")");
        }

        String wavFileName = file.getOriginalFilename();
        String extension = FilenameUtils.getExtension(wavFileName);
        if (!extension.equalsIgnoreCase("wav")) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported file extension(" + extension + "). Please check allow extension(wav)");
        }
    }

    public CommonResponse<Float> doClassification(String apiId, String apiKey, MultipartFile file, String text, HttpServletRequest request) throws MindsRestException, IOException, InterruptedException {
        logger.info("LipReading doClassification Request [apiId = " + apiId + "]");
        validate(apiId, apiKey, LipReadingCommonCode.SERVICE_NAME, 1 ,request);
        logger.debug("usage validate for LipReading-doClassification");
        validateParameter(file, text);
        return new CommonResponse<>(new CommonMsg(), client.lipReading(file, text));
    }

}
