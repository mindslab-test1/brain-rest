package brain.api.lipreading.dao;

import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.LipReadingMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LipReadingDao {
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public List<String> getActiveLipReadingTextList() {
        try (SqlSession session = sessionFactory.openSession(true)){
            LipReadingMapper mapper = session.getMapper(LipReadingMapper.class);
            return mapper.getActiveLipReadingTextList();
        }
    }
}
