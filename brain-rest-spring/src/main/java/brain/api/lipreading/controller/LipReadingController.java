package brain.api.lipreading.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.lipreading.LipReadingCommonCode;
import brain.api.lipreading.service.LipReadingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/lip-reading")
public class LipReadingController {
    private static final CloudApiLogger logger = new CloudApiLogger(LipReadingCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);
    private final LipReadingService service;

    @PostMapping(value = "/classification", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> doClassification(@RequestParam("apiId") String apiId, @RequestParam("apiKey") String apiKey,
                                           @RequestParam("file") MultipartFile file, @RequestParam("text") String text,
                                           HttpServletRequest request) {

        try {
            return ResponseEntity.ok().body(service.doClassification(apiId, apiKey, file, text, request));
        } catch(MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch(Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }
}
