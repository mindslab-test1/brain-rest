package brain.api.segment.service;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.segment.SegmentCommonCode;
import brain.api.segment.client.SegmentClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

@Service
public class SegmentService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(SegmentCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    @Autowired
    SegmentClient segmentClient;

    public ByteArrayOutputStream streamSegment(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws Exception {
        validate(apiId, apiKey, SegmentCommonCode.SERVICE_NAME, 1 ,request);
        segmentClient.setDestination("182.162.19.12", 333);
        return segmentClient.streamSegment(file);
    }
}
