package brain.api.style.service;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.style.StyleCommonCode;
import brain.api.style.client.StyleClient;
import brain.api.style.data.RetrievedItem;
import brain.api.style.data.UserItemData;
import brain.api.style.dto.StyleRetrievalResponse;
import brain.api.style.dto.StyleSegmentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class StyleService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(StyleCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    @Autowired
    StyleClient styleClient;

    public byte[] tryOn(String apiId, String apiKey, String type, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        validate(apiId, apiKey, StyleCommonCode.SERVICE_NAME, 1 ,request);
        styleClient.setDestination("182.162.19.12", 50061);
        int typeVal;
        try {
            typeVal = Integer.parseInt(type);
        } catch (NumberFormatException e){
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "Number format exception: Not a INT!");
        }
        return styleClient.doTryOn(typeVal, file);
    }

    public StyleSegmentResponse segment(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        validate(apiId, apiKey, StyleCommonCode.SERVICE_NAME, 1, request);
        List<UserItemData> resultList = styleClient.segment(file);

        return new StyleSegmentResponse(resultList);
    }

    public StyleRetrievalResponse retrieve(String apiId, String apiKey, String itemId, HttpServletRequest request) throws MindsRestException, IOException {
        validate(apiId, apiKey, StyleCommonCode.SERVICE_NAME, 1, request);

        return new StyleRetrievalResponse(
                (Serializable) styleClient.retreive(itemId)
        );
    }

    public StyleRetrievalResponse retrieve(String apiId, String apiKey, List<String> itemIdList, HttpServletRequest request) throws MindsRestException, IOException{
        validate(apiId, apiKey, StyleCommonCode.SERVICE_NAME, itemIdList.size(), request);
        List<List<RetrievedItem>> resultList = new ArrayList<>();

        for(String itemId: itemIdList){
            resultList.add(styleClient.retreive(itemId));
        }

        return new StyleRetrievalResponse(
                (Serializable) resultList
        );
    }
}
