package brain.api.style.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.style.StyleCommonCode;
import brain.api.style.data.RetrievedItem;
import brain.api.style.data.UserItemData;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.stylebot_try_on.StylebotTryOnGrpc;
import maum.brain.stylebot_try_on.StylebotTryOnOuterClass.MyInput;
import maum.brain.stylebot_try_on.StylebotTryOnOuterClass.MyResult;
import maum.brain.stylebot_wild_retrieval.StylebotRetrievalGrpc;
import maum.brain.stylebot_wild_retrieval.StylebotWildRetrieval;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class StyleClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(StyleCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private StylebotTryOnGrpc.StylebotTryOnBlockingStub stylebotTryOnBlockingStub;

    private final ManagedChannel wildRetrievalChannel = CommonUtils.getManagedChannel("182.162.19.12", 50000);

    public CommonMsg setDestination(String ip, int port){
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.stylebotTryOnBlockingStub = StylebotTryOnGrpc.newBlockingStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }
        catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

//    public byte[] doTryOn(String type, MultipartFile file) throws IOException {
//        ByteString input = ByteString.copyFrom(file.getBytes());
//        MyInput request = MyInput.newBuilder().setClassId(ClassID.valueOf(type)).setData(input).build();
//        MyResult response = stylebotTryOnBlockingStub.getWarpedClothes(request);
//
//        return response.getData().toByteArray();
//    }

    public byte[] doTryOn(int typeVal, MultipartFile file) throws IOException{
        ByteString input = ByteString.copyFrom(file.getBytes());
        MyInput request = MyInput.newBuilder().setClassId(typeVal).setData(input).build();
        MyResult response = stylebotTryOnBlockingStub.getWarpedClothes(request);

        return response.getData().toByteArray();
    }

    public List<UserItemData> segment(MultipartFile file) throws IOException, MindsRestException {
        List<StylebotWildRetrieval.UserItemOutput> responseList = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();

        final CountDownLatch finishLatch = new CountDownLatch(1);
        StreamObserver<StylebotWildRetrieval.UserItemOutput> responseObserver = new StreamObserver<StylebotWildRetrieval.UserItemOutput>() {
            @Override
            public void onNext(StylebotWildRetrieval.UserItemOutput personImageInput) {
                logger.debug(String.format("got output, size: %d", personImageInput.getItemsCount()));
                responseList.add(personImageInput);
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                errorList.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                finishLatch.countDown();
            }
        };

        StylebotRetrievalGrpc.StylebotRetrievalStub stub = StylebotRetrievalGrpc.newStub(this.wildRetrievalChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        StreamObserver<StylebotWildRetrieval.PersonImageInput> requestObserver = stub.getSegmentationResult(responseObserver);

        InputStream imageStream = new ByteArrayInputStream(file.getBytes());
        byte[] buffer = new byte[1024 * 512];
        int readBytesCount = 0;
        while ((readBytesCount = imageStream.read(buffer))> 0){
            if(readBytesCount < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, readBytesCount);
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(StylebotWildRetrieval.PersonImageInput.newBuilder()
                    .setImage(inputByteString)
                    .build());
        }

        requestObserver.onCompleted();
        try {
            if(!finishLatch.await(1, TimeUnit.MINUTES))
                throw new MindsRestException();
        } catch (InterruptedException e){
            logger.error(e.getMessage());
            throw new RuntimeException("interrupted while waiting response");
        }

        if(!errorList.isEmpty()){
            logger.error(errorList.get(0).getMessage());
            throw new MindsRestException();
        }

        List<StylebotWildRetrieval.UserItem> userItemList = responseList.get(0).getItemsList();
        List<UserItemData> userItemDataList = new ArrayList<>();
        for (StylebotWildRetrieval.UserItem userItem: userItemList) {
            logger.debug(String.format("result class id: %d", userItem.getClassId()));
            logger.debug(String.format("result item id: %s", userItem.getItemId()));
            logger.debug(String.format("bitmask object: %s", userItem.getBitmask().toString()));
            userItemDataList.add(new UserItemData(userItem));
        }

        return userItemDataList;
    }

    public List<RetrievedItem> retreive(String itemId) throws IOException {
        StylebotRetrievalGrpc.StylebotRetrievalBlockingStub stub = StylebotRetrievalGrpc.newBlockingStub(this.wildRetrievalChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        Iterator<StylebotWildRetrieval.RetrievalResult> resultIterator = stub.withDeadlineAfter(1, TimeUnit.MINUTES).getRetrievalResult(
                StylebotWildRetrieval.ItemInput.newBuilder()
                        .setItemId(itemId)
                        .build());

        List<ByteArrayOutputStream> resultImages = new ArrayList<>();
        List<String> resultPath = new ArrayList<>();
        int currentTop = -1;
        ByteArrayOutputStream tempStream = new ByteArrayOutputStream();

        while (resultIterator.hasNext()){
            StylebotWildRetrieval.RetrievalResult resultPartial = resultIterator.next();
            logger.debug(currentTop);
            if(resultPartial.getTop() != currentTop){
                if(currentTop != -1){
                    logger.debug(String.format("new top: %d", currentTop));
                    resultImages.add(tempStream);
                    tempStream = new ByteArrayOutputStream();
                }
                currentTop = resultPartial.getTop();
                tempStream.write(resultPartial.getImagePart().toByteArray());
                resultPath.add(resultPartial.getPath());
            } else {
                logger.debug(String.format("write to current top: %d", currentTop));
                tempStream.write(resultPartial.getImagePart().toByteArray());
            }
        }
        if(tempStream.size() > 0) resultImages.add(tempStream);

        List<RetrievedItem> retrievedItems = new ArrayList<>();
        logger.debug(String.format("retrieved images count: %d", resultImages.size()));
        for (int i = 0; i < resultImages.size(); i++){
            logger.debug(String.format("top %d: image size = %d. image path = %s", i + 1, resultImages.get(i).size(), resultPath.get(i)));
            retrievedItems.add(new RetrievedItem(
                    i + 1,
                    resultImages.get(i).toByteArray()
            ));
        }

        return retrievedItems;
    }
}
