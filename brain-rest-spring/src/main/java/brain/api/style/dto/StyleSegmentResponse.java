package brain.api.style.dto;

import brain.api.common.data.CommonMsg;
import brain.api.style.data.UserItemData;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StyleSegmentResponse implements Serializable {
    private CommonMsg message;
    private List<UserItemData> payload;

    public StyleSegmentResponse() {
        this.payload = new ArrayList<>();
    }

    public StyleSegmentResponse(List<UserItemData> payload) {
        this.message = new CommonMsg();
        this.payload = payload;
    }

    public StyleSegmentResponse(CommonMsg message, List<UserItemData> payload) {
        this.message = message;
        this.payload = payload;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public List<UserItemData> getPayload() {
        return payload;
    }

    public void setPayload(List<UserItemData> payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "StyleSegmentResponse{" +
                "message=" + message +
                ", payload=" + payload +
                '}';
    }
}
