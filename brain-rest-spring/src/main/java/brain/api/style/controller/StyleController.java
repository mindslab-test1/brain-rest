package brain.api.style.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.utils.CommonUtils;
import brain.api.style.data.UserItemData;
import brain.api.style.dto.StyleRetrievalResponse;
import brain.api.style.dto.StyleSegmentResponse;
import brain.api.style.service.StyleService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RestController
@RequestMapping("/style")
public class StyleController {
    @Autowired
    StyleService styleService;

    @RequestMapping(value = "/tryOn", method = RequestMethod.POST)
    public ResponseEntity tryon(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("type") String type,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ){
        String orgFilename = file.getOriginalFilename();
        String filename = FilenameUtils.getBaseName(orgFilename);

        try {
            byte[] result = styleService.tryOn(apiId, apiKey, type, file, httpRequest);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" +"tryon-" + filename + ".png")
                    .contentType(MediaType.IMAGE_PNG)
                    .body(result);
        } catch (MindsRestException e){
            return ResponseEntity
                    .status(CommonExceptionCode.getHttpStatusOnException(e))
                    .body(new CommonMsg(e.getErrCode(), e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(500).contentType(MediaType.APPLICATION_JSON).body(new CommonMsg(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage()));
        }
    }

    @PostMapping(
            value = "/segmentAndRetrieve",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity wildRetrieval(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request
    ){
        StyleSegmentResponse response;
        try {
            response = styleService.segment(apiId, apiKey, file, request);
            List<String> itemIdList = new ArrayList<>();

            Consumer<? super UserItemData> consumer = (Consumer<UserItemData>) userItemData -> itemIdList.add(userItemData.getItemId());
            response.getPayload().forEach(consumer);
            return ResponseEntity.ok(styleService.retrieve(apiId, apiKey, itemIdList, request));
        } catch (MindsRestException e) {
            e.printStackTrace();
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new StyleRetrievalResponse(
                    e.createMessage()
            ));
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).build();
        }
    }


}
