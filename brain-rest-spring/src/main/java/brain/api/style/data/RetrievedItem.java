package brain.api.style.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Arrays;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RetrievedItem implements Serializable {
    private int top;
    private byte[] image;

    public RetrievedItem() {
    }

    public RetrievedItem(int top, byte[] image) {
        this.top = top;
        this.image = image;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "RetrievedItems{" +
                "top=" + top +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}
