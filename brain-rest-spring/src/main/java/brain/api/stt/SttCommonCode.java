package brain.api.stt;

import org.apache.commons.io.FileUtils;

public class SttCommonCode {
    private SttCommonCode(){}

    public static final String SERVICE_NAME = "STT";
    public static final String DEFAULT_MODEL_NAME = "baseline";

    // Success
    public static final int STT_RES_SUCCESS = 0;

    // Communication related error
    public static final int STT_ERR_CONN_REFUSED = 50001;
    public static final int STT_ERR_CONN_TIMEOUT = 50002;
    public static final int STT_ERR_REQ_TIMEOUT = 50003;

    public static final int STT_ERR_AUTH_VERIFICATION_ERROR = 40001;

    // TODO: Refactoring Target, Value Injection으로 개선이 필요
    public static final long STT_MAX_FILE_SIZE = 500 * FileUtils.ONE_KB;
}
