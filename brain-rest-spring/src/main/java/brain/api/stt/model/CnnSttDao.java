package brain.api.stt.model;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.stt.SttCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.CnnSttModelMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CnnSttDao {
    private static final CloudApiLogger logger = new CloudApiLogger(SttCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public CnnSttModelDto readCnnSttModel(CnnSttModelDto cnnSttModelDto){
        try (SqlSession session = sessionFactory.openSession(true)){
            CnnSttModelMapper mapper = session.getMapper(CnnSttModelMapper.class);
            return mapper.readCnnSttModel(cnnSttModelDto);
        }
    }
    public int readCnnSttAccess(CnnSttAccessDto cnnSttAccessDto){
        try (SqlSession session = sessionFactory.openSession(true)){
            CnnSttModelMapper mapper = session.getMapper(CnnSttModelMapper.class);
            return mapper.readCnnSttAccess(cnnSttAccessDto);
        }
    }
}
