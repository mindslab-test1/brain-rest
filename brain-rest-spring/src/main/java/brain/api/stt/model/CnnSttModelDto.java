package brain.api.stt.model;

public class CnnSttModelDto {
    private String lang;
    private String model;
    private Integer sampleRate;
    private String ip;
    private Integer port;

    public String getLang() {
        return lang;
    }
    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public Integer getSampleRate() {
        return sampleRate;
    }
    public void setSampleRate(Integer sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }
    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "CnnSttModelDto{" +
                "lang='" + lang + '\'' +
                ", model='" + model + '\'' +
                ", sampleRate=" + sampleRate +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
