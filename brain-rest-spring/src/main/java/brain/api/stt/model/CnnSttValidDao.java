package brain.api.stt.model;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.stt.SttCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.CnnSttValidMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class CnnSttValidDao {
    private static final CloudApiLogger logger = new CloudApiLogger(SttCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public SttModelDto validateModelSimple(Map<String, String> params){
        try (SqlSession session = sessionFactory.openSession(true)){
            CnnSttValidMapper mapper = session.getMapper(CnnSttValidMapper.class);
            return mapper.checkValidSimple(params);
        }
    }

    public SttModelDto validateModelDetail(Map<String, String> params){
        try (SqlSession session = sessionFactory.openSession(true)){
            CnnSttValidMapper mapper = session.getMapper(CnnSttValidMapper.class);
            return mapper.checkValidDetail(params);
        }
    }
}
