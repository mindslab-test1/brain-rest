package brain.api.stt.model;


public class SttModelDto {
    private int id;
    private String createdAt;
    private String updatedAt;
    private String modelName;
    private int port;
    private String host;
    private int sampleRate;
    private String lang;
    private int open;

    public SttModelDto() {
    }

    public SttModelDto(int id, String createdAt, String updatedAt, String modelName, int port, String host, int sampleRate, String lang, int open) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.modelName = modelName;
        this.port = port;
        this.host = host;
        this.sampleRate = sampleRate;
        this.lang = lang;
        this.open = open;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    @Override
    public String toString() {
        return "CnnSttModelSimpleDto{" +
                "id=" + id +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", modelName='" + modelName + '\'' +
                ", port=" + port +
                ", host='" + host + '\'' +
                ", sampleRate=" + sampleRate +
                ", lang='" + lang + '\'' +
                ", open=" + open +
                '}';
    }
}
