package brain.api.stt.data;

import brain.api.common.data.CommonMsg;

public class SttRes {
    private String status;
    private SttResExtraData extra_data;
    private String data;

    public SttRes(String status,SttResExtraData extra_data, String data) {
        this.status = status;
        this.extra_data = extra_data;
        this.data = data;
    }

    public SttRes(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public SttResExtraData getExtra_data() {
        return extra_data;
    }
    public void setExtra_data(SttResExtraData extra_data) {
        this.extra_data = extra_data;
    }

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SttRes{" +
                "status='" + status + '\'' +
                ", extra_data=" + extra_data +
                ", data='" + data + '\'' +
                '}';
    }
}
