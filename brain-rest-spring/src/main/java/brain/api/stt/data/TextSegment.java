package brain.api.stt.data;

public class TextSegment {
    private double start;
    private double end;
    private String txt;

    public TextSegment(double start, double end, String txt) {
        this.start = start;
        this.end = end;
        this.txt = txt;
    }

    public double getStart() {
        return start;
    }
    public void setStart(double start) {
        this.start = start;
    }

    public double getEnd() {
        return end;
    }
    public void setEnd(double end) {
        this.end = end;
    }

    public String getTxt() {
        return txt;
    }
    public void setTxt(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        return "TextSegment{" +
                "start=" + start +
                ", end=" + end +
                ", txt='" + txt + '\'' +
                '}';
    }
}
