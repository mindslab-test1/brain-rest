package brain.api.stt.data;

public class SttResExtraData {
    private Integer stt_length;
    private String stt_data;
    private String stt_duration;

    public SttResExtraData(Integer stt_length, String stt_data, String stt_duration) {
        this.stt_length = stt_length;
        this.stt_data = stt_data;
        this.stt_duration = stt_duration;
    }

    public Integer getStt_length() {
        return stt_length;
    }
    public void setStt_length(Integer stt_lenght) {
        this.stt_length = stt_lenght;
    }

    public String getStt_data() {
        return stt_data;
    }
    public void setStt_data(String stt_data) {
        this.stt_data = stt_data;
    }

    public String getStt_duration() {
        return stt_duration;
    }
    public void setStt_duration(String stt_duration) {
        this.stt_duration = stt_duration;
    }

    @Override
    public String toString() {
        return "SttResExtraData{" +
                "stt_length=" + stt_length +
                ", stt_data='" + stt_data + '\'' +
                ", stt_duration=" + stt_duration +
                '}';
    }
}
