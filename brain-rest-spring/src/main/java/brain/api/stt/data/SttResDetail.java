package brain.api.stt.data;

import java.util.List;

public class SttResDetail {
    private String status;
    private List<SttResDetailData> data;
    private String extra_data;

    public SttResDetail(String status, List<SttResDetailData> data, String extra_data) {
        this.status = status;
        this.data = data;
        this.extra_data = extra_data;
    }

    public SttResDetail(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public List<SttResDetailData> getData() {
        return data;
    }
    public void setData(List<SttResDetailData> data) {
        this.data = data;
    }

    public String getExtra_data() {
        return extra_data;
    }
    public void setExtra_data(String extra_data) {
        this.extra_data = extra_data;
    }

    @Override
    public String toString() {
        return "SttResDetail{" +
                "status='" + status + '\'' +
                ", data=" + data +
                ", extra_data='" + extra_data + '\'' +
                '}';
    }
}
