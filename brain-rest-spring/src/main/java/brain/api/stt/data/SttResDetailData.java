package brain.api.stt.data;

public class SttResDetailData {
    private float end;
    private float start;
    private String txt;

    public SttResDetailData(float end, float start, String txt) {
        this.end = end;
        this.start = start;
        this.txt = txt;
    }

    public float getEnd() {
        return end;
    }
    public void setEnd(float end) {
        this.end = end;
    }

    public float getStart() {
        return start;
    }
    public void setStart(float start) {
        this.start = start;
    }

    public String getTxt() {
        return txt;
    }
    public void setTxt(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        return "SttResDetailData{" +
                "end=" + end +
                ", start=" + start +
                ", txt='" + txt + '\'' +
                '}';
    }
}
