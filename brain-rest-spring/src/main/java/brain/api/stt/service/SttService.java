package brain.api.stt.service;

import brain.api.common.CommonCode;
import brain.api.common.client.MediaClient;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.common.utils.CommonUtils;
import brain.api.stt.SttCommonCode;
import brain.api.stt.client.CnnSttClient;
import brain.api.stt.data.CnnSttRes;
import brain.api.stt.data.StreamCnnSttRes;
import brain.api.stt.model.*;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.CnnSttModelMetaMapper;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class SttService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(SttCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    @Autowired
    CnnSttClient cnnSttClient;

    @Autowired
    CnnSttDao cnnSttDao;

    @Autowired
    CnnSttValidDao cnnSttValidDao;

    @Autowired
    MediaClient mediaClient;

    public CnnSttRes cnnStt(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        logger.info(String.format("Sending to Engine: %s => model= %s, fileName= %s, fileSize= %s",
                apiId, SttCommonCode.DEFAULT_MODEL_NAME, file.getOriginalFilename(), CommonUtils.readableFileSizeUnit(file.getSize())));

        String requestUniqueKey = CommonUtils.generateRequestUniqueKey(true);

        // check params
        validateSttFileSize(file);

        validate(apiId, apiKey, SttCommonCode.SERVICE_NAME, 1, request);
        cnnSttClient.setDestination("114.108.173.112",15001);

        try {
            CnnSttRes response = cnnSttClient.doCnnStt(file);

            logger.info(String.format("Response Received: %s => model= %s, result= %s", apiId, SttCommonCode.DEFAULT_MODEL_NAME, response.getResult()));

            // TODO: Refactoring Target, Change into AOP about upload MediaServer
            mediaClient.uploadFile(apiId, SttCommonCode.DEFAULT_MODEL_NAME, file.getBytes(), file.getOriginalFilename(), requestUniqueKey);
            mediaClient.uploadFile(apiId, SttCommonCode.DEFAULT_MODEL_NAME, response.getResult().getBytes(), "response.txt", requestUniqueKey);
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    public CnnSttRes cnnSttSimple(
            String apiId, String apiKey,
            String model, MultipartFile file, HttpServletRequest request
    ) throws MindsRestException, IOException{
        logger.info(String.format("Sending to Engine: %s => model= %s, fileName= %s, fileSize= %s",
                apiId, model, file.getOriginalFilename(), CommonUtils.readableFileSizeUnit(file.getSize())));

        String requestUniqueKey = CommonUtils.generateRequestUniqueKey(true);

        // check params
        validateSttFileSize(file);

        Map<String, String> modelParams = new HashMap<>();
        modelParams.put("apiId", apiId);
        modelParams.put("model", model);
        SttModelDto dto = cnnSttValidDao.validateModelSimple(modelParams);
        if(dto == null){
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "no usable model present");
        }

        int bitrate = 16;
        double duration = (double)file.getSize()*8 / dto.getSampleRate() / bitrate;
        int usage = (int)(duration*10);

        validate(apiId, apiKey, SttCommonCode.SERVICE_NAME, usage, request);
        cnnSttClient.setDestination(dto.getHost(), dto.getPort());

        try {
            CnnSttRes cnnSttRes = cnnSttClient.doCnnStt(file);

            logger.info(String.format("Response Received: %s => model= %s, result= %s", apiId, model, cnnSttRes.getResult()));

            // TODO: Refactoring Target, Change into AOP about upload MediaServer
            mediaClient.uploadFile(apiId, model, file.getBytes(), file.getOriginalFilename(), requestUniqueKey);
            mediaClient.uploadFile(apiId, model, cnnSttRes.getResult().getBytes(), "response.txt", requestUniqueKey);
            return cnnSttRes;
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    public StreamCnnSttRes cnnSttScript(
            String apiId, String apiKey,
            String model, MultipartFile file, HttpServletRequest request
    ) throws MindsRestException, IOException{
        logger.info(String.format("Sending to Engine: %s => model= %s, fileName= %s, fileSize= %s",
                apiId, model, file.getOriginalFilename(), CommonUtils.readableFileSizeUnit(file.getSize())));

        String requestUniqueKey = CommonUtils.generateRequestUniqueKey(true);

        // check params
        validateSttFileSize(file);

        Map<String, String> modelParams = new HashMap<>();
        modelParams.put("apiId", apiId);
        modelParams.put("model", model);
        SttModelDto dto = cnnSttValidDao.validateModelDetail(modelParams);
        if(dto == null){
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "no usable model present");
        }

        int bitrate = 16;
        double duration = (double)file.getSize()*8 / dto.getSampleRate() / bitrate;
        int usage = (int)(duration*10);

        validate(apiId, apiKey, SttCommonCode.SERVICE_NAME, usage, request);
        cnnSttClient.setDestination(dto.getHost(), dto.getPort());

        try {
            StreamCnnSttRes streamCnnSttRes = cnnSttClient.doStreamCnnStt(file, dto.getSampleRate());

            StringBuffer stringBuffer = new StringBuffer();
            streamCnnSttRes.getResult().forEach(textSegment -> {
                stringBuffer.append(textSegment.getTxt());
            });

            logger.info(String.format("Response Received: %s => model= %s, result= %s", apiId, model, stringBuffer.toString()));

            // TODO: Refactoring Target, Change into AOP about upload MediaServer
            mediaClient.uploadFile(apiId, model, file.getBytes(), file.getOriginalFilename(), requestUniqueKey);
            mediaClient.uploadFile(apiId, model, stringBuffer.toString().getBytes(), "response.txt", requestUniqueKey);

            return streamCnnSttRes;
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    public StreamCnnSttRes streamCnnStt(String apiId, String apiKey, String lang, String model, Integer samplerate, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        logger.info(String.format("Sending to Engine: %s => model= %s, fileName= %s, fileSize= %s",
                apiId, model, file.getOriginalFilename(), CommonUtils.readableFileSizeUnit(file.getSize())));

        String requestUniqueKey = CommonUtils.generateRequestUniqueKey(true);

        // check params
        validateSttFileSize(file);

        int bitrate = 16;
        double duration = (double)file.getSize()*8/samplerate/bitrate;
        int usage = (int)(duration*10);
        validate(apiId, apiKey, SttCommonCode.SERVICE_NAME, usage, request);

        CnnSttModelDto cnnSttModel = getCnnSttModel(apiId, lang, model, samplerate);
        if(cnnSttModel == null) return new StreamCnnSttRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "Access denied: you don't have permission to access on this model"));
        cnnSttClient.setDestination(cnnSttModel.getIp(),cnnSttModel.getPort());

        try {
            StreamCnnSttRes streamCnnSttRes = cnnSttClient.doStreamCnnStt(file, samplerate);

            StringBuffer stringBuffer = new StringBuffer();
            streamCnnSttRes.getResult().forEach(textSegment -> {
                stringBuffer.append(textSegment.getTxt());
            });

            logger.info(String.format("Response Received: %s => model= %s, result= %s", apiId, model, stringBuffer.toString()));

            // TODO: Refactoring Target, Change into AOP about upload MediaServer
            mediaClient.uploadFile(apiId, model, file.getBytes(), file.getOriginalFilename(), requestUniqueKey);
            mediaClient.uploadFile(apiId, model, stringBuffer.toString().getBytes(), "response.txt", requestUniqueKey);

            return streamCnnSttRes;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    public CnnSttModelDto getCnnSttModel(String apiId, String lang, String model, Integer samplerate) throws MindsRestException {
        CnnSttModelDto cnnSttModelDto = new CnnSttModelDto();
        cnnSttModelDto.setLang(lang);
        cnnSttModelDto.setModel(model);
        cnnSttModelDto.setSampleRate(samplerate);
        CnnSttModelDto result;

        if (model.equalsIgnoreCase(SttCommonCode.DEFAULT_MODEL_NAME) && lang.equalsIgnoreCase("kor") && samplerate.equals(16000)) {
            result = cnnSttDao.readCnnSttModel(cnnSttModelDto);
        } else {
            CnnSttAccessDto cnnSttAccessDto = new CnnSttAccessDto();
            cnnSttAccessDto.setApiId(apiId);
            cnnSttAccessDto.setLang(lang);
            cnnSttAccessDto.setSttModel(model);
            int accessible = cnnSttDao.readCnnSttAccess(cnnSttAccessDto);

            if (accessible < 1) {
                return null;
            }
            result = cnnSttDao.readCnnSttModel(cnnSttModelDto);
        }
        return result;
    }

    private void validateSttFileSize(MultipartFile file) throws MindsRestException {
        if (file.getSize() > SttCommonCode.STT_MAX_FILE_SIZE) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    String.format("Invalid file size. It must be less than %s.",
                            CommonUtils.readableFileSizeUnit(SttCommonCode.STT_MAX_FILE_SIZE))
            );
        }
    }

    // Legarcy API 내에서 삭제 처리 되지 않고 있는 파일 소거
    @Scheduled(cron = "10 0 0 * * ?")
    public void sttLegacyFileRemover() {
        logger.info("[Legacy STT FileRemover Start]");
        try {
            File legacyPath = new File("/home/ubuntu/minds-api-web/media");

            if (!legacyPath.exists()) return;

            File[] deleteFileArray = legacyPath.listFiles();
            long filesSize = 0;

            for (File deleteFile : deleteFileArray) {
                logger.info(String.format("[Legacy STT File Remove] : %s", deleteFile.getName()));
                filesSize += deleteFile.length();
                deleteFile.delete();
            }

            logger.info("[Legacy STT FileRemover done] : " + filesSize);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public SttAccessMetaDao getSttModel(String lang, String model, int samplerate) throws MindsRestException{
        SttAccessMetaoDto sttDto = new SttAccessMetaoDto();
        SttAccessMetaDao sttDao;
        sttDto.setLang(lang);
        sttDto.setModel(model);
        sttDto.setSampleRate(samplerate);

        try {
            CnnSttModelMetaMapper cnnSttModelMetaMapper = BrainRestSqlConfig
                    .getSqlSessionInstance()
                    .getMapper(CnnSttModelMetaMapper.class);

            sttDao = cnnSttModelMetaMapper.getModelInfo(sttDto);

            if(sttDao == null){
                sttDao = new SttAccessMetaDao();
                sttDao.setIp("114.108.173.112");
                sttDao.setPort(15001);
            }

            logger.info("[CNN STT ip, port ] : "+ sttDao.getIp()+", "+sttDao.getPort());

        }catch (Exception e){
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
        return sttDao;
    }

}
