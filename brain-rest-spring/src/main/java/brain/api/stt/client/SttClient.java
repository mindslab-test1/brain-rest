package brain.api.stt.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.stt.SttCommonCode;
import brain.api.stt.data.SttRes;
import brain.api.stt.data.SttResDetail;
import brain.api.stt.data.SttResDetailData;
import brain.api.stt.data.SttResExtraData;
import com.google.protobuf.ByteString;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.Stt;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@Component
public class SttClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(SttCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private SpeechToTextServiceGrpc.SpeechToTextServiceStub sttStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if (super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            return new CommonMsg();
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public SttRes doSimpleRecog(MultipartFile file, String lang, int sampling, String model) throws IOException {
        sttStub = SpeechToTextServiceGrpc.newStub(
                CommonUtils.getManagedChannel(this.destIp, this.destPort)
        ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        Metadata serverMeta = this.setServerMeta(lang, model, Integer.toString(sampling));
        sttStub = MetadataUtils.attachHeaders(sttStub, serverMeta).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        final CountDownLatch finishLatch = new CountDownLatch(1);

        ArrayList<String> resultArray = new ArrayList<>();

        StreamObserver<Stt.Text> responseObserver = new StreamObserver<Stt.Text>() {
            @Override
            public void onNext(Stt.Text text) {
                resultArray.add(text.getTxt());
                logger.info("Result: " + text.getTxt());
            }

            @Override
            public void onError(Throwable t) {
                logger.error(t.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished from responseObserver");
                finishLatch.countDown();
            }
        };

        StreamObserver<Stt.Speech> requestObserver = sttStub.simpleRecognize(responseObserver);
        Stt.Speech.Builder speechBuilder = Stt.Speech.newBuilder();
        try{
            byte[] byteArray;
            if(distinguishPCM(file)){
                byteArray = file.getBytes();
            }
            else {
                byteArray = CommonUtils.getBytes(file, sampling);
            }

            for(int i = 0; i < byteArray.length; i++) {
                ByteString inputByteString = ByteString.copyFrom(new byte[] {byteArray[i]});
                requestObserver.onNext(speechBuilder.setBin(inputByteString).build());
            }
        }catch (RuntimeException | IOException e) {
            requestObserver.onError(e);
            throw e;
        }
        requestObserver.onCompleted();

        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        String resultData = resultArray.get(0);
        Integer resultLength = resultData.length();
        String resultDuration = Float.toString(file.getSize() / (float) sampling);

        SttRes sttRes = new SttRes("Success", new SttResExtraData(resultLength, resultData, resultDuration), resultData);
        logger.info(String.format("Result: %s", sttRes));

        return sttRes;
    }

    public SttResDetail doDetailRecog(MultipartFile file, String lang, int sampling, String model) throws IOException {
        Metadata serverMeta = this.setServerMeta(lang, model, Integer.toString(sampling));
        sttStub = MetadataUtils.attachHeaders(sttStub, serverMeta).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        final CountDownLatch finishLatch = new CountDownLatch(1);
        ArrayList<Stt.Segment> resultArray = new ArrayList<>();

        StreamObserver<Stt.DetailText> responseObserver = new StreamObserver<Stt.DetailText>() {
            @Override
            public void onNext(Stt.DetailText text) {
                resultArray.addAll(text.getSegmentsList());
                logger.info("Result: " + text.getTxt());
            }

            @Override
            public void onError(Throwable t) {
                logger.error(t.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished from responseObserver");
                finishLatch.countDown();
            }
        };

        StreamObserver<Stt.Speech> requestObserver = sttStub.detailRecognize(responseObserver);
        Stt.Speech.Builder speechBuilder = Stt.Speech.newBuilder();
        try{
            byte[] byteArray;
            if(distinguishPCM(file)){
                byteArray = file.getBytes();
            }
            else {
                byteArray = CommonUtils.getBytes(file, sampling);
            }

            for(int i = 0; i < byteArray.length; i++) {
                ByteString inputByteString = ByteString.copyFrom(new byte[] {byteArray[i]});
                requestObserver.onNext(speechBuilder.setBin(inputByteString).build());
            }
        }catch (RuntimeException | IOException e) {
            requestObserver.onError(e);
            throw e;
        }
        requestObserver.onCompleted();

        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        ArrayList<SttResDetailData> data = new ArrayList<>();

        for(Stt.Segment s: resultArray){
            logger.debug("end => " + s.getEnd());
            logger.debug("start => " + s.getStart());
            logger.debug("txt => " + s.getTxt());
            data.add(new SttResDetailData(s.getEnd()/100.0f, s.getStart()/100.0f, s.getTxt()));
        }

        SttResDetail sttResDetail = new SttResDetail("Success", data, "None");
        logger.info(String.format("Result: %s", sttResDetail));

        return sttResDetail;
    }

    // FUNCTION
    private Metadata setServerMeta(String lang, String model, String sampling){
        if(model.equals("baseline")) { model = "base_unilstm"; }

        Metadata serverMeta = new Metadata();
        Metadata.Key<String> keyLang = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> keyModel = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> keySampling = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);

        serverMeta.put(keyLang, lang);
        serverMeta.put(keyModel, model);
        serverMeta.put(keySampling, sampling);

        return serverMeta;
    }

    private boolean distinguishPCM(MultipartFile file) throws IOException {
        String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        if(extension.equals(".pcm")) {
            return true;
        }else{
            return false;
        }
    }
}


