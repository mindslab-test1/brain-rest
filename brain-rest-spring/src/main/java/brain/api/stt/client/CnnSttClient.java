package brain.api.stt.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.stt.SttCommonCode;
import brain.api.stt.data.CnnSttRes;
import brain.api.stt.data.StreamCnnSttRes;
import brain.api.stt.data.TextSegment;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import maum.brain.w2l.SpeechToTextGrpc;
import maum.brain.w2l.W2L;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class CnnSttClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(SttCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private SpeechToTextGrpc.SpeechToTextStub cnnSttStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if (super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.cnnSttStub = SpeechToTextGrpc.newStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

            return new CommonMsg();
        } catch (Exception e){
            logger.debug(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public CnnSttRes doCnnStt(MultipartFile file) throws IOException, MindsRestException {
        // 1. Getting Result Data
        final CountDownLatch finishLatch = new CountDownLatch(1);
        // 1-1. Getting data stream from requestObserver asynchronously
        List<String> resultArray = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();

        StreamObserver<W2L.Text> responseObserver = new StreamObserver<W2L.Text>() {
            @Override
            public void onNext(W2L.Text text) {
                resultArray.add(text.getTxt());
                logger.info("Result: " + text.getTxt());
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Err from responseObserver in CnnStt");
                logger.error(t.getMessage());
                errorList.add(t);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished from responseObserver in CnnStt");
                finishLatch.countDown();
            }
        };
        // 1-2. Sending data to responseObserver asynchronously
        StreamObserver<W2L.Speech> requestObserver = cnnSttStub.recognize(responseObserver);
        W2L.Speech.Builder speechBuilder = W2L.Speech.newBuilder();

        ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
        int len = 0;
        byte[] buffer = new byte[1024*512];
        while((len = bis.read(buffer)) > 0){
            if(len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(speechBuilder.setBin(inputByteString).build());
        }

        requestObserver.onCompleted();
        boolean timeout;
        // 2. Waiting till async work is done
        try {
            timeout = finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.warn(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
        if(!timeout){
            logger.error("request timeout");
            throw new MindsRestException();
        }
        if(!errorList.isEmpty()){
            logger.error(errorList.get(0));
            throw new MindsRestException();
        }

        String result = resultArray.get(0);
        logger.debug(result);
        return new CnnSttRes(result);
    }

    public StreamCnnSttRes doStreamCnnStt(MultipartFile file, int samplerate) throws IOException, MindsRestException {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        List<TextSegment> resultArray = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();

        StreamObserver<W2L.Speech> requestObserver = cnnSttStub.streamRecognize(new StreamObserver<W2L.TextSegment>(){

            @Override
            public void onNext(W2L.TextSegment textSegment) {
                TextSegment result = new TextSegment((double)textSegment.getStart()/samplerate, (double)textSegment.getEnd()/samplerate, textSegment.getTxt());
                resultArray.add(result);
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Err from responseObserver in CnnStt");
                logger.error(t.getMessage());
                errorList.add(t);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished from responseObserver in StreamCnnStt");
                finishLatch.countDown();
            }
        });
        W2L.Speech.Builder speechBuilder = W2L.Speech.newBuilder();

        ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
        int len = 0;
        byte[] buffer = new byte[1024 * 512];
        while((len = bis.read(buffer)) > 0){
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(speechBuilder.setBin(inputByteString).build());
        }

        requestObserver.onCompleted();
        boolean timeout;
        // 2. Waiting till async work is done
        try {
            timeout = finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.warn(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
        if(!timeout){
            logger.error("request timeout");
            throw new MindsRestException();
        }
        if(!errorList.isEmpty()){
            logger.error(errorList.get(0));
            throw new MindsRestException();
        }

        return new StreamCnnSttRes(resultArray);
    }
}
