package brain.api.classification.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class FaceConditionResponse {
    private List<Result> resultList;

    @Getter
    @Setter
    @ToString
    public static class Result {
        private String category;
        private int pred;
    }
}
