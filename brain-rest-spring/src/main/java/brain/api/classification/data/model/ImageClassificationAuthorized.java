package brain.api.classification.data.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ImageClassificationAuthorized {
    private Long id;
    private String apiId;
    private String imageClassifyModel;
    private LocalDateTime createDate;
    private boolean active;
}
