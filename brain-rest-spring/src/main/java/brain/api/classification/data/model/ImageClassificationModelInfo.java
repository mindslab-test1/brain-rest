package brain.api.classification.data.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ImageClassificationModelInfo {
    private Long id;
    private boolean active;
    private String model;
    private String host;
    private Integer port;
    private boolean open;
}
