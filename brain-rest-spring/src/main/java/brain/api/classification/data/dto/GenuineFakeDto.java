package brain.api.classification.data.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
public class GenuineFakeDto {
    private String apiId;
    private String apiKey;
    private String model;
    private MultipartFile image;
}
