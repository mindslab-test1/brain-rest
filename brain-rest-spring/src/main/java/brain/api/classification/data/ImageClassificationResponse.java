package brain.api.classification.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ImageClassificationResponse {
    private String result;
    private int code;
}
