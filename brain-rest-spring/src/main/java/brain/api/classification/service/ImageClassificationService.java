package brain.api.classification.service;

import brain.api.classification.ClassificationCommonCode;
import brain.api.classification.client.BioposhClient;
import brain.api.classification.client.TripleEClient;
import brain.api.classification.dao.ImageClassificationAuthorizedDao;
import brain.api.classification.dao.ImageClassificationModelInfoDao;
import brain.api.classification.data.FaceConditionResponse;
import brain.api.classification.data.ImageClassificationResponse;
import brain.api.classification.data.dto.FaceConditionDto;
import brain.api.classification.data.dto.GenuineFakeDto;
import brain.api.classification.data.model.ImageClassificationAuthorized;
import brain.api.classification.data.model.ImageClassificationModelInfo;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@Service
public class ImageClassificationService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(ClassificationCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    private final ImageClassificationModelInfoDao imageClassificationModelInfoDao;
    private final ImageClassificationAuthorizedDao imageClassificationAuthorizedDao;

    private final TripleEClient tripleEClient;
    private final BioposhClient bioposhClient;

    // @Transactional TODO: Refactoring Target
    public CommonResponse<ImageClassificationResponse> classify(GenuineFakeDto genuineFakeDto, HttpServletRequest request) throws MindsRestException {
        // input validation
        String apiId = genuineFakeDto.getApiId();
        String apiKey = genuineFakeDto.getApiKey();
        String model = genuineFakeDto.getModel();
        MultipartFile image = genuineFakeDto.getImage();

        // TODO: Refactoring Target => Enhancement to check @Validated
        if (validateImage(image)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "not found requested image resource");
        }

        // TODO: Refactoring Target
        if (!validateImageType(image)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported media type.");
        }

        String filename = image.getOriginalFilename().toLowerCase();
        String fileExtension = FilenameUtils.getExtension(filename);
        if (!(fileExtension.equalsIgnoreCase("jpg") ||
                fileExtension.equalsIgnoreCase("jpeg"))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported file extension. Please check allow extension(jpg, jpeg)");
        }

        // get Model
        ImageClassificationModelInfo imageClassificationModelInfo = imageClassificationModelInfoDao.getModelInfo(model);
        checkToAvailableModelAndKey(imageClassificationModelInfo, apiId, model);

        validate(apiId, apiKey, ClassificationCommonCode.SERVICE_NAME, 1, request);
        tripleEClient.setDestination(imageClassificationModelInfo.getHost(), imageClassificationModelInfo.getPort());

        try {
            ImageClassificationResponse response = tripleEClient.classifyImage(image.getBytes());
            return new CommonResponse<>(new CommonMsg(), response);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

//    @Transactional TODO: Refactoring Target
    public CommonResponse<FaceConditionResponse> classify(FaceConditionDto faceConditionDto, HttpServletRequest request) throws MindsRestException {
        // input validation
        String apiId = faceConditionDto.getApiId();
        String apiKey = faceConditionDto.getApiKey();
        String model = faceConditionDto.getModel();
        MultipartFile front = faceConditionDto.getFront();
        MultipartFile left = faceConditionDto.getLeft();
        MultipartFile right = faceConditionDto.getRight();

        // TODO: Refactoring Target => Enhancement to check @Validated
        if (validateImage(front) || validateImage(left) || validateImage(right)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "not found requested image resource");
        }

        // TODO: Refactoring Target
        if (!(validateImageType(front) || validateImageType(left) || validateImageType(right))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported media type.");
        }

        String leftFilename = left.getOriginalFilename().toLowerCase();
        String rightFilename = right.getOriginalFilename().toLowerCase();
        String frontFilename = front.getOriginalFilename().toLowerCase();

        if (
                validateImageFileExtension(leftFilename) ||
                        validateImageFileExtension(frontFilename) ||
                        validateImageFileExtension(rightFilename)
        ) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported file extension. Please check allow extension(jpg, jpeg, png)");
        }

        validate(apiId, apiKey, ClassificationCommonCode.SERVICE_NAME, 1, request);

        // get Model
        ImageClassificationModelInfo imageClassificationModelInfo = imageClassificationModelInfoDao.getModelInfo(model);
        checkToAvailableModelAndKey(imageClassificationModelInfo, apiId, model);

        bioposhClient.setDestination(imageClassificationModelInfo.getHost(), imageClassificationModelInfo.getPort());

        try {
            FaceConditionResponse response = bioposhClient.classifyImage(front.getBytes(), left.getBytes(), right.getBytes());
            return new CommonResponse<>(new CommonMsg(), response);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    private ImageClassificationAuthorized checkToAvailableModelAndKey(ImageClassificationModelInfo imageClassificationModelInfo, String apiId, String model) throws MindsRestException {
        ImageClassificationAuthorized imageClassificationAuthorized = null;

        // validation area
        if (imageClassificationModelInfo == null) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "unsupported model: " + model);
        }
        if (!imageClassificationModelInfo.isOpen()) {
            if (!imageClassificationModelInfo.isActive()) {
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        "unused model: " + model);
            }
            // 특정 API Key에 대해서만 이용 가능한지 체크
            imageClassificationAuthorized = imageClassificationAuthorizedDao.getAuthorized(apiId, model);
            if (imageClassificationAuthorized == null) {
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        "not found apiId(" + apiId + "), model(" + model +")");
            }
            if (!imageClassificationAuthorized.isActive()) {
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        "unused apiId: " + apiId);
            }
        }
        return imageClassificationAuthorized;
    }

    private boolean validateImageFileExtension(String filename) {
        String fileExtension = FilenameUtils.getExtension(filename);
        return !(fileExtension.equalsIgnoreCase("jpg") ||
                fileExtension.equalsIgnoreCase("jpeg") ||
                fileExtension.equalsIgnoreCase("png"));
    }

    private boolean validateImageType(MultipartFile image) {
        return image.getContentType().contains(CommonCode.IMAGE);
    }

    private boolean validateImage(MultipartFile image) {
        return image == null || image.isEmpty() || image.getSize() == 0;
    }
}
