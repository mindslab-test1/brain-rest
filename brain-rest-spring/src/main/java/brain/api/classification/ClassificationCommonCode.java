package brain.api.classification;

public class ClassificationCommonCode {
    private ClassificationCommonCode() {}

    public static final String SERVICE_NAME = "Classification";

    public static final String FRONT_IMAGE = "Front";
    public static final String LEFT_IMAGE = "Left";
    public static final String RIGHT_IMAGE = "Right";
}
