package brain.api.classification.dao;

import brain.api.classification.ClassificationCommonCode;
import brain.api.classification.data.model.ImageClassificationAuthorized;
import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.ImageClassificationAuthorizedMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ImageClassificationAuthorizedDao {
    private static final CloudApiLogger logger = new CloudApiLogger(ClassificationCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public ImageClassificationAuthorized getAuthorized(String apiId, String model) {
        try (SqlSession session = sessionFactory.openSession(true)){
            Map<String, String> params = new HashMap<>();
            params.put("apiId", apiId);
            params.put("model", model);
            ImageClassificationAuthorizedMapper mapper = session.getMapper(ImageClassificationAuthorizedMapper.class);
            logger.debug(params);
            return mapper.findByApiIdAndModel(params);
        }
    }

}
