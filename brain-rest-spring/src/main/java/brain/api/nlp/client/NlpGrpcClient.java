package brain.api.nlp.client;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.nlp.NlpCommonCode;
import brain.api.nlp.data.NlpBaseResponse;
import brain.api.nlp.data.NlpDocument;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.Nlp;
import maum.common.LangOuterClass;
import org.springframework.stereotype.Component;

@Component
public class NlpGrpcClient {
    private static final CloudApiLogger logger = new CloudApiLogger(NlpCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public NlpBaseResponse nlpLegacy(String reqText, String lang){
        logger.debug("nlp legacy");
        NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(
                CommonUtils.getManagedChannel("125.132.250.243", 9823)
        ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        Nlp.InputText inputText;
        if(lang.equals("kor") || lang.equals("ko_KR")) {
            inputText = Nlp.InputText.newBuilder()
                    .setText(reqText)
                    .setLang(LangOuterClass.LangCode.kor)
                    .setSplitSentence(true)
                    .setUseTokenizer(false)
                    .setUseSpace(false)
                    .build();
            stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel("13.125.52.27", 9823)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        }
        else if (lang.equals("eng") || lang.equals("en_US")) {
            inputText = Nlp.InputText.newBuilder()
                    .setText(reqText)
                    .setLang(LangOuterClass.LangCode.eng)
                    .setSplitSentence(true)
                    .setUseTokenizer(false)
                    .setUseSpace(false)
                    .build();
            stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel("13.125.52.27", 9814)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        }
        else
            inputText = Nlp.InputText.newBuilder()
                    .setText(reqText)
                    .setLang(LangOuterClass.LangCode.UNRECOGNIZED)
                    .setSplitSentence(true)
                    .setUseTokenizer(false)
                    .setUseSpace(false)
                    .build();

        logger.debug("build");
        Nlp.Document retDoc = stub.analyze(inputText);
        logger.debug("get ret");

        NlpDocument document = CommonUtils.nlpDocumentBuilder(retDoc);

        logger.info(String.format("Result: %s", document));

        return new NlpBaseResponse(
                new CommonMsg(),
                document
        );
    }
}
