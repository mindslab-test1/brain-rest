package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpDocument implements Serializable {
    private String category;
    private float categoryWeight;
    private List<NlpSentence> sentences;
    private List<NlpKeywordFrequency> keywordFrequencies;

    public NlpDocument(String category, float categoryWeight, List<NlpSentence> sentences, List<NlpKeywordFrequency> keywordFrequencies) {
        this.category = category;
        this.categoryWeight = categoryWeight;
        this.sentences = sentences;
        this.keywordFrequencies = keywordFrequencies;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCategoryWeight() {
        return categoryWeight;
    }

    public void setCategoryWeight(float categoryWeight) {
        this.categoryWeight = categoryWeight;
    }

    public List<NlpSentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<NlpSentence> sentences) {
        this.sentences = sentences;
    }

    public List<NlpKeywordFrequency> getKeywordFrequencies() {
        return keywordFrequencies;
    }

    public void setKeywordFrequencies(List<NlpKeywordFrequency> keywordFrequencies) {
        this.keywordFrequencies = keywordFrequencies;
    }

    @Override
    public String toString() {
        return "NlpDocument{" +
                "category='" + category + '\'' +
                ", categoryWeight=" + categoryWeight +
                ", sentences=" + sentences +
                ", keywordFrequencies=" + keywordFrequencies +
                '}';
    }
}
