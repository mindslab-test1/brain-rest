package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpMorphemeEval implements Serializable {
    private int seq;
    private String target;
    private String result;
    private int wordId;
    private int mBegin;
    private int mEnd;

    public NlpMorphemeEval(int seq, String target, String result, int wordId, int mBegin, int mEnd) {
        this.seq = seq;
        this.target = target;
        this.result = result;
        this.wordId = wordId;
        this.mBegin = mBegin;
        this.mEnd = mEnd;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getWordId() {
        return wordId;
    }

    public void setWordId(int wordId) {
        this.wordId = wordId;
    }

    public int getmBegin() {
        return mBegin;
    }

    public void setmBegin(int mBegin) {
        this.mBegin = mBegin;
    }

    public int getmEnd() {
        return mEnd;
    }

    public void setmEnd(int mEnd) {
        this.mEnd = mEnd;
    }

    @Override
    public String toString() {
        return "NlpMorphemeEval{" +
                "seq=" + seq +
                ", target='" + target + '\'' +
                ", result='" + result + '\'' +
                ", wordId=" + wordId +
                ", mBegin=" + mBegin +
                ", mEnd=" + mEnd +
                '}';
    }
}
