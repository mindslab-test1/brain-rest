package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpKeywordFrequency implements Serializable {
    private int sequence;
    private String keyword;
    private int frequency;
    private String wordType;
    private String wordTypeName;

    public NlpKeywordFrequency(int sequence, String keyword, int frequency, String wordType, String wordTypeName) {
        this.sequence = sequence;
        this.keyword = keyword;
        this.frequency = frequency;
        this.wordType = wordType;
        this.wordTypeName = wordTypeName;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getWordType() {
        return wordType;
    }

    public void setWordType(String wordType) {
        this.wordType = wordType;
    }

    public String getWordTypeName() {
        return wordTypeName;
    }

    public void setWordTypeName(String wordTypeName) {
        this.wordTypeName = wordTypeName;
    }

    @Override
    public String toString() {
        return "NlpKeywordFrequency{" +
                "sequence=" + sequence +
                ", keyword='" + keyword + '\'' +
                ", frequency=" + frequency +
                ", wordType='" + wordType + '\'' +
                ", wordTypeName='" + wordTypeName + '\'' +
                '}';
    }
}
