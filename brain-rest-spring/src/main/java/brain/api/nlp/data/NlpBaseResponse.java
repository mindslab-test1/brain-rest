package brain.api.nlp.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpBaseResponse implements Serializable {
    private CommonMsg message;
    private NlpDocument document;

    public NlpBaseResponse() {
    }

    public NlpBaseResponse(CommonMsg message, NlpDocument document) {
        this.message = message;
        this.document = document;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public NlpDocument getDocument() {
        return document;
    }

    public void setDocument(NlpDocument document) {
        this.document = document;
    }

    @Override
    public String toString() {
        return "NlpBaseResponse{" +
                "message=" + message +
                ", document=" + document +
                '}';
    }
}
