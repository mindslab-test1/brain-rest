package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpSentence implements Serializable {
    private int sequence;
    private String text;
    private List<NlpWord> words;
    private List<NlpMorpheme> morps;
    private List<NlpMorphemeEval> morphEvals;
    private List<NlpNamedEntity> namedEntities;

    public NlpSentence(int sequence, String text, List<NlpWord> words, List<NlpMorpheme> morps, List<NlpMorphemeEval> morphEvals, List<NlpNamedEntity> namedEntities) {
        this.sequence = sequence;
        this.text = text;
        this.words = words;
        this.morps = morps;
        this.morphEvals = morphEvals;
        this.namedEntities = namedEntities;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<NlpWord> getWords() {
        return words;
    }

    public void setWords(List<NlpWord> words) {
        this.words = words;
    }

    public List<NlpMorpheme> getMorps() {
        return morps;
    }

    public void setMorps(List<NlpMorpheme> morps) {
        this.morps = morps;
    }

    public List<NlpMorphemeEval> getMorphEvals() {
        return morphEvals;
    }

    public void setMorphEvals(List<NlpMorphemeEval> morphEvals) {
        this.morphEvals = morphEvals;
    }

    public List<NlpNamedEntity> getNamedEntities() {
        return namedEntities;
    }

    public void setNamedEntities(List<NlpNamedEntity> namedEntities) {
        this.namedEntities = namedEntities;
    }

    @Override
    public String toString() {
        return "NlpSentence{" +
                "sequence=" + sequence +
                ", text='" + text + '\'' +
                '}';
    }
}
