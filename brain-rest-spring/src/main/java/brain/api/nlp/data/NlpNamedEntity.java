package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpNamedEntity implements Serializable {
    private int seq;
    private String text;
    private String type;
    private int begin;
    private int end;
    private String typeName;
    private double weight;
    private int commonNoun;
    private int beginSid;
    private int endSid;

    public NlpNamedEntity(int seq, String text, String type, int begin, int end, String typeName, double weight, int commonNoun, int beginSid, int endSid) {
        this.seq = seq;
        this.text = text;
        this.type = type;
        this.begin = begin;
        this.end = end;
        this.typeName = typeName;
        this.weight = weight;
        this.commonNoun = commonNoun;
        this.beginSid = beginSid;
        this.endSid = endSid;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCommonNoun() {
        return commonNoun;
    }

    public void setCommonNoun(int commonNoun) {
        this.commonNoun = commonNoun;
    }

    public int getBeginSid() {
        return beginSid;
    }

    public void setBeginSid(int beginSid) {
        this.beginSid = beginSid;
    }

    public int getEndSid() {
        return endSid;
    }

    public void setEndSid(int endSid) {
        this.endSid = endSid;
    }

    @Override
    public String toString() {
        return "NlpNamedEntity{" +
                "seq=" + seq +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", begin=" + begin +
                ", end=" + end +
                ", typeName='" + typeName + '\'' +
                ", weight=" + weight +
                ", commonNoun=" + commonNoun +
                ", beginSid=" + beginSid +
                ", endSid=" + endSid +
                '}';
    }
}
