package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpMorpheme implements Serializable {
    private int seq;
    private String lemma;
    private String type;
    private int position;
    private int wid;
    private double weight;

    public NlpMorpheme(int seq, String lemma, String type, int position, int wid, double weight) {
        this.seq = seq;
        this.lemma = lemma;
        this.type = type;
        this.position = position;
        this.wid = wid;
        this.weight = weight;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "NlpMorpheme{" +
                "seq=" + seq +
                ", lemma='" + lemma + '\'' +
                ", type='" + type + '\'' +
                ", position=" + position +
                ", wid=" + wid +
                ", weight=" + weight +
                '}';
    }
}
