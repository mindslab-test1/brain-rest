package brain.api.nlp.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NlpWord implements Serializable {
    private int seq;
    private String text;
    private String taggedText;
    private String type;
    private int begin;
    private int end;
    private int beginSid;
    private int endSid;
    private int position;

    public NlpWord(
            int seq,
            String text,
            String taggedText,
            String type,
            int begin,
            int end,
            int beginSid,
            int endSid,
            int position
    ) {
        this.seq = seq;
        this.text = text;
        this.taggedText = taggedText;
        this.type = type;
        this.begin = begin;
        this.end = end;
        this.beginSid = beginSid;
        this.endSid = endSid;
        this.position = position;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTaggedText() {
        return taggedText;
    }

    public void setTaggedText(String taggedText) {
        this.taggedText = taggedText;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getBeginSid() {
        return beginSid;
    }

    public void setBeginSid(int beginSid) {
        this.beginSid = beginSid;
    }

    public int getEndSid() {
        return endSid;
    }

    public void setEndSid(int endSid) {
        this.endSid = endSid;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "NlpWord{" +
                "seq=" + seq +
                ", text='" + text + '\'' +
                ", taggedText='" + taggedText + '\'' +
                ", type='" + type + '\'' +
                ", begin=" + begin +
                ", end=" + end +
                ", beginSid=" + beginSid +
                ", endSid=" + endSid +
                ", position=" + position +
                '}';
    }
}
