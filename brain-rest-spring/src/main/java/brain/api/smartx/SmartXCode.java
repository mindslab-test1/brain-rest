package brain.api.smartx;

public class SmartXCode {
    public static final String SMARTX_ADDRESS = "http://182.162.19.16:5000/";
    public static final String SMARTX_ABNORMAL_ADDRESS = "http://182.162.19.16";
    public static final int SMARTX_ABNORMAL_FALLDOWN_PORT = 5001;
    public static final int SMARTX_ABNORMAL_LOITERING_PORT = 5002;
    public static final int SMARTX_ABNORMAL_INTRUSION_PORT = 5003;
    public static final String SERVICE_NAME = "SMARTX";

    public static final String PLATE_RECOG = "PlateRecog";
    public static final String POSE_EXTRACT = "PoseExtract";
    public static final String ANOM_DETECT = "AnomalyDetect";
    public static final String HOSP_RECEIPT = "OCR_HOSP";
    public static final String FEAT_SELECT = "FeatureSelection";
    public static final String SUB_EXTRACT = "VCASubtitleExtract";
    public static final String FACE_TRACK = "VCAFaceTracking";

    private SmartXCode() {}
}
