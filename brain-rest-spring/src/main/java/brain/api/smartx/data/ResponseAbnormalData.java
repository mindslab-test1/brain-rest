package brain.api.smartx.data;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseAbnormalData {
    private String status;
    private byte[] abnormalImg;

    @Override
    public String toString() {
        return "ResponseAbnormalData{" +
                "status='" + status + '\'' +
                ", abnormalImg(length)=" + abnormalImg.length +
                '}';
    }
}
