package brain.api.smartx.data;

import brain.api.common.data.CommonMsg;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

public class OcrHospRes {
    private CommonMsg message;
    private OcrBaseRes result_img;
    private OcrBaseRes result_csv;

    public OcrHospRes(){
      this.message = new CommonMsg();
    }

    public OcrHospRes(CommonMsg message) {
        this.message = message;
        result_img = null;
        result_csv = null;
    }

    public CommonMsg getMessage() {
        return message;
    }
    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public OcrBaseRes getResult_img() {
        return result_img;
    }
    public void setResult_img(OcrBaseRes result_img) {
        this.result_img = result_img;
    }

    public OcrBaseRes getResult_csv() {
        return result_csv;
    }
    public void setResult_csv(OcrBaseRes result_csv) {
        this.result_csv = result_csv;
    }

    @Override
    public String toString() {
        return "OcrHospRes{" +
                "message=" + message +
                ", result_img=" + result_img +
                ", result_csv=" + result_csv +
                '}';
    }
}
