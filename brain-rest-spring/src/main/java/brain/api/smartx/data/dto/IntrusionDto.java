package brain.api.smartx.data.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
public class IntrusionDto {
    private String apiId;
    private String apiKey;
    private MultipartFile video;
    private String roiList;
}
