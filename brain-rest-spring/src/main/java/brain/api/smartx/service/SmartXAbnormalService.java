package brain.api.smartx.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.service.BaseService;
import brain.api.smartx.SmartXCode;
import brain.api.smartx.SmartXValidateUtil;
import brain.api.smartx.client.SmartXAbnormalClient;
import brain.api.smartx.data.ResponseAbnormalData;
import brain.api.smartx.data.dto.FalldownDto;
import brain.api.smartx.data.dto.IntrusionDto;
import brain.api.smartx.data.dto.LoiteringDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class SmartXAbnormalService extends BaseService {
    private final SmartXAbnormalClient smartXAbnormalClient;

    public CommonResponse<ResponseAbnormalData> detectFalldown(FalldownDto falldownDto, HttpServletRequest request) throws MindsRestException, IOException, MessagingException {
        validate(falldownDto.getApiId(), falldownDto.getApiKey(), SmartXCode.SERVICE_NAME, 1, request);
        // TODO: Refactoring Target, @Valid 로 input validation 해결가능
        MultipartFile video = falldownDto.getVideo();
        if (video.isEmpty()) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found video file");
        }
        if (!SmartXValidateUtil.checkVideoContentType(video)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not supported content-type. Only support video/* content-type");
        }
        if (!SmartXValidateUtil.checkVideoExtensionMp4(video.getOriginalFilename())) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Invalid video file. Only mp4 file is allowed");
        }
        if (SmartXValidateUtil.limitVideoSize(video)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Invalid video file size. Only files under 50mb are accepted.");
        }

        String roiList = Optional.ofNullable(falldownDto.getRoiList()).orElseThrow(() -> new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "Not found roiList"));
        if (!SmartXValidateUtil.hasOverallOfJsonType(roiList)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found overall field in roiList. Please check roiList.");
        }
        if (!SmartXValidateUtil.hasFieldOfJsonType(roiList, "falldown")) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found falldown field in roiList. Please check roiList.");
        }

        ResponseAbnormalData responseAbnormalData = smartXAbnormalClient.detectFalldown(video, roiList);
        return new CommonResponse<>(new CommonMsg(), responseAbnormalData);
    }

    public CommonResponse<ResponseAbnormalData> detectLoitering(LoiteringDto loiteringDto, HttpServletRequest request) throws MindsRestException, IOException, MessagingException {
        validate(loiteringDto.getApiId(), loiteringDto.getApiKey(), SmartXCode.SERVICE_NAME, 1, request);
        // TODO: Refactoring Target, @Valid 로 input validation 해결가능
        MultipartFile video = loiteringDto.getVideo();
        if (video.isEmpty()) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found video file");
        }
        if (!SmartXValidateUtil.checkVideoContentType(video)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not supported content-type. Only support video/* content-type");
        }
        if (!SmartXValidateUtil.checkVideoExtensionMp4(video.getOriginalFilename())) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Invalid video file. Only mp4 file is allowed");
        }
        if (SmartXValidateUtil.limitVideoSize(video)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Invalid video file size. Only files under 50mb are accepted.");
        }

        String roiList = Optional.ofNullable(loiteringDto.getRoiList()).orElseThrow(() -> new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "Not found roiList"));
        if (!SmartXValidateUtil.hasOverallOfJsonType(roiList)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found overall field in roiList. Please check roiList.");
        }
        if (!SmartXValidateUtil.hasFieldOfJsonType(roiList, "loitering")) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found loitering field in roiList. Please check roiList.");
        }
        ResponseAbnormalData responseAbnormalData = smartXAbnormalClient.detectLoitering(video, roiList);
        return new CommonResponse<>(new CommonMsg(), responseAbnormalData);
    }

    public CommonResponse<ResponseAbnormalData> detectIntrusion(IntrusionDto intrusionDto, HttpServletRequest request) throws MindsRestException, IOException, MessagingException {
        validate(intrusionDto.getApiId(), intrusionDto.getApiKey(), SmartXCode.SERVICE_NAME, 1, request);
        // TODO: Refactoring Target, @Valid 로 input validation 해결가능
        MultipartFile video = intrusionDto.getVideo();
        if (video.isEmpty()) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found video file");
        }
        if (!SmartXValidateUtil.checkVideoContentType(video)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not supported content-type. Only support video/* content-type");
        }
        if (!SmartXValidateUtil.checkVideoExtensionMp4(video.getOriginalFilename())) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Invalid video file. Only mp4 file is allowed");
        }
        if (SmartXValidateUtil.limitVideoSize(video)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Invalid video file size. Only files under 50mb are accepted.");
        }

        String roiList = Optional.ofNullable(intrusionDto.getRoiList()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "Not found roiList"));
        if (!SmartXValidateUtil.hasOverallOfJsonType(roiList)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found overall field in roiList. Please check roiList.");
        }
        if (!SmartXValidateUtil.hasFieldOfJsonType(roiList, "intrusion")) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not found intrusion field in roiList. Please check roiList.");
        }
        ResponseAbnormalData responseAbnormalData = smartXAbnormalClient.detectIntrusion(video, roiList);
        return new CommonResponse<>(new CommonMsg(), responseAbnormalData);
    }
}
