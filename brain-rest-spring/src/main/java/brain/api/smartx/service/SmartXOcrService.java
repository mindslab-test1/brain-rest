package brain.api.smartx.service;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.smartx.SmartXCode;
import brain.api.smartx.client.SmartXOcrClient;
import brain.api.smartx.data.OcrHospRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
public class SmartXOcrService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(SmartXCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    SmartXOcrClient ocrClient;

    public OcrHospRes hospitalReceipt(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        validate(apiId, apiKey, SmartXCode.HOSP_RECEIPT,1, request);
        return ocrClient.doOcrHospitalReceipt(file);
    }
}
