package brain.api.smartx;


import com.google.common.net.MediaType;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

public class SmartXValidateUtil {

    public static boolean hasOverallOfJsonType(String roiList) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(roiList);
        return jsonObject.has("overall");
    }

    public static boolean hasFieldOfJsonType(String roiList, String field) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(roiList);
        return jsonObject.has(field);
    }

    public static boolean checkVideoExtensionMp4(String filename) {
        return FilenameUtils.isExtension(filename, "mp4");
    }

    public static boolean limitVideoSize(MultipartFile video) {
        // TODO: Refactoring Target, @Value Injection
        return video.getSize() > (50 * FileUtils.ONE_MB);
    }

    public static boolean checkVideoContentType(MultipartFile video) {
        return video.getContentType().contains(MediaType.ANY_VIDEO_TYPE.type());
    }
}
