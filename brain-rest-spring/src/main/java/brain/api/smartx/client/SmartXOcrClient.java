package brain.api.smartx.client;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.smartx.SmartXCode;
import brain.api.smartx.data.OcrBaseRes;
import brain.api.smartx.data.OcrHospRes;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Component
public class SmartXOcrClient {
    private static final CloudApiLogger logger = new CloudApiLogger(SmartXCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public OcrHospRes doOcrHospitalReceipt(MultipartFile file) throws IOException {

        File tempDir = Files.createTempDir();
        OcrHospRes ocrHospRes = new OcrHospRes();
        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(SmartXCode.SMARTX_ADDRESS + "HospitalReceipt");
            File avrFile = new File(tempDir.getPath());
            if(!avrFile.exists()){
                avrFile.mkdirs();
            }
            avrFile = new File(tempDir.getPath()+"/"+file.getOriginalFilename());
            file.transferTo(avrFile);

            FileBody fileBody = new FileBody(avrFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("receipt_img", fileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            InputStream is = new BufferedInputStream(response.getEntity().getContent());

            ByteArrayDataSource dataSource = new ByteArrayDataSource(is, "multipart/form-data");
            MimeMultipart multipart = new MimeMultipart(dataSource);

            int count = multipart.getCount();
            for(int i = 0; i < count; i++){
                BodyPart bodyPart = multipart.getBodyPart(i);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                String resultName = bodyPart.getFileName();
                InputStream resultIs = bodyPart.getInputStream();
                byte[] resultByte = IOUtils.toByteArray(resultIs);
                if(bodyPart.isMimeType("image/jpeg")){
                    ocrHospRes.setResult_img(new OcrBaseRes(resultName, resultByte));
                }
                else if(bodyPart.isMimeType("text/csv")){
                    ocrHospRes.setResult_csv(new OcrBaseRes(resultName, resultByte));
                }
                else{
                    return new OcrHospRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
                }
            }
        } catch (MessagingException e) {
            logger.debug(e.getMessage());
            return new OcrHospRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }

        FileUtils.cleanDirectory(tempDir);
        tempDir.delete();
        return ocrHospRes;
    }
}
