package brain.api.smartx.controller;

import brain.api.smartx.service.SmartXRoadService;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/smartXLoad")
public class SmartXRoadController {

    @Autowired
    SmartXRoadService smartXRoadService;

    @PostMapping(
            value = "/PlateRecog",
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}
    )
    public String plateRecog(@RequestParam("car_img") MultipartFile car_img
            , @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , HttpServletRequest request){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>();
        String mapToJson = "";

        try{
            map = smartXRoadService.getplateRecog(car_img, apiId, apiKey, request);
            mapToJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mapToJson;
    }

    @PostMapping(
            value = "/poseExtract",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public String runPoseRecog(@RequestParam("pose_img") MultipartFile img
            , @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , HttpServletRequest request){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>();
        String mapToJson = "";

        try{
            map = smartXRoadService.getPoseRecog(img, apiId, apiKey, request);
            mapToJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mapToJson;
    }

    @PostMapping(
            value = "/anomalyDetect",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public String anomalyDetect(@RequestParam("video") MultipartFile video
            , @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , HttpServletRequest request){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>();
        String mapToJson = "";

        try{
            map = smartXRoadService.getAnomalyDetect(video, apiId, apiKey, request);
            mapToJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mapToJson;
    }

}
