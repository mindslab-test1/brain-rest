package brain.api.smartx.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.smartx.SmartXCode;
import brain.api.smartx.data.dto.FalldownDto;
import brain.api.smartx.data.dto.IntrusionDto;
import brain.api.smartx.data.dto.LoiteringDto;
import brain.api.smartx.service.SmartXAbnormalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/abnormal-behavior")
public class SmartXAbnormalController {
    private final SmartXAbnormalService smartXAbnormalService;

    private static final CloudApiLogger logger = new CloudApiLogger(SmartXCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @PostMapping(
            value = "/falldown:detect",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> falldown(FalldownDto falldownDto, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(smartXAbnormalService.detectFalldown(falldownDto, request));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @PostMapping(
            value = "/loitering:detect",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> loitering(LoiteringDto loiteringDto, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(smartXAbnormalService.detectLoitering(loiteringDto, request));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @PostMapping(
            value = "/intrusion:detect",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> intrusion(IntrusionDto intrusionDto, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(smartXAbnormalService.detectIntrusion(intrusionDto, request));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }
}
