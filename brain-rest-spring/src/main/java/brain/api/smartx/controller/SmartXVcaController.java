package brain.api.smartx.controller;

import brain.api.smartx.service.SmartXVcaService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/Vca")
public class SmartXVcaController
{

    @Autowired
    SmartXVcaService smartXVcaService;

    @PostMapping(
            value = "/VCASubtitleExtract",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String subtitleExtract(@RequestParam MultipartFile scene_img
            , @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , HttpServletRequest request){

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>();
        String mapToJson = "";

        try{
            map = smartXVcaService.getSubtitleExtract(scene_img, apiId, apiKey, request);
            mapToJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mapToJson;
    }

    @PostMapping(
            value = "/faceTracking",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public String faceTracking(@RequestParam MultipartFile video
            , @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , HttpServletRequest request){
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>();
        String mapToJson = "";

        try{
            map = smartXVcaService.getFaceTracking(video, apiId, apiKey, request);
            mapToJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return mapToJson;
    }
}
