package brain.api.insight.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.insight.InsightCommonCode;
import brain.api.insight.data.InsightBaseResponse;
import com.google.protobuf.ByteString;
import maum.brain.insight.Insight;
import maum.brain.insight.InsightFaceGrpc;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InsightGrpcClient extends GrpcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(InsightCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private InsightFaceGrpc.InsightFaceBlockingStub insightBlockingStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.insightBlockingStub = InsightFaceGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e){
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public InsightBaseResponse insightFace(MultipartFile faceImg) throws MindsRestException {

        try {
            ByteString faceImgByteString = ByteString.copyFrom(faceImg.getBytes());

            Insight.FaceImage faceImage = Insight.FaceImage.newBuilder()
                    .setName("")
                    .setFaceImg(faceImgByteString).build();

            Insight.FaceVector faceVector = insightBlockingStub.getFaceVector(faceImage);

            List<Float> vectorArray = faceVector.getFaceVectorList();

            logger.info("[Insight result] vectorArraySize : " + vectorArray.size());

            return new InsightBaseResponse(
                    new CommonMsg()
                    , vectorArray
            );

        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
    }

    public Map setFace(String name, MultipartFile faceImg) throws MindsRestException {

        try {
            Map<String, Object> result = new HashMap <>();

            ByteString faceImgByteString = ByteString.copyFrom(faceImg.getBytes());
            Insight.FaceImage faceImage = Insight.FaceImage.newBuilder().setName(name).setFaceImg(faceImgByteString).build();
            Insight.FaceSet faceSet =
                    insightBlockingStub.setFace(faceImage);


            if(faceSet.getSuccess()) {
                result.put("result", true);
                result.put("name", name);
            } else {
                result.put("result", false);
            }

            logger.info("[setFace result] : " + result);

            return result;

        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );
        }

    }

    public Map recogFace(MultipartFile faceImg) throws MindsRestException {

        try {
            Map<String, Object> result = new HashMap <>();

            ByteString faceImgByteString = ByteString.copyFrom(faceImg.getBytes());
            Insight.FaceImage faceImage = Insight.FaceImage.newBuilder().setName("").setFaceImg(faceImgByteString).build();
            Insight.FaceRecog faceRecog = insightBlockingStub.recogFace(faceImage);
            if (faceRecog.getSuccess()) {
                result.put("result", true);
                result.put("name", faceRecog.getName());
            } else {
                result.put("result", false);
            }

            logger.info("[recogFace result] : " + result.toString());

            return result;
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );
        }

    }
}
