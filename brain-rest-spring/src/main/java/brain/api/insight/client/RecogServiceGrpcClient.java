package brain.api.insight.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.insight.InsightCommonCode;
import brain.api.insight.data.*;
import com.google.gson.Gson;
import maum.brain.app.insight.FaceRecogAppServiceGrpc;
import maum.brain.app.insight.RecogService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class RecogServiceGrpcClient extends GrpcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(InsightCommonCode.SERVICE_APP_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private FaceRecogAppServiceGrpc.FaceRecogAppServiceBlockingStub faceRecogAppServiceBlockingStub;
    private Gson gson = new Gson();

    public CommonMsg setDestination(String ip, int port) {
        if(super.checkDestination(ip, port)) return new CommonMsg();
        logger.debug(String.format("set destination %s, %d", ip, port));
        this.faceRecogAppServiceBlockingStub = FaceRecogAppServiceGrpc.newBlockingStub(
                CommonUtils.getManagedChannel(ip, port)
        ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        return new CommonMsg();
    }

    public SetFaceResponse setFace(String apiId, String dbId, String faceId, List<Float> faceVector) {
        RecogService.SetFaceRequest request = RecogService.SetFaceRequest.newBuilder()
                .setApiId(apiId)
                .setDbId(dbId)
                .setFaceId(faceId)
                .addAllFaceVector(faceVector)
                .build();

        RecogService.SetFaceResponse result = faceRecogAppServiceBlockingStub.setFace(request);
        SetFaceResponse response = new SetFaceResponse(new CommonMsg(result.getStatus(), result.getMessage()));

        logger.info("[SetFace Success Result] apiId : " + apiId + ", dbId : " + dbId + ", faceId : " + faceId);

        return response;
    }

    public GetFaceResponse getFaceList(String apiId, String dbId) {
        RecogService.GetFaceRequest request = RecogService.GetFaceRequest.newBuilder()
                .setApiId(apiId)
                .setDbId(dbId)
                .build();

        RecogService.GetFaceResponse result = faceRecogAppServiceBlockingStub.getFaceList(request);
        List<FaceData> payLoad = new ArrayList<>();

        for (RecogService.FaceData payLoadVal : result.getPayloadList()) {
            FaceData faceData = new FaceData();
            faceData.setId(payLoadVal.getId());

            List<Float> faceVector = new ArrayList<>();
            for (float vector : payLoadVal.getFaceVectorList()) {
                faceVector.add(vector);
            }
            faceData.setFaceVector(faceVector);

            FaceMetaData metaData = new FaceMetaData();
            faceData.setMetaData(metaData);
            setMetadata(metaData, payLoadVal.getMetadata());
            payLoad.add(faceData);
        }

        GetFaceResponse response = new GetFaceResponse(new CommonMsg(), payLoad);

        logger.info("[GetFaceList Success Result] apiId : " + apiId + ", dbId : " + dbId + ", ListSize : " + payLoad.size());

        return response;
    }

    public DeleteFaceResponse deleteFace(String apiId, String dbId, String faceId) {
        RecogService.DeleteFaceRequest request = RecogService.DeleteFaceRequest.newBuilder()
                .setApiId(apiId)
                .setDbId(dbId)
                .setFaceId(faceId)
                .build();
        RecogService.DeleteFaceResponse result = faceRecogAppServiceBlockingStub.deleteFace(request);

        DeleteFaceResponse response = new DeleteFaceResponse(new CommonMsg(result.getStatus(), result.getMessage()));

        logger.info("[DeleteFace Success Result] apiId : " + apiId + ", dbId : " + dbId + ", faceId : " + faceId);

        return response;
    }

    public FaceRecogResponse recogFace(String apiId, String dbId, List<Float> faceVector) {
        RecogService.FaceRecogRequest request = RecogService.FaceRecogRequest.newBuilder()
                .setApiId(apiId)
                .setDbId(dbId)
                .addAllFaceVector(faceVector)
                .build();

        RecogService.FaceRecogResponse result = faceRecogAppServiceBlockingStub.recogFace(request);

        FaceData faceData = new FaceData();
        faceData.setId(result.getResult().getId());
        faceData.setFaceVector(result.getResult().getFaceVectorList());

        FaceMetaData metaData = new FaceMetaData();
        setMetadata(metaData, result.getResult().getMetadata());
        faceData.setMetaData(metaData);

        FaceRecogResponse response = new FaceRecogResponse(new CommonMsg(result.getStatus(), result.getMessage()), faceData);

        logger.info("[RecogFace Success Result] apiId : " + apiId + ", dbId : " + dbId + ", result : " + result.getMessage());

        return response;
    }

    private FaceMetaData setMetadata(FaceMetaData faceMetaData, RecogService.MetaData metaData) {
        faceMetaData.setUpdateTime(new Date(metaData.getUpdateTime().getSeconds()*1000));
        faceMetaData.setCreateTime(new Date(metaData.getCreateTime().getSeconds()*1000));

        return faceMetaData;
    }
}
