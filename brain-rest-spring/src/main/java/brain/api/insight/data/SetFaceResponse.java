package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SetFaceResponse extends InsightAppBaseResponse {
    public SetFaceResponse(CommonMsg message) {
        super(message);
    }
}
