package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InsightBaseResponse {
    private CommonMsg message;
    private List<Float> vector;

    public InsightBaseResponse() {
        this.message = new CommonMsg(5000);
    }

    public InsightBaseResponse(CommonMsg message, List <Float> vector) {
        this.message = message;
        this.vector = vector;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public List<Float> getVector() {
        return vector;
    }

    public void setVector(List<Float> vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {
        return "InsightBaseResponse{" +
                "message=" + message +
                ", vector=" + vector +
                '}';
    }
}
