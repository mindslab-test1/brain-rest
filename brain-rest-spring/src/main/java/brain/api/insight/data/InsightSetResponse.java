package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InsightSetResponse extends InsightBaseResponse implements Serializable {
    private CommonMsg message;
    private Object data;

    public InsightSetResponse() {
    }

    public InsightSetResponse(CommonMsg message, Object data) {
        this.message = message;
        this.data = data;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InsightRecogResponse{" +
                "message=" + message +
                ", data=" + data +
                '}';
    }
}
