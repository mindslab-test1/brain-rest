package brain.api.insight.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FaceData implements Serializable {
    private String id;
    private List<Float> faceVector;
    private FaceMetaData metaData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Float> getFaceVector() {
        return faceVector;
    }

    public void setFaceVector(List<Float> faceVector) {
        this.faceVector = faceVector;
    }

    public FaceMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(FaceMetaData metaData) {
        this.metaData = metaData;
    }

    @Override
    public String toString() {
        return "FaceData{" +
                "id='" + id + '\'' +
                ", faceVector=" + faceVector +
                ", metaData=" + metaData +
                '}';
    }
}
