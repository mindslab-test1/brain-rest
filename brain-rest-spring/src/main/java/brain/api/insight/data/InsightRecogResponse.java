package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InsightRecogResponse {
    private CommonMsg message;
    private Object data;

    public InsightRecogResponse() {
    }

    public InsightRecogResponse(CommonMsg message, Object data) {
        this.message = message;
        this.data = data;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InsightRecogResponse{" +
                "message=" + message +
                ", data=" + data +
                '}';
    }
}
