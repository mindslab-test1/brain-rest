package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FaceRecogResponse extends InsightAppBaseResponse {
    private FaceData result;

    public FaceRecogResponse(CommonMsg message, FaceData result) {
        super(message);
        this.result = result;
    }

    public FaceData getResult() {
        return result;
    }

    public void setResult(FaceData result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "FaceRecogResponse{" +
                "result=" + result +
                "} " + super.toString();
    }
}
