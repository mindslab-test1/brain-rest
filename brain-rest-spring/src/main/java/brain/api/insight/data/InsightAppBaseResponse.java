package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InsightAppBaseResponse implements Serializable {
    private CommonMsg message;

    public InsightAppBaseResponse(CommonMsg message) {
        this.message = message;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "InsigtAppBaseResponse{" +
                "message=" + message +
                '}';
    }
}
