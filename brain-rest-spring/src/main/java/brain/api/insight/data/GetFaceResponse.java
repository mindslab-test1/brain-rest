package brain.api.insight.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetFaceResponse extends InsightAppBaseResponse {
    List<FaceData> payload;

    public GetFaceResponse(CommonMsg message, List<FaceData> payload) {
        super(message);
        this.payload = payload;
    }

    public List<FaceData> getPayload() {
        return payload;
    }

    public void setPayload(List<FaceData> payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "GetFaceResponse{" +
                "payload=" + payload +
                "} " + super.toString();
    }
}
