package brain.api.insight.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.insight.InsightCommonCode;
import brain.api.insight.client.InsightGrpcClient;
import brain.api.insight.client.RecogServiceGrpcClient;
import brain.api.insight.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Service
public class InsightService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(InsightCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    InsightGrpcClient insightClient;

    @Autowired
    RecogServiceGrpcClient recogServiceClient;

    public InsightSetResponse setFace(String apiId, String apiKey, String name, MultipartFile file, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, InsightCommonCode.SERVICE_APP_NAME, 1, request);

        insightClient.setDestination("182.162.19.10", 56101);

        return new InsightSetResponse(new CommonMsg(), insightClient.setFace(name, file));

    }

    public InsightRecogResponse recogFace(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException{
        validate(apiId, apiKey, InsightCommonCode.SERVICE_APP_NAME, 1, request);
        insightClient.setDestination("182.162.19.10", 56101);

        Map data = insightClient.recogFace(file);
        String name = "unknown";
        if ((boolean) data.get("result")) {
            name = (String) data.get("name");
        }

        try {
            logger.info("fileSave : " + faceDataSave(file, name));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return new InsightRecogResponse(new CommonMsg(), data);
    }

    private boolean faceDataSave(MultipartFile file, String name) throws IOException {
        String path = "/home/ubuntu/maum/insight";
//        String path = "/home/gom/maum/insight";

        boolean result = false;
        FileOutputStream fileOutputStream = null;
        try {
            File saveDir = new File(path);

            if (!saveDir.exists()) {
                saveDir.mkdirs();
            }

            if (name != "unknown") {
                name = "success";
            }

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String dataInfo = sdf.format(cal.getTime());

            byte[] data = file.getBytes();
            fileOutputStream = new FileOutputStream(path + "/"+ dataInfo + "_" + name + ".jpg");
            fileOutputStream.write(data);
            result = true;

        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            if(fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
        return result;
    }

    public SetFaceResponse setFaceApp(String apiId, String apiKey, String dbId, String faceId, MultipartFile file, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, InsightCommonCode.SERVICE_APP_NAME, 1, request);
        setDestination();

        logger.debug("insight");
        List<Float> faceVector = insightClient.insightFace(file).getVector();

        logger.debug("insight_app");
        return recogServiceClient.setFace(apiId, dbId, faceId, faceVector);
    }

    public GetFaceResponse getFaceList(String apiId, String apiKey, String dbId, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, InsightCommonCode.SERVICE_APP_NAME, 1, request);
        setDestination();

        return recogServiceClient.getFaceList(apiId, dbId);
    }

    public DeleteFaceResponse deleteFace(String apiId, String apiKey, String dbId, String faceId, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, InsightCommonCode.SERVICE_APP_NAME, 1, request);
        setDestination();

        return recogServiceClient.deleteFace(apiId, dbId, faceId);
    }

    public FaceRecogResponse recogFace(String apiId, String apiKey, String dbId, MultipartFile file, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, InsightCommonCode.SERVICE_APP_NAME, 1, request);
        setDestination();

        return recogServiceClient.recogFace(apiId, dbId, insightClient.insightFace(file).getVector());
    }

    private void setDestination() {
        insightClient.setDestination("182.162.19.10", 56101);
        recogServiceClient.setDestination("182.162.19.10", 56200);
    }
}
