package brain.api.insight;

public class InsightCommonCode {
    private InsightCommonCode(){}

    public static final String SERVICE_NAME = "INS";
    public static final String SERVICE_APP_NAME = "INS_APP";

    // Success
    public static final int INSIGHT_RES_SUCCESS = 0;

    // Communication related error
    public static final int INSIGHT_ERR_CONN_REFUSED = 50001;
    public static final int INSIGHT_ERR_CONN_TIMEOUT = 50002;
    public static final int INSIGHT_ERR_REQ_TIMEOUT = 50003;

    public static final int INSIGHT_ERR_AUTH_VERIFICATION_ERROR = 40001;

}
