package brain.api.insight.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.insight.InsightCommonCode;
import brain.api.insight.service.InsightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/insight")
public class InsightController {
    private static final CloudApiLogger logger = new CloudApiLogger(InsightCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    InsightService service;

    @RequestMapping(
            value = "/setFace"
            , produces = "application/json"
    )
    public ResponseEntity setFace(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam("name") String name
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        try {
            logger.info("[setFace Request] apiId : " + apiId + ", name : " + name);

            return new ResponseEntity(service.setFace(apiId, apiKey, name, file, request), HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.warn(e.toString());

            return new ResponseEntity(new ErrorResponse(e), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/recogFace"
            , produces = "application/json"
    )
    public ResponseEntity recogFace(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        try {
            logger.info("[recogFace Request] apiId : " + apiId);

            return new ResponseEntity(service.recogFace(apiId, apiKey, file, request), HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());

            return new ResponseEntity(new ErrorResponse(4000), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity(new ErrorResponse(5000), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/setFace"
            , method = RequestMethod.POST
            , consumes = MediaType.MULTIPART_FORM_DATA_VALUE
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appSetFace(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , @RequestParam("faceId") String faceId
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        try {
            logger.info("[SetFace invoked] apiId : " + apiId + ", dbId : " + dbId + ", faceId : " + faceId);
            return new ResponseEntity(service.setFaceApp(apiId, apiKey, dbId, faceId, file, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity(new ErrorResponse(5000), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/getFaceList"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appGetFaceList(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , HttpServletRequest request
    ) {
        try {
            logger.info("[GetFaceList invoked] apiId : " + apiId + ", dbId : " + dbId);
            return new ResponseEntity(service.getFaceList(apiId, apiKey, dbId, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity(new ErrorResponse(5000), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/deleteFace"
            , method = RequestMethod.DELETE
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appDeleteFace(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , @RequestParam("faceId") String faceId
            , HttpServletRequest request
    ) {
        logger.info("[DeleteFace invoked] apiId : " + apiId + ", dbId : " + dbId);
        try {
            return new ResponseEntity(service.deleteFace(apiId, apiKey, dbId, faceId, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity(new ErrorResponse(5000), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/app/recogFace"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity appRecogFace(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam(value = "dbId", defaultValue = "default", required = false) String dbId
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        try {
            logger.info("[RecogFace invoked] apiId : " + apiId + ", dbId : " + dbId);
            return new ResponseEntity(service.recogFace(apiId, apiKey, dbId, file, request), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity(new ErrorResponse(5000), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
