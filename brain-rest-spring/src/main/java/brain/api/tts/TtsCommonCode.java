package brain.api.tts;

public class TtsCommonCode {
    private TtsCommonCode(){}

    public static final String SERVICE_NAME = "TTS";

    public static final int TTS_ERR_AUTH_VERIFICATION_ERROR   = 40001;
    public static final int TTS_ERR_SERVER_NOT_FOUND_ERROR    = 40002;
    public static final int TTS_ERR_NOT_SUPPORTED_LANG_ERROR    = 40003;

    //Service related error
    public static final int TTS_ERR_SVC_ERROR = 1;
    public static final int TTS_ERR_SVC_EXCEEDED_TEXT_LENGTH = 60001;
    public static final String TTS_MSG_SVC_EXCEEDED_TEXT_LENGTH = "Requested text exceeded max. length.";
    public static final String TTS_MSG_INVALID_SPEAKER = "Invalid speaker id";
    public static final String TTS_MSG_SUCCESS = "Success";

    //TTS Service IOException
    public static final String TTS_MSG_IOException = "TTS File Exception.";
    public static final int TTS_ERR_FILE_NOT_FOUND = 40004;
    public static final String TTS_MSG_FILE_NOT_FOUND = "File does not exist.";
    public static final String TTS_FILE_DOWNLOAD_URI = "/tts/fileDownload?fileId=";
    public static final int TTS_FILE_EXPIRY_DAY = 15;
    public static final String TTS_WAV_FILE_DOWNLOAD_NAME = "MindsLab_TTS.wav";
    public static final String TTS_PCM_FILE_DOWNLOAD_NAME = "MindsLab_TTS.pcm";

    public static final String TTS_USAGE_TYPE_STREAM = "Stream";
    public static final String TTS_USAGE_TYPE_FILEDOWNLOAD = "FileDownLoad";

    public static final String TTS_CONTENT_TYPE_WAV = "audio/x-wav";
    public static final String TTS_CONTENT_TYPE_PCM = "audio/PCMA";

}
