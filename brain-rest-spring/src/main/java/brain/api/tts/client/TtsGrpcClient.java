package brain.api.tts.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.tts.TtsCommonCode;
import io.grpc.ManagedChannel;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class TtsGrpcClient extends GrpcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private final Map<String, ChannelHolder> channelMap = new HashMap<>();
    private NgTtsServiceGrpc.NgTtsServiceBlockingStub ttsBlockingStub;

    public CommonMsg setDestination(String ip, int port) {
        String channelKey = String.format("%s:%d", ip ,port);
        try {
            logger.debug(String.format("set destination %s, %d", ip, port));
            if(channelMap.containsKey(channelKey)){
                ttsBlockingStub = NgTtsServiceGrpc.newBlockingStub(channelMap.get(channelKey).channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
                channelMap.get(channelKey).lastReferencedEpoch = System.currentTimeMillis();
            }
            else {
                ManagedChannel destination = CommonUtils.getManagedChannel(ip, port);
                channelMap.put(channelKey, new ChannelHolder(
                        destination, System.currentTimeMillis()
                ));
                ttsBlockingStub = NgTtsServiceGrpc.newBlockingStub(channelMap.get(channelKey).channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            }
            return new CommonMsg();
        }catch (Exception e){
            logger.error(String.format("Destination set failure! (%s:%d)", ip, port));
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode());
        } finally {
            this.manageChannel();
        }
    }

    public List<String> setIpPool(String ip){
        List<String> ipsList = new LinkedList<>();
        String[] ips = ip.split(",");
        for (String s : ips) {
            ipsList.add(s.trim());
        }

        return ipsList;
    }

    public Iterator<TtsMediaResponse> doTts(String text, String lang, int speaker, int samplerate, List<String> ipPool, int port) throws MindsRestException {
        boolean flag = false;

        for(int retry=1;; retry++) {
            if(retry+1>=ipPool.size()) {flag =true;}

            try {
                return ttsBlockingStub.withDeadlineAfter(
                        1, TimeUnit.MINUTES
                ).speakWav(this.buildBaseRequest(text, lang, speaker, samplerate));
            }catch (Exception e){
                logger.error(e.getMessage());
                if(!flag){
                    logger.error(String.format("doTts fail, retry (%d)", retry));
                    setDestination(ipPool.get(retry), port);
                }
                else{
                    throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), "DoTts all fail after " + ipPool.size() + " retries");
                }
            }
        }
    }

    private TtsRequest buildBaseRequest(String text, String lang, int speaker, int samplerate) throws MindsRestException {
        TtsRequest.Builder builder = TtsRequest.newBuilder();
        builder.setLangValue(this.getLangVal(lang))
                .setSampleRate(samplerate)
                .setText(text)
                .setSpeaker(speaker);

        logger.debug("text : " + text + ", lang : " + lang + ", speaker : " + speaker + ", samplerate : " + samplerate);

        return builder.build();
    }

    private int getLangVal(String lang) throws MindsRestException {
        int langVal;

        switch (lang) {
            case "0" :
            case "ko_KR" :
            case "kor" :
            case "ko" : langVal = 0; break;
            case "1" :
            case "en_US" :
            case "eng" :
            case "en" : langVal = 1; break;
            default : throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "UnSupport Language : " + lang);
        }

        return langVal;
    }

    private class ChannelHolder{
        public ManagedChannel channel;
        public long lastReferencedEpoch;

        ChannelHolder(ManagedChannel channel, long lastReferencedEpoch){
            this.channel = channel;
            this.lastReferencedEpoch = lastReferencedEpoch;
        }
    }

    private void manageChannel(){
        logger.debug("manage channel");
        if(channelMap.size() > 10){
            logger.debug("maintained channel over 10");
            String channelKey = "";
            long latestReferencedTime = System.currentTimeMillis();
            for(Map.Entry<String, ChannelHolder> entry: channelMap.entrySet()){
                if(entry.getValue().lastReferencedEpoch < latestReferencedTime){
                    channelKey = entry.getKey();
                    latestReferencedTime = entry.getValue().lastReferencedEpoch;
                }
            }
            ManagedChannel removeTargetChannel = channelMap.get(channelKey).channel;
            channelMap.remove(channelKey);
            removeTargetChannel.shutdown();
            logger.debug(String.format("removed %s", channelKey));
        }
        logger.debug(String.format("channel map size %d", channelMap.size()));
    }

    @PreDestroy
    private void onDestroy(){
        for(Map.Entry<String, ChannelHolder> entry: channelMap.entrySet()){
            entry.getValue().channel.shutdown();
        }
    }

}

