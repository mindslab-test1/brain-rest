package brain.api.tts.client;

import brain.api.common.data.CommonMsg;

public interface TtsClientInterface {
    /*
    TODO
        a request contains metadata about the destination
        if destination metadata and request metadata differs,
        an Exception should be necessary
     */


    public CommonMsg setDestination(String ip, int port, int maxSpeaker);
}
