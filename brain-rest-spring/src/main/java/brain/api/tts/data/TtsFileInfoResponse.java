package brain.api.tts.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TtsFileInfoResponse implements Serializable {

    private CommonMsg message;
    private String downloadURL;
    private String expiryDate;

    public TtsFileInfoResponse() {
    }

    public TtsFileInfoResponse(CommonMsg message, String downloadURL, Date expiryDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        this.message = message;
        this.downloadURL = downloadURL;
        this.expiryDate = simpleDateFormat.format(expiryDate);
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return "TtsFileInfoResponse{" +
                "message=" + message +
                ", downloadURL='" + downloadURL + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                '}';
    }
}
