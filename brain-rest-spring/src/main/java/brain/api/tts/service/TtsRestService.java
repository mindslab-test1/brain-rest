package brain.api.tts.service;


import brain.api.common.CommonCode;
import brain.api.common.data.BaseResponse;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.common.utils.CommonUtils;
import brain.api.tts.TtsCommonCode;
import brain.api.tts.client.TtsGrpcClient;
import brain.api.tts.data.TtsFileInfoResponse;
import brain.api.tts.data.TtsResUsage;
import brain.api.tts.model.TtsReqAccessMeta;
import brain.api.tts.model.TtsReqModelMeta;
import brain.api.tts.model.TtsReqUsage;
import brain.api.tts.model.server.*;
import com.google.protobuf.ByteString;
import maum.brain.tts.TtsMediaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TtsRestService extends BaseService {

    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    private String ipPattern = "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$";
    @Autowired
    TtsModelDao modelDao;

    @Autowired
    TtsFileDao fileDao;

    @Autowired
    TtsUsageDao usageDao;

    @Autowired
    TtsModelMetaDao voicefontDao;

    @Autowired
    TtsAccessMetaDao accessDao;

    @Autowired
    TtsGrpcClient ttsClient;

    public Map <String, String> parseJson(Map<String, String> params, HttpServletRequest request) throws IOException {
        if (params.size() == 0) {
            try {
                params = CommonUtils.parsingJson(request);
            } catch (IOException e) {
                logger.error(e.getMessage());
                throw e;
            }
        }

        return params;
    }

    public void runTtsStream (Map<String, String> params, HttpServletRequest request, HttpServletResponse response) throws MindsRestException {
        boolean pcm = false;
        String contentType = TtsCommonCode.TTS_CONTENT_TYPE_WAV;
        String admin;
        try {
            admin = validate(
                    params.get("apiId"),
                    params.get("apiKey"),
                    TtsCommonCode.SERVICE_NAME,
                    params.get("text").length(),
                    request
            );
        } catch (NullPointerException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage());
        }

        if(params.containsKey("extension") && params.get("extension").equals("pcm")) {
            pcm = true;
            contentType = null;
        }

        TtsModelDto modelDto = getTtsModel(params, admin);
        TtsUsageDto usageDto = new TtsUsageDto(params.get("apiId"), params.get("voiceName"), params.get("text"), TtsCommonCode.TTS_USAGE_TYPE_STREAM);
        if (usageDao.insertTtsUsage(usageDto) != 1) {
            logger.warn("DB ERROR (TTS Usage)");
        }
        logger.debug(usageDto.getId());

        List<String> ipPool = new LinkedList<>();
        if(modelDto.getIp().split(",").length>1){ ipPool = ttsClient.setIpPool(modelDto.getIp()); }
        else{ ipPool.add(modelDto.getIp()); }

        ttsClient.setDestination(ipPool.get(0), modelDto.getPort());
        Iterator <TtsMediaResponse> iter  = ttsClient.doTts(params.get("text"), modelDto.getLang(), modelDto.getSpeaker(), modelDto.getSamplerate(), ipPool, modelDto.getPort());

        logger.info("TTS Done User : " + params.get("apiId") + " voiceName : " + params.get("voiceName"));
        writeToResponseStream(
                iter, pcm, response, usageDto.getId()
        );
    }

//    public ResponseEntity <StreamingResponseBody> runTtsStream (Map<String, String> params, HttpServletRequest request) throws MindsRestException {
//        boolean pcm = false;
//        String contentType = TtsCommonCode.TTS_CONTENT_TYPE_WAV;
//        String admin;
//        try {
//            admin = validate(
//                                params.get("apiId"),
//                                params.get("apiKey"),
//                                TtsCommonCode.SERVICE_NAME,
//                                params.get("text").length(),
//                                request
//                           );
//        } catch (NullPointerException e) {
//            logger.error(e.getMessage());
//            throw new MindsRestException(CommonCode.COMMON_ERR_BAD_REQUEST, CommonCode.COMMON_MSG_BAD_REQUEST);
//        }
//
//        if(params.containsKey("extension") && params.get("extension").equals("pcm")) {
//            pcm = true;
//            contentType = null;
//        }
//
//        TtsModelDto modelDto = getTtsModel(params, admin);
//        TtsUsageDto usageDto = new TtsUsageDto(params.get("apiId"), params.get("voiceName"), params.get("text"), TtsCommonCode.TTS_USAGE_TYPE_STREAM);
//        if (usageDao.insertTtsUsage(usageDto) != 1) {
//            logger.warn("DB ERROR (TTS Usage)");
//        }
//        logger.debug(usageDto.getId());
//
//        List<String> ipPool = new LinkedList<>();
//        if(modelDto.getIp().split(",").length>1){ ipPool = ttsClient.setIpPool(modelDto.getIp()); }
//        else{ ipPool.add(modelDto.getIp()); }
//
//        ttsClient.setDestination(ipPool.get(0), modelDto.getPort());
//        Iterator <TtsMediaResponse> iter  = ttsClient.doTts(params.get("text"), modelDto.getLang(), modelDto.getSpeaker(), modelDto.getSamplerate(), ipPool, modelDto.getPort());
//        StreamingResponseBody responseBody;
//
//        responseBody = getStreamingResBody(iter, pcm, usageDto.getId());
//        logger.info("TTS Done User : " + params.get("apiId") + " voiceName : " + params.get("voiceName"));
//
//        return ResponseEntity.ok().header("Content-Type", contentType).body(responseBody);
//    }

    public TtsFileInfoResponse runTtsDownload (Map<String, String> params, HttpServletRequest request) throws MindsRestException {
        boolean pcm = false;

        String admin = validate(params.get("apiId"),
                                params.get("apiKey"),
                                TtsCommonCode.SERVICE_NAME,
                                params.get("text").length(),
                                request
                        );

        if(params.containsKey("extension") && params.get("extension").equals("pcm")) pcm = true;

        TtsModelDto modelDto = getTtsModel(params, admin);

        List<String> ipPool = new LinkedList<>();
        if(modelDto.getIp().split(",").length>1){ ipPool = ttsClient.setIpPool(modelDto.getIp()); }
        else{ ipPool.add(modelDto.getIp()); }

        ttsClient.setDestination(ipPool.get(0), modelDto.getPort());
        Iterator <TtsMediaResponse> iter  = ttsClient.doTts(params.get("text"), modelDto.getLang(), modelDto.getSpeaker(), modelDto.getSamplerate(), ipPool, modelDto.getPort());
        String fileName;

        try {
            fileName = getFileName(iter, pcm);
            logger.debug(fileName);
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(TtsCommonCode.TTS_ERR_FILE_NOT_FOUND, TtsCommonCode.TTS_MSG_FILE_NOT_FOUND);
        }

        //DB에 입력 부분
        String voiceName;
        String ip;

        if (params.containsKey("voiceName")) {
            voiceName = params.get("voiceName");
        } else {
            voiceName = params.get("model") + ", " + params.get("speaker");
        }

        ip = CommonUtils.getIp(request);

        TtsFileInfoDto fileDto = new TtsFileInfoDto(fileName, params.get("apiId"), voiceName, ip, new Date());

        fileDao.insertFileInfo(fileDto);
        //to TtsUsageTabe DB
        TtsUsageDto usageDto = new TtsUsageDto(params.get("apiId"), voiceName, params.get("text"), TtsCommonCode.TTS_USAGE_TYPE_FILEDOWNLOAD);
        logger.debug(usageDto.toString());
        if (usageDao.insertTtsUsage(usageDto) != 1) {
            logger.warn("DB ERROR (TTS Usage)");
        }

        logger.info("TTS Done User : " + params.get("apiId") + " voiceName : " + voiceName);

        String url = CommonUtils.getDomain(request) + TtsCommonCode.TTS_FILE_DOWNLOAD_URI + fileName;

        return new TtsFileInfoResponse(new CommonMsg(), url, fileDto.getExpiryDate());
    }

    private TtsModelDto getTtsModel(Map<String, String> params, String admin) throws MindsRestException  {
        Map<String, String> ttsInfo = new HashMap <>();
        String apiId = params.get("apiId");
        String voiceName = params.get("voiceName");
        TtsModelDto modelDto = null;

        // voiceName 입력이 없는 경우
        if (!params.containsKey("voiceName")) {

            // 사용자가 Admin 권한이 없는 경우
            if (!admin.equals(CommonCode.API_CLIENT_ADMIN_MSG)) {
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage());
            }

            // 사용자가 Admin 인 경우 (voiceName 대신 model명, speaker로 tts를 실행하는 경우)
            try {
                String model = params.get("model");
                int speaker = Integer.parseInt(params.get("speaker"));
                modelDto = modelDao.getTtsModelDtoForAdmin(model);
                if (modelDto.getMaxSpeaker() < speaker) {
                    throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "Unable Speaker : Model - " + model + ", maxSpeaker - " + modelDto.getMaxSpeaker() + ", reqSpeaker - " + speaker);
                }
                logger.info("TTS Admin_API Connected");
                modelDto.setLang(params.get("lang"));
                modelDto.setSpeaker(speaker);
                voiceName = params.get("model") + ", " + params.get("speaker");
            } catch (MindsRestException e) {
                logger.error(e.getMessage());
                throw e;
            } catch (Exception e) {
                logger.warn("It does not match the parameters for admin.");
                logger.error(e.getMessage());
            }
            // 사용자의 VoiceName이 BaseLine_kor / BaseLine_eng 인 경우
//        } else if (voiceName.matches("(?i)baseLine_...")) {
//            String modelLang = voiceName.split("_")[1];
//            logger.debug(modelLang);
//            if (modelLang.equalsIgnoreCase("kor")) modelDto = new TtsModelDto().baseLine("ko_KR");
//            if (modelLang.equalsIgnoreCase("eng")) modelDto = new TtsModelDto().baseLine("en_US");
//
//            logger.debug("baseLine Model : " + modelDto);
//        } else if(voiceName.matches("(?i)kor_kids_.")) {
//
//            String modelGender = voiceName.split("_")[2];
//            logger.debug(modelGender);
//            if(modelGender.equalsIgnoreCase("m")) modelDto = new TtsModelDto().korKids(2);
//            if(modelGender.equalsIgnoreCase("f")) modelDto = new TtsModelDto().korKids(1);
//
//            logger.debug("korKids Model : " + modelDto);
//        } else if(voiceName.matches("(?i)kor_.*") || voiceName.matches("(?i)eng_.*")){
//            Map<String, String> modelInfo = new HashMap <>();
//            modelInfo.put("model", voiceName);
//            modelDto = modelDao.getPublicTtsModelDto(modelInfo);
//            logger.debug("public Model : " + modelDto);
        }


        // 사용자가 VoiceName을 입력 한 경우
        else {
            ttsInfo.put("apiId", apiId);
            ttsInfo.put("voiceName", voiceName);

            modelDto = modelDao.getTtsModelDto(ttsInfo);
            if (modelDto == null) {
                Map<String, String> modelInfo = new HashMap<>();
                modelInfo.put("model", voiceName);
                modelDto = modelDao.getOpenModel(modelInfo);
                logger.debug("open model found : " + modelDto);
            }
        }

        // 모든 상황에서 modelDto가 잡히지 않은 경우 (error)
        if (modelDto == null) {
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "Not Allowed User : " + apiId + " voiceName : " + voiceName
            );
        }

        return modelDto;
    }

    private void writeToResponseStream(Iterator<TtsMediaResponse> iter, boolean pcm, HttpServletResponse response, int usageId){
        List<Integer> responseByteLengthList = new ArrayList<>();
        String contentType = pcm ? null : TtsCommonCode.TTS_CONTENT_TYPE_WAV;
        TtsResultDto dto = new TtsResultDto();
        dto.setStatus(200);
        dto.setTtsLogId(usageId);
        dto.setExceptionTrace("");
        WritableByteChannel och;
        try {
            och = Channels.newChannel(response.getOutputStream());
            boolean flag = pcm;
            try {
                TtsMediaResponse ttsResp = null;
                while (iter.hasNext()) {
                    ttsResp = iter.next();
                    ByteString data = ttsResp.getMediaData();
                    ByteBuffer buff;
                    if(flag){
                        buff = null;
                        byte[] tempArr = data.toByteArray();
                        byte[] tempArrSliced = Arrays.copyOfRange(tempArr, 44, tempArr.length);
                        buff = ByteBuffer.wrap(tempArrSliced);
                        flag = false;
                    }
                    else{
                        buff = data.asReadOnlyByteBuffer();
                    }
                    responseByteLengthList.add(buff.capacity());
                    och.write(buff);
                }
            } catch (Exception e){
                logger.error(e.getMessage());
                response.setStatus(500);
                dto.setExceptionTrace(e.getMessage());
                dto.setStatus(500);
            } finally {
                och.close();
                int byteLengthSum = 0;
                for(Integer byteLength: responseByteLengthList){
                    byteLengthSum += byteLength;
                }
                logger.debug(String.format("result sum: %d", byteLengthSum));
                dto.setResponseSize(byteLengthSum);
                usageDao.insertTtsResult(dto);
                response.setHeader("Content-Type", contentType);
                response.getOutputStream().close();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            dto.setExceptionTrace(e.getMessage());
            dto.setResponseSize(0);
            dto.setStatus(500);
            usageDao.insertTtsResult(dto);
            response.setStatus(500);
        }
    }

    private StreamingResponseBody getStreamingResBody(Iterator <TtsMediaResponse> iter, boolean pcm, int usageId) {
        List<Integer> responseByteLengthList = new ArrayList<>();

        return out -> {
            WritableByteChannel och = Channels.newChannel(out);
            boolean flag = pcm;
            try {
                TtsMediaResponse ttsResp = null;
                while (iter.hasNext()) {
                    ttsResp = iter.next();
                    ByteString data = ttsResp.getMediaData();
                    ByteBuffer buff;
                    if(flag){
                        buff = null;
                        byte[] tempArr = data.toByteArray();
                        byte[] tempArrSliced = Arrays.copyOfRange(tempArr, 44, tempArr.length);
                        buff = ByteBuffer.wrap(tempArrSliced);
                        flag = false;
                    }
                    else{
                        buff = data.asReadOnlyByteBuffer();
                    }
                    responseByteLengthList.add(buff.capacity());
                    och.write(buff);
                }
            }finally{
                och.close();
                out.close();
                int byteLengthSum = 0;
                for(Integer byteLength: responseByteLengthList){
                    byteLengthSum += byteLength;
                }
                logger.debug(String.format("result sum: %d", byteLengthSum));
                TtsResultDto dto = new TtsResultDto();
                dto.setTtsLogId(usageId);
                dto.setExceptionTrace("");
                dto.setResponseSize(byteLengthSum);
                usageDao.insertTtsResult(dto);
            }
        };
    }

    private String getFileName(Iterator <TtsMediaResponse> iter, boolean pcm) throws IOException, MindsRestException {
        boolean flag = pcm;

        String suffix = CommonCode.WAV_SUFFIX;
        if (flag) suffix = CommonCode.PCM_SUFFIX;

        // 경로가 없으면 해당 경로 생성
        File pathHome = new File(CommonCode.DOWNLOAD_PATH);
        if (!pathHome.exists()) {
            pathHome.mkdirs();
        }

        String fileName = UUID.randomUUID().toString();

        String filePath = CommonCode.DOWNLOAD_PATH + fileName + suffix;

        logger.debug(filePath);

        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            TtsMediaResponse ttsResp = null;
            while (iter.hasNext()) {
                ttsResp = iter.next();
                ByteString data = ttsResp.getMediaData();
                byte[] tempArr = data.toByteArray();
                if(flag){
                    tempArr = Arrays.copyOfRange(tempArr, 44, tempArr.length);
                    flag = false;
                }
                fileOutputStream.write(tempArr);
            }
        }catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(TtsCommonCode.TTS_ERR_SVC_ERROR, "Can not get filename");
        }
        return fileName;
    }

    public void fileDownload(HttpServletRequest request, HttpServletResponse response, String fileId) throws MindsRestException{
        File file = new File(CommonCode.DOWNLOAD_PATH + fileId + CommonCode.WAV_SUFFIX);
        String fileName = TtsCommonCode.TTS_WAV_FILE_DOWNLOAD_NAME;

        if (!file.exists()) {
            file = new File(CommonCode.DOWNLOAD_PATH + fileId + CommonCode.PCM_SUFFIX);
            fileName = TtsCommonCode.TTS_PCM_FILE_DOWNLOAD_NAME;
        }

        TtsFileDownloadDto downloadDto = new TtsFileDownloadDto(fileId, CommonUtils.getIp(request), new Date());

        if (!file.exists()) {
            logger.warn("[TTS FileDownload] : File is not exists " + fileId);
            downloadDto.setStatus(-1);
            insertDownloadHistory(downloadDto);
            throw new MindsRestException(TtsCommonCode.TTS_ERR_FILE_NOT_FOUND, TtsCommonCode.TTS_MSG_FILE_NOT_FOUND);
        }

        FileInputStream fileInputStream = null;
        ServletOutputStream servletOutputStream = null;

        try {
            String downName = null;
            String browser = request.getHeader("User-Agent");
            if(browser.contains("MSIE") || browser.contains("Trident") || browser.contains("Chrome")) {
                downName = URLEncoder.encode(fileName, "UTF-8").replace("\\+", "%20");
            } else {
                downName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
            }

            response.setHeader("Content-Disposition","attachment;filename=\"" + downName+"\"");
            response.setContentType("application/octer-stream");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            fileInputStream = new FileInputStream(file);
            servletOutputStream = response.getOutputStream();

            byte[] byteArray = new byte[1024];
            int data = 0;

            while ((data=(fileInputStream.read(byteArray, 0, byteArray.length))) != -1) {
                servletOutputStream.write(byteArray, 0, data);
            }

            servletOutputStream.flush();
            logger.info("[TTS FileDownload Done] : " + fileId);
            downloadDto.setStatus(0);
            insertDownloadHistory(downloadDto);
        } catch (IOException e) {

            downloadDto.setStatus(-99);
            insertDownloadHistory(downloadDto);
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), TtsCommonCode.TTS_MSG_IOException);
        }finally{

            if(servletOutputStream!=null){
                try{
                    servletOutputStream.close();
                } catch (IOException e){
                    logger.error(e.getMessage());
                }
            }

            if(fileInputStream!=null){
                try{
                    fileInputStream.close();
                } catch (IOException e){
                    logger.error(e.getMessage());
                }
            }
        }
    }

    int insertDownloadHistory(TtsFileDownloadDto downloadDto) {
        return fileDao.insertFileDownload(downloadDto);
    }

    public TtsResUsage ttsUsage(TtsReqUsage params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);
        TtsUsageSelectDto usageDto;
        if(params.getEndDate() != null)
        {
            usageDto = new TtsUsageSelectDto(params.getStartDate(), params.getEndDate());
        } else {
            usageDto = new TtsUsageSelectDto(params.getStartDate());
        }

        List<TtsUsageDto> response = usageDao.selectByDate(usageDto);

        return new TtsResUsage( response.size(), response);
    }

    public BaseResponse setVoicefont(TtsReqModelMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        boolean ipValidate = CommonUtils.validateRegex(ipPattern, params.getIp());
        if(!ipValidate) { return new BaseResponse(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode())); }

        TtsModelMetaDto voicefontDto = new TtsModelMetaDto(params.getLang(), params.getIp(), params.getModel(), params.getPort(), params.getSampleRate(), params.getMaxSpeaker());
        int insertedRow = voicefontDao.insertTtsVoicefont(voicefontDto);

        String response = insertedRow + " row is Inserted";

        return new BaseResponse(response);
    }

    public BaseResponse getVoicefont(TtsReqModelMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsModelMetaDto voicefontDto = new TtsModelMetaDto(params.getModel());
        List<TtsModelMetaDto> response = voicefontDao.readTtsVoicefont(voicefontDto);

        return new BaseResponse(response);
    }

    public BaseResponse updateVoicefont(TtsReqModelMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsModelMetaDto voicefontDto = new TtsModelMetaDto(params.getModel());
        if(params.getLang() != null) { voicefontDto.setLang(params.getLang()); }
        if(params.getIp() != null) {

            boolean ipValidate = CommonUtils.validateRegex(ipPattern, params.getIp());
            if(!ipValidate) { return new BaseResponse(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode())); }

            voicefontDto.setIp(params.getIp());
        }
        if(params.getPort() != null) { voicefontDto.setPort(params.getPort()); }
        if(params.getSampleRate() != null) { voicefontDto.setSampleRate(params.getSampleRate()); }
        if(params.getMaxSpeaker() != null) { voicefontDto.setMaxSpeaker(params.getMaxSpeaker()); }
        int updatedRow = voicefontDao.updateTtsVoicefont(voicefontDto);

        String response = updatedRow + " row is updated";

        return new BaseResponse(response);
    }

    public BaseResponse deleteVoicefont(TtsReqModelMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsModelMetaDto voicefontDto = new TtsModelMetaDto(params.getModel());
        int deletedRow = voicefontDao.deleteTtsVoicefont(voicefontDto);

        String response = deletedRow + " row is deleted";

        return new BaseResponse(response);
    }

    public BaseResponse setAccess(TtsReqAccessMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsAccessMetaDto accessDto = new TtsAccessMetaDto(params.getApiId(), params.getModel(), params.getSpeaker(), params.getVoiceName());
        int insertedRow = accessDao.insertTtsAccess(accessDto);

        String response = insertedRow + " row is Inserted";

        return new BaseResponse(response);
    }

    public BaseResponse getAccess(TtsReqAccessMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsAccessMetaDto accessDto = new TtsAccessMetaDto();
        if(params.getVoiceName() != null){ accessDto.setVoiceName(params.getVoiceName()); }
        else if(params.getSpeaker() != null){ accessDto.setSpeaker(params.getSpeaker()); }
        else if(params.getModel() != null) { accessDto.setTtsModel(params.getModel()); }
        else { accessDto.setApiId(params.getApiId());}
        List<TtsAccessMetaDto> response = accessDao.readTtsAccess(accessDto);

        return new BaseResponse(response);
    }

    public BaseResponse updateAccess(TtsReqAccessMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsAccessMetaDto accessDto = new TtsAccessMetaDto(params.getApiId(), params.getVoiceName());
        if(params.getModel() != null) { accessDto.setTtsModel(params.getModel());}
        else if(params.getSpeaker() != null) { accessDto.setSpeaker(params.getSpeaker());}
        int updatedRow = accessDao.updateTtsAccess(accessDto);

        String response = updatedRow + " row is updated";

        return new BaseResponse(response);
    }

    public BaseResponse deleteAccess(TtsReqAccessMeta params, HttpServletRequest request) throws MindsRestException {
        validate(params.getApiId(), params.getApiKey(), TtsCommonCode.SERVICE_NAME, 1, request);

        TtsAccessMetaDto accessDto = new TtsAccessMetaDto(params.getApiId(), params.getVoiceName());
        int deletedRow = accessDao.deleteTtsAccess(accessDto);

        String response = deletedRow + " row is deleted";

        return new BaseResponse(response);
    }

    @Scheduled(cron = "1 0 0 * * ?")
    public void ttsFileRemover() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(new Date());
        logger.info("[TTS FileRemover Start] : " + date);
        try {
            List<String> removeFileList = fileDao.selectRemoveFileList(date);
            logger.info(removeFileList.toString());
            for (String fileId : removeFileList) {
                String fileFullQuilityPath = CommonCode.DOWNLOAD_PATH + fileId + CommonCode.WAV_SUFFIX;
                File file = new File(fileFullQuilityPath);

                if (!file.exists()) {
                    fileFullQuilityPath = CommonCode.DOWNLOAD_PATH + fileId + CommonCode.PCM_SUFFIX;
                    file = new File(fileFullQuilityPath);
                }

                if (file.exists()) {
                    file.delete();
                    fileDao.updateDeletedFileStatus(fileId);
                } else {
                    logger.warn("[TTS FileRemover File not exits] : " + fileId);
                }
            }

        logger.info("[TTS FileRemover done] : " + removeFileList.size());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
