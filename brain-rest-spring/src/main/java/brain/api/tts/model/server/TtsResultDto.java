package brain.api.tts.model.server;

public class TtsResultDto {
    private int id;
    private int ttsLogId;
    private String exceptionTrace;
    private int responseSize;
    private int status;

    public TtsResultDto() {
    }

    public TtsResultDto(int id, int ttsLogId, String exceptionTrace, int responseSize, int status) {
        this.id = id;
        this.ttsLogId = ttsLogId;
        this.exceptionTrace = exceptionTrace;
        this.responseSize = responseSize;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTtsLogId() {
        return ttsLogId;
    }

    public void setTtsLogId(int ttsLogId) {
        this.ttsLogId = ttsLogId;
    }

    public String getExceptionTrace() {
        return exceptionTrace;
    }

    public void setExceptionTrace(String exceptionTrace) {
        this.exceptionTrace = exceptionTrace;
    }

    public int getResponseSize() {
        return responseSize;
    }

    public void setResponseSize(int responseSize) {
        this.responseSize = responseSize;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TtsResultDto{" +
                "id=" + id +
                ", ttsLogId=" + ttsLogId +
                ", exceptionTrace='" + exceptionTrace + '\'' +
                ", responseSize=" + responseSize +
                ", status=" + status +
                '}';
    }
}
