package brain.api.tts.model;

public class TtsReqModelMeta {
    private String apiId;
    private String apiKey;
    private String lang;
    private String ip;
    private String model;
    private Integer port;
    private Integer sampleRate;
    private Integer maxSpeaker;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getLang() {
        return lang;
    }
    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public Integer getPort() {
        return port;
    }
    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getSampleRate() {
        return sampleRate;
    }
    public void setSampleRate(Integer sampleRate) {
        this.sampleRate = sampleRate;
    }

    public Integer getMaxSpeaker() {
        return maxSpeaker;
    }
    public void setMaxSpeaker(Integer maxSpeaker) {
        this.maxSpeaker = maxSpeaker;
    }

    @Override
    public String toString() {
        return "TtsReqVoicefont{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", lang='" + lang + '\'' +
                ", ip='" + ip + '\'' +
                ", model='" + model + '\'' +
                ", port='" + port + '\'' +
                ", sampleRate='" + sampleRate + '\'' +
                ", maxSpeaker='" + maxSpeaker + '\'' +
                '}';
    }
}
