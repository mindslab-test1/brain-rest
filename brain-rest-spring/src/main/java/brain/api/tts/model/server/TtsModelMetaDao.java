package brain.api.tts.model.server;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.TtsModelMetaMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TtsModelMetaDao {
    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public int insertTtsVoicefont(TtsModelMetaDto modelMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsModelMetaMapper mapper = session.getMapper(TtsModelMetaMapper.class);
            return mapper.insertTtsVoicefont(modelMetaDto);
        }
    }

    public List<TtsModelMetaDto> readTtsVoicefont(TtsModelMetaDto modelMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsModelMetaMapper mapper = session.getMapper(TtsModelMetaMapper.class);
            return mapper.readTtsVoicefont(modelMetaDto);
        }
    }

    public int updateTtsVoicefont(TtsModelMetaDto modelMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsModelMetaMapper mapper = session.getMapper(TtsModelMetaMapper.class);
            return mapper.updateTtsVoicefont(modelMetaDto);
        }
    }

    public int deleteTtsVoicefont(TtsModelMetaDto modelMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsModelMetaMapper mapper = session.getMapper(TtsModelMetaMapper.class);
            return mapper.deleteTtsVoicefont(modelMetaDto);
        }
    }
}
