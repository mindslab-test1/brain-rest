package brain.api.tts.model;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;


public class TtsReqUsage {
    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);
    private String apiId;
    private String apiKey;
    private String startDate;
    private String endDate;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate){
        this.endDate = endDate + " 23:59:59";
    }


    @Override
    public String toString() {
        return "TtsReqUsage{" +
                "apiId=" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
