package brain.api.tts.model;

public class TtsReqAccessMeta {
    private String apiId;
    private String apiKey;
    private String model;
    private Integer speaker;
    private String voiceName;

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public Integer getSpeaker() {
        return speaker;
    }
    public void setSpeaker(Integer speaker) {
        this.speaker = speaker;
    }

    public String getVoiceName() {
        return voiceName;
    }
    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    @Override
    public String toString() {
        return "TtsReqAccess{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", model='" + model + '\'' +
                ", speaker='" + speaker + '\'' +
                ", voiceName='" + voiceName + '\'' +
                '}';
    }
}
