package brain.api.tts.model.server;

public class TtsAccessMetaDto {
    private String apiId;
    private String ttsModel;
    private String CreateDate;
    private Integer speaker;
    private String voiceName;

    public TtsAccessMetaDto() {}

    public TtsAccessMetaDto(String apiId, String voiceName) {
        this.apiId = apiId;
        this.voiceName = voiceName;
    }

    public TtsAccessMetaDto(String apiId, String ttsModel, Integer speaker, String voiceName) {
        this.apiId = apiId;
        this.ttsModel = ttsModel;
        this.speaker = speaker;
        this.voiceName = voiceName;
    }

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getTtsModel() {
        return ttsModel;
    }
    public void setTtsModel(String ttsModel) {
        this.ttsModel = ttsModel;
    }

    public String getCreateDate() {
        return CreateDate;
    }
    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public Integer getSpeaker() {
        return speaker;
    }
    public void setSpeaker(Integer speaker) {
        this.speaker = speaker;
    }

    public String getVoiceName() {
        return voiceName;
    }
    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    @Override
    public String toString() {
        return "TtsAccessDto{" +
                "apiId='" + apiId + '\'' +
                ", ttsModel='" + ttsModel + '\'' +
                ", CreateDate='" + CreateDate + '\'' +
                ", speaker=" + speaker +
                ", voiceName='" + voiceName + '\'' +
                '}';
    }
}
