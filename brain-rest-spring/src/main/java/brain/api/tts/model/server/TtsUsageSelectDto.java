package brain.api.tts.model.server;

public class TtsUsageSelectDto {
    private String startDate;
    private String endDate;

    public TtsUsageSelectDto(String startDate) {
        this.startDate = startDate;
        this.endDate = startDate + " 23:59:59";
    }

    public TtsUsageSelectDto(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "TtsUsageSelectDto{" +
                "startDate=" + startDate +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
