package brain.api.tts.model.server;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.TtsFileMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TtsFileDao {
    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public int insertFileInfo(TtsFileInfoDto fileDto) {
        try (SqlSession session = sessionFactory.openSession(true)) {
            TtsFileMapper mapper = session.getMapper(TtsFileMapper.class);
            return mapper.insertFileInfo(fileDto);
        }
    }

    public int insertFileDownload(TtsFileDownloadDto downloadDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsFileMapper mapper = session.getMapper(TtsFileMapper.class);
            return mapper.insertFileDownload(downloadDto);
        }
    }

    public List<String> selectRemoveFileList(String date) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsFileMapper mapper = session.getMapper(TtsFileMapper.class);
            return mapper.selectRemoveFileList(date);
        }
    }

    public int updateDeletedFileStatus(String fileId) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsFileMapper mapper = session.getMapper(TtsFileMapper.class);
            return mapper.updateDeletedFileStatus(fileId);
        }
    }
}
