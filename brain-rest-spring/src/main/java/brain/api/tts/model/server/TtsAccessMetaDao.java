package brain.api.tts.model.server;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.TtsAccessMetaMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TtsAccessMetaDao {
    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public int insertTtsAccess(TtsAccessMetaDto accessMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsAccessMetaMapper mapper = session.getMapper(TtsAccessMetaMapper.class);
            return mapper.insertTtsAccess(accessMetaDto);
        }
    }

    public List<TtsAccessMetaDto> readTtsAccess(TtsAccessMetaDto accessMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsAccessMetaMapper mapper = session.getMapper(TtsAccessMetaMapper.class);
            return mapper.readTtsAccess(accessMetaDto);
        }
    }

    public int updateTtsAccess(TtsAccessMetaDto accessMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsAccessMetaMapper mapper = session.getMapper(TtsAccessMetaMapper.class);
            return mapper.updateTtsAccess(accessMetaDto);
        }
    }

    public int deleteTtsAccess(TtsAccessMetaDto accessMetaDto) {
        try (SqlSession session = sessionFactory.openSession(true)){
            TtsAccessMetaMapper mapper = session.getMapper(TtsAccessMetaMapper.class);
            return mapper.deleteTtsAccess(accessMetaDto);
        }
    }
}
