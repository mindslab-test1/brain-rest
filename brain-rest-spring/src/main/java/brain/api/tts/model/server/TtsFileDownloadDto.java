package brain.api.tts.model.server;

import java.util.Date;

public class TtsFileDownloadDto {
    private int id;
    private String fileId;
    private String ip;
    private Date downloadDate;
    private int status;

    public TtsFileDownloadDto() {
        // Nothing to Do
    }

    public TtsFileDownloadDto(String fileId, String ip, Date downloadDate) {
        this.fileId = fileId;
        this.ip = ip;
        this.downloadDate = downloadDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(Date downloadDate) {
        this.downloadDate = downloadDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TtsFileDownloadDto{" +
                "id=" + id +
                ", fileId='" + fileId + '\'' +
                ", ip='" + ip + '\'' +
                ", downloadDate=" + downloadDate +
                ", status=" + status +
                '}';
    }
}
