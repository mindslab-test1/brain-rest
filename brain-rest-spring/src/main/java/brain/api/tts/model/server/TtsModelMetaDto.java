package brain.api.tts.model.server;

public class TtsModelMetaDto {
    private String lang;
    private String ip;
    private String model;
    private Integer port;
    private Integer sampleRate;
    private Integer maxSpeaker;

    public TtsModelMetaDto(String model) {
        this.model = model;
    }

    public TtsModelMetaDto(String lang, String ip, String model, Integer port, Integer sampleRate, Integer maxSpeaker) {
        this.lang = lang;
        this.ip = ip;
        this.model = model;
        this.port = port;
        this.sampleRate = sampleRate;
        this.maxSpeaker = maxSpeaker;
    }

    public String getLang() {
        return lang;
    }
    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public Integer getPort() {
        return port;
    }
    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getSampleRate() {
        return sampleRate;
    }
    public void setSampleRate(Integer sampleRate) {
        this.sampleRate = sampleRate;
    }

    public Integer getMaxSpeaker() {
        return maxSpeaker;
    }
    public void setMaxSpeaker(Integer maxSpeaker) {
        this.maxSpeaker = maxSpeaker;
    }

    @Override
    public String toString() {
        return "TtsVoicefontDto{" +
                "lang='" + lang + '\'' +
                ", ip='" + ip + '\'' +
                ", model='" + model + '\'' +
                ", port='" + port + '\'' +
                ", sampleRate='" + sampleRate + '\'' +
                ", maxSpeaker='" + maxSpeaker + '\'' +
                '}';
    }
}
