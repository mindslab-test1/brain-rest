package brain.api.tts.model.server;

import java.sql.Timestamp;

public class TtsUsageDto {
    private int id;
    private String apiId;
    private String voiceName;
    private String text;
    private String date;
    private String requestType;

    public TtsUsageDto() {
    }

    public TtsUsageDto(String apiId, String voiceName, String text, String requestType) {
        this.apiId = apiId;
        this.voiceName = voiceName;
        this.text = text;
        this.requestType = requestType;
    }

    public TtsUsageDto(int id, String apiId, String voiceName, String text, String date, String requestType) {
        this.id = id;
        this.apiId = apiId;
        this.voiceName = voiceName;
        this.text = text;
        this.date = date;
        this.requestType = requestType;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getVoiceName() {
        return voiceName;
    }
    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getRequestType() {
        return requestType;
    }
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Override
    public String toString() {
        return "TtsUsageDto{" +
                "id=" + id +
                ", apiId='" + apiId + '\'' +
                ", voiceName='" + voiceName + '\'' +
                ", text='" + text + '\'' +
                ", date='" + date + '\'' +
                ", requestType='" + requestType + '\'' +
                '}';
    }
}
