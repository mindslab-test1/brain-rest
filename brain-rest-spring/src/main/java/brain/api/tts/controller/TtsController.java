package brain.api.tts.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.tts.TtsCommonCode;
import brain.api.tts.data.TtsFileInfoResponse;
import brain.api.tts.data.TtsResUsage;
import brain.api.tts.model.TtsReqUsage;
import brain.api.tts.service.TtsRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping("/tts")
public class TtsController {

    private static final CloudApiLogger logger = new CloudApiLogger(TtsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    TtsRestService service;

    @RequestMapping(
            value = "/stream", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity ttsStream(
            @RequestParam Map <String, String> params,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            params = service.parseJson(params, request);
            logger.info("[TTS Stream] : " + params);
            service.runTtsStream(params, request, response);
            return ResponseEntity.ok().build();
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @RequestMapping(
            value = "/makeFile"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity ttsMakeFile(
            @RequestParam Map <String, String> params,
            HttpServletRequest request
    ) {
        TtsFileInfoResponse responseBody = null;
        try {
            params = service.parseJson(params, request);
            logger.info("[TTS MakeFile] : " + params);
            responseBody = service.runTtsDownload(params, request);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/fileDownload"
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody ResponseEntity fileDownload(
            HttpServletRequest request
            , HttpServletResponse response
            , @RequestParam("fileId")String fileId) {
        try {
            logger.info("[TTS FileDownload] : " + fileId);
            service.fileDownload(request, response, fileId);
            return null;
        } catch(MindsRestException e) {
            logger.error(e.getMessage());
            if (e.getErrCode() == CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode()) {
                return new ResponseEntity(new ErrorResponse(e), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/ttsUsage"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody TtsResUsage ttsUsage(
            @RequestBody TtsReqUsage params,
            HttpServletRequest httpRequest
    ){
        try {
            return service.ttsUsage(params, httpRequest);
        } catch (MindsRestException e){
            logger.error(e.getMessage());
            return new TtsResUsage(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }
}
