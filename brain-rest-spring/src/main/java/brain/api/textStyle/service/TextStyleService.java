package brain.api.textStyle.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.textStyle.TextStyleCommonCode;
import brain.api.textStyle.client.TextStyleClient;
import brain.api.textStyle.dao.TextStyleTargetTextDao;
import brain.api.textStyle.data.TextStyleTransferDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequiredArgsConstructor
@Service
public class TextStyleService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(TextStyleCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);
    private final TextStyleClient client;
    private final TextStyleTargetTextDao dao;

    private void validateParameter(String originalText, int numReturnSequences, String targetStyle) throws MindsRestException {

        if (originalText.length() > 80) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "The length of originalText must be less than 80");
        }

        if (numReturnSequences > 5 || numReturnSequences < 1) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "numReturnSequence value is 1 to 5");
        }

        List<String> targetStyleTextList = dao.getActiveTargetList();
        if (!(targetStyleTextList.stream().anyMatch(targetText -> targetText.equalsIgnoreCase(targetStyle)))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "request targetStyle not included in " + targetStyleTextList);
        }
    }

    public CommonResponse<List<String>> doTextStyleTransfer(TextStyleTransferDTO dto, HttpServletRequest request) throws MindsRestException
    {
        logger.info("TextStyle doTextStyleTransfer Request [apiId = " + dto.getApiId() + "]");
        validate(dto.getApiId(), dto.getApiKey(), TextStyleCommonCode.SERVICE_NAME, dto.getOriginalText().length(), request);
        logger.debug("usage validate for TextStyle-doTextStyleTransfer");
        validateParameter(dto.getOriginalText(), dto.getNumReturnSequences(), dto.getTargetStyle());
        return new CommonResponse<>(new CommonMsg(), client.doTransfer(dto.getOriginalText(), dto.getNumReturnSequences(), dto.getTargetStyle()));
    }

}

