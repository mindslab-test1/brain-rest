package brain.api.textStyle.dao;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.textStyle.TextStyleCommonCode;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.TextStyleTargetTextMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TextStyleTargetTextDao {
    private static final CloudApiLogger logger = new CloudApiLogger(TextStyleCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public List<String> getActiveTargetList() {
        try (SqlSession session = sessionFactory.openSession(true)){
            TextStyleTargetTextMapper mapper = session.getMapper(TextStyleTargetTextMapper.class);
            return mapper.getActiveTargetTextList();
        }
    }
}
