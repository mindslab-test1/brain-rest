package brain.api.textStyle.data;


import brain.api.common.data.CommonMsg;
import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class TextStyleTransferResponse implements Serializable {

    private String resultText;
    private CommonMsg message;

    public TextStyleTransferResponse(String text) {
        this.resultText = text;
    }

    public TextStyleTransferResponse(CommonMsg message){
        this.message = message;
    }
}
