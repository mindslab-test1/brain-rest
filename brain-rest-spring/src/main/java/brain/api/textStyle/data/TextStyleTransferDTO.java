package brain.api.textStyle.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TextStyleTransferDTO {
    private String apiId;
    private String apiKey;
    private String originalText;
    private int numReturnSequences;
    private String targetStyle;
}
