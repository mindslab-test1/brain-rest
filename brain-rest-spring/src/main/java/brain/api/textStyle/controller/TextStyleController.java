package brain.api.textStyle.controller;


import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.textStyle.TextStyleCommonCode;
import brain.api.textStyle.data.TextStyleTransferDTO;
import brain.api.textStyle.service.TextStyleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/text-style")
public class TextStyleController {
    private static final CloudApiLogger logger = new CloudApiLogger(TextStyleCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);
    private final TextStyleService service;

    @PostMapping(value = "/transfer", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> textStyleTransfer(@RequestBody TextStyleTransferDTO textStyleTransferDTO, HttpServletRequest request){
        try {
            return ResponseEntity.ok().body(service.doTextStyleTransfer(textStyleTransferDTO, request));
        } catch (MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }
}
