
package brain.api.textStyle.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.textStyle.TextStyleCommonCode;
import com.google.protobuf.ProtocolStringList;
import io.grpc.ManagedChannel;
import maum.brain.text_style_transfer.TextStyleTransferGrpc;
import maum.brain.text_style_transfer.TextStyleTransferOuterClass;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TextStyleClient extends GrpcClientInterface {
    public static final CloudApiLogger logger = new CloudApiLogger(TextStyleCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    public List<String> doTransfer(String originalText, int numReturnSequence, String targetStyle) throws MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(TextStyleCommonCode.IP, TextStyleCommonCode.PORT);
        TextStyleTransferGrpc.TextStyleTransferBlockingStub stub = TextStyleTransferGrpc.newBlockingStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        try {
            TextStyleTransferOuterClass.TextStyleTransferResponse textStyleTransferResponse = stub.generateStylizedText(TextStyleTransferOuterClass.
                    TextStyleTransferRequest.newBuilder()
                    .setOriginalText(originalText)
                    .setNumReturnSequences(numReturnSequence)
                    .setTargetStyle(targetStyle)
                    .build());

            ProtocolStringList stylizedTextsList = textStyleTransferResponse.getStylizedTextsList();
            return stylizedTextsList;
        } catch(io.grpc.StatusRuntimeException e) {
            throw new MindsRestException(CommonExceptionCode.COMMON_TEXT_STYLE_CUSTOM_ERROR_CODE.getErrCode(), e.getLocalizedMessage());
        }

    }
}



