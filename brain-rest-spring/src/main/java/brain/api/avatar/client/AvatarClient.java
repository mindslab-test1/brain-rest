package brain.api.avatar.client;

import brain.api.avatar.AvatarCommonCode;
import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import maum.brain.avatar.Avatar;
import maum.brain.avatar.FileServerGrpc;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class AvatarClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(AvatarCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    private FileServerGrpc.FileServerBlockingStub fileServerBlockingStub;
    private FileServerGrpc.FileServerStub fileServerStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.fileServerBlockingStub = FileServerGrpc.newBlockingStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            this.fileServerStub = FileServerGrpc.newStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public String upload(MultipartFile file){
        final CountDownLatch finishLatch = new CountDownLatch(1);
        List<String> result = new ArrayList<>();
        // RESPONSE
        StreamObserver<Avatar.Request> responseObserver = new StreamObserver<Avatar.Request>() {
            @Override
            public void onNext(Avatar.Request key) {
                result.add(key.getName());
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Error: Response onError from Avatar upload");
                logger.error(t.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Response from Avatar upload");
                finishLatch.countDown();
            }
        };
        // STREAM REQUEST
        StreamObserver<Avatar.Chunk> requestObserver = fileServerStub.upload(responseObserver);
        try{
            Avatar.Chunk.Builder avatarBuilder = Avatar.Chunk.newBuilder();
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            int len = 0;
            byte[] buffer = new byte[1024 * 512];
            while((len = bis.read(buffer)) > 0){
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(avatarBuilder.setBuffer(inputByteString).build());
            }
        }catch (Exception e){
            logger.info("Error: Stream request from Avatar upload");
            requestObserver.onError(e);
        }
        requestObserver.onCompleted();
        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.info("Error: FinishLatch from Avatar upload");
            logger.error(e.getLocalizedMessage());
        }
        return result.get(0);
    }
    public String processing(List<String> processList){
        final CountDownLatch finishLatch = new CountDownLatch(1);
        List<String> result = new ArrayList<>();

        // RESPONSE
        StreamObserver<Avatar.Request> responseObserver = new StreamObserver<Avatar.Request>() {
            @Override
            public void onNext(Avatar.Request key) {
                // Process ID
                result.add(key.getName());
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Error: Response onError from Avatar processing");
                logger.error(t.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Response from Avatar processing");
                finishLatch.countDown();
            }
        };
        // STREAM REQUEST
        StreamObserver<Avatar.Request> requestObserver = fileServerStub.processing(responseObserver);
        try {
            Avatar.Request.Builder avatarBuilder = Avatar.Request.newBuilder();
            for(String key: processList){
                requestObserver.onNext(avatarBuilder.setName(key).build());
            }

        }catch (Exception e){
            logger.info("Error: Stream request from Avatar processing");
            requestObserver.onError(e);
        }

        requestObserver.onCompleted();
        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.info("Error: FinishLatch from Avatar processing");
            logger.error(e.getLocalizedMessage());
        }

        return result.get(0);
    }
    public Integer checkStatus(String processId){
        // REQUEST
        Avatar.Request request = Avatar.Request.newBuilder().setName(processId).build();
        // RESPONSE
        Avatar.StatusMsg respone = null;
        try{
            respone = fileServerBlockingStub.checkStatus(request);
        }catch (Exception e){
            logger.info("Error: Stream request from Avatar checkStatus");
            logger.error(e.getMessage());
        }

        return respone.getStatusCode();
    }
    public ByteArrayOutputStream downloadAvatar(String key){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // REQUEST
        Avatar.Request request = Avatar.Request.newBuilder().setName(key).build();
        // STREAM RESPONSE
        Iterator<Avatar.Chunk> responseStream = fileServerBlockingStub.downloadWithKey(request);
        try{
            for(int i  = 1; responseStream.hasNext(); i++){
                Avatar.Chunk response =responseStream.next();
                outputStream.write(response.getBuffer().toByteArray());
            }
        }catch (Exception e){
            logger.info("Error: Stream response from Avatar downloadAvatar");
            logger.error(e.getMessage());
            throw new InternalException(e.getMessage());
        }
        return outputStream;
    }
}
