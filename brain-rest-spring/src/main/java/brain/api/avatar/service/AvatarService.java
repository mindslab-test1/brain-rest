package brain.api.avatar.service;

import brain.api.avatar.AvatarCommonCode;
import brain.api.avatar.client.AvatarClient;
import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

@Service
public class AvatarService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(AvatarCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    @Autowired
    AvatarClient avatarClient;

    public ByteArrayOutputStream downloadAvatar(String apiId, String apiKey, List<MultipartFile> images, MultipartFile video, HttpServletRequest request) throws MindsRestException, InterruptedException {
        validate(apiId, apiKey, AvatarCommonCode.SERVICE_NAME, 1, request);
        avatarClient.setDestination("182.162.19.12",1446);
        List<String> processList = new ArrayList<>();
        processList.add(avatarClient.upload(video));
        for(MultipartFile image: images){
            processList.add(avatarClient.upload(image));
        }
        String proccessId = avatarClient.processing(processList);
        Integer count = 0;
        while(avatarClient.checkStatus(proccessId) != 1){
            if(avatarClient.checkStatus(proccessId) == 3){
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        "Model processing Error: can not detect a face");
            }
            count++;
            if(count >= 200){throw new InternalException("Error: Download avatar timeout"); }
            sleep(6000);
        }
        return avatarClient.downloadAvatar(proccessId);
    }
}
