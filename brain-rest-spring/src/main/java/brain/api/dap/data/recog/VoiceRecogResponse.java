package brain.api.dap.data.recog;

import brain.api.common.data.CommonMsg;
import brain.api.dap.data.DapBaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class VoiceRecogResponse extends DapBaseResponse {
    private VoiceData result;

    public VoiceRecogResponse(CommonMsg message, VoiceData result){
        super(message);
        this.result = result;
    }

    public VoiceData getResult(){ return result; }
    public void setResult(VoiceData result) { this.result = result; }

    @Override
    public String toString() {
        return "VoiceRecogResponse{" +
                "result=" + result +
                "} " + super.toString();
    }
}
