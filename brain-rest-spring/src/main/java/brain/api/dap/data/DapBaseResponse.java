package brain.api.dap.data;

import brain.api.common.data.CommonMsg;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class DapBaseResponse {
    private List<Integer> diarizeResult;
    private List<Float> dVectorizeResult;
    private MultipartFile cnnNoiseResult;
    private CommonMsg message;

    public DapBaseResponse(CommonMsg message) {
        this.message = message;
    }

    public List<Integer> getDiarizeResult() {
        return diarizeResult;
    }

    public void setDiarizeResult(List<Integer> diarizeResult) {
        this.diarizeResult = diarizeResult;
    }

    public List<Float> getdVectorizeResult() {
        return dVectorizeResult;
    }

    public void setdVectorizeResult(List<Float> dVectorizeResult) {
        this.dVectorizeResult = dVectorizeResult;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public MultipartFile getCnnNoiseResult() {
        return cnnNoiseResult;
    }

    public void setCnnNoiseResult(MultipartFile cnnNoiseResult) {
        this.cnnNoiseResult = cnnNoiseResult;
    }

    @Override
    public String toString() {
        if (this.diarizeResult != null)
            return "DapBaseResponse{" +
                    "diarizeResult=" + diarizeResult +
                    ", message=" + message +
                    '}';
        else if (this.dVectorizeResult != null)
            return "DapBaseResponse{" +
                    "dVectorizeResult=" + dVectorizeResult +
                    ", message=" + message +
                    '}';
        else
            return "DapBaseResponse{" +
                    "message=" + message +
                    '}';
    }
}
