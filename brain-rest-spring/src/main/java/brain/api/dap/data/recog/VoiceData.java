package brain.api.dap.data.recog;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class VoiceData implements Serializable {
    private String id;
    private List<Float> voiceVector;
    private VoiceMetaData metaData;

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public List<Float> getVoiceVector() { return voiceVector; }
    public void setVoiceVector(List<Float> voiceVector) { this.voiceVector = voiceVector; }

    public VoiceMetaData getMetaData() { return metaData; }
    public void setMetaData(VoiceMetaData metaData) { this.metaData = metaData; }

    @Override
    public String toString() {
        return "VoiceData{" +
                "id='" + id + '\'' +
                ", voiceVector=" + voiceVector +
                ", metaData=" + metaData +
                '}';
    }
}
