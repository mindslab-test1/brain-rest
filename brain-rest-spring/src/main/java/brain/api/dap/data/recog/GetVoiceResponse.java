package brain.api.dap.data.recog;

import brain.api.common.data.CommonMsg;
import brain.api.dap.data.DapBaseResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetVoiceResponse extends DapBaseResponse {
    List<VoiceData> payload;

    public GetVoiceResponse(CommonMsg message, List<VoiceData> payload){
        super(message);
        this.payload = payload;
    }

    public List<VoiceData> getPayload() { return payload; }
    public void setPayload(List<VoiceData> payload) { this.payload = payload; }

    @Override
    public String toString() {
        return "GetVoiceResponse{" +
                "payload=" + payload +
                "} " + super.toString();
    }
}
