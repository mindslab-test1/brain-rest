package brain.api.dap;

public class DapCommonCode {
    private DapCommonCode(){}

    public static final String SERVICE_NAME = "DAP";
    public static final String SERVICE_APP_NAME = "DAP_APP";
}
