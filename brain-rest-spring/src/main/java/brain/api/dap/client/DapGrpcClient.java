package brain.api.dap.client;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.dap.DapCommonCode;
import brain.api.dap.data.DapDiarizeSpeaker;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import maum.brain.dap.Dap;
import maum.brain.dap.DiarizeGrpc;
import maum.brain.dap.DvectorizeGrpc;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Component
public class DapGrpcClient {

    private static final CloudApiLogger logger = new CloudApiLogger(DapCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    private static final String diarizeHost = "0.0.0.0";
    private static final int diarizePort = 35000;

    private static final String dvectorizeHost = "0.0.0.0";
    private static final int dvectorizePort = 35000;

//    public ResponseEntity doCnnDenoise(MultipartFile inputWav){
//        String ip = "0.0.0.0";
//        int port = 35000;
//
//        DenoiseGrpc.DenoiseBlockingStub denoiseStub = DenoiseGrpc
//                .newBlockingStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
//
//        denoiseStub.getDenoised(Dap.WavData.newBuilder()
//                .addAllData()
//                .build());
//
//    }

    public ResponseEntity doDiarize(MultipartFile inputWav) throws IOException {
        final CountDownLatch finishLatch = new CountDownLatch(1);

        List<Dap.DiarizeResult> resultList = new ArrayList<>();

        StreamObserver<Dap.DiarizeResult> responseObserver = new StreamObserver<Dap.DiarizeResult>() {
            @Override
            public void onNext(Dap.DiarizeResult diarizeResult) {
                resultList.add(diarizeResult);
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("finished getting response");
                finishLatch.countDown();
            }
        };

        DiarizeGrpc.DiarizeStub stub = DiarizeGrpc.newStub(
                CommonUtils.getManagedChannel(diarizeHost, diarizePort)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        StreamObserver<Dap.WavBinary> requestObserver = stub.getDiarizationFromWav(responseObserver);
        try {
            byte[] wavBytes = inputWav.getBytes();
//            int index = 0;
//            int buffSize = 1024;
//            while (index < wavBytes.length){
//                requestObserver.onNext(
//                        Dap.WavBinary.newBuilder()
//                                .setBin(ByteString.copyFrom(inputWav.getBytes()))
//                                .build());
//            }
            for (byte wavByte : wavBytes) {
                ByteString inputByteString = ByteString.copyFrom(new byte[]{wavByte});
                requestObserver.onNext(Dap.WavBinary.newBuilder().setBin(inputByteString).build());
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        requestObserver.onCompleted();
        try {
            finishLatch.await();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        List<DapDiarizeSpeaker> speakers = new ArrayList<>();

        throw new IOException();
    }

    public ResponseEntity doDvectorize(MultipartFile inputWav) throws IOException {
        final CountDownLatch finishLatch = new CountDownLatch(1);

        DvectorizeGrpc.DvectorizeStub stub = DvectorizeGrpc
                .newStub(CommonUtils.getManagedChannel(dvectorizeHost, dvectorizePort)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        final Dap.EmbList[] response = new Dap.EmbList[1];

        StreamObserver<Dap.EmbList> responseObserver = new StreamObserver<Dap.EmbList>() {
            @Override
            public void onNext(Dap.EmbList embList) {
                response[0] = embList;
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                finishLatch.countDown();
            }
        };

        StreamObserver<Dap.WavBinary> requestObserver = stub.getDvectorFromWav(responseObserver);
        try {
            byte[] wavBytes = inputWav.getBytes();
            for (byte wavByte : wavBytes) {
                ByteString inputByteString = ByteString.copyFrom(new byte[]{wavByte});
                requestObserver.onNext(Dap.WavBinary.newBuilder()
                        .setBin(inputByteString)
                        .build());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            throw e;
        }

        try {
            finishLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        throw new IOException();
    }
}
