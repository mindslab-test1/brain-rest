package brain.api.dap.client;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.dap.DapCommonCode;
import brain.api.dap.data.DapDiarizeResponse;
import brain.api.dap.data.DapDiarizeSpeaker;
import brain.api.dap.data.DvectorizeBaseResponse;
import brain.api.dap.data.GhostvladBaseResponse;
import brain.api.dap.model.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.exceptions.JedisConnectionException;
import sun.misc.BASE64Encoder;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Component
@JsonIgnoreProperties(ignoreUnknown=true)
public class DapDramaClient {

    private static final CloudApiLogger logger = new CloudApiLogger(DapCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private static final String DAP_HOST = "125.132.250.244";   // 65번 서버 외부 IP
//    private static final String DAP_HOST = "10.122.64.65";   // 65번 서버 내부 IP
//    private static final String DAP_HOST = "10.122.64.85";   // 85번 서버 내부 IP
    private static final String REDIS_PASS = "msl1234~";
//    private static final String PATH_STRING = "/home/aquashdw/maum/";
    private static final String PATH_STRING = "/home/ubuntu/maum/api/";
//    private static final String PATH_STRING = "/home/gom/maum/api/";
    private final int TIME_OUT = 1000;

    private JedisPool redisPool = new JedisPool(
            new JedisPoolConfig(),              // config
            DAP_HOST,                         // host
            6379,                         // port
            Protocol.DEFAULT_TIMEOUT,           // timeout
            REDIS_PASS                          // password
    );

    BASE64Encoder encoder = new BASE64Encoder();

    /**
     * CNNoise RabbitMQ Enqueue
     * 2019.6.25 Byeonguk.yu
     */
    public String cnnoiseRabbitMqEnqueue(MultipartFile waveFile) throws IOException, MindsRestException {
        final UUID UNIQUE_ID = UUID.randomUUID();
        CnnoiseRabbitMqVO rabbitMqVO = new CnnoiseRabbitMqVO();
        String messageKey = "";

        ConnectionFactory factory = new ConnectionFactory();
        Gson gson = new Gson();

        Map <String, String> kwargsMap = new HashMap <>();
        Map <String, String> optionsMap = new HashMap <>();
        optionsMap.put("store_results", "True");

        rabbitMqVO.setMessage_id(UNIQUE_ID.toString());
        rabbitMqVO.setMessage_timestamp(Calendar.getInstance().getTimeInMillis());
        rabbitMqVO.setKwargs(kwargsMap);
        rabbitMqVO.setOptions(optionsMap);


        byte[] audioBytes = waveFile.getBytes();
        String audioBase64 = encoder.encode(audioBytes);

        Object[] args = new Object[1];
        args[0] = audioBase64;
        rabbitMqVO.setArgs(args);

        String objJson = gson.toJson(rabbitMqVO);

        factory.setHost(DAP_HOST);
        factory.setUsername("minds");
        factory.setPassword("msl1234~");


        Map <String, Object> declareArgs = new HashMap <>();
        declareArgs.put("x-dead-letter-exchange", "");
        declareArgs.put("x-dead-letter-routing-key", rabbitMqVO.getQueue_name() + ".XQ");

        try (
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            messageKey = CommonUtils.dramaBuildMessageKey(rabbitMqVO);
            channel.queueDeclare(rabbitMqVO.getQueue_name(), true, false, false, declareArgs);
            channel.basicPublish("", rabbitMqVO.getQueue_name(), null, objJson.getBytes("UTF-8"));
        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "Denoise Connect Err");
        }

        return messageKey;

    }

    /**
     * CNNoise Redis GetWave
     * 2019.6.25 Byeonguk.yu
     */
    public ResponseEntity cnnoiseRedisGetWave(String messagekey) throws IOException, MindsRestException {
        Jedis redis = null;
        String result = "";

        try {
            redis = redisPool.getResource();
            List<String> resultList = redis.blpop(TIME_OUT, messagekey);

            for (String tempResult : resultList) {
                result = tempResult;
            }

        }
        catch(JedisConnectionException e) {
            if (redis != null) {
                redis.close();
                redis = null;
            }
            throw e;
        }
        finally {
            if (redis != null) {
                redis.close();
            }
        }

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(result);

        List<Short> audioList = new ArrayList <>();
        for (int i=0; i<jsonArray.size(); i++) {
            audioList.add((short)(Float.parseFloat(jsonArray.get(i).toString()) * 16000));
        }
        
        String originalFileName = "cnn_" + Calendar.getInstance().getTimeInMillis() + ".wav";
        String outputPath = PATH_STRING + "/" + originalFileName;
        Path path = Paths.get(outputPath);
        byte[] abAudioData = null;

        try {
            if (Files.exists(path)) {
                FileUtils.copyFile(new File(outputPath), new File(outputPath + "0"));
            } else {
                FileUtils.touch(new File(outputPath));
            }

            AudioFormat wavFormat = new AudioFormat(16000, 16, 1, true, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);

            for (int value : audioList) {
                dos.writeShort(value);
            }

            abAudioData = baos.toByteArray();
            ByteArrayInputStream bais = new ByteArrayInputStream(abAudioData);
            AudioInputStream stream = new AudioInputStream(bais, wavFormat, abAudioData.length/2);
            AudioSystem.write(stream, AudioFileFormat.Type.WAVE, new File(outputPath));

            dos.close();
            baos.close();
            stream.close();
            bais.close();

        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "Denoise Connect Err");
        }

        File file = new File(outputPath);
        FileInputStream contentStream = new FileInputStream(file);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentLength(file.length());
        httpHeaders.setContentType(new MediaType("audio", "wave"));
        httpHeaders.setCacheControl(CacheControl.noCache().getHeaderValue());
        logger.debug(outputPath);
        ResponseEntity responseEntity = new ResponseEntity(IOUtils.toByteArray(contentStream), httpHeaders, HttpStatus.OK);
        contentStream.close();
        file.delete();

        logger.info("[Legacy Denoise] - messageKey : "+ messagekey +", fileLength : " + file.length());

        return responseEntity;
    }

    /**
     * ghostvlad RabbitMQ Enqueue
     * 2019.6.25 Byeonguk.yu
     */
    public String ghostvladRabbitMqEnqueue(MultipartFile reqVoice) throws IOException, MindsRestException {
        final UUID UNIQUE_ID = UUID.randomUUID();
        GhostvladRabbitMqVO rabbitMqVO = new GhostvladRabbitMqVO();
        String messageKey = "";

        ConnectionFactory factory = new ConnectionFactory();
        Gson gson = new Gson();

        Map <String, String> kwargsMap = new HashMap <>();
        Map <String, String> optionsMap = new HashMap <>();
        optionsMap.put("store_results", "True");

        rabbitMqVO.setMessage_id(UNIQUE_ID.toString());
        rabbitMqVO.setMessage_timestamp(Calendar.getInstance().getTimeInMillis());
        rabbitMqVO.setKwargs(kwargsMap);
        rabbitMqVO.setOptions(optionsMap);

        byte[] audioBytes = reqVoice.getBytes();
        String audioBase64 = encoder.encode(audioBytes);

        Object[] args = new Object[1];
        args[0] = audioBase64;
        rabbitMqVO.setArgs(args);

        String objJson = gson.toJson(rabbitMqVO);

        factory.setHost(DAP_HOST);
        factory.setUsername("minds");
        factory.setPassword("msl1234~");

        Map <String, Object> declareArgs = new HashMap <>();
        declareArgs.put("x-dead-letter-exchange", "");
        declareArgs.put("x-dead-letter-routing-key", rabbitMqVO.getQueue_name() + ".XQ");

        try (
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()
        ) {
            messageKey = CommonUtils.dramaBuildMessageKey(rabbitMqVO);
            channel.queueDeclare(rabbitMqVO.getQueue_name(), true, false, false, declareArgs);
            channel.basicPublish("", rabbitMqVO.getQueue_name(), null, objJson.getBytes("UTF-8"));
        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "ghostvlad Connect Err");
        }
        logger.debug("RebbitMQ Enqueue Done : " + messageKey);
        return messageKey;
    }

    /**
     * ghostvlad Redis Get Vector
     * 2019.6.25 Byeonguk.yu
     */
    public JsonArray ghostvladRedisGetVector(String messagekey) {
        Gson gson = new Gson();
        Jedis redis = null;
        boolean isLast = false;
        String result = "";

        logger.debug("Redis Get_Vector : " + messagekey);

        try {
            redis = redisPool.getResource();
            List<String> resultList = redis.blpop(TIME_OUT, messagekey);

            for (String tempResult : resultList) {
                result = tempResult;

            }

        }
        catch(JedisConnectionException e) {
            if (redis != null) {
                redis.close();
                redis = null;
            }
            throw e;
        }
        finally {
            if (redis != null) {
                redis.close();
            }
        }

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(result);

        return jsonArray;

    }

    /**
     * gvoiceFilter RabbitMQ Enqueue
     * 2019.6.25 Byeonguk.yu
     */
    public String gvoiceFilterRabbitMqEnqueue(MultipartFile mixedVoice, JsonArray vector) throws IOException, MindsRestException {
        final UUID UNIQUE_ID = UUID.randomUUID();
        GvoiceFilterRabbitMqVO rabbitMqVO = new GvoiceFilterRabbitMqVO();
        String messageKey = "";

        ConnectionFactory factory = new ConnectionFactory();
        Gson gson = new Gson();

        Map <String, String> kwargsMap = new HashMap <>();
        Map <String, String> optionsMap = new HashMap <>();
        optionsMap.put("store_results", "True");

        rabbitMqVO.setMessage_id(UNIQUE_ID.toString());
        rabbitMqVO.setMessage_timestamp(Calendar.getInstance().getTimeInMillis());
        rabbitMqVO.setKwargs(kwargsMap);
        rabbitMqVO.setOptions(optionsMap);

        byte[] audioBytes = mixedVoice.getBytes();
        String audioBase64 = encoder.encode(audioBytes);

        Object[] args = new Object[2];
        args[0] = vector;
        args[1] = audioBase64;
        rabbitMqVO.setArgs(args);

        String objJson = gson.toJson(rabbitMqVO);

        factory.setHost(DAP_HOST);
        factory.setUsername("minds");
        factory.setPassword("msl1234~");

        Map <String, Object> declareArgs = new HashMap <>();
        declareArgs.put("x-dead-letter-exchange", "");
        declareArgs.put("x-dead-letter-routing-key", rabbitMqVO.getQueue_name() + ".XQ");

        try (
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()
        ) {
            messageKey = CommonUtils.dramaBuildMessageKey(rabbitMqVO);
            channel.queueDeclare(rabbitMqVO.getQueue_name(), true, false, false, declareArgs);
            channel.basicPublish("", rabbitMqVO.getQueue_name(), null, objJson.getBytes("UTF-8"));
        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "gvoiceFilter Connect Err");
        }
        logger.debug("RebbitMQ Enqueue Done : " + messageKey);
        return messageKey;
    }

    /**
     * gvoiceFilter Redis GetWave
     * 2019.7.1 Byeonguk.yu
     */
    public ResponseEntity gvoiceFilterGetWave(String messagekey) throws IOException, MindsRestException {
        Gson gson = new Gson();
        Jedis redis = null;
        boolean isLast = false;
        String result = "";

        logger.debug("Redis Get_Wave : " + messagekey);

        try {
            redis = redisPool.getResource();
            List<String> resultList = redis.blpop(TIME_OUT, messagekey);

            for (String tempResult : resultList) {
                result = tempResult;
            }

        }
        catch(JedisConnectionException e) {
            if (redis != null) {
                redis.close();
                redis = null;
            }
            throw e;
        }
        finally {
            if (redis != null) {
                redis.close();
            }
        }

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(result);

        List<Short> audioList = new ArrayList <>();
        for (int i=0; i<jsonArray.size(); i++) {
            audioList.add((short)(Float.parseFloat(jsonArray.get(i).toString()) * 16000));
        }

        String originalFileName = "vf_" + Calendar.getInstance().getTimeInMillis() + ".wav";
        String outputPath = PATH_STRING + "/" + originalFileName;
        Path path = Paths.get(outputPath);
        byte[] abAudioData = null;

        try {
            if (Files.exists(path)) {
                FileUtils.copyFile(new File(outputPath), new File(outputPath + "0"));
            } else {
                FileUtils.touch(new File(outputPath));
            }

            AudioFormat wavFormat = new AudioFormat(16000, 16, 1, true, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);

            for (int value : audioList) {
                dos.writeShort(value);
            }

            abAudioData = baos.toByteArray();
            ByteArrayInputStream bais = new ByteArrayInputStream(abAudioData);
            AudioInputStream stream = new AudioInputStream(bais, wavFormat, abAudioData.length/2);
            AudioSystem.write(stream, AudioFileFormat.Type.WAVE, new File(outputPath));

            dos.close();
            baos.close();
            stream.close();
            bais.close();

        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "gvoiceFilter Connect Err");
        }

        File file = new File(outputPath);
        FileInputStream contentStream = new FileInputStream(file);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentLength(file.length());
        httpHeaders.setContentType(new MediaType("audio", "wave"));
        httpHeaders.setCacheControl(CacheControl.noCache().getHeaderValue());
        logger.debug("File length : " + file.length());
        ResponseEntity resultResponse =  new ResponseEntity(IOUtils.toByteArray(contentStream), httpHeaders, HttpStatus.OK);
        contentStream.close();
        file.delete();

        logger.info("[Legacy voiceFilter] - messageKey : "+ messagekey +", fileLength : " + file.length());

        return resultResponse;
    }

    public ResponseEntity doVoiceFilter(MultipartFile reqVoice, MultipartFile mixedVoice) {
        ResponseEntity entity = new ResponseEntity(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            logger.debug("doVoiceFilter");
            JsonArray vector = this.ghostvladRedisGetVector(this.ghostvladRabbitMqEnqueue(reqVoice));
            logger.debug("VoiceFilter - ghostvlad Done");
            ResponseEntity response = this.gvoiceFilterGetWave(this.gvoiceFilterRabbitMqEnqueue(mixedVoice, vector));

            logger.info(String.format("[Legacy voiceFilter] - %s", response.getBody()));

            return response;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return entity;
        }

    }

    /**
     * dvectorize RabbitMQ Enqueue
     * 2019.7.10 Byeonguk.yu
     */
    public String dvectorizeRabbitMqEnqueue(MultipartFile reqVoice) throws IOException, MindsRestException {
        final UUID UNIQUE_ID = UUID.randomUUID();
        DvectorizeRabbitMqVo rabbitMqVO = new DvectorizeRabbitMqVo();
        String messageKey = "";

        ConnectionFactory factory = new ConnectionFactory();
        Gson gson = new Gson();

        Map <String, String> kwargsMap = new HashMap <>();
        Map <String, String> optionsMap = new HashMap <>();
        optionsMap.put("store_results", "True");

        rabbitMqVO.setMessage_id(UNIQUE_ID.toString());
        rabbitMqVO.setMessage_timestamp(Calendar.getInstance().getTimeInMillis());
        rabbitMqVO.setKwargs(kwargsMap);
        rabbitMqVO.setOptions(optionsMap);

        byte[] audioBytes = reqVoice.getBytes();
        String audioBase64 = encoder.encode(audioBytes);

        Object[] args = new Object[1];
        args[0] = audioBase64;
        rabbitMqVO.setArgs(args);

        String objJson = gson.toJson(rabbitMqVO);

        factory.setHost(DAP_HOST);
        factory.setUsername("minds");
        factory.setPassword("msl1234~");

        Map <String, Object> declareArgs = new HashMap <>();
        declareArgs.put("x-dead-letter-exchange", "");
        declareArgs.put("x-dead-letter-routing-key", rabbitMqVO.getQueue_name() + ".XQ");

        try (
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()
        ) {
            messageKey = CommonUtils.dramaBuildMessageKey(rabbitMqVO);
            channel.queueDeclare(rabbitMqVO.getQueue_name(), true, false, false, declareArgs);
            channel.basicPublish("", rabbitMqVO.getQueue_name(), null, objJson.getBytes("UTF-8"));
        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "dvectorize Connect Err");
        }
        logger.debug("RebbitMQ Enqueue Done : " + messageKey);
        return messageKey;
    }

    /**
     * dvectorize Redis Get Vector
     * 2019.7.10 Byeonguk.yu
     */
    public JsonArray dvectorizeRedisGetVector(String messagekey) {
        Jedis redis = null;
        String result = "";

        logger.debug("Redis Get_Vector : " + messagekey);

        try {
            redis = redisPool.getResource();
            List<String> resultList = redis.blpop(TIME_OUT, messagekey);

            for (String tempResult : resultList) {
                result = tempResult;

            }

        }
        catch(JedisConnectionException e) {
            if (redis != null) {
                redis.close();
                redis = null;
            }
            throw e;
        }
        finally {
            if (redis != null) {
                redis.close();
            }
        }

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(result);

        return jsonArray;

    }

    public String diarizeRabbitMqEnqueue(JsonArray vector) throws IOException, MindsRestException {
        final UUID UNIQUE_ID = UUID.randomUUID();
        DiarizeRabbitMqVo rabbitMqVO = new DiarizeRabbitMqVo();
        String messageKey = "";

        ConnectionFactory factory = new ConnectionFactory();
        Gson gson = new Gson();

        Map <String, String> kwargsMap = new HashMap <>();
        Map <String, String> optionsMap = new HashMap <>();
        optionsMap.put("store_results", "True");

        rabbitMqVO.setMessage_id(UNIQUE_ID.toString());
        rabbitMqVO.setMessage_timestamp(Calendar.getInstance().getTimeInMillis());
        rabbitMqVO.setKwargs(kwargsMap);
        rabbitMqVO.setOptions(optionsMap);

        Object[] args = new Object[1];
        args[0] = vector;
        rabbitMqVO.setArgs(args);

        String objJson = gson.toJson(rabbitMqVO);

        factory.setHost(DAP_HOST);
        factory.setUsername("minds");
        factory.setPassword("msl1234~");

        Map <String, Object> declareArgs = new HashMap <>();
        declareArgs.put("x-dead-letter-exchange", "");
        declareArgs.put("x-dead-letter-routing-key", rabbitMqVO.getQueue_name() + ".XQ");

        try (
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()
        ) {
            messageKey = CommonUtils.dramaBuildMessageKey(rabbitMqVO);
            channel.queueDeclare(rabbitMqVO.getQueue_name(), true, false, false, declareArgs);
            channel.basicPublish("", rabbitMqVO.getQueue_name(), null, objJson.getBytes("UTF-8"));
        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "diarize Connect Err");
        }
        logger.debug("RebbitMQ Enqueue Done : " + messageKey);
        return messageKey;
    }

    /**
     * dvectorize Redis Get Vector
     * 2019.7.10 Byeonguk.yu
     */
    public ResponseEntity diarizeRedisGetVector(String messagekey) {
        Jedis redis = null;
        String result = "";

        logger.debug("Redis Get_Vector : " + messagekey);

        try {
            redis = redisPool.getResource();
            List<String> resultList = redis.blpop(TIME_OUT, messagekey);

            for (String tempResult : resultList) {
                result = tempResult;

            }

        }
        catch(JedisConnectionException e) {
            if (redis != null) {
                redis.close();
                redis = null;
            }
            throw e;
        }
        finally {
            if (redis != null) {
                redis.close();
            }
        }

        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        List<DapDiarizeSpeaker> speakers = gson.fromJson(result, new TypeToken <List<DapDiarizeSpeaker>>(){}.getType());

        DapDiarizeResponse response = new DapDiarizeResponse(
                new CommonMsg(),
                speakers
        );

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(new MediaType("application", "json"));
        httpHeaders.setCacheControl(CacheControl.noCache().getHeaderValue());

        logger.info("[Legacy voicdFilter] - messageKey : "+ messagekey +", response : " + response.toString());

        return new ResponseEntity(response, httpHeaders, HttpStatus.OK);

    }

    public ResponseEntity doDiarize(MultipartFile reqVoice) {
        ResponseEntity entity = new ResponseEntity(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        try {
            logger.debug("doDiarize");
            JsonArray vector = this.dvectorizeRedisGetVector(this.dvectorizeRabbitMqEnqueue(reqVoice));
            logger.debug(vector.toString());
            logger.debug("Diarize - dvectorize Done");
            return this.diarizeRedisGetVector(this.diarizeRabbitMqEnqueue(vector));
        } catch (Exception ioExc) {
            logger.error(ioExc.getMessage());
            return entity;
        }

    }

    public DvectorizeBaseResponse doDvecotrize(MultipartFile reqVoice) throws MindsRestException {
        try {
            logger.debug("doDevectorize");
            JsonArray vector = this.dvectorizeRedisGetVector(this.dvectorizeRabbitMqEnqueue(reqVoice));
            logger.debug(vector.size());
            logger.debug(vector);
            Gson gson = new Gson();
            List<Object> vectorList = gson.fromJson(vector.toString(), new TypeToken<List<Object>>(){}.getType());
            DvectorizeBaseResponse dvectorizeBaseResponse = new DvectorizeBaseResponse(new CommonMsg(), vectorList);
            logger.info(String.format("Result: %s", dvectorizeBaseResponse));

            return dvectorizeBaseResponse;
        } catch(IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
    }

    public GhostvladBaseResponse doGhostvlad(MultipartFile reqVoice) throws MindsRestException {
        try {
            Gson gson = new Gson();
            logger.debug("doGhostvlad");
            JsonArray vector = this.ghostvladRedisGetVector(this.ghostvladRabbitMqEnqueue(reqVoice));
            logger.debug("vector Size : " + vector.size());
            List<Object> vectorList = gson.fromJson(vector.toString(), new TypeToken<List<Object>>(){}.getType());
            GhostvladBaseResponse ghostvladBaseResponse = new GhostvladBaseResponse(new CommonMsg(), vectorList);
            logger.info(String.format("Result: %s", ghostvladBaseResponse));

            return ghostvladBaseResponse;
        } catch(IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
    }

    //DAP Source Size Check (5MB 이상 처리 불가)
    public boolean dapFileSizeCheck(MultipartFile file) {
        if (file.getSize() > 5242880) {
            logger.info("File size is Too Large : " + file.getSize());
            return false;
        }
        return true;
    }
}
