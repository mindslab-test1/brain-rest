package brain.api.dap.model;

import brain.api.common.model.drama.RabbitMqDto;

public class GhostvladRabbitMqVO extends RabbitMqDto {

    public GhostvladRabbitMqVO() {
        setQueue_name("ghostvlad_actor");
        setActor_name("ghostvlad_actor");
    }
}
