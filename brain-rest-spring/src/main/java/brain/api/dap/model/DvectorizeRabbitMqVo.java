package brain.api.dap.model;

import brain.api.common.model.drama.RabbitMqDto;

public class DvectorizeRabbitMqVo extends RabbitMqDto {

    public DvectorizeRabbitMqVo() {
        setQueue_name("dvectorize_actor");
        setActor_name("dvectorize_actor");
    }
}
