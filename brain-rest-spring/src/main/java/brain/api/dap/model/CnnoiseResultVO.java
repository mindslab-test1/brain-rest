package brain.api.dap.model;

import java.util.List;

public class CnnoiseResultVO {

    private List <Double> doubleArray;

    public List <Double> getDoubleArray() {
        return doubleArray;
    }

    public void setDoubleArray(List <Double> doubleArray) {
        this.doubleArray = doubleArray;
    }
}
