package brain.api.dap.model;

import brain.api.common.model.drama.RabbitMqDto;

public class GvoiceFilterRabbitMqVO extends RabbitMqDto {

    public GvoiceFilterRabbitMqVO() {
        setQueue_name("gvoicefilter_actor");
        setActor_name("gvoicefilter_actor");
    }
}
