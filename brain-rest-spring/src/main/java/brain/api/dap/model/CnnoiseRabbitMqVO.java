package brain.api.dap.model;

import brain.api.common.model.drama.RabbitMqDto;

public class CnnoiseRabbitMqVO extends RabbitMqDto {

    public CnnoiseRabbitMqVO() {
        setQueue_name("cnnoise_actor");
        setActor_name("cnnoise_actor");
    }
}
