package brain.api.txr.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.data.CommonMsg;

import java.io.Serializable;
import java.util.Arrays;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TxrBaseResponse implements Serializable {
    private CommonMsg message;
    private byte[] imageAsBytes;

    public TxrBaseResponse(CommonMsg message, byte[] imageAsBytes) {
        this.message = message;
        this.imageAsBytes = imageAsBytes;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public byte[] getImageAsBytes() {
        return imageAsBytes;
    }

    public void setImageAsBytes(byte[] imageAsBytes) {
        this.imageAsBytes = imageAsBytes;
    }

    @Override
    public String toString() {
        return "TxrBaseResponse{" +
                "message=" + message +
                ", imageAsBytes=" + Arrays.toString(imageAsBytes) +
                '}';
    }
}
