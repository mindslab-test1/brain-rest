package brain.api.txr.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.txr.TxrCommonCode;
import brain.api.txr.data.TxrBaseResponse;
import com.google.protobuf.ByteString;
import maum.brain.txr.ts.TextRemovalGrpc;
import maum.brain.txr.ts.TrEn;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class TxrGrpcClient extends GrpcClientInterface implements TxrClientInterface{
    private static final CloudApiLogger logger = new CloudApiLogger(TxrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private TextRemovalGrpc.TextRemovalBlockingStub txrStub;

    @Override
    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip ,port));
            this.txrStub = TextRemovalGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e){
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    @Override
    public TxrBaseResponse textRemoval(MultipartFile img) throws MindsRestException{
        try {
            ByteString imgByteString = ByteString.copyFrom(
                    img.getBytes()
            );
            TrEn.InputImg inputImg = TrEn.InputImg.newBuilder()
                    .setImgBytes(imgByteString)
                    .build();

            TrEn.OutputImg outputImg = txrStub.removeTextFromImage(inputImg);

            logger.info("[Legacy Txtr] - resultImageSize : " + outputImg.getImgBytes().size());

            return new TxrBaseResponse(
                    new CommonMsg(),
                    outputImg.getImgBytes().toByteArray()
            );
        } catch (IOException e){
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );
        }
    }

    @Override
    public TxrBaseResponse textMasking(MultipartFile img) throws MindsRestException{
        throw new MindsRestException(
                CommonExceptionCode.COMMON_ERR_NOT_IMPLEMENTED.getErrCode(),
                CommonExceptionCode.COMMON_ERR_NOT_IMPLEMENTED.getMessage()
        );
    }
}
