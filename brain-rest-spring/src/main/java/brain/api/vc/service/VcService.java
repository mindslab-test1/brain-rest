package brain.api.vc.service;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.vc.VcCommonCode;
import brain.api.vc.client.VcClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

@Service
public class VcService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(VcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    VcClient vcClient;

    public ByteArrayOutputStream streamVc(String apiId, String apiKey, MultipartFile file, String text, int speaker, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, VcCommonCode.SERVICE_NAME, 1, request);
        vcClient.setDestination("182.162.19.12", 34001);
        return vcClient.streamVc(file, text, speaker);
    }
}
