package brain.api.vc.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.vc.VcCommonCode;
import brain.api.vc.service.VcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

@RestController
@RequestMapping("/vc")
public class VcController {
    private static final CloudApiLogger logger = new CloudApiLogger(VcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    VcService vcService;

    @RequestMapping(
            value = "/stream",
            method = RequestMethod.POST
    ) public ResponseEntity streamVc(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            @RequestParam("text") String text,
            @RequestParam("speaker") Integer speaker,
            HttpServletRequest httpRequest
    ){
        String orgFilename = file.getOriginalFilename();
        try {
            ByteArrayOutputStream buffer = vcService.streamVc(apiId, apiKey, file, text, speaker, httpRequest);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=test.wav")
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode())
                    .body(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage()));
        }
    }
}
