package brain.api.vc.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.vc.VcCommonCode;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import maum.brain.vc.Vc;
import maum.brain.vc.VoiceConversionGrpc;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class VcClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(VcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private VoiceConversionGrpc.VoiceConversionStub vcStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.vcStub = VoiceConversionGrpc.newStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public ByteArrayOutputStream streamVc(MultipartFile file, String text, int speaker) throws MindsRestException {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // STREAM RESPONSE
        StreamObserver<Vc.WavTxtTargetspk> requestObserver = vcStub.wavTxtTargetspk2Wav(new StreamObserver<Vc.WavData>() {
            @Override
            public void onNext(Vc.WavData wavData) {
                try {
                    outputStream.write(wavData.getData().toByteArray());
                } catch (IOException e) {
                    throw new InternalException(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Error: Stream Response onError from streamVc");
                logger.error(t.getMessage());
                finishLatch.countDown();
                throw new InternalException(t.getMessage());
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Stream Response from streamVc");
                finishLatch.countDown();
            }
        });
        // STREAM REQUEST
        try {
            Vc.WavTxtTargetspk.Builder vcBuilder = Vc.WavTxtTargetspk.newBuilder();
            vcBuilder.setText(text);
            vcBuilder.setSpkTarget(speaker);
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            int len = 0;
            byte[] buffer = new byte[1024 * 512];
            while ((len = bis.read(buffer)) > 0){
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(vcBuilder.setWavSource(inputByteString).build());
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    e.getMessage());
        }
        requestObserver.onCompleted();
        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    e.getMessage());
        }
        return outputStream;
    }
}
