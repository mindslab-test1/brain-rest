package brain.api.konglish.data;

import brain.api.common.data.CommonMsg;

import java.util.List;

public class KonglishResWords {
    private CommonMsg message;
    private List<String> result;

    public KonglishResWords(List<String> result){
        this.result = result;
        this.message = new CommonMsg();
    }

    public KonglishResWords(CommonMsg message) {
        this.result = null;
        this.message = message;
    }

    public CommonMsg getMessage() {
        return message;
    }
    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public List<String> getResult() {
        return result;
    }
    public void setResult(List<String> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "KonglishResWords{" +
                "message=" + message +
                ", result='" + result + '\'' +
                '}';
    }
}
