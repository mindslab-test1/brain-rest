package brain.api.konglish.data;

import brain.api.common.data.CommonMsg;

public class KonglishResSentence {
    private CommonMsg message;
    private String result;

    public KonglishResSentence(String result) {
        this.message = new CommonMsg();
        this.result = result;
    }
    public KonglishResSentence(CommonMsg message) {
        this.message = message;
        this.result = null;
    }

    public CommonMsg getMessage() {
        return message;
    }
    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "KonglishResSentence{" +
                "message=" + message +
                ", result='" + result + '\'' +
                '}';
    }
}
