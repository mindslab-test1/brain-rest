package brain.api.konglish.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.konglish.KonglishCommonCode;
import brain.api.konglish.data.KonglishResSentence;
import brain.api.konglish.data.KonglishResWords;
import brain.api.konglish.model.KonglishReqSentenceParams;
import brain.api.konglish.model.KonglishReqWordsParams;
import brain.api.konglish.service.KonglishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/konglish")
public class KonglishController {
    private static final CloudApiLogger logger = new CloudApiLogger(KonglishCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    KonglishService service;

    @RequestMapping(
            value = "/words",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody KonglishResWords words(
            @RequestBody KonglishReqWordsParams params,
            HttpServletRequest httpRequest
    ) {
        try {
            return service.konglishWords(params, httpRequest);
        } catch (MindsRestException e) {
            return new KonglishResWords(new CommonMsg(5000));
        }
    }

    @RequestMapping(
            value = "/sentence",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody KonglishResSentence sentence(
            @RequestBody KonglishReqSentenceParams params,
            HttpServletRequest httpRequest
    ) {
        try {
            return service.konglishSentence(params, httpRequest);
        } catch (MindsRestException e) {
            return new KonglishResSentence(new CommonMsg(5000));
        }
    }

}

