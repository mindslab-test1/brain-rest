package brain.api.correct.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.correct.data.CorrectRes;
import brain.api.correct.model.CorrectReq;
import brain.api.correct.service.CorrectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/correct")
public class CorrectController {

    @Autowired
    CorrectService service;

    @RequestMapping(
            value = "/sentence",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    ) public @ResponseBody CorrectRes correctSentence(
            @RequestBody CorrectReq params,
            HttpServletRequest httpRequest
    ){
        try {
            return service.correctSentence(params, httpRequest);
        } catch (Exception e) {
            return new CorrectRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage()));
        }
    }
}
