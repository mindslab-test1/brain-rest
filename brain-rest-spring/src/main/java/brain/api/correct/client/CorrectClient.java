package brain.api.correct.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.correct.CorrectCommonCode;
import maum.brain.bert.correct.BertCorrectGrpc;
import maum.brain.bert.correct.CorrectOuterClass.Correct;
import maum.brain.bert.correct.CorrectOuterClass.Sentence;
import org.springframework.stereotype.Component;

@Component
public class CorrectClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(CorrectCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    private BertCorrectGrpc.BertCorrectBlockingStub correctBlockingStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.correctBlockingStub = BertCorrectGrpc.newBlockingStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public String GetSentenceSimple(String sentence) throws MindsRestException {
        String result = null;
        try {
            Sentence request = Sentence.newBuilder().setSen(sentence).build();
            Correct response = correctBlockingStub.getSentenceSimple(request);
            result = response.getCorrect();
            result = result.replaceAll("<sot>", "");
            result = result.replaceAll("<eot>", "");
            result = result.replaceAll("<cls>", "\n");
        }catch (Exception e){
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage());
        }
        return result;
    }
}
