package brain.api.correct.data;

import brain.api.common.data.CommonMsg;

public class CorrectRes {
    private CommonMsg message;
    private String result;

    public CorrectRes(String result) {
        this.message = new CommonMsg();
        this.result = result;
    }
    public CorrectRes(CommonMsg message) {
        this.message = message;
        this.result = null;
    }

    public CommonMsg getMessage() {
        return message;
    }
    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CorrectRes{" +
                "message=" + message +
                ", result='" + result + '\'' +
                '}';
    }
}
