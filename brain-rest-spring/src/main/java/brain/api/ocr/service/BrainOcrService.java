package brain.api.ocr.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import brain.api.common.service.BaseService;
import brain.api.ocr.BrainOcrCommonCode;
import brain.api.ocr.client.BrainOcrClient;
import brain.api.ocr.dto.BrainOcrResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
public class BrainOcrService extends BaseService {

    @Autowired
    BrainOcrClient ocrClient;

    public BrainOcrResponse doOcr(
            String apiId,
            String apiKey,
            MultipartFile file,
            HttpServletRequest request
    ) throws IOException, MindsRestException {
        byte[] imageBytes = file.getBytes();

        validate(apiId, apiKey, BrainOcrCommonCode.SERVICE_NAME, imageBytes.length, request);
        return new BrainOcrResponse(
                ocrClient.doOcrSimple(imageBytes)
        );
    }
}
