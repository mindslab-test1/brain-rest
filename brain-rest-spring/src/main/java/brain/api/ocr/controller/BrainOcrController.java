package brain.api.ocr.controller;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.ocr.BrainOcrCommonCode;
import brain.api.ocr.dto.BrainOcrResponse;
import brain.api.ocr.service.BrainOcrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping(
        "ocr"
)
public class BrainOcrController {
    private static final CloudApiLogger logger = new CloudApiLogger(BrainOcrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    private BrainOcrService service;

    @PostMapping(
            value = "readDocument",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    ) public ResponseEntity<BrainOcrResponse> readDocument(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request
    ) {
        try {
            return ResponseEntity.ok(service.doOcr(
                    apiId, apiKey, file, request
            ));
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e))
                    .body(new BrainOcrResponse(e.createMessage()));
        } catch (IOException e) {
            return ResponseEntity.status(500)
                    .body(new BrainOcrResponse(
                            new MindsRestException().createMessage()
                    ));
        }
    }
}
