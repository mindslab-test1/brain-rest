package brain.api.ocr.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BrainOcrPoint implements Serializable {
    private int x;
    private int y;

    public BrainOcrPoint() {
    }

    public BrainOcrPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "BrainOcrPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
