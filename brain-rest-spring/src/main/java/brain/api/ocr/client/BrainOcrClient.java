package brain.api.ocr.client;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.ocr.BrainOcrCommonCode;
import brain.api.ocr.dto.BrainOcrPoint;
import brain.api.ocr.dto.BrainOcrTextBox;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.ocr.BrainOcr;
import maum.brain.ocr.OCRServiceGrpc;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class BrainOcrClient {
    private static final CloudApiLogger logger = new CloudApiLogger(BrainOcrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private final ManagedChannel channel = CommonUtils.getManagedChannel("182.162.19.12" , 50023);

    public List<BrainOcrTextBox> doOcrSimple(byte[] imageBytes) throws IOException, MindsRestException {
        List<BrainOcr.OcrResult> resultList = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();
        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<BrainOcr.OcrResult> responseObserver = new StreamObserver<BrainOcr.OcrResult>() {
            @Override
            public void onNext(BrainOcr.OcrResult ocrResult) {
                logger.debug(ocrResult.toString());
                resultList.add(ocrResult);
            }

            @Override
            public void onError(Throwable throwable) {
                errorList.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                finishLatch.countDown();
            }
        };

        OCRServiceGrpc.OCRServiceStub stub = OCRServiceGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        StreamObserver<BrainOcr.Document> requestObserver = stub.readDocument(responseObserver);

        BrainOcr.Document.Builder documentBuilder = BrainOcr.Document.newBuilder();

        ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes);
        int len = 0;
        byte[] buffer = new byte[1024*512];
        while((len = bis.read(buffer)) > 0){
            if(len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(documentBuilder.setImage(inputByteString).build());
        }

        requestObserver.onCompleted();
        boolean timeout;
        // 2. Waiting till async work is done
        try {
            timeout = finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.warn(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }
        if(!timeout){
            logger.error("request timeout");
            throw new MindsRestException();
        }
        if(!errorList.isEmpty()){
            logger.error(errorList.get(0));
            throw new MindsRestException();
        }

        logger.debug(resultList);

        List<BrainOcrTextBox> textBoxes = new ArrayList<>();
        for(BrainOcr.TextBox textBox: resultList.get(0).getTextboxList()){
            String text = textBox.getText();
            List<BrainOcrPoint> points = new ArrayList<>();
            for(BrainOcr.Point point: textBox.getBboxList()){
                points.add(new BrainOcrPoint(
                   point.getX(), point.getY()
                ));
            }
            textBoxes.add(new BrainOcrTextBox(
                    text, points
            ));
        }

        return textBoxes;
    }
}
