package brain.api.edueng;

public class EduengCommonCode {
    private EduengCommonCode(){}

    public static final String SERVICE_NAME = "EngEdu";
    public static final String STT_SERVICE_NAME = "EngEdu_STT";
    public static final String PRON_SERVICE_NAME = "EngEdu_Pron";
    public static final String PHONICS_SERVICE_NAME = "EngEdu_Phonics";

    public static final String API_URL = "https://maieng.maum.ai:7777/engedu/v1";
    public static final String STT_SUB_URL = "/stt";
    public static final String PRON_SUB_URL = "/pron";
    public static final String PHONICS_SUB_URL = "/phonics";

}
