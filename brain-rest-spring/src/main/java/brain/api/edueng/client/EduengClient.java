package brain.api.edueng.client;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.edueng.EduengCommonCode;
import brain.api.edueng.data.EduengOutterResult;
import brain.api.edueng.data.EduengResult;
import brain.api.edueng.model.EduengApiCallType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
@JsonIgnoreProperties(ignoreUnknown=true)
public class EduengClient {
    private static final CloudApiLogger logger = new CloudApiLogger(EduengCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    static final Gson gson = new Gson();

    public EduengResult eduengApiCall(EduengApiCallType type, String apiId, String apiKey, String userId, String model, String answerText, MultipartFile file) throws MindsRestException, IOException {

        return eduengApiCall(type, apiId, apiKey, userId, model, answerText, null, file);
    }

    public EduengResult eduengApiCall(EduengApiCallType type, String apiId, String apiKey, String userId, String model, String answerText, String chkSym, MultipartFile file) throws MindsRestException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost postConnection = new HttpPost(EduengCommonCode.API_URL + type.getSubUrl());
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        File tmpFile = null;
        EduengOutterResult response;

        try {
            tmpFile = new File(System.getProperty("java.io.tmpdir") + file.getOriginalFilename());
            file.transferTo(tmpFile);
            builder.addTextBody("apiId", apiId);
            builder.addTextBody("apiKey", apiKey);
            builder.addTextBody("userId", userId);
            builder.addTextBody("model", model);
            builder.addTextBody("answerText", answerText);
            if (chkSym != null) builder.addTextBody("chkSym", chkSym);
            builder.addPart("file", new FileBody(tmpFile));
            HttpEntity entity = builder.build();
            postConnection.setEntity(entity);
            CloseableHttpResponse httpResponse = httpClient.execute(postConnection);
            HttpEntity responseEntity = httpResponse.getEntity();
            String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
            response = gson.fromJson(responseBody, EduengOutterResult.class);
        } catch (Exception e) {
            logger.error(e);
            throw new MindsRestException(e);
        } finally {
            httpClient.close();
            if (tmpFile != null) {
                tmpFile.delete();
            }
        }
        return response.getResult();
    }
}
