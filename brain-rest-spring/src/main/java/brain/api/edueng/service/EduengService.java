package brain.api.edueng.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import brain.api.common.service.BaseService;
import brain.api.edueng.client.EduengClient;
import brain.api.edueng.data.EduengResult;
import brain.api.edueng.model.EduengApiCallType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
public class EduengService extends BaseService {
    final EduengClient client;

    public EduengService(EduengClient client) {
        this.client = client;
    }

    public EduengResult eduengStt(String apiId, String apiKey, String userId, String model, String answerText, MultipartFile file, HttpServletRequest httpRequest) throws MindsRestException, IOException {
        validate(apiId, apiKey, EduengApiCallType.STT.getServiceName(), 1, httpRequest);

        return client.eduengApiCall(EduengApiCallType.STT, apiId, apiKey, userId, model, answerText, file);
    }

    public EduengResult eduengPron(String apiId, String apiKey, String userId, String model, String answerText, MultipartFile file, HttpServletRequest httpRequest) throws MindsRestException, IOException {
        validate(apiId, apiKey, EduengApiCallType.PRON.getServiceName(), 1, httpRequest);

        return client.eduengApiCall(EduengApiCallType.PRON, apiId, apiKey, userId, model, answerText, file);
    }

    public EduengResult eduengPhonics(String apiId, String apiKey, String userId, String model, String answerText, MultipartFile file, String chkSym, HttpServletRequest httpRequest) throws MindsRestException, IOException {
        validate(apiId, apiKey, EduengApiCallType.PHONICS.getServiceName(), 1, httpRequest);

        return client.eduengApiCall(EduengApiCallType.PHONICS, apiId, apiKey, userId, model, answerText, chkSym, file);
    }
}
