package brain.api.edueng.data;

public class EduengOutterResult {
    private EduengResult result;

    public EduengResult getResult() {
        return result;
    }

    public void setResult(EduengResult result) {
        this.result = result;
    }
}

