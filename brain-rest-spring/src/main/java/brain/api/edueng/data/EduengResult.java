package brain.api.edueng.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class EduengResult {
    private String resultText;
    private int resCode;
    private String answerText;
    private String recordUrl;
    private int sentenceScore;
    private List<Word> words;
    private int rhythmScore;
    private int segmentalScore;
    private int totalScore;
    private int pronScore;
    private int speedScore;
    private int intonationScore;

    private String userPron;
    private String correctPron;
    private int status;

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getRecordUrl() {
        return recordUrl;
    }

    public void setRecordUrl(String recordUrl) {
        this.recordUrl = recordUrl;
    }

    public int getSentenceScore() {
        return sentenceScore;
    }

    public void setSentenceScore(int sentenceScore) {
        this.sentenceScore = sentenceScore;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public int getRhythmScore() {
        return rhythmScore;
    }

    public void setRhythmScore(int rhythmScore) {
        this.rhythmScore = rhythmScore;
    }

    public int getSegmentalScore() {
        return segmentalScore;
    }

    public void setSegmentalScore(int segmentalScore) {
        this.segmentalScore = segmentalScore;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public int getPronScore() {
        return pronScore;
    }

    public void setPronScore(int pronScore) {
        this.pronScore = pronScore;
    }

    public int getSpeedScore() {
        return speedScore;
    }

    public void setSpeedScore(int speedScore) {
        this.speedScore = speedScore;
    }

    public int getIntonationScore() {
        return intonationScore;
    }

    public void setIntonationScore(int intonationScore) {
        this.intonationScore = intonationScore;
    }

    public String getUserPron() {
        return userPron;
    }

    public void setUserPron(String userPron) {
        this.userPron = userPron;
    }

    public String getCorrectPron() {
        return correctPron;
    }

    public void setCorrectPron(String correctPron) {
        this.correctPron = correctPron;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

class Word {
    private String word;
    private int pronScore;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getPronScore() {
        return pronScore;
    }

    public void setPronScore(int pronScore) {
        this.pronScore = pronScore;
    }
}