package brain.api.user.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.usage.SumUsageRequestParam;
import brain.api.common.model.user.ApiClientDto;
import brain.api.user.UserCommonCode;
import brain.api.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    private static final CloudApiLogger logger = new CloudApiLogger(UserCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    private UserService service;

    @RequestMapping(
            value = "/selectUsage",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<?> selectUsage(@RequestBody Map<String, String> input, HttpServletRequest request) {
        logger.info("[User selectUsage] : apiId : " + input.get("apiId"));

        try {
            return new ResponseEntity<>(service.selectUsage(input, request), HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/sumUsage",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<?> selectSumUsage(@RequestBody SumUsageRequestParam param, HttpServletRequest request) {
        logger.info("[User selectSumUsage] : apiId : " + param.getApiId());

        try {
            return new ResponseEntity<>(service.selectSumUsage(param, request), HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/reIssueKey",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<?> reIssueKey(@RequestBody ApiClientDto dto, HttpServletRequest request) {
        logger.info("[User reIssueKey] : apiId : " + dto.getApiId());

        try {
            return new ResponseEntity<>(service.reIssueKey(dto, request), HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/validate",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<?> validate(@RequestBody ApiClientDto dto, HttpServletRequest request) {
        logger.info("[User validate] : apiId : " + dto.getApiId());

        try {
            return new ResponseEntity<>(service.validate(dto, request), HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
