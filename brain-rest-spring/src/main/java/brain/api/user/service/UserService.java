package brain.api.user.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.usage.SumUsageDto;
import brain.api.common.model.usage.SumUsageRequestParam;
import brain.api.common.model.usage.UsageDto;
import brain.api.common.model.user.ApiClientDto;
import brain.api.common.service.BaseService;
import brain.api.user.UserCommonCode;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class UserService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(UserCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    public CommonResponse<List<UsageDto>> selectUsage(Map<String, String> input, HttpServletRequest request) throws MindsRestException {
        String service = input.get("service");
        String apiId = input.get("apiId");

        validate(apiId, input.get("apiKey"), UserCommonCode.SERVICE_NAME, 1, request);
        logger.info("[UserUsage] : " + apiId + ", " + service);

        if (input.get("startDate") != null && input.get("endDate") != null
                && input.get("startDate").compareTo(input.get("endDate")) == 1) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "The startDate cannot be greater than the endDate.");
        }

        Map<String, String> sqlParam = new HashMap<>();
        sqlParam.put("reqId", apiId);
        sqlParam.put("startDate", input.get("startDate"));
        sqlParam.put("endDate", input.get("endDate"));
        if(service != null) sqlParam.put("service", service);
        List<UsageDto> usages = apiClientDao.selectUsage(sqlParam);

        if (service == null || service.equalsIgnoreCase("STT")) {
            // TODO: Refactoring Target. Dnn STT 이관시 변경해야할 부분
            List<UsageDto> sttUsage = apiClientDao.selectSttusage(sqlParam);
            usages.addAll(sttUsage);
        }

        logger.info("selectUsage - searcher : " + apiId + ", usageNum : " + usages.size());
        return new CommonResponse<>(new CommonMsg(), usages);
    }

    public CommonResponse<ApiClientDto> reIssueKey(ApiClientDto dto, HttpServletRequest request) throws MindsRestException {
        validate(dto.getApiId(), dto.getApiKey(), UserCommonCode.SERVICE_NAME, 1, request);
        String newKey = UUID.randomUUID().toString().replaceAll("-", "");
        dto.setApiKey(newKey);
        if (apiClientDao.updateClientKey(dto) != 1) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }

        logger.info("reIssueKey Success - apiId : " + dto.getApiId());
        return new CommonResponse<>(new CommonMsg(), dto);
    }

    public CommonResponse<ApiClientDto> validate(ApiClientDto dto, HttpServletRequest request) throws MindsRestException {
        validate(dto.getApiId(), dto.getApiKey(), UserCommonCode.SERVICE_NAME, 1, request);

        return new CommonResponse<>(new CommonMsg(), dto);
    }

    public CommonResponse<List<SumUsageDto>> selectSumUsage(SumUsageRequestParam param, HttpServletRequest request) throws MindsRestException {
        validate(param.getApiId(), param.getApiKey(), UserCommonCode.SERVICE_NAME, 1, request);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        logger.debug(sdf.format(param.getStartDate()));

        return new CommonResponse<>(new CommonMsg(), usageDao.sumUsageDto(param));
    }
}
