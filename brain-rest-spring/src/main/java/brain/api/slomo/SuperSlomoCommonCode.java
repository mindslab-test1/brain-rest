package brain.api.slomo;

public class SuperSlomoCommonCode {
    public static final String SERVICE_NAME = "SLOMO";

    public static final int CHUNK_SIZE = 1048576;

    public static final String SUCCESS = "success";

    public static final String STATUS_QUEUED = "process is in queue";
    public static final String STATUS_PROCESSING = "processing";
    public static final String STATUS_COMPLETED = "available";
    public static final String STATUS_DELETED = "file key expired";

    public static final String ERROR_WRONG_KEY = "wrong file key presented";}
