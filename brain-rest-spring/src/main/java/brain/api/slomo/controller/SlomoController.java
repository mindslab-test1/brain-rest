package brain.api.slomo.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.slomo.SuperSlomoCommonCode;
import brain.api.slomo.dto.QueryRequest;
import brain.api.slomo.dto.QueryResponse;
import brain.api.slomo.dto.UploadResponse;
import brain.api.slomo.service.SlomoService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/slomo")
public class SlomoController {
    private static final CloudApiLogger logger = new CloudApiLogger(SuperSlomoCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    final SlomoService service;

    public SlomoController(SlomoService slomoService){
        this.service = slomoService;
    }

    @PostMapping(
            value = "/request",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<UploadResponse> slomoRequest(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam(value = "fps", defaultValue = "0") String fps,
            @RequestParam(value = "scale", defaultValue = "0") String scale,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ){
        try {
            UploadResponse response = service.uploadRequest(apiId, apiKey, fps, scale, file, httpRequest);
            return ResponseEntity
                    .status(response.getPayload().getStatus())
                    .body(response);
        } catch (MindsRestException e){
            return ResponseEntity
                    .status(CommonExceptionCode.getHttpStatusOnException(e))
                    .body(new UploadResponse(
                            new CommonMsg(e.getErrCode()),
                            null
                    ));
        }
    }

    @PostMapping(
            value = "/query",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<QueryResponse> slomoQuery(
            @RequestBody QueryRequest request,
            HttpServletRequest httpRequest
    ){
        try {
            QueryResponse response = service.queryRequest(request.getApiId(), request.getApiKey(), request.getFileKey(), httpRequest);
            return ResponseEntity
                    .status(response.getPayload().getStatus())
                    .body(response);
        } catch (MindsRestException e){
            return ResponseEntity
                    .status(CommonExceptionCode.getHttpStatusOnException(e))
                    .body(new QueryResponse(
                            new CommonMsg(e.getErrCode()),
                            null
                    ));
        }
    }

    @PostMapping(
            value = "/download"
    )
    public @ResponseBody byte[] slomoDownload(
            @RequestBody QueryRequest request,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse
    ){
        try {
            byte[] responseBytes = service.downloadResult(request.getApiId(), request.getApiKey(), request.getFileKey(), httpRequest);
            httpResponse.setStatus(200);
            return responseBytes;

        } catch (MindsRestException e){
            byte[] nullBytes = new byte[1];
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return nullBytes;
        }
    }
}
