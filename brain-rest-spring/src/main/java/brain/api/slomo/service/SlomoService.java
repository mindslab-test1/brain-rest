package brain.api.slomo.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.slomo.SuperSlomoCommonCode;
import brain.api.slomo.client.SlomoGrpcClient;
import brain.api.slomo.dto.QueryResponse;
import brain.api.slomo.dto.UploadResponse;
import maum.brain.super_slomo.SuperSlomoOuterClass;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Iterator;

@Service
public class SlomoService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(SuperSlomoCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    final SlomoGrpcClient grpcClient;

    public SlomoService(SlomoGrpcClient grpcClient) {
        this.grpcClient = grpcClient;
    }

    public UploadResponse uploadRequest(
            String apiId,
            String apiKey,
            String fps,
            String scale,
            MultipartFile file,
            HttpServletRequest request
    ) throws MindsRestException {
        int parseFps;
        int parseScale;
        try {
            parseFps = Integer.parseInt(fps);
            parseScale = Integer.parseInt(scale);
        } catch (NumberFormatException e){
            logger.warn(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "fps / scale parse exception");
        }

        validate(apiId, apiKey, SuperSlomoCommonCode.SERVICE_NAME, (int) file.getSize(), request);
        try {
            return new UploadResponse(
                    new CommonMsg(),
                    grpcClient.upload(file, parseFps, parseScale)
            );
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new MindsRestException();
        }
    }

    public QueryResponse queryRequest(
            String apiId,
            String apiKey,
            String fileKey,
            HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, SuperSlomoCommonCode.SERVICE_NAME, 0, request);
        try {
            return new QueryResponse(
                    new CommonMsg(),
                    grpcClient.checkProcess(fileKey)
            );
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException();
        }
    }

    public byte[] downloadResult(
            String apiId,
            String apiKey,
            String fileKey,
            HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, SuperSlomoCommonCode.SERVICE_NAME, 1, request);
        Iterator<SuperSlomoOuterClass.FileChunk> iter = grpcClient.downLoad(fileKey);
        SuperSlomoOuterClass.FileChunk firstPart = iter.next();
        if (firstPart.getFileBytes().isEmpty()) throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "invalid file key");
        byte[] response = firstPart.getFileBytes().toByteArray();

        SuperSlomoOuterClass.FileChunk responsePart;
        while (iter.hasNext()) {
            responsePart = iter.next();
            byte[] data = responsePart.getFileBytes().toByteArray();
            response = ArrayUtils.addAll(response, data);
            logger.debug(String.format("response length: %d", response.length));
        }

        return response;
    }
}
