package brain.api.slomo.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import static brain.api.slomo.SuperSlomoCommonCode.*;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProcessStatus implements Serializable {
    private int status;
    private String message;

    public ProcessStatus() {
    }

    public ProcessStatus(int messageStatus){
        switch (messageStatus){
            case 6:
                this.status = 202;
                this.message = STATUS_QUEUED;
                break;
            case 7:
                this.status = 202;
                this.message = STATUS_PROCESSING;
                break;
            case 8:
                this.status = 201;
                this.message = STATUS_COMPLETED;
                break;
            case 9:
                this.status = 400;
                this.message = STATUS_DELETED;
                break;
            case 10:
                this.status = 400;
                this.message = ERROR_WRONG_KEY;
                break;
            default:
                this.status = 500;
                this.message = "Internal Server Error";
                break;
        }
    }
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ProcessStatus{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
