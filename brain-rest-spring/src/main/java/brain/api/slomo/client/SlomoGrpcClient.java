package brain.api.slomo.client;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.slomo.SuperSlomoCommonCode;
import brain.api.slomo.data.ProcessStatus;
import brain.api.slomo.data.UploadStatus;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.super_slomo.SuperSlomoGrpc;
import maum.brain.super_slomo.SuperSlomoOuterClass;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Component
public class SlomoGrpcClient {
    private static final CloudApiLogger logger = new CloudApiLogger(
            SuperSlomoCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT
    );
    private final ManagedChannel managedChannel = CommonUtils.getManagedChannel("182.162.19.12", 10051);

    public UploadStatus upload(MultipartFile file, int fps, int scale) throws IOException {
        SuperSlomoGrpc.SuperSlomoStub stub = SuperSlomoGrpc.newStub(managedChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        logger.info("prepare request to grpc server");

        final CountDownLatch finishLatch = new CountDownLatch(1);
        final List<SuperSlomoOuterClass.UploadStatus> result = new ArrayList<>();
        final List<Throwable> error = new ArrayList<>();

        StreamObserver<SuperSlomoOuterClass.UploadStatus> responseObserver = new StreamObserver<SuperSlomoOuterClass.UploadStatus>() {

            @Override
            public void onNext(SuperSlomoOuterClass.UploadStatus uploadStatus) {
                logger.debug("message retrieved from server");
                result.add(uploadStatus);
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                error.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.debug("on completed called from server");
                finishLatch.countDown();
            }
        };

        StreamObserver<SuperSlomoOuterClass.FileChunk> requestObserver = stub.upload(responseObserver);

        SuperSlomoOuterClass.FileChunk.Builder requestBuilder = SuperSlomoOuterClass.FileChunk.newBuilder();
        ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
        int len = 0;
        byte[] buffer = new byte[SuperSlomoCommonCode.CHUNK_SIZE];
        while((len = bis.read(buffer)) > 0){
            logger.debug("send filechunk");
            if(len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(requestBuilder
                    .setFps(fps)
                    .setScale(scale)
                    .setFileBytes(inputByteString)
                    .build());
        }
        requestObserver.onCompleted();

        try {
            finishLatch.await();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        if(error.size() != 0){
            return new UploadStatus(2, null, "upload error");
        } else {
            return new UploadStatus(result.get(0).getStatus(), result.get(0).getFileKey(), result.get(0).getErrorMsg());
        }
    }

    public ProcessStatus checkProcess(String fileKey) {
        SuperSlomoGrpc.SuperSlomoBlockingStub stub = SuperSlomoGrpc.newBlockingStub(managedChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        SuperSlomoOuterClass.ProcessStatus status = stub.checkProcess(SuperSlomoOuterClass.KeyMessage.newBuilder()
                .setFileKey(fileKey)
                .build());
        return new ProcessStatus(status.getStatus());
    }

    public Iterator<SuperSlomoOuterClass.FileChunk> downLoad(String fileKey){
        SuperSlomoGrpc.SuperSlomoBlockingStub stub = SuperSlomoGrpc.newBlockingStub(managedChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        return stub.download(SuperSlomoOuterClass.KeyMessage.newBuilder()
                .setFileKey(fileKey)
                .build());
    }
}
