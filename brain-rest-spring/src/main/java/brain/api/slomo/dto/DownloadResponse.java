package brain.api.slomo.dto;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DownloadResponse implements Serializable {
    private CommonMsg message;
    private Object payload;

    public DownloadResponse() {
    }

    public DownloadResponse(CommonMsg message) {
        this.message = message;
        this.payload = null;
    }
}
