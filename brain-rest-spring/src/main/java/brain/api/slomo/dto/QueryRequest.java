package brain.api.slomo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QueryRequest implements Serializable {
    private String apiId;
    private String apiKey;
    private String fileKey;

    public QueryRequest() {
    }

    public QueryRequest(String apiId, String apiKey, String fileKey) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.fileKey = fileKey;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    @Override
    public String toString() {
        return "QueryRequest{" +
                "apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", fileKey='" + fileKey + '\'' +
                '}';
    }
}
