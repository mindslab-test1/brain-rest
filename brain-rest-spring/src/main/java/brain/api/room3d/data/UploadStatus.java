package brain.api.room3d.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class UploadStatus {
    private int status;
    private String fileKey;
    private String msg;
}
