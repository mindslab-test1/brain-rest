package brain.api.room3d.data;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProcessStatus {
    private int status;
    private String msg;
}
