package brain.api.room3d.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString
public class FileChunkDTO {
    private String apiId;
    private String apiKey;
    private MultipartFile image;
}
