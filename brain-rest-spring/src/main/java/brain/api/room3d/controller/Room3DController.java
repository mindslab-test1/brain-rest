package brain.api.room3d.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.room3d.Room3DCommonCode;
import brain.api.room3d.data.FileChunkDTO;
import brain.api.room3d.data.KeyMessageDTO;
import brain.api.room3d.service.Room3DService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/holistic3d")
public class Room3DController {
    private static final CloudApiLogger logger = new CloudApiLogger(Room3DCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);
    private final Room3DService service;

    @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> upload(@ModelAttribute FileChunkDTO fileChunkDTO, HttpServletRequest request) {
        try {
            return ResponseEntity.ok().body(service.upload(fileChunkDTO, request));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @PostMapping(value = "/check:process", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> checkProcess(@RequestBody KeyMessageDTO keyMessageDTO, HttpServletRequest request) {
        try {
            return ResponseEntity.ok().body(service.statusCheck(keyMessageDTO, request));
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @PostMapping(value = "/download",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<?> download(@RequestBody KeyMessageDTO keyMessageDTO, HttpServletRequest request) {
        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=maum_ai_holistic3d_%s.pcd",
                            keyMessageDTO.getFileKey()))
                    .body(service.download(keyMessageDTO, request).toByteArray());
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

}

