package brain.api.room3d.client;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.room3d.Room3DCommonCode;
import brain.api.room3d.data.FileChunkDTO;
import brain.api.room3d.data.KeyMessageDTO;
import brain.api.room3d.data.ProcessStatus;
import brain.api.room3d.data.UploadStatus;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.holistic3d.Room3D;
import maum.brain.holistic3d.room3dGrpc;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class Room3DClient {
    private static final CloudApiLogger logger = new CloudApiLogger(Room3DCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private static final String IP = "182.162.19.6";
    private static final int PORT = 39995;

    public UploadStatus upload(FileChunkDTO fileChunkDTO) throws IOException, InterruptedException, MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(IP, PORT);
        room3dGrpc.room3dStub stub = room3dGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        List<UploadStatus> responseList = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<Room3D.UploadStatus> responseObserver = new StreamObserver<Room3D.UploadStatus>() {
            @Override
            public void onNext(Room3D.UploadStatus uploadStatus) {
                responseList.add(new UploadStatus(uploadStatus.getStatus(), uploadStatus.getFileKey(), uploadStatus.getMsg()));
                logger.info("[Holistic3D] request key received");
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                errorList.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("[Holistic3D] server on completed called");
                finishLatch.countDown();
            }
        };

        StreamObserver<Room3D.FileChunk> requestObserver = stub.upload(responseObserver);
        Room3D.FileChunk.Builder inputBuilder = Room3D.FileChunk.newBuilder();

        if(fileChunkDTO.getImage().getBytes() != null) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fileChunkDTO.getImage().getBytes());
            int len = 0;
            byte[] buffer = new byte[1024 * 512];
            while ((len = byteArrayInputStream.read(buffer)) > 0) {
                logger.debug(String.format("image chunk size: %d", len));
                if (len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(inputBuilder.setFileBytes(inputByteString).build());
                logger.debug("send image chunk to server");
            }
        }
        requestObserver.onCompleted();

        if(!finishLatch.await(1, TimeUnit.MINUTES)) {
            logger.error("room3d connection timeout");
            throw new MindsRestException("connection timeout with internal server");
        }
        if(!errorList.isEmpty()) {
            logger.error(errorList.get(0));
            throw new MindsRestException();
        }

        if(responseList.get(0).getStatus() != 1) {
            logger.error("room3d upload error");
            throw new MindsRestException("upload error to holistic3D Upload");
        }

        return responseList.get(0);
    }

    public ProcessStatus statusCheck(KeyMessageDTO keyMessageDTO) {
        ManagedChannel channel = CommonUtils.getManagedChannel(IP, PORT);
        room3dGrpc.room3dBlockingStub stub = room3dGrpc.newBlockingStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        Room3D.ProcessStatus processStatus = stub.checkProcess(Room3D.KeyMessage.newBuilder().setFileKey(keyMessageDTO.getFileKey()).build());
        ProcessStatus result = new ProcessStatus(processStatus.getStatus(), processStatus.getMsg());
        return result;
    }

    public ByteArrayOutputStream download(String fileKey) throws MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(IP, PORT);
        room3dGrpc.room3dBlockingStub stub = room3dGrpc.newBlockingStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        Room3D.ProcessStatus processStatus = stub.checkProcess(Room3D.KeyMessage.newBuilder().setFileKey(fileKey).build());

        if (processStatus.getStatus() != 8) {
            logger.warn(processStatus.getStatus());
            logger.warn(processStatus.getMsg());
            throw new MindsRestException(2002, "Process is not done");
        }

        Iterator<Room3D.FileChunk> rawIterator = stub.download(Room3D.KeyMessage.newBuilder().setFileKey(fileKey).build());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            while (rawIterator.hasNext()) {
                Room3D.FileChunk response = rawIterator.next();
                outputStream.write(response.getFileBytes().toByteArray());
            }
        } catch (Exception e) {
            logger.info("Error: Stream response form Holistic3D download");
            logger.error(e.getMessage());
            throw new MindsRestException(e.getMessage());
        }

        return outputStream;
    }
}
