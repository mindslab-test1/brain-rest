package brain.api.room3d.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.room3d.Room3DCommonCode;
import brain.api.room3d.client.Room3DClient;
import brain.api.room3d.data.FileChunkDTO;
import brain.api.room3d.data.KeyMessageDTO;
import brain.api.room3d.data.ProcessStatus;
import brain.api.room3d.data.UploadStatus;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class Room3DService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(Room3DCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);
    private final Room3DClient client;

    public CommonResponse<UploadStatus> upload(FileChunkDTO fileChunkDTO, HttpServletRequest request) throws MindsRestException, IOException, InterruptedException {
        String apiId = Optional.ofNullable(fileChunkDTO.getApiId()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present api-id"));
        String apiKey = Optional.ofNullable(fileChunkDTO.getApiKey()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present api-Key"));
        MultipartFile image = fileChunkDTO.getImage();

        if (validateImage(image)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not found requested image resource");
        }

        if (!validateImageType(image)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported media type.");
        }

        String filename = image.getOriginalFilename().toLowerCase();
        String fileExtension = FilenameUtils.getExtension(filename);
        if (!(fileExtension.equalsIgnoreCase("png"))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported file extension. Please check allow extension(png)");
        }

        if (image.getSize() > (FileUtils.ONE_MB * 2)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "size of the request image is larger than the limit of 2MB");
        }

        logger.info("FileChunkDTO => " + fileChunkDTO);
        validate(apiId, apiKey, Room3DCommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validate for holistic3d-upload");

        UploadStatus upload = client.upload(fileChunkDTO);

        return new CommonResponse<>(new CommonMsg(), upload);
    }

    public CommonResponse<ProcessStatus> statusCheck(KeyMessageDTO keyMessageDTO, HttpServletRequest request) throws MindsRestException {
        String apiId = Optional.ofNullable(keyMessageDTO.getApiId()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present api-id"));
        String apiKey = Optional.ofNullable(keyMessageDTO.getApiKey()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present api-Key"));
        Optional.ofNullable(keyMessageDTO.getFileKey()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present file-key"));

        logger.info("KeyMessageDTO => " + keyMessageDTO);
        validate(apiId, apiKey, Room3DCommonCode.SERVICE_NAME, 0, request);

        logger.debug("check process for holistic3d-statusCheck");
        ProcessStatus processStatus = client.statusCheck(keyMessageDTO);

        return new CommonResponse<>(new CommonMsg(), processStatus);
    }

    public ByteArrayOutputStream download(KeyMessageDTO keyMessageDTO, HttpServletRequest request) throws MindsRestException {
        Optional.ofNullable(keyMessageDTO.getFileKey()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present file-key"));
        validate(keyMessageDTO.getApiId(), keyMessageDTO.getApiKey(), Room3DCommonCode.SERVICE_NAME, 0, request);
        logger.info("holistic 3d download => [" + keyMessageDTO.getFileKey() + "]");
        return client.download(keyMessageDTO.getFileKey());
    }

    private boolean validateImageType(MultipartFile image) {
        return image.getContentType().contains(CommonCode.IMAGE);
    }

    private boolean validateImage(MultipartFile image) {
        return image == null || image.isEmpty() || image.getSize() == 0;
    }
}
