package brain.api.mrc.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.CommonCode;
import brain.api.common.data.BaseRequest;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MrcRestRequest extends BaseRequest implements Serializable {

    private String model;
    private String context;
    private String question;
    private CommonCode.Lang lang;

    public MrcRestRequest(){

    }

    public MrcRestRequest(
        String apiId, String apiKey,
        String model,
        String context,
        String question,
        CommonCode.Lang lang
    ){
        super.setApiId(apiId);
        super.setApiKey(apiKey);
        this.model = model;
        this.context = context;
        this.question = question;
        this.lang = lang;
    }

    public void setModel(String model){
        this.model = model;
    }

    public String getModel(){
        return this.model;
    }

    public void setContext(String context){
        this.context = context;
    }

    public String getContext(){
        return this.context;
    }

    public void setQuestion(String question){
        this.question = question;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setLang(CommonCode.Lang lang){
        this.lang = lang;
    }

    public CommonCode.Lang getLang(){
        return this.lang;
    }
}
