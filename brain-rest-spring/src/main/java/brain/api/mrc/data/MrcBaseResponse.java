package brain.api.mrc.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.data.CommonMsg;

import java.io.Serializable;

// TODO
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MrcBaseResponse implements Serializable {
    private CommonMsg message;
    private String answer;
    private float prob;
    private int startIdx;
    private int endIdx;

    public MrcBaseResponse(CommonMsg message){
        this.message = message;
        this.answer = "";
        this.prob = (float)0.0;
        this.startIdx = 0;
        this.endIdx = 0;
    }

    public MrcBaseResponse(String answer, float prob, int startIdx, int endIdx){
        this.message = new CommonMsg();
        this.answer = answer;
        this.prob = prob;
        this.startIdx = startIdx;
        this.endIdx = endIdx;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setAnswer(String answer){
        this.answer = answer;
    }

    public String getAnswer(){
        return this.answer;
    }

    public void setProb(float prob){
        this.prob = prob;
    }

    public float getProb(){
        return this.prob;
    }

    public void setStartIdx(int startIdx){
        this.startIdx = startIdx;
    }

    public int getStartIdx(){
        return this.startIdx;
    }

    public void setEndIdx(int endIdx){
        this.endIdx = endIdx;
    }

    public int getEndIdx(){
        return this.endIdx;
    }

    @Override
    public String toString() {
        return "MrcBaseResponse{" +
                "message=" + message +
                ", answer='" + answer + '\'' +
                ", prob=" + prob +
                ", startIdx=" + startIdx +
                ", endIdx=" + endIdx +
                '}';
    }
}
