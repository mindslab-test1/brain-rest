package brain.api.mrc.model;

import brain.api.common.model.drama.RabbitMqDto;

public class MrcRabbitMqVO extends RabbitMqDto {

    public MrcRabbitMqVO() {
        setQueue_name("bert_mrc_actor");
        setActor_name("bert_mrc_actor");
    }
}
