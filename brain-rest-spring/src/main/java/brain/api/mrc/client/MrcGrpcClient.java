package brain.api.mrc.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.mrc.MrcCommonCode;
import brain.api.mrc.data.MrcBaseResponse;
import maum.brain.bert.mrc.BertSquadGrpc;
import maum.brain.bert.mrc.Bs;
import org.springframework.stereotype.Component;

@Component
public class MrcGrpcClient extends GrpcClientInterface implements MrcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(MrcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private BertSquadGrpc.BertSquadBlockingStub bertMrcBlockingStub;

    @Override
    public CommonMsg setDestination(String ip, int port){
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.bertMrcBlockingStub = BertSquadGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e){
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    @Override
    public MrcBaseResponse bertSquadMrc(String context, String question){
        Bs.InputText.Builder inputTextBuilder = Bs.InputText.newBuilder();

        logger.debug("context : " + context);
        logger.debug("question : " + question);

        Bs.InputText inputText = inputTextBuilder
                .addContexts(Bs.Context.newBuilder().setContext(context).build())
                .setQuestion(question)
                .setTopK(1)
                .build();
        Bs.Outputs outputs;
        outputs = this.bertMrcBlockingStub.answer(inputText);
        for (Bs.OutputText output : outputs.getAnswersList()) {
            logger.debug(output.getText());
        }

        MrcBaseResponse response = new MrcBaseResponse(
                                            outputs.getAnswers(0).getText(),
                                            outputs.getAnswers(0).getProb(),
                                            outputs.getAnswers(0).getStartIndex(),
                                            outputs.getAnswers(0).getEndIndex()
                                    );
        logger.info("[Legacy MRC] - response : " + response.toString());

        return response;
    }
}

