package brain.api.esr;

public class EsrCommonCode {
    private EsrCommonCode(){}

    public static final String SERVICE_NAME = "ESR";

    public static final String TOO_LARGE_FILE = "Too large Image file size.";
}
