package brain.api.idr.client;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.idr.IdrCommonCode;
import brain.api.idr.data.IdrBaseResponse;
import brain.api.idr.data.IdrBillData;
import brain.api.idr.data.IdrNoteData;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Service
public class IdrFlaskClient {
    private static final CloudApiLogger logger = new CloudApiLogger(IdrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public IdrBaseResponse doIdr(MultipartFile reqFile) throws MindsRestException{
        File file = null;
        try {
            logger.debug("enter clienet");
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost("http://diarl.maum.ai/call_recog");


//            file = new File("/home/aquashdw/maum/tmp/" + UUID.randomUUID());
            file = new File("/home/ubuntu/maum/tmp" + UUID.randomUUID());
            logger.debug("download file");
            reqFile.transferTo(file);
            FileBody fileBody = new FileBody(file);
            logger.debug("set form");

            HttpEntity entity = MultipartEntityBuilder.create()
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                    .addPart("file", fileBody)
                    .build();

            logger.debug("post");

            HttpResponse httpResponse = CommonUtils.sendPostRequest(client, post, entity);
            if(httpResponse.getStatusLine().getStatusCode() != 200){
                throw new MindsRestException(
                        CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getMessage()
                );
            }
            logger.debug("prepare response");

            InputStream inputStream = httpResponse.getEntity().getContent();

            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = (JsonObject) jsonParser.parse(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8)
            );

            logger.info(jsonObject);

            IdrBaseResponse response = new IdrBaseResponse(
                    new CommonMsg()
            );

            String imgClass = jsonObject.get("image_class").getAsString();
            JsonObject meta = jsonObject.getAsJsonObject("meta");
            if(imgClass.equals("BILL"))
                response.setData(new IdrBillData(
                        imgClass
                ));
            else {
                response.setData(new IdrNoteData(
                        imgClass,
                        meta.get("amount").getAsString(),
                        meta.get("confidence_level").getAsDouble(),
                        meta.get("currency").getAsString()
                ));
            }

            logger.info(String.format("Result: %s", response));

            return response;

        } catch (MindsRestException e){
            throw e;
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );
        } finally {
            if(file != null)
                if(!file.delete())
                    logger.warn("file was not mopped up");
                else
                    logger.info("temp file deleted");
        }
    }
}
