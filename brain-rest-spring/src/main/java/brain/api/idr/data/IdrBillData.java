package brain.api.idr.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IdrBillData implements Serializable {
    private String imageClass;

    public IdrBillData(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    @Override
    public String toString() {
        return "IdrBillData{" +
                "imageClass='" + imageClass + '\'' +
                '}';
    }
}
