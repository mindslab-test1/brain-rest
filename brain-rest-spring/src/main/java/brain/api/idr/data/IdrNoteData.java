package brain.api.idr.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IdrNoteData implements Serializable {
    private String imageClass;
    private String amount;
    private double confidence;
    private String currency;

    public IdrNoteData(String imageClass, String amount, double confidence, String currency) {
        this.imageClass = imageClass;
        this.amount = amount;
        this.confidence = confidence;
        this.currency = currency;
    }

    public String getImageClass() {
        return imageClass;
    }

    public void setImageClass(String imageClass) {
        this.imageClass = imageClass;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "IdrNoteData{" +
                "imageClass='" + imageClass + '\'' +
                ", amount='" + amount + '\'' +
                ", confidence=" + confidence +
                ", currency='" + currency + '\'' +
                '}';
    }
}
