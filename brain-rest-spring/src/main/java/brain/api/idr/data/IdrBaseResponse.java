package brain.api.idr.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IdrBaseResponse implements Serializable {
    private CommonMsg message;
    private Object data;

    public IdrBaseResponse(CommonMsg message) {
        this.message = message;
    }

    public IdrBaseResponse(CommonMsg message, Object data) {
        this.message = message;
        this.data = data;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "IdrBaseResponse{" +
                "message=" + message +
                ", data=" + data +
                '}';
    }
}
