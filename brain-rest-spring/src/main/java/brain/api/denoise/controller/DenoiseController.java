package brain.api.denoise.controller;

import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import brain.api.denoise.service.DenoiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.util.UUID;

@RestController
@RequestMapping("/denoise")
public class DenoiseController {
    @Autowired
    DenoiseService denoiseService;

    @RequestMapping(
            value = "/stream",
            method = RequestMethod.POST
    ) public ResponseEntity streamDenoise(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ){
        try {
            ByteArrayOutputStream buffer = denoiseService.streamDenoise(apiId, apiKey, file, httpRequest);
            String filename = "denoised-" + UUID.randomUUID() +".wav";
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (MindsRestException e) {
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
