package brain.api.denoise.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.denoise.DenoiseCommonCode;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import maum.brain.denoise.DenoiseGrpc;
import maum.brain.denoise.DenoiseOuterClass.WavFileBinary;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class DenoiseClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(DenoiseCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    private DenoiseGrpc.DenoiseStub denoiseStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.denoiseStub = DenoiseGrpc.newStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public ByteArrayOutputStream streamDenoise(MultipartFile file){
        final CountDownLatch finishLatch = new CountDownLatch(1);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // STREAM RESPONSE
        StreamObserver<WavFileBinary> requestObserver = denoiseStub.streamDenoise(new StreamObserver<WavFileBinary>() {
            @Override
            public void onNext(WavFileBinary wavFileBinary) {
                try {
                    outputStream.write(wavFileBinary.getBin().toByteArray());
                } catch (IOException e) {
                    logger.info("Error: Stream Response from streamDenoise");
                    throw new InternalException(e.getMessage());
                }
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Error: Stream Response onError from streamDenoise");
                logger.error(t.getMessage());
                finishLatch.countDown();
                throw new InternalException(t.getMessage());
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Stream Response from streamDenoise");
                finishLatch.countDown();
            }
        });
        // STREAM REQUEST
        try {
            WavFileBinary.Builder wavFileBinaryBuilder = WavFileBinary.newBuilder();
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            int len = 0;
            byte[] buffer = new byte[1024*1024];
            while ((len = bis.read(buffer)) > 0){
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(wavFileBinaryBuilder.setBin(inputByteString).build());
            }
        } catch (IOException e) {
            logger.info("Error: Stream request from streamDenoise");
            requestObserver.onError(e);
            throw new InternalException(e.getMessage());
        }
        requestObserver.onCompleted();
        try{
            finishLatch.await(1, TimeUnit.MINUTES);
        }catch (InterruptedException e){
            logger.info("Error: FinishLatch from Avatar processing");
            logger.error(e.getLocalizedMessage());
            throw new InternalException(e.getMessage());
        }
        return outputStream;
    }
}
