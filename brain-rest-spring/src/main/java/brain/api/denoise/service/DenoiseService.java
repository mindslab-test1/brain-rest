package brain.api.denoise.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.exception.MindsRestException;
import brain.api.common.service.BaseService;
import brain.api.denoise.DenoiseCommonCode;
import brain.api.denoise.client.DenoiseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

@Service
public class DenoiseService extends BaseService {
    //Todo: logger

    @Autowired
    DenoiseClient denoiseClient;

    public ByteArrayOutputStream streamDenoise(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DenoiseCommonCode.SERVICE_NAME, 1, request);
        denoiseClient.setDestination("125.132.250.244",36002);
//        denoiseClient.setDestination("10.122.64.65",36002);
        return denoiseClient.streamDenoise(file);
    }
}
