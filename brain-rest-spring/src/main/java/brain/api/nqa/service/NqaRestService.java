package brain.api.nqa.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.service.BaseService;
import brain.api.nqa.NqaCommonCode;
import brain.api.nqa.client.NqaClient;
import brain.api.nqa.data.OpenCloseRes;
import brain.api.nqa.data.TextToTextTalkRes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class NqaRestService extends BaseService {
    Logger logger = LoggerFactory.getLogger(NqaRestService.class);

    @Autowired
    NqaClient nqaClient;

    public OpenCloseRes nqaOpenCall(Map<String, String> params, HttpServletRequest request) throws MindsRestException {
        validate(params.get("apiId"), params.get("apiKey"), NqaCommonCode.SERVICE_NAME, 1, request);
        return nqaClient.openNqa();
    }

    public TextToTextTalkRes nqaTextToTextTalkCall(Map<String, String > params, HttpServletRequest request) throws MindsRestException{
        validate(params.get("apiId"), params.get("apiKey"), NqaCommonCode.SERVICE_NAME, 1, request);
        if(!params.containsKey("utter")){
            System.out.println("utter check");
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage());
        }
        return nqaClient.getTextToTextTalk(params.get("utter"));
    }

    public OpenCloseRes nqaCloseCall(Map<String, String> params, HttpServletRequest request) throws MindsRestException{
        validate(params.get("apiId"), params.get("apiKey"), NqaCommonCode.SERVICE_NAME, 1, request);
        return nqaClient.closeNqa();
    }
}
