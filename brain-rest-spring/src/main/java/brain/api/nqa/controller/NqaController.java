package brain.api.nqa.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.nqa.NqaCommonCode;
import brain.api.nqa.data.OpenCloseRes;
import brain.api.nqa.data.TextToTextTalkRes;
import brain.api.nqa.service.NqaRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/nqa")
public class NqaController {
    private static final CloudApiLogger logger = new CloudApiLogger(NqaCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    NqaRestService nqaService;

    @PostMapping(value = "/openNqa")
    public OpenCloseRes nqaOepn(
            @RequestBody Map <String, String> params,
            HttpServletRequest request
            ){
        try {
            return nqaService.nqaOpenCall(params, request);
        }catch (MindsRestException e){
            return new OpenCloseRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }

    @PostMapping(value = "/talkNqa")
    public TextToTextTalkRes npaTextToTextTalk(
            @RequestBody Map<String, String> params,
            HttpServletRequest request
    ){
        try {
            return nqaService.nqaTextToTextTalkCall(params, request);
        }catch (MindsRestException e){
            return new TextToTextTalkRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }

    @PostMapping(value = "/closeNqa")
    public OpenCloseRes nqaClose(
            @RequestBody Map<String, String> params,
            HttpServletRequest request
    ){
        try{
            return nqaService.nqaCloseCall(params, request);
        }catch (Exception e){
            return new OpenCloseRes(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }
}
