package brain.api.nqa.data;

import brain.api.common.data.CommonMsg;

import java.util.ArrayList;
import java.util.List;

public class TextToTextTalkRes {
    private CommonMsg message;
    private String utter;
    private String layer_info;
    private List<Button> buttons;
    private String meta;

    public TextToTextTalkRes(){
        this.message = new CommonMsg();
        this.buttons = new ArrayList<Button>();
    }

    public TextToTextTalkRes(CommonMsg commonMsg){
        this.message = commonMsg;
        this.utter = null;
        this.layer_info = null;
        this.buttons = new ArrayList<Button>();
        this.meta = null;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getUtter() {
        return utter;
    }

    public void setUtter(String utter) {
        this.utter = utter;
    }

    public String getLayer_info() {
        return layer_info;
    }

    public void setLayer_info(String layer_info) {
        this.layer_info = layer_info;
    }

    public List<Button> getButtons() {
        return buttons;
    }

    public void setButtons(List<Button> buttons) {
        this.buttons = buttons;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public void addBotton(Button button){
        this.buttons.add(button);
    }
}
