package brain.api.nqa.data;

import brain.api.common.data.CommonMsg;

public class OpenCloseRes {
    private CommonMsg message;
    private String utter;

    public OpenCloseRes() {
        this.message = new CommonMsg();
    }

    public OpenCloseRes(CommonMsg commonMsg){
        this.message = commonMsg;
        utter = null;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getUtter() {
        return utter;
    }

    public void setUtter(String utter) {
        this.utter = utter;
    }
}
