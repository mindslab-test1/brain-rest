package brain.api.nqa;


public class NqaCommonCode {
    private NqaCommonCode() {}

    public static final String NQA_ADDRESS = "http://54.180.19.55:9990/api/v3/dialog";
    public static final String SERVICE_NAME = "NQA";
    public static final String OPEN = "open";
    public static final String TEXT_TO_TEXTTALK = "textToTextTalk";
    public static final String CLOSE ="close";
}
