package brain.api.nqa.client;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.nqa.NqaCommonCode;
import brain.api.nqa.data.Button;
import brain.api.nqa.data.OpenCloseRes;
import brain.api.nqa.data.TextToTextTalkRes;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class NqaClient {
    public OpenCloseRes openNqa() throws MindsRestException {
        OpenCloseRes openRes = new OpenCloseRes();
        String url = NqaCommonCode.NQA_ADDRESS + "/" + NqaCommonCode.OPEN;
        JSONObject requestData = new JSONObject();
        JSONObject payload = new JSONObject();

        payload.put("chatbot", "NQA");
        payload.put("lang", "ko_KR");
        payload.put("utter", "");
        payload.put("inputType", "KEYBOARD");

        requestData = makeRquestData();
        requestData.put("payload", payload);

        try {
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(requestNqa(url, requestData));

            if (element.getAsJsonObject().has("directive")) {
                String utter = element.getAsJsonObject().get("directive")
                        .getAsJsonObject().get("payload")
                        .getAsJsonObject().get("response")
                        .getAsJsonObject().get("speech")
                        .getAsJsonObject().get("utter").getAsString();

                openRes.setUtter(utter);
            } else {
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
            }
        } catch (Exception e) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }

        return openRes;
    }

    public TextToTextTalkRes getTextToTextTalk(String utter) throws MindsRestException {
        TextToTextTalkRes textToTextTalkRes = new TextToTextTalkRes();
        String url = NqaCommonCode.NQA_ADDRESS + "/" + NqaCommonCode.TEXT_TO_TEXTTALK;

        JSONObject requestData = new JSONObject();
        JSONObject payload = new JSONObject();

        payload.put("utter", utter);
        payload.put("inputType", "KEYBOARD");
        payload.put("lang", "ko_KR");

        requestData = makeRquestData();
        requestData.put("payload", payload);

        try {
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(requestNqa(url, requestData));

            if (element.getAsJsonObject().has("directive")) {
                JsonObject jsonObject = element.getAsJsonObject().get("directive")
                        .getAsJsonObject().get("payload")
                        .getAsJsonObject().get("response").getAsJsonObject();

                if (jsonObject.get("cards").getAsJsonArray().get(0).getAsJsonObject().has("select")) {
                    JsonObject cardsObject = jsonObject.get("cards").getAsJsonArray()
                            .get(0).getAsJsonObject()
                            .get("select").getAsJsonObject();

                    /* get bottom value */
                    JsonArray itemArray = cardsObject.get("items").getAsJsonArray();
                    itemArray.forEach(item -> {
                        JsonObject jsonItem = (JsonObject) item;
                        Button button = new Button();

                        button.setTitle(jsonItem.get("title").getAsString());

                        textToTextTalkRes.addBotton(button);
                        textToTextTalkRes.setLayer_info(cardsObject.get("header").getAsString());
                    });
                }
                /* */
                textToTextTalkRes.setUtter(jsonObject.get("speech").getAsJsonObject().get("utter").getAsString());
                textToTextTalkRes.setMeta(jsonObject.get("meta").getAsJsonObject().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }

        return textToTextTalkRes;
    }

    public OpenCloseRes closeNqa() {
        String url = NqaCommonCode.NQA_ADDRESS + "/" + NqaCommonCode.CLOSE;

        JSONObject requestData = new JSONObject();
        requestData = makeRquestData();

        requestNqa(url, requestData);

        OpenCloseRes openCloseRes = new OpenCloseRes();
        openCloseRes.setUtter("채팅종료");

        return openCloseRes;
    }

    public String requestNqa(String url, JSONObject requestData) {
        String result = null;

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            post.setHeader("m2u-auth-internal", "m2u-auth-internal");
            StringEntity entity = new StringEntity(requestData.toString(), "UTF-8");
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
            }

            result = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public JSONObject makeRquestData() {
        JSONObject requestData = new JSONObject();
        JSONObject device = new JSONObject();
        JSONObject location = new JSONObject();

        device.put("id", "admin_1234");
        device.put("type", "WEB");
        device.put("version", "0.1v");
        device.put("channel", "CHANNEL01");

        location.put("latitude", 10.3);
        location.put("longitude", 20.5);
        location.put("location", "mindslab");

        requestData.put("device", device);
        requestData.put("location", location);
        requestData.put("authToken", "1602a950-e651-11e1-84be-00145e76c700");

        return requestData;
    }

}
