package brain.api.xdc.client;

import brain.api.common.data.CommonMsg;
import brain.api.xdc.data.XdcBaseResponse;

public interface XdcClientInterface {
    public XdcBaseResponse bertXdc(String reqText);
    public CommonMsg setDestination(String ip, int port);
}
