package brain.api.xdc.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.legacy.data.xdc.*;
import brain.api.xdc.XdcCommonCode;
import brain.api.xdc.data.XdcAttention;
import brain.api.xdc.data.XdcBaseResponse;
import brain.api.xdc.data.XdcLabel;
import io.grpc.ManagedChannel;
import maum.brain.bert.xdc.BertXDCGrpc;
import maum.brain.bert.xdc.Bx;
import maum.brain.han.run.HanClassifierGrpc;
import maum.brain.han.run.HanRun;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.Nlp;
import maum.common.LangOuterClass;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class XdcGrpcClient extends GrpcClientInterface implements XdcClientInterface {
    private static final CloudApiLogger logger =
            new CloudApiLogger(XdcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private static final String NLP_IP = "125.132.250.242";
    private static final int NLP_PORT = 9823;
    private static final String HAN_IP = "125.132.250.243";
    private static final int HAN_PORT = 40001;

    public XdcResponseLegacy xdcGetClassLegacy(String text){
        logger.debug("get communication");
        logger.debug("get channel");
        ManagedChannel hanChannel = CommonUtils.getManagedChannel(HAN_IP, HAN_PORT);
        try {
            HanClassifierGrpc.HanClassifierBlockingStub hanStub = HanClassifierGrpc.newBlockingStub(hanChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            Nlp.Document nlpDoc = this.makeNlpDocument(text);
            HanRun.HanResult result = hanStub.getClass(
                    this.makeInputDocument(
                            nlpDoc
                    )
            );

            XdcResponseLegacy response = this.makeResponseFromResult(result, nlpDoc);
            logger.info("[Legacy XDC] - response : " + response);

            return response;
        } finally {
            hanChannel.shutdown();
        }
    }

    private Nlp.Document makeNlpDocument(String text){
        ManagedChannel nlpChannel = CommonUtils.getManagedChannel(NLP_IP, NLP_PORT);
        try {
            NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub nlpStub =
                    NaturalLanguageProcessingServiceGrpc.newBlockingStub(
                            nlpChannel
                    ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return nlpStub.analyze(this.makeNlpInputText(text));
        } catch (Exception e){
            logger.debug(e.getMessage());
            nlpChannel.shutdown();
            return null;
        }
    }

    private HanRun.HanInputCommon makeHanCommon(){
        return HanRun.HanInputCommon.newBuilder()
                .setLang(LangOuterClass.LangCode.kor)
                .setTopN(3)
                .setUseAttnOutput(true)
                .build();
    }

    private HanRun.HanInputDocument makeInputDocument(Nlp.Document document){
        return HanRun.HanInputDocument.newBuilder()
                .setCommon(this.makeHanCommon())
                .setDocument(document)
                .build();
    }

    private Nlp.InputText makeNlpInputText(String text){
        Nlp.InputText.Builder builder = Nlp.InputText.newBuilder();
        return builder
                .setText(text)
                .setLang(LangOuterClass.LangCode.kor)
                .setSplitSentence(true)
                .setUseTokenizer(false)
                .setKeywordFrequencyLevelValue(2)
                .build();
    }

    private XdcResponseLegacy makeResponseFromResult(HanRun.HanResult rawResult, Nlp.Document nlpResults){
        List<Nlp.Sentence> nlpSentences = nlpResults.getSentencesList();
        ArrayList<String> sentences = new ArrayList<>();

        for(Nlp.Sentence nlpSentence: nlpSentences)
            sentences.add(nlpSentence.getText());

        List<HanRun.HanClassified> rawResultClsList = rawResult.getClsList();
        List<HanRun.SentenceAttention> rawSentAttentions = rawResult.getSentAttnsList();

        ArrayList<Classification> classifications = new ArrayList<>();
        ArrayList<List<Integer>> topIndicesWordList = new ArrayList<>();

        for(HanRun.HanClassified cls: rawResultClsList){
            classifications.add(
                    new Classification(cls.getLabel(), cls.getProbability())
            );
        }

        ArrayList<SentenceAttention> sentenceAttentions = new ArrayList<>();
        ArrayList<WordAttention> wordAttentions = new ArrayList<>();

        ArrayList<attentionSort> sentAttentionsIndex = new ArrayList<>();
        ArrayList<attentionSort> wordAttentionsIndex = new ArrayList<>();

        ArrayList<Integer> topIndicesSent = new ArrayList<>();
        ArrayList<Integer> topIndicesWord = new ArrayList<>();

        int sIndex;
        for(sIndex = 0; sIndex < rawSentAttentions.size(); sIndex++){
            HanRun.SentenceAttention sentenceAttention = rawSentAttentions.get(sIndex);
            wordAttentions.clear();
            wordAttentionsIndex.clear();
            int wIndex;
            for(wIndex = 0; wIndex < sentenceAttention.getWordAttnsList().size(); wIndex++){
                HanRun.WordAttention wordAttention = sentenceAttention.getWordAttns(wIndex);
                logger.debug("word index: " + wIndex);
                logger.debug("start: " + wordAttention.getStart());
                logger.debug("end: " + wordAttention.getEnd());
                logger.debug("weight: " + wordAttention.getWeight());
                wordAttentions.add(
                        new WordAttention(
                                wordAttention.getStart(),
                                wordAttention.getEnd(),
                                wordAttention.getWeight()
                        )
                );
                wordAttentionsIndex.add(
                        new attentionSort(wIndex, wordAttention.getWeight())
                );
            }
            Collections.sort(wordAttentionsIndex);
            for(attentionSort tmp: wordAttentionsIndex){
                logger.debug(tmp);
            }
            topIndicesWord.clear();
            int cntWords = wordAttentions.size();
            int ret = 3;
            if(cntWords < 3) ret = cntWords;
            for(int i = 0; i < ret; i++) {
                logger.debug(wordAttentionsIndex.get(cntWords - i - 1).getIndex());
                logger.debug(wordAttentionsIndex.get(cntWords - i - 1).getWeight());
                topIndicesWord.add(wordAttentionsIndex.get(cntWords - i - 1).getIndex());
            }
            logger.debug("");
            topIndicesWordList.add((List<Integer>) topIndicesWord.clone());

            sentenceAttentions.add(
            new SentenceAttention(
                            sentenceAttention.getWeight(),
                            wordAttentions
                    )
            );
            sentAttentionsIndex.add(
                    new attentionSort(sIndex,sentenceAttention.getWeight())
            );
            logger.debug("sent index: " + sIndex);
            logger.debug("weight: " + sentenceAttention.getWeight());
        }
        Collections.sort(sentAttentionsIndex);
        for(attentionSort tmp: sentAttentionsIndex){
            logger.debug(tmp);
        }
        int cntSents = sentenceAttentions.size();
        int ret = 3;
        if(cntSents < 3) ret = cntSents;
        for(int i = 0; i < ret; i++) {
            logger.debug(sentAttentionsIndex.get(cntSents - i - 1).getIndex());
            logger.debug(sentAttentionsIndex.get(cntSents - i - 1).getWeight());
            topIndicesSent.add(sentAttentionsIndex.get(cntSents - i - 1).getIndex());
        }

        XdcData xdcData = new XdcData();
        xdcData.setCls(classifications);
        xdcData.setSent_attns(sentenceAttentions);
        xdcData.setTop_sent_indices(topIndicesSent);
        xdcData.setTop_word_indices_lists(topIndicesWordList);
        xdcData.setSentences(sentences);

        return new XdcResponseLegacy(
                xdcData,
                "Success"
        );
    }

    public class attentionSort implements Comparable<attentionSort>{
        private int index;
        private Float weight;

        private attentionSort(int index, Float weight) {
            this.index = index;
            this.weight = weight;
        }

        private int getIndex() {
            return this.index;
        }

        private void setIndex(int index) {
            this.index = index;
        }

        private Float getWeight() {
            return this.weight;
        }

        private void setWeight(float weight) {
            this.weight = weight;
        }

        @Override
        public String toString() {
            return "attentionSort{" +
                    "index=" + index +
                    ", weight=" + weight +
                    '}';
        }

        @Override
        public int compareTo(attentionSort attentionSort){
            return this.getWeight().compareTo(attentionSort.getWeight());
        }
    }

    /////////////////////////////////////////////////////////////////////

    private BertXDCGrpc.BertXDCBlockingStub bertXdcStub;

    @Override
    public CommonMsg setDestination(String ip, int port) {
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.bertXdcStub = BertXDCGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    @Override
    public XdcBaseResponse bertXdc(String reqText) {
        Bx.Outputs outputs = bertXdcStub.answer(
            Bx.InputText.newBuilder()
                    .setContext(reqText)
                    .build()
        );
        ArrayList<XdcLabel> labels = new ArrayList<>();
        ArrayList<XdcAttention> wordAttentions = new ArrayList<>();
        ArrayList<XdcAttention> sentAttentions = new ArrayList<>();
        for (Bx.Classify label:
            outputs.getLabelsList())
            labels.add(new XdcLabel(label.getLabel(), label.getProb()));
        for (Bx.Attention attn:
                outputs.getWordIdxesList())
            wordAttentions.add(new XdcAttention(attn.getStart(), attn.getEnd(), attn.getWeight()));
        for (Bx.Attention attn:
                outputs.getSentIdxesList())
            sentAttentions.add(new XdcAttention(attn.getStart(), attn.getEnd(), attn.getWeight()));

        Collections.sort(wordAttentions);
        Collections.sort(sentAttentions);

        XdcBaseResponse xdcBaseResponse =  new XdcBaseResponse(
                new CommonMsg(),
                reqText,
                labels,
                wordAttentions,
                sentAttentions
        );
        logger.info(String.format("Result: %s", xdcBaseResponse));
        return xdcBaseResponse;
    }
}
