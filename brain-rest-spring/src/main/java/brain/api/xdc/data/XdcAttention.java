package brain.api.xdc.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class XdcAttention implements Serializable, Comparable<XdcAttention> {
    private int startIdx;
    private int endIdx;
    private double weight;

    public XdcAttention(int startIdx, int endIdx, double weight) {
        this.startIdx = startIdx;
        this.endIdx = endIdx;
        this.weight = weight;
    }

    public int getStartIdx() {
        return startIdx;
    }

    public void setStartIdx(int startIdx) {
        this.startIdx = startIdx;
    }

    public int getEndIdx() {
        return endIdx;
    }

    public void setEndIdx(int endIdx) {
        this.endIdx = endIdx;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public int compareTo(@NotNull XdcAttention o) {
        return this.startIdx - o.startIdx;
    }

    @Override
    public boolean equals(Object o){
        if (o != null) {
            return this.toString().equals(o.toString());
        }
        else return false;
    }

    @Override
    public int hashCode(){
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return "XdcAttention{" +
                "startIdx=" + startIdx +
                ", endIdx=" + endIdx +
                ", weight=" + weight +
                '}';
    }
}
