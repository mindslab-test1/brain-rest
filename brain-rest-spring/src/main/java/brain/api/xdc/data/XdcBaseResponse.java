package brain.api.xdc.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.data.CommonMsg;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class XdcBaseResponse implements Serializable {
    private CommonMsg message;
    private String context;
    private List<XdcLabel> labels;
    private List<XdcAttention> wordIndices;
    private List<XdcAttention> sentenceIndices;

    public XdcBaseResponse(
            CommonMsg message,
            List<XdcLabel> labels,
            List<XdcAttention> wordIndices,
            List<XdcAttention> sentenceIndices
    ) {
        this.message = message;
        this.labels = labels;
        this.wordIndices = wordIndices;
        this.sentenceIndices = sentenceIndices;
    }

    public XdcBaseResponse(CommonMsg message, String context, List<XdcLabel> labels, List<XdcAttention> wordIndices, List<XdcAttention> sentenceIndices) {
        this.message = message;
        this.context = context;
        this.labels = labels;
        this.wordIndices = wordIndices;
        this.sentenceIndices = sentenceIndices;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public List<XdcLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<XdcLabel> labels) {
        this.labels = labels;
    }

    public List<XdcAttention> getWordIndices() {
        return wordIndices;
    }

    public void setWordIndices(List<XdcAttention> wordIndices) {
        this.wordIndices = wordIndices;
    }

    public List<XdcAttention> getSentenceIndices() {
        return sentenceIndices;
    }

    public void setSentenceIndices(List<XdcAttention> sentenceIndices) {
        this.sentenceIndices = sentenceIndices;
    }

    @Override
    public String toString() {
        return "XdcBaseResponse{" +
                "message=" + message +
                ", context='" + context + '\'' +
                ", labels=" + labels +
                ", wordIndices=" + wordIndices +
                ", sentenceIndices=" + sentenceIndices +
                '}';
    }
}
