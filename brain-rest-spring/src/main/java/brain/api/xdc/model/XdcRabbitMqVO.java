package brain.api.xdc.model;

import brain.api.common.model.drama.RabbitMqDto;

public class XdcRabbitMqVO extends RabbitMqDto {

    public XdcRabbitMqVO() {
        setQueue_name("bert_xdc_actor");
        setActor_name("bert_xdc_actor");
    }
}
