package brain.api.vsr.service;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.vsr.VsrCommonCode;
import brain.api.vsr.client.VsrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;

import static java.lang.Thread.sleep;

@Service
public class VsrService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(VsrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    VsrClient vsrClient;

    public ByteArrayOutputStream downloadVsr(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws InterruptedException, MindsRestException {
        validate(apiId, apiKey, VsrCommonCode.SERVICE_NAME, 1, request);
        vsrClient.setDestination("182.162.19.12", 50051);
        String key= vsrClient.upload(file);
        int count = 0;
        while (vsrClient.checkProcess(key) != 8){
            if(vsrClient.checkProcess(key) == 11){ throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "File Processing Error"); }
            count++;
            if(count >= 100){ throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    "Error: Download VSR timeout"); }
            sleep(6000);
        }
        return vsrClient.download(key);
    }
}
