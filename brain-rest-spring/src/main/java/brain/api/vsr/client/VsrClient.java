package brain.api.vsr.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.vsr.VsrCommonCode;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import maum.brain.resolution.Resolution;
import maum.brain.resolution.SuperResolutionGRPCGrpc;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class VsrClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(VsrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private SuperResolutionGRPCGrpc.SuperResolutionGRPCBlockingStub vsrBlockingStub;
    private SuperResolutionGRPCGrpc.SuperResolutionGRPCStub vsrStub;

    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("Set destination %s, %d", ip, port));
            this.vsrBlockingStub = SuperResolutionGRPCGrpc.newBlockingStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            this.vsrStub = SuperResolutionGRPCGrpc.newStub(CommonUtils.getManagedChannel(ip, port)).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        }catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public String upload(MultipartFile file) throws InterruptedException, MindsRestException {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        final Integer[] status = {null};
        final String[] message = {null};

        // RESPONSE
        StreamObserver<Resolution.UploadStatus> responseObserver = new StreamObserver<Resolution.UploadStatus>() {
            @Override
            public void onNext(Resolution.UploadStatus uploadStatus) {
                status[0] = uploadStatus.getStatus();

                if(status[0] == 1) message[0] = uploadStatus.getFileKey();
                else message[1] = uploadStatus.getErrorMsg();
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Error: Response onError from vsr upload");
                logger.error(t.getMessage());
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("Finished: Response from vsr upload");
                finishLatch.countDown();
            }
        };
        // STREAM REQUEST
        StreamObserver<Resolution.FileChunk> requestObserver = vsrStub.upload(responseObserver);
        try{
            Resolution.FileChunk.Builder vsrBuilder = Resolution.FileChunk.newBuilder();
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            int len = 0;
            byte[] buffer = new byte[1024 * 512];
            while ((len = bis.read(buffer)) > 0){
                ByteString inputByteString = ByteString.copyFrom(buffer);
                requestObserver.onNext(vsrBuilder.setFileBytes(inputByteString).build());
            }
        }catch (Exception e){
            logger.info("Error: Stream request from vsr upload");
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage());
        }
        requestObserver.onCompleted();
        finishLatch.await(1, TimeUnit.MINUTES);

        if(status[0] == 1){
            return message[0];
        }
        else {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), message[1]);
        }
    }

    public Integer checkProcess(String key) throws MindsRestException {
        // REQUEST
        Resolution.KeyMessage request = Resolution.KeyMessage.newBuilder().setFileKey(key).build();
        // RESPONSE
        Resolution.ProcessStatus response = null;
        try{
            response = vsrBlockingStub.checkProcess(request);
        }catch (Exception e){
            logger.info("Error: response from vsr checkProcess");
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage());
        }
        return response.getStatus();
    }

    public ByteArrayOutputStream download(String key) throws MindsRestException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // REQUEST
        Resolution.KeyMessage request = Resolution.KeyMessage.newBuilder().setFileKey(key).build();
        // STREAM RESPONSE
        Iterator<Resolution.FileChunk> responseStream = vsrBlockingStub.download(request);
        try{
            for(int i = 1; responseStream.hasNext(); i++){
                Resolution.FileChunk response = responseStream.next();
                outputStream.write(response.getFileBytes().toByteArray());
            }
        } catch (Exception e) {
            logger.info("Error: Stream response from vsr download");
            logger.error(e.getMessage());
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage());
        }
        return outputStream;
    }
}
