package brain.api.lipsync;

import brain.api.common.CommonCode;
import brain.api.common.RedisCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.RedisService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Duration;

@RequiredArgsConstructor
@Component
public class LipsyncKeyCollector {
    private final CloudApiLogger logger = new CloudApiLogger(
            String.format("%s-%s", LipSyncCommonCode.SERVICE_NAME, RedisCode.SERVICE_NAME),
            CommonCode.COMMON_FIRST_CLASS_COLLECTION);
    private final RedisService<LipsyncKeyHolder> redisService;

    public String getExtensionByTransparent(boolean transparent, byte[] image) {
        // TODO: Refactoring Target, Value Injection으로 추후 변경 필요
        return transparent && (image == null || image.length == 0) ? "webm" : "mp4";
    }

    public void createLipsyncKey(String requestKey, String fileExtension, String channelKey, String version) throws MindsRestException {
        redisService.saveWithTime(requestKey, new LipsyncKeyHolder(fileExtension, channelKey, version), Duration.ofMinutes(LipSyncCommonCode.REQUEST_KEY_ALIVE_TIME_MINUTES));
        logger.info(String.format("Created %s", redisService.get(requestKey)));
    }

    public LipsyncKeyHolder getByRequestKey(String requestKey) throws MindsRestException {
        LipsyncKeyHolder lipsyncKeyHolder = redisService.get(requestKey);
        logger.info(String.format("Get %s", lipsyncKeyHolder));
        return lipsyncKeyHolder;
    }

    public LipsyncKeyHolder removeByRequestKey(String requestKey) throws MindsRestException {
        LipsyncKeyHolder lipsyncKeyHolder = getByRequestKey(requestKey);
        redisService.remove(requestKey);
        logger.info(String.format("Removed %s", lipsyncKeyHolder));
        return lipsyncKeyHolder;
    }

}

