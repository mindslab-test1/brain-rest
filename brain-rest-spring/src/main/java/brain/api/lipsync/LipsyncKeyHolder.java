package brain.api.lipsync;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Builder
@Getter
@Setter
@ToString
public class LipsyncKeyHolder implements Serializable {
    private String fileExtension;
    private String channelKey;
    private String version;
}
