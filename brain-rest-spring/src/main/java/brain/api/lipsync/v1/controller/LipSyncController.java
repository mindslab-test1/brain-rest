package brain.api.lipsync.v1.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.lipsync.LipSyncCommonCode;
import brain.api.lipsync.LipsyncKeyCollector;
import brain.api.lipsync.v1.dto.LipSyncDataResponse;
import brain.api.lipsync.v1.dto.LipSyncDownloadRequestDTO;
import brain.api.lipsync.v1.service.LipSyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Map;

@RestController("V1_LipSyncController")
@RequestMapping("/lipsync")
public class LipSyncController {
    private static final CloudApiLogger logger = new CloudApiLogger(LipSyncCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    private final LipSyncService service;

    private final LipsyncKeyCollector lipsyncKeyCollector;

    public LipSyncController(
            @Autowired @Qualifier("V1_LipSyncService") LipSyncService service,
            @Autowired LipsyncKeyCollector lipsyncKeyCollector
    ) {
        this.service = service;
        this.lipsyncKeyCollector = lipsyncKeyCollector;
    }

    @PostMapping(
            value = "/upload",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity upload(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("text") String text,
            @RequestParam(value = "image", required = false) MultipartFile image,
            @RequestParam(value = "model", defaultValue = "baseline") String model,
            @RequestParam(value = "transparent", defaultValue = "false") boolean transparent,
            @RequestParam(value = "resolution", defaultValue = "HD") String resolution,
            HttpServletRequest request
    ) throws InterruptedException {
        logger.debug(image);
        logger.debug(model);
        logger.debug(transparent);
        logger.debug(resolution);
        try {
            LipSyncDataResponse response = service.upload(
                    apiId,
                    apiKey,
                    text,
                    model,
                    resolution,
                    transparent,
                    image,
                    request
            );

            return ResponseEntity.ok(response);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (InterruptedException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.COMMON_ERR_INTERNAL.getStatus()).body(new ErrorResponse());
        }
    }

    @PostMapping(
            value = "/statusCheck",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity statusCheck(
            @RequestBody Map<String, String> params,
            HttpServletRequest request
    ) {
        try {
            LipSyncDataResponse response = service.statusCheck(params.get("apiId"), params.get("apiKey"), params.get("requestKey"), request);
            return ResponseEntity.ok(response);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
            return ResponseEntity
                    .status(CommonExceptionCode.getHttpStatusOnException(e))
                    .body(new LipSyncDataResponse(
                            new CommonMsg(
                                    e.getErrCode(),
                                    e.getMessage()
                            ), null
                    ));
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "/download",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public ResponseEntity download(
            @RequestBody LipSyncDownloadRequestDTO lipsyncDownloadRequestDTO,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            ByteArrayOutputStream responseOutputStream = service.download(lipsyncDownloadRequestDTO, request);
            String requestKey = lipsyncDownloadRequestDTO.getRequestKey();
            String fileExtension = lipsyncKeyCollector.removeByRequestKey(requestKey).getFileExtension();

            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=maum_ai_lipsync_%s.%s",
                            lipsyncDownloadRequestDTO.getRequestKey(),
                            fileExtension
                        )
                    )
                    .body(responseOutputStream.toByteArray());
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e))
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(new LipSyncDataResponse(
                            new CommonMsg(
                                    e.getErrCode(),
                                    e.getMessage()
                            ), null
                    ));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(500).contentType(MediaType.APPLICATION_JSON_UTF8).build();
        }
    }
}
