package brain.api.lipsync.v1.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LipSyncStatus implements Serializable {
    private int statusCode;
    private String message;
    private int waiting;

    public LipSyncStatus() {
    }

    public LipSyncStatus(int statusCode, String message, int waiting) {
        this.statusCode = statusCode;
        this.message = message;
        this.waiting = waiting;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getWaiting() {
        return waiting;
    }

    public void setWaiting(int waiting) {
        this.waiting = waiting;
    }

    @Override
    public String toString() {
        return "LipSyncStatus{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", waiting=" + waiting +
                '}';
    }
}
