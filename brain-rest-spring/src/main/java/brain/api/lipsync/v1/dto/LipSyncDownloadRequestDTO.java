package brain.api.lipsync.v1.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LipSyncDownloadRequestDTO {
    private String requestKey;
    private String apiId;
    private String apiKey;
}
