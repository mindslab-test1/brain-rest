package brain.api.lipsync.v1.dto;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LipSyncDataResponse implements Serializable {
    private CommonMsg message;
    private Serializable payload;

    public LipSyncDataResponse() {
    }

    public LipSyncDataResponse(CommonMsg message, Serializable payload) {
        this.message = message;
        this.payload = payload;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Serializable getPayload() {
        return payload;
    }

    public void setPayload(Serializable payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "LipSyncUploadResponse{" +
                "message=" + message +
                ", payload=" + payload +
                '}';
    }
}
