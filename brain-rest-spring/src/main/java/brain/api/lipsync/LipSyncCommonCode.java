package brain.api.lipsync;

public class LipSyncCommonCode {
    private LipSyncCommonCode(){}

    public static final String NOT_SUPPORT_V1_WARNING_MESSAGE = "Lipsync v1 will be deprecated.";
    public static final String SERVICE_NAME = "LIPSYNC";
    public static final String INFERENCE_DOT = "  .";
    public static final Long REQUEST_KEY_ALIVE_TIME_MINUTES = 10L;
}
