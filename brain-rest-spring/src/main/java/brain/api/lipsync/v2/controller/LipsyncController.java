package brain.api.lipsync.v2.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.lipsync.LipSyncCommonCode;
import brain.api.lipsync.LipsyncKeyCollector;
import brain.api.lipsync.v1.dto.LipSyncDataResponse;
import brain.api.lipsync.v1.dto.LipSyncDownloadRequestDTO;
import brain.api.lipsync.v2.dto.LipsyncUploadDto;
import brain.api.lipsync.v2.service.LipSyncService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/lipsync/v2")
public class LipsyncController {
    private static final CloudApiLogger logger = new CloudApiLogger(LipSyncCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    private final LipSyncService service;
    private final LipsyncKeyCollector lipsyncKeyCollector;

    @PostMapping(
            value = "/upload",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity upload(
            @Validated LipsyncUploadDto lipsyncUploadDto,
            HttpServletRequest request
    ) throws InterruptedException {
        logger.debug(lipsyncUploadDto.getBackgroundImage());
        logger.debug(lipsyncUploadDto.getModel());
        logger.debug(lipsyncUploadDto.isTransparent());
        logger.debug(lipsyncUploadDto.getResolution());
        try {
            LipSyncDataResponse response = service.upload(
                    lipsyncUploadDto.getApiId(),
                    lipsyncUploadDto.getApiKey(),
                    lipsyncUploadDto.getText(),
                    lipsyncUploadDto.getModel(),
                    lipsyncUploadDto.getResolution(),
                    lipsyncUploadDto.isTransparent(),
                    lipsyncUploadDto.getBackgroundImage(),
                    request
            );

            return ResponseEntity.ok(response);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (InterruptedException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.COMMON_ERR_INTERNAL.getStatus()).body(new ErrorResponse());
        }
    }

    @PostMapping(
            value = "/statusCheck",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity statusCheck(
            @RequestBody Map<String, String> params,
            HttpServletRequest request
    ) {
        try {
            LipSyncDataResponse response = service.statusCheck(params.get("apiId"), params.get("apiKey"), params.get("requestKey"), request);
            return ResponseEntity.ok(response);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
            return ResponseEntity
                    .status(CommonExceptionCode.getHttpStatusOnException(e))
                    .body(new LipSyncDataResponse(
                            new CommonMsg(
                                    e.getErrCode(),
                                    e.getMessage()
                            ), null
                    ));
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(e.getStackTrace());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "/download",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public ResponseEntity download(
            @RequestBody LipSyncDownloadRequestDTO lipsyncDownloadRequestDTO,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            ByteArrayOutputStream responseOutputStream = service.download(lipsyncDownloadRequestDTO, request);

            String requestKey = lipsyncDownloadRequestDTO.getRequestKey();
            String fileExtension = lipsyncKeyCollector.removeByRequestKey(requestKey).getFileExtension();
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=maum_ai_lipsync_%s.%s",
                            lipsyncDownloadRequestDTO.getRequestKey(),
                            fileExtension
                        )
                    )
                    .body(responseOutputStream.toByteArray());
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e))
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(new LipSyncDataResponse(
                            new CommonMsg(
                                    e.getErrCode(),
                                    e.getMessage()
                            ), null
                    ));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(500).contentType(MediaType.APPLICATION_JSON_UTF8).build();
        }
    }
}
