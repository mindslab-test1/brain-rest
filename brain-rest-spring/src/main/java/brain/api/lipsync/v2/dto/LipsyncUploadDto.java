package brain.api.lipsync.v2.dto;

import brain.api.lipsync.ResolutionType;
import brain.api.lipsync.v2.data.Resolution;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class LipsyncUploadDto {
    @NotNull
    private String apiId;
    @NotNull
    private String apiKey;
    @NotNull
    private String text;

    private MultipartFile backgroundImage;
    @NotNull
    private String model;
    @NotNull
    private boolean transparent;
    @NotNull
    private Resolution resolution;

    public LipsyncUploadDto() {
        model = "baseline";
        transparent = false;
        resolution = ResolutionType.SD.getResolution();
    }
}
