package brain.api.lipsync.v2.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Resolution {
    private Integer width;
    private Integer height;

    public Resolution(Integer width, Integer height) {
        this.width = width;
        this.height = height;
    }
}
