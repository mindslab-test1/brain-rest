package brain.api.lipsync.v2.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.server.ChannelDto;
import brain.api.common.service.BaseService;
import brain.api.lipsync.LipSyncCommonCode;
import brain.api.lipsync.LipsyncKeyCollector;
import brain.api.lipsync.v1.data.LipSyncStatus;
import brain.api.lipsync.v1.data.LipsyncStatusCode;
import brain.api.lipsync.v1.dto.LipSyncDataResponse;
import brain.api.lipsync.v1.dto.LipSyncDownloadRequestDTO;
import brain.api.lipsync.v2.client.LipSyncClient;
import brain.api.lipsync.v2.data.Resolution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class LipSyncService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(LipSyncCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    private final LipSyncClient client;
    private final LipsyncKeyCollector lipsyncKeyCollector;

    public LipSyncService(
            @Autowired LipSyncClient client,
            @Autowired LipsyncKeyCollector lipsyncKeyCollector
    ){
        this.client = client;
        this.lipsyncKeyCollector = lipsyncKeyCollector;
    }

    public LipSyncDataResponse upload(
        String apiId,
        String apikey,
        String text,
        String model,
        Resolution resolution,
        boolean transparent,
        MultipartFile file, HttpServletRequest request
    ) throws MindsRestException, IOException, InterruptedException {
        validate(apiId, apikey, LipSyncCommonCode.SERVICE_NAME, 1, request);
        if (text.trim().isEmpty()) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "Please enter text");
        }
        ChannelDto dto = serverDao.getLipSyncServerInfo(model);
        if(dto == null) throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                "no usable model present");

        logger.info("Sending to Engine: " + apiId + " => text= " + text + ", textByteLength= " + text.getBytes().length + ", model= "
                + model + ", version= " + dto.getVersion() + ", resolution= " + resolution + ", transparent= " + transparent);

        checkModelApiVersion(dto.getVersion());

        LipSyncDataResponse lipSyncDataResponse = new LipSyncDataResponse(new CommonMsg(), client.upload(
                text + LipSyncCommonCode.INFERENCE_DOT,
                file == null ? null : file.getBytes(),
                resolution,
                transparent,
                dto.getHost(),
                dto.getPort(),
                dto.getVersion()
        ));

        logger.info("Response Received: " + apiId + " => text= " + text + ", textByteLength= " + text.getBytes().length + ", model= "
                + model + ", version= " + dto.getVersion() + ", resolution= " + resolution + ", transparent= " + transparent + ", requestKey= " + lipSyncDataResponse.getPayload());

        return lipSyncDataResponse;
    }

    public LipSyncDataResponse statusCheck(
            String apiId, String apiKey, String requestKey, HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, LipSyncCommonCode.SERVICE_NAME, 0, request);

        String modelApiVersion = lipsyncKeyCollector.getByRequestKey(requestKey).getVersion();
        checkModelApiVersion(modelApiVersion);

        LipSyncStatus lipSyncStatus = client.statusCheck(requestKey);
        CommonMsg commonMsg;
        switch (LipsyncStatusCode.fromOrdinal(lipSyncStatus.getStatusCode())) {
            case NOT_YET:
            case PROCESSING:
            case DONE:
                commonMsg = new CommonMsg(CommonCode.COMMON_SUCCESS,
                        CommonCode.COMMON_MSG_SUCCESS);
                break;
            case DELETED:
                commonMsg = new CommonMsg(CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getErrCode()
                        , CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getMessage());
                break;
            case WRONG_KEY:
                commonMsg = new CommonMsg(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage());
                break;
            default:
                commonMsg = new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
                break;
        }
        return new LipSyncDataResponse(commonMsg, lipSyncStatus);
    }

    public ByteArrayOutputStream download(
            LipSyncDownloadRequestDTO lipSyncDownloadRequestDTO,
            HttpServletRequest request
    ) throws MindsRestException {
        validate(lipSyncDownloadRequestDTO.getApiId(), lipSyncDownloadRequestDTO.getApiKey(),
                LipSyncCommonCode.SERVICE_NAME, 0,  request);

        String modelApiVersion = lipsyncKeyCollector.getByRequestKey(lipSyncDownloadRequestDTO.getRequestKey()).getVersion();
        checkModelApiVersion(modelApiVersion);

        ByteArrayOutputStream byteArrayOutputStream = client.download(lipSyncDownloadRequestDTO.getRequestKey());
        logger.info("Download: " + lipSyncDownloadRequestDTO.getApiId() + " => " + lipSyncDownloadRequestDTO.getRequestKey());
        return byteArrayOutputStream;
    }

    private void checkModelApiVersion(String modelApiVersion) throws MindsRestException {
        if (!"v2".equalsIgnoreCase(modelApiVersion)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    String.format("The requested requestKey does not yet support v2. (requested version=%s)", modelApiVersion));
        }
    }
}
