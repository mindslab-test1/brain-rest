package brain.api.lipsync;

import brain.api.lipsync.v1.data.LipsyncStatusCode;
import brain.api.lipsync.v2.data.Resolution;
import lombok.Getter;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Getter
public enum ResolutionType {
    SD(new Resolution(720, 480), "Standard Definition"),
    HD(new Resolution(1280, 720), "High Definition"),
    FHD(new Resolution(1920, 1080), "Full HD"),
    QHD(new Resolution(2560, 1440), "Quad HD"),
    UHD(new Resolution(3840, 2160), "Ultra HD");

    private Resolution resolution;
    private String description;

    private static final Map<Integer, ResolutionType> RESOLUTION_TYPE_MAP = Stream.of(values()).collect(toMap(e -> e.resolution.getWidth(), e -> e));

    ResolutionType(Resolution resolution, String description) {
        this.resolution = resolution;
        this.description = description;
    }

}
