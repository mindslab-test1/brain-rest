package brain.api.d2a.client;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.d2a.D2ACommonCode;
import io.grpc.ManagedChannel;
import maum.brain.d2a.precedent.Jose;
import maum.brain.d2a.precedent.Precedent2ArticleGrpc;
import maum.brain.d2a.table.Table2ArticleGrpc;
import maum.brain.d2a.table.Totto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class D2AClient {
    private static final CloudApiLogger logger = new CloudApiLogger(D2ACommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private final ManagedChannel precedentChannel;
    private final ManagedChannel tableChannel;

    public D2AClient(){
        precedentChannel = CommonUtils.getManagedChannel("182.162.19.8", 35016);
        tableChannel = CommonUtils.getManagedChannel("182.162.19.8", 35017);
    }

    public List<String> withPrecedentData(List<String> values){
        logger.debug("set precedent stub");
        Precedent2ArticleGrpc.Precedent2ArticleBlockingStub stub = Precedent2ArticleGrpc.newBlockingStub(precedentChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        logger.debug("send precedent request");
        Jose.ArticleData articleData = stub.withPrecedent(Jose.PrecedentData.newBuilder()
                .addAllValues(values)
                .build());

        logger.debug("return response");
        return articleData.getValuesList();
    }

    public List<String> withTableData(String title, List<String> keys, List<List<String>> values, List<String> positions){
        logger.debug("set table stub");
        Table2ArticleGrpc.Table2ArticleBlockingStub stub = Table2ArticleGrpc.newBlockingStub(tableChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        StringBuilder csvBuilder = new StringBuilder();
        for (String key: keys){
            if(key.contains(",")) key = key.replace(",", "");
            csvBuilder.append(key);
            csvBuilder.append(",");
        }
        csvBuilder.deleteCharAt(csvBuilder.lastIndexOf(","));
        csvBuilder.append("\n");

        for (List<String> valueList: values){
            for(String value: valueList){
                if(value.contains(",")) value = value.replace(",", "");
                csvBuilder.append(value);
                csvBuilder.append(",");
            }
            csvBuilder.deleteCharAt(csvBuilder.lastIndexOf(","));
            csvBuilder.append("\n");
        }
        csvBuilder.deleteCharAt(csvBuilder.lastIndexOf("\n"));
        logger.debug("\n" + csvBuilder.toString());
        for (String position: positions){
            logger.debug(position);
        }

        logger.debug("send table request");
        Totto.ArticleData articleData = stub.withTable(Totto.TableData.newBuilder()
                .setTitle(title)
                .setValues(csvBuilder.toString())
                .addAllHighlights(positions)
                .build());

        logger.debug("return response");
        return articleData.getValuesList();
    }
}
