package brain.api.d2a.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ArticlePayload implements Serializable {
    private List<String> passages;

    public ArticlePayload() {
    }

    public ArticlePayload(List<String> passages) {
        this.passages = passages;
    }

    public List<String> getPassages() {
        return passages;
    }

    public void setPassages(List<String> passages) {
        this.passages = passages;
    }

    @Override
    public String toString() {
        return "ArticlePayload{" +
                "passages=" + passages +
                '}';
    }
}
