package brain.api.d2a.dto;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ArticleResponse implements Serializable {
    private CommonMsg message;
    private ArticlePayload payload;

    public ArticleResponse() {
    }

    public ArticleResponse(CommonMsg message, ArticlePayload payload) {
        this.message = message;
        this.payload = payload;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public ArticlePayload getPayload() {
        return payload;
    }

    public void setPayload(ArticlePayload payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "ArticleResponse{" +
                "message=" + message +
                ", payload=" + payload +
                '}';
    }
}
