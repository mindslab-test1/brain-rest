package brain.api.d2a.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.d2a.D2ACommonCode;
import brain.api.d2a.client.D2AClient;
import brain.api.d2a.dto.ArticlePayload;
import brain.api.d2a.dto.ArticleResponse;
import brain.api.d2a.dto.WithPrecedentDto;
import brain.api.d2a.dto.WithTableDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class D2AService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(D2ACommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    private D2AClient client;

    public D2AService(@Autowired D2AClient client){
        this.client = client;
    }

    public ArticleResponse withPrecedent(WithPrecedentDto dto, HttpServletRequest request) throws MindsRestException {
        validate(dto.getApiId(), dto.getApiKey(), D2ACommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validated for precedent");

        return new ArticleResponse(
                new CommonMsg(),
                new ArticlePayload(client.withPrecedentData(dto.getValues()))
        );
    }

    public ArticleResponse withTable(WithTableDto dto, HttpServletRequest request) throws MindsRestException {
        logger.debug("check input");
        int keyCount = dto.getKeys().size();
        for (List<String> value: dto.getValues()){
            if (value.size() != keyCount){
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        "given values does not match key count");
            }
        }

        logger.debug(dto.toString());
        validate(dto.getApiId(), dto.getApiKey(), D2ACommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validated for table");

        return new ArticleResponse(
                new CommonMsg(),
                new ArticlePayload(
                        client.withTableData(
                                dto.getTitle(),
                                dto.getKeys(),
                                dto.getValues(),
                                dto.getPositions()
                        ))
        );
    }
}
