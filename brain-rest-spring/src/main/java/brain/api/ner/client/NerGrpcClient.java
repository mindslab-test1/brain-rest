package brain.api.ner.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.mrc.MrcCommonCode;
import brain.api.ner.data.NerBaseResponse;
import maum.brain.bert.ner.BertNERGrpc;
import maum.brain.bert.ner.Bn;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class NerGrpcClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(MrcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private BertNERGrpc.BertNERBlockingStub bertNERBlockingStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if (super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.bertNERBlockingStub = BertNERGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public NerBaseResponse getNer(String context){
        logger.info("[getNer] - "+ context);
        Bn.InputText.Builder inputBuilder = Bn.InputText.newBuilder();

        Bn.InputText input = inputBuilder.setContext(context).build();
        Bn.Dictionary outputs = this.bertNERBlockingStub.answer(input);

        ArrayList<String> pairArr = new ArrayList<>();
        for(Bn.Pair p : outputs.getTextsList()){
            pairArr.add(p.toString());
            logger.debug(p.toString());
        }


        NerBaseResponse response = new NerBaseResponse(pairArr);
        logger.debug(response.toString());

        return response;
    }
}
