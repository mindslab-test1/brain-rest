package brain.api.fakenews.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.fakenews.FakeNewsCommonCode;
import io.grpc.ManagedChannel;
import maum.brain.fakenews.FakeNewsGrpc;
import maum.brain.fakenews.FakeNewsOuterClass;
import org.springframework.stereotype.Component;


@Component
public class FakeNewsClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(FakeNewsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    public FakeNewsOuterClass.Prediction execFakeNews(String inputText) {
        ManagedChannel managedChannel = CommonUtils.getManagedChannel(FakeNewsCommonCode.IP, FakeNewsCommonCode.PORT);
        FakeNewsGrpc.FakeNewsBlockingStub stub = FakeNewsGrpc.newBlockingStub(managedChannel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        FakeNewsOuterClass.Output output = stub.makePrediction(FakeNewsOuterClass.Input.newBuilder().setText(inputText).build());
        return output.getPrediction();
    }

}
