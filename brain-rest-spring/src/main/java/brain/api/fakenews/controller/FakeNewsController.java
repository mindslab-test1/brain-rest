package brain.api.fakenews.controller;

import brain.api.common.CommonCode;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.fakenews.FakeNewsCommonCode;
import brain.api.fakenews.data.MakePredictionDTO;
import brain.api.fakenews.service.FakeNewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/fake")
@RequiredArgsConstructor
public class FakeNewsController {
    private static final CloudApiLogger logger = new CloudApiLogger(FakeNewsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    private final FakeNewsService service;

    @PostMapping(value = "/prediction:make", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE ,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> makePrediction(@RequestBody MakePredictionDTO makePredictionDTO, HttpServletRequest httpServletRequest) {
        try {
            return ResponseEntity.ok().body(service.makePrediction(makePredictionDTO, httpServletRequest));
        } catch(MindsRestException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch(Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }


}
