package brain.api.fakenews.data;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import maum.brain.fakenews.FakeNewsOuterClass;

import java.util.List;


@Getter
@Builder
@ToString
public class FakeNewsResponse {
    private String category;
    private String label;
    private float reliability;
}
