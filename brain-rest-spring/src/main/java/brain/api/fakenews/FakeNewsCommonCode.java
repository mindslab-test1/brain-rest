package brain.api.fakenews;

public class FakeNewsCommonCode {

    private FakeNewsCommonCode() {}

    public static final String SERVICE_NAME = "FAKE_NEWS";
    public static final String END_OF_STRING_TOKEN = "[EOS]";
    public static final String IP = "182.162.19.12";
    public static final int PORT = 8080;
}
