package brain.api.fakenews.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.fakenews.FakeNewsCommonCode;
import brain.api.fakenews.client.FakeNewsClient;
import brain.api.fakenews.data.FakeNewsResponse;
import brain.api.fakenews.data.MakePredictionDTO;
import io.netty.util.internal.StringUtil;
import lombok.RequiredArgsConstructor;
import maum.brain.fakenews.FakeNewsOuterClass;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class FakeNewsService extends BaseService {

    private static final CloudApiLogger logger = new CloudApiLogger(FakeNewsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);
    private final FakeNewsClient fakeNewsClient;

    public CommonResponse<FakeNewsResponse> makePrediction(MakePredictionDTO makePredictionDTO, HttpServletRequest request) throws MindsRestException {

        String apiId = Optional.ofNullable(makePredictionDTO.getApiId()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present api-id"));
        String apiKey = Optional.ofNullable(makePredictionDTO.getApiKey()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present api-Key"));
        String title = Optional.ofNullable(makePredictionDTO.getTitle()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present title"));
        String description = Optional.ofNullable(makePredictionDTO.getDescription()).orElseThrow(() ->
                new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not present description"));

        logger.info("MakePredictionDTO => " + makePredictionDTO);
        validate(apiId, apiKey, FakeNewsCommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validated for make-prediction");

        String inputText = title + StringUtil.SPACE + FakeNewsCommonCode.END_OF_STRING_TOKEN + StringUtil.SPACE + description;

        FakeNewsOuterClass.Prediction predictions = fakeNewsClient.execFakeNews(inputText);
        FakeNewsResponse fakeNewsResponse = FakeNewsResponse.builder()
                .category(predictions.getCategory())
                .label(predictions.getLabel())
                .reliability(predictions.getReliability()).build();

        return new CommonResponse<>(new CommonMsg(), fakeNewsResponse);

    }
}
