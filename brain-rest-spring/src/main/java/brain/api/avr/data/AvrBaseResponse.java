package brain.api.avr.data;

import brain.api.common.data.CommonMsg;

import java.util.Arrays;

public class AvrBaseResponse {
    private CommonMsg message;
    private data data;

    public AvrBaseResponse(){

    }

    public AvrBaseResponse(CommonMsg message, byte[] imageAsBytes) {
        this.message = message;
        this.data = new data(imageAsBytes);
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public AvrBaseResponse.data getData() {
        return data;
    }

    public void setData(AvrBaseResponse.data data) {
        this.data = data;
    }

    public class data{
        private byte[] imageAsBytes;

        private data(byte[] imageAsBytes){
            this.imageAsBytes = imageAsBytes;
        }

        public byte[] getImageAsBytes() {
            return imageAsBytes;
        }

        public void setImageAsBytes(byte[] imageAsBytes) {
            this.imageAsBytes = imageAsBytes;
        }

        @Override
        public String toString() {
            return "data{" +
                    "imageAsBytes=" + Arrays.toString(imageAsBytes) +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AvrBaseResponse{" +
                "message=" + message +
                ", data=" + data +
                '}';
    }
}
