package brain.api.avr.client;

import brain.api.avr.AvrCommonCode;
import brain.api.avr.data.AvrBaseResponse;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Base64;
import java.util.UUID;

@Service
public class AvrFlaskClient {
    private static final CloudApiLogger logger = new CloudApiLogger(AvrCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public AvrBaseResponse doAvr(MultipartFile reqFile, String option) throws MindsRestException{
        File file = null;
        try {
            logger.debug("enter client");
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost("http://diarl.maum.ai:8080/run_avr");

            file = new File("/home/ubuntu/maum/tmp" + UUID.randomUUID());
            logger.debug("download file");
            reqFile.transferTo(file);
            FileBody fileBody = new FileBody(file);
            logger.debug("set form");

            HttpEntity entity = MultipartEntityBuilder.create()
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                    .addPart("file", fileBody)
                    .addPart("option", new StringBody(option, ContentType.MULTIPART_FORM_DATA))
                    .build();

            logger.debug("post");

            HttpResponse response = CommonUtils.sendPostRequest(client, post, entity);
            if(response.getStatusLine().getStatusCode() != 200){
                throw new MindsRestException(
                        CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_SERVICE_UNAVAILABLE.getMessage()
                );
            }
            logger.debug("prepare response");
            HttpEntity responseEntity = response.getEntity();

            byte[] responseBaseBytes = IOUtils.toByteArray(
                    responseEntity.getContent()
            );
            byte[] responseBytes = Base64.getDecoder()
                    .decode(responseBaseBytes);

            logger.debug("send response");
            AvrBaseResponse avrBaseResponse = new AvrBaseResponse(
                    new CommonMsg(),
                    responseBytes
            );
            logger.info(String.format("Result: %s", avrBaseResponse));

            return avrBaseResponse;

        } catch (MindsRestException e){
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );
        } finally {
            if(file != null)
                if(!file.delete())
                    logger.warn("file was not mopped up");
                else
                    logger.info("temp file deleted");
        }
    }
}
