package brain.api.gpt.data;

import brain.api.common.data.CommonMsg;

public class GptBaseResponse {
    private CommonMsg message;
    private String result;
    private String reqText;

    public GptBaseResponse(String result, String reqText) {
        this.result = result;
        this.reqText = reqText;
        this.message = new CommonMsg();
    }

    public GptBaseResponse(CommonMsg message) {
        this.result = null;
        this.reqText = null;
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getReqText() {
        return reqText;
    }

    public void setReqText(String reqText) {
        this.reqText = reqText;
    }

    @Override
    public String toString() {
        return "GptBaseResponse{" +
                "message=" + message +
                ", result='" + result + '\'' +
                ", reqText='" + reqText + '\'' +
                '}';
    }
}
