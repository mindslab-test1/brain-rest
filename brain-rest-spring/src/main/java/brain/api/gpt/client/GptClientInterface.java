package brain.api.gpt.client;

import brain.api.common.data.CommonMsg;
import brain.api.gpt.data.GptBaseResponse;

public interface GptClientInterface {
    public GptBaseResponse textGenerate(String reqText);
    public CommonMsg setDestination(String ip, int port);
}
