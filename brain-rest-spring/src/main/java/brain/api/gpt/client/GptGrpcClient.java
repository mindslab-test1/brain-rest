package brain.api.gpt.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.gpt.GptCommonCode;
import brain.api.gpt.data.GptBaseResponse;
import maum.brain.gpt.textgen.Gpt;
import maum.brain.gpt.textgen.GptTextGenGrpc;
import org.springframework.stereotype.Component;

// TODO GPT service reference 신현철 매니저님
@Component
public class GptGrpcClient extends GrpcClientInterface implements GptClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(GptCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private GptTextGenGrpc.GptTextGenBlockingStub gptTextGenBlockingStub;

    @Override
    public CommonMsg setDestination(String ip, int port){
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.gptTextGenBlockingStub = GptTextGenGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e){
            logger.error(String.format("Destination set failure! (%s:%d)", ip, port));
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    @Override
    public GptBaseResponse textGenerate (String reqText){
        Gpt.InputText inputText = Gpt.InputText.newBuilder()
                .setContext(reqText)
                .build();

        String retString = gptTextGenBlockingStub.gen(inputText).getGentext();
        logger.debug("retString : " + retString);
        logger.debug("reqText : " + reqText);
        GptBaseResponse response = new GptBaseResponse(retString, reqText);
        logger.info("[Legacy GPT] - response : " + response.toString());

        return response;
    }
}
