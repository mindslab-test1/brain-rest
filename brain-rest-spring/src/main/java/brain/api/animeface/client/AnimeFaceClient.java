package brain.api.animeface.client;

import brain.api.animeface.AnimeFaceCommonCode;
import brain.api.animeface.data.AnimeFaceResponse;
import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.animeface.AnimeGrpc;
import maum.brain.animeface.AnimeOuterClass;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class AnimeFaceClient {
    private static final CloudApiLogger logger = new CloudApiLogger(AnimeFaceCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public AnimeFaceResponse newSample() throws MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(AnimeFaceCommonCode.IP, AnimeFaceCommonCode.PORT_NEW_SAMPLE_STYLE_EDIT);
        AnimeGrpc.AnimeBlockingStub stub = AnimeGrpc.newBlockingStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        byte[] latentArr = null;
        byte[] imgArr = null;

        Iterator<AnimeOuterClass.Server2User> server2UserIterator = stub.newSample(Empty.newBuilder().build());
        while(server2UserIterator.hasNext()) {
            AnimeOuterClass.Server2User response = server2UserIterator.next();
            latentArr = response.getLatent().toByteArray();
            imgArr = response.getImg().toByteArray();
        }

        if (latentArr.length == 0 || imgArr.length == 0) {
            logger.error("Error: Stream response from AnimeFace newSample");
            throw new MindsRestException("Error: Stream response from AnimeFace makeSample");
        }

        return new AnimeFaceResponse(imgArr, latentArr);
    }

    public AnimeFaceResponse project(MultipartFile image, int steps) throws IOException, MindsRestException, InterruptedException {
        ManagedChannel channel = CommonUtils.getManagedChannel(AnimeFaceCommonCode.IP, AnimeFaceCommonCode.PORT_FACE_MAKE);
        AnimeGrpc.AnimeStub stub = AnimeGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        List<AnimeOuterClass.Server2User> responseList = new ArrayList<>();
        List<Throwable> errorList = new ArrayList<>();

        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<AnimeOuterClass.Server2User> responseObserver = new StreamObserver<AnimeOuterClass.Server2User>() {

            @Override
            public void onNext(AnimeOuterClass.Server2User server2User) {
                responseList.add(server2User);
                logger.info("[AnimeFace-Project()] ProjectRequest Received");
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.getMessage());
                errorList.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.info("[AnimeFace-Project()] server on completed call");
                finishLatch.countDown();
            }
        };

        StreamObserver<AnimeOuterClass.ProjectRequest> requestObserver = stub.project(responseObserver);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(image.getBytes());
        int len = 0;
        byte[] buffer = new byte[1024 * 512];
        while ((len = byteArrayInputStream.read(buffer)) > 0) {
            logger.debug(String.format("[AnimeFace-Project()] image chunk size: %d", len));
            if (len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(AnimeOuterClass.ProjectRequest.newBuilder().setImg(inputByteString).setSteps(steps).build());
            logger.debug("[AnimeFace-Project()] send image chunk to server");
        }

        requestObserver.onCompleted();
        logger.debug("[AnimeFace-Project()] send clothing info request completion to server");
        byteArrayInputStream.close();
        logger.debug("[AnimeFace-Project()] close input stream");

        if(!finishLatch.await(5, TimeUnit.MINUTES)) {
            logger.error("[AnimeFace-Project()] connection timeout");
            throw new MindsRestException("connection timeout with internal server");
        }
        if(!errorList.isEmpty()) {
            logger.error("[AnimeFace-Project()]");
            logger.error(errorList.get(0));
            throw new MindsRestException("Error: Stream response from AnimeFace MakeFace");
        }

        return new AnimeFaceResponse(responseList.get(0).getImg().toByteArray(), responseList.get(0).getLatent().toByteArray());
    }

    public AnimeFaceResponse moveStyle(MultipartFile latent, MultipartFile dict) throws IOException, MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(AnimeFaceCommonCode.IP, AnimeFaceCommonCode.PORT_NEW_SAMPLE_STYLE_EDIT);
        AnimeGrpc.AnimeBlockingStub stub = AnimeGrpc.newBlockingStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
        Iterator<AnimeOuterClass.Server2User> server2UserIterator = stub.moveStyle(AnimeOuterClass.User2Server.newBuilder()
                                                                    .setLatent(ByteString.copyFrom(latent.getBytes()))
                                                                    .setDict(ByteString.copyFrom(dict.getBytes())).build());
        byte[] imgArr = null;
        byte[] latentArr = null;

        while (server2UserIterator.hasNext()) {
            AnimeOuterClass.Server2User response = server2UserIterator.next();
            imgArr = response.getImg().toByteArray();
            latentArr = response.getLatent().toByteArray();
        }

        if (latentArr.length == 0 || imgArr.length == 0) {
            logger.error("Error: Stream response from AnimeFace moveStyle");
            throw new MindsRestException("Error: Stream response from AnimeFace EditStyle");
        }

        return new AnimeFaceResponse(imgArr, latentArr);
    }
}
