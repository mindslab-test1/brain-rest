package brain.api.animeface.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AnimeFaceResponse {
    private byte[] img;
    private byte[] latent;
}
