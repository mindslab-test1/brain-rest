package brain.api.animeface.service;

import brain.api.animeface.AnimeFaceCommonCode;
import brain.api.animeface.client.AnimeFaceClient;
import brain.api.animeface.data.AnimeFaceResponse;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RequiredArgsConstructor
@Service
public class AnimeFaceService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(AnimeFaceCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);
    private final AnimeFaceClient client;

    public CommonResponse<AnimeFaceResponse> makeSample(String apiId, String apiKey, HttpServletRequest request) throws MindsRestException {
        logger.info("AnimeFace makeSample Request [apiId = " + apiId + "]");
        validate(apiId, apiKey, AnimeFaceCommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validate for AnimeFace-MakeSample");

        return new CommonResponse<>(new CommonMsg(), client.newSample());
    }

    public CommonResponse<AnimeFaceResponse> makeAnimeFace(String apiId, String apiKey, MultipartFile image, String steps, HttpServletRequest request) throws MindsRestException, IOException, InterruptedException {
        logger.info("AnimeFace makeAnimeFace Request [apiId = " + apiId + "]");
        validate(apiId, apiKey, AnimeFaceCommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validate for AnimeFace-MakeAnimeFace");

        if (validateImage(image)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not found requested image resource");
        }

        if (!validateImageType(image)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported media type.");
        }

        String filename = image.getOriginalFilename().toLowerCase();
        String fileExtension = FilenameUtils.getExtension(filename);
        if (!(fileExtension.equalsIgnoreCase("png") || fileExtension.equalsIgnoreCase("jpg") || fileExtension.equalsIgnoreCase("bmp"))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported file extension. Please check allow extension(png, jpg, bmp)");
        }

        if (image.getSize() > (FileUtils.ONE_MB * 4)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "size of the request image is larger than the limit of 4MB");
        }

        return new CommonResponse<>(new CommonMsg(), client.project(image, Integer.parseInt(steps)));
    }

    public CommonResponse<AnimeFaceResponse> editStyle(String apiId, String apiKey, MultipartFile latent, MultipartFile dict, HttpServletRequest request) throws MindsRestException, IOException {
        logger.info("AnimeFace editStyle Request [apiId = " + apiId + "]");
        validate(apiId, apiKey, AnimeFaceCommonCode.SERVICE_NAME, 1, request);
        logger.debug("usage validate for AnimeFace-EditStyle");

        if (validateFile(latent)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not found requested latent resource");
        }

        if (validateFile(dict)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "not found requested dict resource");
        }

        if (!validateTxtType(dict)) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported media type (dict is .txt file).");
        }

        String latentFileName = latent.getOriginalFilename();
        String latentFileExtension = FilenameUtils.getExtension(latentFileName);
        String dictFileName = dict.getOriginalFilename();
        String dictFileExtension = FilenameUtils.getExtension(dictFileName);

        if (!(latentFileExtension.equalsIgnoreCase("pkl"))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported file extension. Please check allow extension(pkl)");
        }

        if (!(dictFileExtension.equalsIgnoreCase("txt"))) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "unsupported file extension. Please check allow extension(txt)");
        }

        if (latent.getSize() > FileUtils.ONE_KB * 100) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "size of the request image is larger than the limit of 100KB");
        }

        if (dict.getSize() > FileUtils.ONE_MB) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(), "size of the request image is larger than the limit of 1MB");
        }

        return new CommonResponse<>(new CommonMsg(), client.moveStyle(latent, dict));
    }

    // TODO Refactoring Target(Validate File)

    private boolean validateImageType(MultipartFile image) {
        return image.getContentType().contains(CommonCode.IMAGE);
    }

    private boolean validateTxtType(MultipartFile txt) {
        return txt.getContentType().contains(CommonCode.TEXT);
    }

    private boolean validateImage(MultipartFile image) {
        return image == null || image.isEmpty() || image.getSize() == 0;
    }

    private boolean validateFile(MultipartFile file) {
        return file == null || file.isEmpty() || file.getSize() == 0;
    }

    // TODO Refactoring Target(Validate File) End
}
