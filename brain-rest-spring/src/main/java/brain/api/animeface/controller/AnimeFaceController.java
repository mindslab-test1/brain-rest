package brain.api.animeface.controller;

import brain.api.animeface.AnimeFaceCommonCode;
import brain.api.animeface.service.AnimeFaceService;
import brain.api.common.CommonCode;
import brain.api.common.data.BaseRequest;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/anime")
public class AnimeFaceController {
    private static final CloudApiLogger logger = new CloudApiLogger(AnimeFaceCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);
    private final AnimeFaceService service;

    @PostMapping(value = "/sample:make", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> makeSample(@RequestBody BaseRequest baseRequest, HttpServletRequest request) {
        try {
            return ResponseEntity.ok().body(service.makeSample(baseRequest.getApiId(), baseRequest.getApiKey(), request));
        } catch (MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @PostMapping(value = "/face:make", consumes = MediaType.MULTIPART_FORM_DATA_VALUE ,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> makeAnimeFace(@RequestParam(value = "apiId") String apiId,
                                  @RequestParam(value = "apiKey") String apiKey,
                                  @RequestParam(value = "image") MultipartFile image,
                                  @RequestParam(value = "steps", required = false, defaultValue = "500") String steps,
                                  HttpServletRequest request) {
        try {
            return ResponseEntity.ok().body(service.makeAnimeFace(apiId, apiKey, image, steps, request));
        } catch (MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }

    @PostMapping(value = "/style:edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> editStyle(@RequestParam(value = "apiId") String apiId,
                                    @RequestParam(value = "apiKey") String apiKey,
                                    @RequestParam(value = "latent") MultipartFile latent,
                                    @RequestParam(value = "dict") MultipartFile dict,
                                    HttpServletRequest request) {
        try {
            return ResponseEntity.ok().body(service.editStyle(apiId, apiKey, latent, dict, request));
        } catch (MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse());
        }
    }
}
