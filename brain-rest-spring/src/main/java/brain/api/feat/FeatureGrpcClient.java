package brain.api.feat;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.feat.data.FeatClothPayload;
import brain.api.feat.data.FeatClothPayloadMeta;
import brain.api.feat.data.FeatHairPayload;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.pa.air.clothing.AirClothing;
import maum.pa.air.clothing.AirClothingMAGrpc;
import maum.pa.hair.info.HairInfoGrpc;
import maum.pa.hair.info.HairInfoOuterClass;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class FeatureGrpcClient {
    private static final CloudApiLogger logger = new CloudApiLogger("FEAT", CommonCode.COMMON_CLASSNAME_CLIENT);

    private static final String hairIp = "182.162.19.14";
    private static final int hairPort = 30001;

    private static final String clothingIp = "182.162.19.14";
    private static final int clothingPort = 30002;

    FeatHairPayload getHairInfo(MultipartFile file) throws IOException, InterruptedException, MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(hairIp, hairPort);
        HairInfoGrpc.HairInfoStub stub = HairInfoGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        final CountDownLatch finishLatch = new CountDownLatch(1);

        List<HairInfoOuterClass.OutputImageWithInfo> responseList = new ArrayList<>();
        List<Throwable> exceptions = new ArrayList<>();

        StreamObserver<HairInfoOuterClass.OutputImageWithInfo> responseObserver = new StreamObserver<HairInfoOuterClass.OutputImageWithInfo>() {
            @Override
            public void onNext(HairInfoOuterClass.OutputImageWithInfo outputImageWithInfo) {
                logger.debug("next message");
                responseList.add(outputImageWithInfo);
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("Exception in response observer");
                logger.error(throwable.getMessage());
                exceptions.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.debug("completed");
                finishLatch.countDown();
            }
        };

        StreamObserver<HairInfoOuterClass.InputImageBytes> requestObserver = stub.getHairInfo(responseObserver);
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(file.getBytes());
        int len = 0;
        byte[] buffer = new byte[1024 * 16];
        while ((len = byteInputStream.read(buffer)) > 0){
            if(len < buffer.length)
                buffer = Arrays.copyOfRange(buffer, 0, len);

            ByteString inputByteString = ByteString.copyFrom(buffer);
            logger.debug(String.format("read byte length: %d", len));
            logger.debug(String.format("buffer length: %d", buffer.length));

            requestObserver.onNext(HairInfoOuterClass.InputImageBytes.newBuilder().setInput(inputByteString).build());
        }

        requestObserver.onCompleted();
        logger.debug("send hair info request completion to server");

        byteInputStream.close();
        logger.debug("close input stream");

        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e){
            logger.warn(e.getMessage());
            channel.shutdownNow();
            throw e;
        }
        channel.shutdown();

        FeatHairPayload payload = new FeatHairPayload();
        if(!exceptions.isEmpty()){
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }

        byte[] resultImgBytes = null;

        for(HairInfoOuterClass.OutputImageWithInfo responsePart: responseList){
            if(StringUtils.isEmpty(responsePart.getInfo())){
                if(resultImgBytes == null) resultImgBytes = responsePart.getImage().toByteArray();
                else resultImgBytes = this.concatBytes(resultImgBytes, responsePart.getImage().toByteArray());
            } else {
                payload.setHairInfo(responsePart.getInfo());
            }
        }

        payload.setResultImage(resultImgBytes);

        return payload;
    }

    FeatClothPayload getClothingInfo(MultipartFile file) throws IOException, InterruptedException, MindsRestException {
        ManagedChannel channel = CommonUtils.getManagedChannel(clothingIp, clothingPort);
        AirClothingMAGrpc.AirClothingMAStub stub = AirClothingMAGrpc.newStub(channel).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        final CountDownLatch finishLatch = new CountDownLatch(1);

        List<AirClothing.OutputImageWithInfo> responseList = new ArrayList<>();
        List<Throwable> exceptions = new ArrayList<>();

        StreamObserver<AirClothing.OutputImageWithInfo> responseObserver = new StreamObserver<AirClothing.OutputImageWithInfo>() {
            @Override
            public void onNext(AirClothing.OutputImageWithInfo outputImageWithInfo) {
                logger.debug("next message");
                responseList.add(outputImageWithInfo);
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("Exception in response observer");
                logger.error(throwable.getMessage());
                exceptions.add(throwable);
                finishLatch.countDown();
            }

            @Override
            public void onCompleted() {
                logger.debug("completed");
                finishLatch.countDown();
            }
        };

        StreamObserver<AirClothing.InputImageBytes> requestObserver = stub.doAirClothing(responseObserver);
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(file.getBytes());
        int len = 0;
        byte[] buffer = new byte[1024 * 16];
        while ((len = byteInputStream.read(buffer)) > 0){
            if(len < buffer.length)
                buffer = Arrays.copyOfRange(buffer, 0, len);

            ByteString inputByteString = ByteString.copyFrom(buffer);
            logger.debug(String.format("read byte length: %d", len));
            logger.debug(String.format("buffer length: %d", buffer.length));

            requestObserver.onNext(AirClothing.InputImageBytes.newBuilder().setInput(inputByteString).build());
        }

        requestObserver.onCompleted();
        logger.debug("send clothing info request completion to server");

        byteInputStream.close();
        logger.debug("close input stream");

        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e){
            logger.warn(e.getMessage());
            channel.shutdownNow();
            throw e;
        }
        channel.shutdown();

        FeatClothPayload payload = new FeatClothPayload();
        List<FeatClothPayloadMeta> metaList = new ArrayList<>();

        byte[] resultImgBytes = null;

        if(!exceptions.isEmpty()){
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        }

        for(AirClothing.OutputImageWithInfo responsePart: responseList){
            if(StringUtils.isEmpty(responsePart.getInfo())){
                if(resultImgBytes == null) resultImgBytes = responsePart.getImage().toByteArray();
                else resultImgBytes = this.concatBytes(resultImgBytes, responsePart.getImage().toByteArray());
            } else {
                metaList.add(new FeatClothPayloadMeta(
                        responsePart.getKey(),
                        responsePart.getInfo()
                ));
            }
        }

        payload.setMetaData(metaList);
        payload.setResultImage(resultImgBytes);

        return payload;
    }

    private byte[] concatBytes(byte[] first, byte[] second){
        byte[] returnBytes = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, returnBytes, first.length, second.length);
        return returnBytes;
    }
}
