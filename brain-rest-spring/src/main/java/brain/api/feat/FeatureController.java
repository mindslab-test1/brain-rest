package brain.api.feat;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.feat.data.FeatBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("feat")
public class FeatureController {
    private static final CloudApiLogger logger = new CloudApiLogger("FEAT", CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    FeatureService service;

    @RequestMapping(
            value = "getClothFeature",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody FeatBaseResponse getClothFeatures(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("image") MultipartFile file,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        logger.debug("get request");
        try {
            return service.getFeature(apiId, apiKey, file, request, FeatureService.Features.clothing);
        } catch (MindsRestException e){
            logger.warn("Internal Exception");
            logger.warn(e.getMessage());
            response.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new FeatBaseResponse(new CommonMsg(e.getErrCode()));
        } catch (Exception e){
            logger.error("Exception while processing request");
            logger.error(e.getMessage());
            response.setStatus(500);
            return new FeatBaseResponse(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }

    @RequestMapping(
            value = "getHairInfo",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody FeatBaseResponse getHairInfo(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("image") MultipartFile file,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        try {
            return service.getFeature(apiId, apiKey, file, request, FeatureService.Features.hair);
        } catch (MindsRestException e){
            logger.warn("Internal Exception");
            logger.warn(e.getMessage());
            response.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new FeatBaseResponse(new CommonMsg(e.getErrCode()));
        } catch (Exception e){
            logger.error("Exception while processing request");
            logger.error(e.getMessage());
            response.setStatus(500);
            return new FeatBaseResponse(new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()));
        }
    }
}
