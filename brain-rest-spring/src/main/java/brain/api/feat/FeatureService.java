package brain.api.feat;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.feat.data.FeatBaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
public class FeatureService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger("FEAT", CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    FeatureGrpcClient client;

    public FeatBaseResponse getFeature(
            String apiId,
            String apiKey,
            MultipartFile file,
            HttpServletRequest request,
            Features features
    ) throws MindsRestException, IOException, InterruptedException {
        validate(apiId, apiKey, "FEAT", 1, request);
        switch (features){
            case clothing:
                return new FeatBaseResponse(client.getClothingInfo(file));
            case hair:
                return new FeatBaseResponse(client.getHairInfo(file));
            default:
                throw new MindsRestException(CommonExceptionCode.COMMON_ERR_NOT_IMPLEMENTED.getErrCode(),
                        CommonExceptionCode.COMMON_ERR_NOT_IMPLEMENTED.getMessage());
        }
    }

    enum Features{
        clothing, hair
    }
}
