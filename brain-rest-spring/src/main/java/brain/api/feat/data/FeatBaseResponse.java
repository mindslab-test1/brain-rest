package brain.api.feat.data;

import brain.api.common.data.CommonMsg;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FeatBaseResponse {
    private CommonMsg message;
    private Serializable payload;

    public FeatBaseResponse(CommonMsg message) {
        this.message = message;
        this.payload = null;
    }

    public FeatBaseResponse(Serializable payload) {
        this.message = new CommonMsg();
        this.payload = payload;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Serializable payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "FeatBaseResponse{" +
                "message=" + message +
                ", payload=" + payload +
                '}';
    }
}
