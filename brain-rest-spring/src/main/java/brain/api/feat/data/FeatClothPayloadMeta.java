package brain.api.feat.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FeatClothPayloadMeta implements Serializable {
    private int key;
    private String infoSentence;

    public FeatClothPayloadMeta() {
    }

    public FeatClothPayloadMeta(int key, String infoSentence) {
        this.key = key;
        this.infoSentence = infoSentence;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getInfoSentence() {
        return infoSentence;
    }

    public void setInfoSentence(String infoSentence) {
        this.infoSentence = infoSentence;
    }

    @Override
    public String toString() {
        return "FeatClothPayloadMeta{" +
                "key=" + key +
                ", infoSentence='" + infoSentence + '\'' +
                '}';
    }
}
