package brain.api.common.service;

import brain.api.common.data.BaseRequest;
import brain.api.common.exception.MindsRestException;
import brain.api.common.model.server.ServerDto;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface IBaseService {
    String validate(String apiId, String apiKey, String service, int usage, HttpServletRequest request) throws MindsRestException;
    List<ServerDto> getServerByService(String service);
}
