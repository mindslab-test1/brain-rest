package brain.api.common.service;

import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Service
public class RedisService<V> {
    private final RedisTemplate<String, V> redisTemplate;

    public void saveWithTime(String key, V value, Duration duration) {
        ValueOperations<String, V> valueOperations = redisTemplate.opsForValue();
        valueOperations.set(key, value, duration);
    }

    public void save(String key, V value) {
        ValueOperations<String, V> valueOperations = redisTemplate.opsForValue();
        valueOperations.set(key, value);
    }

    public void listAdd(String key, V value) {
        ListOperations<String, V> valueOperations = redisTemplate.opsForList();
        valueOperations.rightPush(key, value);
    }

    public V get(String key) throws MindsRestException {
        ValueOperations<String, V> valueOperations = redisTemplate.opsForValue();
        return Optional.ofNullable(valueOperations.get(key))
                .orElseThrow(() -> new MindsRestException(
                        CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                        String.format("Not exist key(%s)", key))
                );
    }

    public Optional<List<V>> getList(String key) {
        ListOperations<String, V> valueOperations = redisTemplate.opsForList();
        return Optional.ofNullable(valueOperations.range(key, 0, -1));
    }

    public Long getListSize(String key) {
        ListOperations<String, V> valueOperations = redisTemplate.opsForList();
        return valueOperations.size(key);
    }

    public Long increment(String apiId) {
        ValueOperations<String, V> valueOperations = redisTemplate.opsForValue();
        return valueOperations.increment(apiId);
    }

    public void remove(String key) {
        redisTemplate.delete(key);
    }

    public void setTtlForKey(String key, Long timeout) {
        redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
    }
}
