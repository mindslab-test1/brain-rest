package brain.api.common.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.server.ServerDao;
import brain.api.common.model.server.ServerDto;
import brain.api.common.model.usage.UsageDao;
import brain.api.common.model.usage.UsageDto;
import brain.api.common.model.user.ApiClientDao;
import brain.api.common.model.user.ApiClientDto;
import brain.api.common.utils.CommonUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;


public abstract class BaseService implements IBaseService{
    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    protected ServerDao serverDao;

    @Autowired
    protected UsageDao usageDao;

    @Autowired
    protected ApiClientDao apiClientDao;

    @Autowired
    protected CommonUtils utils;

    // TODO: Refactoring Target => API-Key 검사하는 책임과 사용량 업데이트하는 책임이 같이 있어 유지보수함에 있어서 안좋음. 분리 필요
    public String validate(String apiId, String apiKey, String service, int usage, HttpServletRequest request) throws MindsRestException {
        logger.debug("validate id / key");

        CommonMsg msg = this.checkValid(apiId, apiKey);
        logger.debug(msg.getStatus());
        if(msg.getStatus() != CommonCode.COMMON_SUCCESS) throw new MindsRestException(
                msg.getStatus(),
                msg.getMessage()
        );
        String admin = msg.getMessage();
        logger.debug("validate usage feasibility");
        msg = this.updateUsage(service, apiId, usage, request);
        if(msg.getStatus() != CommonCode.COMMON_SUCCESS) throw new MindsRestException(
                msg.getStatus(),
                msg.getMessage()
        );

        return admin;
    }

    public final CommonMsg validateLegacy(String apiId, String apiKey){
        try{
            logger.debug("validate legacy");
            logger.debug("validate id / key");

            return this.checkValid(apiId, apiKey);

        } catch (Exception e){
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode());
        }
    }

    private CommonMsg checkValid(String reqId, String reqKey){
        ApiClientDto requestClient = apiClientDao.getApiClientInfo(reqId);
        if(requestClient == null) return new CommonMsg(CommonExceptionCode.COMMON_ERR_UNAUTHORIZED.getErrCode());
        if(!requestClient.getApiKey().equals(reqKey)) return new CommonMsg(CommonExceptionCode.COMMON_ERR_UNAUTHORIZED.getErrCode());
        if(requestClient.getGrade() == CommonCode.API_CLIENT_DISABLE) return new CommonMsg(CommonExceptionCode.COMMON_ERR_FORBIDDEN.getErrCode());
        if(requestClient.getGrade() == CommonCode.API_CLIENT_ADMIN) return new CommonMsg(CommonCode.COMMON_SUCCESS, CommonCode.API_CLIENT_ADMIN_MSG);
        else return new CommonMsg();
    }

    private CommonMsg updateUsage(String service, String apiId, int usage, HttpServletRequest request) throws MindsRestException{
        logger.debug("check usage and validate");
        UsageDto usageDto;
        try {
            usageDto = new UsageDto(
                    service,
                    apiId,
                    usage
            );
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );
        }
        try {
            usageDto.setUserIp(CommonUtils.getIp(request));
        } catch (Exception e) {
            logger.warn("Unable to collect IP.");
            logger.warn(e.getMessage());
        }
        try {
            usageDao.updateUsage(usageDto);
        } catch (Exception e){
            logger.error(e.getMessage());
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );
        }

        return new CommonMsg();
    }

    @Override
    public List<ServerDto> getServerByService(String service){
        return serverDao.getServerByService(service);
    }
}
