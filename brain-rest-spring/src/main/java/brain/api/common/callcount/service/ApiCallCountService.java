package brain.api.common.callcount.service;

import brain.api.common.callcount.dao.ApiCallCountDAO;
import brain.api.common.callcount.data.ApiCallCountDTO;
import brain.api.common.callcount.data.ApiCallCountQuotaDTO;
import brain.api.common.callcount.data.RequestApiCallCountDTO;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.common.service.RedisService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.internal.util.Assert;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static brain.api.common.CommonCode.COMMON_SERVICE_NAME;
import static brain.api.common.callcount.CallCountCommonCode.COMMON_CALL_COUNT_SERVICE;

@RequiredArgsConstructor
@Getter
@Service
public class ApiCallCountService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(COMMON_CALL_COUNT_SERVICE, COMMON_SERVICE_NAME);

    private final ApiCallCountDAO apiCallCountDAO;
    private final RedisService redisService;
    private final String IO_CALL_COUNT_LIST_KEY_NAME = "callCountList";
    private final String CALL_COUNT_QUOTA_KEY_NAME = "callCountQuota";

    private ApiCallCountQuotaDTO quota = null;

    public synchronized void putCallCount(ApiCallCountDTO callCount) {
        Assert.notNull(quota, "Could not get quota from Redis.");
        redisService.listAdd(IO_CALL_COUNT_LIST_KEY_NAME, callCount);
        long listSize = redisService.getListSize(IO_CALL_COUNT_LIST_KEY_NAME);
        if (listSize >= quota.getListFlushQuota()) {
            this.storeCallCount();
        }
    }

    public void restoreMemoryCallCountQuota() throws MindsRestException {
        quota = (ApiCallCountQuotaDTO) redisService.get(CALL_COUNT_QUOTA_KEY_NAME);
    }

    public synchronized void storeCallCount() {
        StringBuffer insertLogBuffer = new StringBuffer();

        redisService.getList(IO_CALL_COUNT_LIST_KEY_NAME).ifPresent(collection -> {
            insertLogBuffer.append("\n =========== insert data ========== \n");
            List<ApiCallCountDTO> callCountList = (List<ApiCallCountDTO>) collection;
            callCountList.forEach(callCount -> {
                apiCallCountDAO.insertApiCallCount(callCount);
                insertLogBuffer.append(callCount + "\n");
            });
            insertLogBuffer.append("================================== \n");
            logger.info(insertLogBuffer.toString());
            redisService.remove(IO_CALL_COUNT_LIST_KEY_NAME);
        });

    }

    public CommonResponse getCallCountList(RequestApiCallCountDTO dto, HttpServletRequest request) throws MindsRestException {
        validate(dto.getApiId(), dto.getApiKey(), COMMON_CALL_COUNT_SERVICE, 0, request);

        this.storeCallCount();

        if (dto.getStartDateRange() != null) {
            dto.setStartDateRange(dto.getStartDateRange().atZone(ZoneId.systemDefault())
                    .withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
        }

        if (dto.getEndDateRange() != null) {
            dto.setEndDateRange(dto.getEndDateRange().atZone(ZoneId.systemDefault())
                    .withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
        }

        List<ApiCallCountDTO> responseList = apiCallCountDAO.getApiCallCountList(dto);
        responseList.forEach(call -> {
            call.setCallTime(call.getCallTime().atZone(ZoneOffset.UTC)
                    .withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
            call.setRegDate(call.getRegDate().atZone(ZoneOffset.UTC)
                    .withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
        });

        return new CommonResponse(new CommonMsg(), apiCallCountDAO.getApiCallCountList(dto));
    }


    public synchronized boolean isBlocked(String apiId) {
        Assert.notNull(quota, "Could not get quota from Redis.");
        Long apiIdCountValue = redisService.increment(apiId);

        if (apiIdCountValue == 1) {
            redisService.setTtlForKey(apiId, quota.getTimeQuota());
        }

        return apiIdCountValue > quota.getRequestQuota();
    }

    @PostConstruct
    public void initCallCountQuota() {
        try {
            ApiCallCountQuotaDTO callCountQuotaDTO = (ApiCallCountQuotaDTO) redisService.get(CALL_COUNT_QUOTA_KEY_NAME);
            logger.info("init ===> callCountQuota { \n timeQuota: " + callCountQuotaDTO.getTimeQuota() +
                    ", listFlushQuota: " + callCountQuotaDTO.getListFlushQuota() +
                    ", requestQuota: " + callCountQuotaDTO.getRequestQuota() + " }");
        } catch (MindsRestException e) {
            logger.info("init ===> callCountQuota Not Exist, insert new CallCount");
            redisService.save(CALL_COUNT_QUOTA_KEY_NAME,
                    new ApiCallCountQuotaDTO(60L, 500L, 1000L));
        }
    }

    @PreDestroy
    public void shutdownListenerInCallCountService() {
        this.storeCallCount();
    }

    public CommonResponse updateApiCallCountQuota(ApiCallCountQuotaDTO dto) throws MindsRestException {
        this.restoreMemoryCallCountQuota();

        if (dto.getRequestQuota() == null && dto.getListFlushQuota() == null && dto.getTimeQuota() == null) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "None of the method's parameters were passed.(request, list, time Quota)");
        }

        if (dto.getTimeQuota() != null) {
            quota.setTimeQuota(dto.getTimeQuota());
        }
        if (dto.getListFlushQuota() != null) {
            quota.setListFlushQuota(dto.getListFlushQuota());
        }
        if (dto.getRequestQuota() != null) {
            quota.setRequestQuota(dto.getRequestQuota());
        }

        redisService.save(CALL_COUNT_QUOTA_KEY_NAME, quota);

        return new CommonResponse(new CommonMsg());
    }
}
