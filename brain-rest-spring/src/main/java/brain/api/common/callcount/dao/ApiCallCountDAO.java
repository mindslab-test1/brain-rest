package brain.api.common.callcount.dao;

import brain.api.common.callcount.data.ApiCallCountDTO;
import brain.api.common.callcount.data.RequestApiCallCountDTO;
import brain.api.common.log.CloudApiLogger;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.ApiCallCountMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import static brain.api.common.CommonCode.COMMON_CLASSNAME_DAO;
import static brain.api.common.callcount.CallCountCommonCode.COMMON_CALL_COUNT_SERVICE;

@Repository
public class ApiCallCountDAO {
    private static final CloudApiLogger logger = new CloudApiLogger(COMMON_CALL_COUNT_SERVICE, COMMON_CLASSNAME_DAO);
    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public int insertApiCallCount(ApiCallCountDTO callCountDTO){
        try (SqlSession session = sessionFactory.openSession(true)){
            ApiCallCountMapper mapper = session.getMapper(ApiCallCountMapper.class);
            return mapper.insertApiCallCount(callCountDTO);
        }
    }

    public List<ApiCallCountDTO> getApiCallCountList(RequestApiCallCountDTO requestApiCallCountDTO){
        try (SqlSession session = sessionFactory.openSession(true)){
            ApiCallCountMapper mapper = session.getMapper(ApiCallCountMapper.class);
            return mapper.getApiCallCountList(requestApiCallCountDTO);
        }
    }

}
