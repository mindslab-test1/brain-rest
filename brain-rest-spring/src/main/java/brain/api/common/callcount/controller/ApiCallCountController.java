package brain.api.common.callcount.controller;

import brain.api.common.callcount.data.ApiCallCountQuotaDTO;
import brain.api.common.callcount.data.RequestApiCallCountDTO;
import brain.api.common.callcount.service.ApiCallCountService;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static brain.api.common.CommonCode.COMMON_CLASSNAME_CONTROLLER;
import static brain.api.common.callcount.CallCountCommonCode.COMMON_CALL_COUNT_SERVICE;

@RequiredArgsConstructor
@RequestMapping("/call-count")
@RestController
public class ApiCallCountController {
    private final CloudApiLogger logger = new CloudApiLogger(COMMON_CALL_COUNT_SERVICE, COMMON_CLASSNAME_CONTROLLER);
    private final ApiCallCountService apiCallCountService;

    @PostMapping(value = "/get", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getApiCallCount(@RequestBody RequestApiCallCountDTO requestApiCallCountDTO, HttpServletRequest request) {
        try {
            return ResponseEntity.ok().body(apiCallCountService.getCallCountList(requestApiCallCountDTO, request));
        } catch (MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.COMMON_ERR_INTERNAL.getStatus()).body(new ErrorResponse());
        }
    }

    @PostMapping(value = "/quota:update", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateApiCallCountQuota(@RequestBody ApiCallCountQuotaDTO apiCallCountQuotaDTO) {
        try {
            return ResponseEntity.ok().body(apiCallCountService.updateApiCallCountQuota(apiCallCountQuotaDTO));
        } catch (MindsRestException e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.getHttpStatusOnException(e)).body(new ErrorResponse(e));
        } catch(Exception e) {
            logger.error(e.getLocalizedMessage());
            return ResponseEntity.status(CommonExceptionCode.COMMON_ERR_INTERNAL.getStatus()).body(new ErrorResponse());
        }
    }
}
