package brain.api.common.callcount;

public class CallCountCommonCode {
    public static final String COMMON_CALL_COUNT_SERVICE = "CallCount";
    public static final String COMMON_CALL_COUNT_ANONYMOUS = "anonymous";
    public static final String CALL_COUNT_QUOTA_BASEMAP_TIME_QUOTA_KEY = "timeQuota";
    public static final String CALL_COUNT_QUOTA_BASEMAP_REQUEST_QUOTA_KEY = "requestQuota";
    public static final String CALL_COUNT_QUOTA_BASEMAP_LIST_QUOTA_KEY = "listFlushQuota";

    private CallCountCommonCode() {}
}
