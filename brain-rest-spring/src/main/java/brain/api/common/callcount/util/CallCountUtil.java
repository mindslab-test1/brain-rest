package brain.api.common.callcount.util;

import brain.api.common.CommonCode;
import brain.api.common.callcount.CallCountCommonCode;
import brain.api.common.log.CloudApiLogger;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.sqlserver.jdbc.StringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static brain.api.common.CommonCode.*;

public class CallCountUtil {
    private static final CloudApiLogger logger = new CloudApiLogger(CallCountCommonCode.COMMON_CALL_COUNT_SERVICE, CommonCode.COMMON_CLASSNAME_UTILS);

    public static String extractApiIdInRequest(HttpServletRequest request) throws IOException {
        String apiId = null;
        if (isMimeTypeJson(request)) {
           String requestBody = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
           if (isValidRequestJSON(requestBody) && requestBody.contains(CONST_STRING_API_ID)) {
               apiId = convertStringToJsonNode(requestBody).get(CONST_STRING_API_ID).asText();
           }
        } else {
            apiId = request.getParameter(CONST_STRING_API_ID);
        }

        return apiId;
    }

    public static String extractRequestBodyInJSON(HttpServletRequest request) throws IOException {
        JsonNode jsonNode = convertStringToJsonNode(IOUtils.toString(request.getInputStream(), request.getCharacterEncoding()));
        List<String> jsonNodeKeyList = new ArrayList<>();
        jsonNode.fieldNames().forEachRemaining(jsonNodeKeyList::add);
        Map<String, String> jsonNodeMap = jsonNodeKeyList.stream().collect(Collectors.toMap(key -> key, key -> jsonNode.get(key).asText()));

        return appendString(jsonNodeMap, true).toString();
    }

    public static String extractRequestBodyInFormData(HttpServletRequest request) {
        StringBuffer sb;
        List<String> paramsList = Collections.list(request.getParameterNames());
        Map<String, String> formDataMap = paramsList.stream().collect(Collectors.toMap(param -> param, param -> request.getParameter(param)));
        sb = appendString(formDataMap, false);
        if (request.getContentType().contains(ContentType.MULTIPART_FORM_DATA.getMimeType())) {
            if (request.getMethod().equalsIgnoreCase(HttpMethod.POST.name())) {
                MultiValueMap<String, MultipartFile> multiFileMap = ((DefaultMultipartHttpServletRequest) request).getMultiFileMap();
                sb.append(appendMultipartFile(multiFileMap));
            } else {
                StandardMultipartHttpServletRequest standardMultipartHttpServletRequest = new StandardMultipartHttpServletRequest(request);
                MultiValueMap<String, MultipartFile> multiFileMap = standardMultipartHttpServletRequest.getMultiFileMap();
                sb.append(appendMultipartFile(multiFileMap));
            }
        }
        return sb.toString();
    }

    private static String appendMultipartFile(MultiValueMap<String, MultipartFile> multiValueMap) {
        StringBuffer sb = new StringBuffer();

        if (multiValueMap.size() != 0) {
            multiValueMap.keySet().forEach(key -> {
                sb.append(COMMON_CONST_COMMA + StringUtils.SPACE + key + "=" + multiValueMap.get(key).get(0).getOriginalFilename()
                        + COMMON_CONST_COMMA + StringUtils.SPACE + "type=" + multiValueMap.get(key).get(0).getContentType()
                        + COMMON_CONST_COMMA + StringUtils.SPACE + "size=" + multiValueMap.get(key).get(0).getSize());
            });
        }

        return sb.toString();
    }

    private static StringBuffer appendString(Map<String, String> sentenceMap, boolean jsonFlag) {
        StringBuffer sb = new StringBuffer();
        AtomicInteger index = new AtomicInteger();
        sentenceMap.entrySet().forEach(map -> {
            if (map.getValue() != null && !map.getValue().isEmpty() && map.getValue().length() >= STRING_COMPRESSION_CRITERIA) {
                map.setValue(map.getValue().substring(0, STRING_COMPRESSION_CRITERIA) + "...(" + (map.getValue().length() - STRING_COMPRESSION_CRITERIA) + ")");
            }
            if (sentenceMap.size() - 1 == index.get()) {
                sb.append(map.getKey() + "=" + map.getValue());
            } else {
                sb.append(map.getKey() + "=" + map.getValue() + COMMON_CONST_COMMA + StringUtils.SPACE);
            }
            index.incrementAndGet();
        });
        if (jsonFlag) {
            sb.insert(0, "{");
            sb.append("}");
        }
        return sb;
    }

    public static boolean isMimeTypeJson(HttpServletRequest request) {
        return request.getContentType().contains(ContentType.APPLICATION_JSON.getMimeType());
    }

    public static JsonNode convertStringToJsonNode(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(json);
        return jsonNode;
    }

    public static boolean isValidRequestJSON(String json) {
        boolean valid = false;
        try {
            final JsonParser parser = new ObjectMapper().getJsonFactory()
                    .createJsonParser(json);
            while (parser.nextToken() != null) {
            }
            valid = true;
        } catch (JsonParseException jpe) {
            logger.error(jpe.getLocalizedMessage());
        } catch (IOException ioe) {
            logger.error(ioe.getLocalizedMessage());
        }

        return valid;
    }
}
