package brain.api.common.callcount.data;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ApiCallCountQuotaDTO {
    private Long timeQuota;
    private Long requestQuota;
    private Long listFlushQuota;
}
