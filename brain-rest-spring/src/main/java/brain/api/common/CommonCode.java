package brain.api.common;

import io.grpc.Metadata;

import java.nio.charset.StandardCharsets;

public class CommonCode {
    private CommonCode(){}

    // success
    public static final int COMMON_SUCCESS = 0;
    public static final String COMMON_MSG_SUCCESS = "Success";

    public static final String COMMON_SERVICE_NAME = "COMMON";
    public static final String COMMON_CLASSNAME_CONTROLLER = "Controller";
    public static final String COMMON_CLASSNAME_INTERCEPTOR = "Interceptor";
    public static final String COMMON_CLASSNAME_SERVICE = "Service";
    public static final String COMMON_CLASSNAME_CLIENT = "Client";
    public static final String COMMON_CLASSNAME_DAO = "Dao";
    public static final String COMMON_CLASSNAME_UTILS = "Utils";
    public static final String COMMON_CLASSNAME_REST = "Rest";
    public static final String COMMON_CLASSNAME_HANDLER = "Handler";
    public static final String COMMON_CLASSNAME_SCHEDULER = "Scheduler";
    public static final String COMMON_FIRST_CLASS_COLLECTION = "First-Collection";
    public static final String COMMON_CLASSNAME_CONFIG = "Config";

    public enum Lang{
        ko_KR,
        en_US
    }

    public enum ClientImpl {
        GRPC,
        SOAP
    }

    public static final String CONST_STRING_API_ID = "apiId";
    public static final char COMMON_CONST_COMMA = ',';
    public static final int API_CLIENT_ENABLE = 0;
    public static final int API_CLIENT_DISABLE = -1;
    public static final int API_CLIENT_ADMIN = 99;
    public static final String API_CLIENT_ADMIN_MSG = "API Admin";
    public static final int STRING_COMPRESSION_CRITERIA = 64;

    public static final String DOWNLOAD_PATH = "/home/ubuntu/maum/download/TTS";
    public static final String WAV_SUFFIX = ".wav";
    public static final String PCM_SUFFIX = ".pcm";

    public static final String COMMON_CONST_STRING_REQ_BODY = "requestBody";

    public static final String IMAGE = "image";
    public static final String TEXT = "text";
    public static final String WAV = "wav";
    public static final String NEWLINE = "\n";

    public static final String MINDS_LOG_ID = "X-Minds-Trace-Id";
    public static final String TRACE_ID_KEY = "traceId";

    // AWS
    public static final String AWS_ALB_LOG_ID = "x-amzn-trace-id";

    // Grpc
    public static final Metadata.Key<String> GRPC_REQUEST_KEY = Metadata.Key.of(MINDS_LOG_ID.toLowerCase().concat("-bin"), new Metadata.BinaryMarshaller<String>() {
        @Override
        public byte[] toBytes(String value) {
            return value.getBytes(StandardCharsets.UTF_8);
        }

        @Override
        public String parseBytes(byte[] bytes) {
            return new String(bytes, StandardCharsets.UTF_8);
        }
    });
}
