package brain.api.common.utils;

import brain.api.common.CommonCode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * example:
 *  - X-Amzn-Trace-Id: Self=1-67891234-12456789abcdef012345678;Root=1-67891233-abcdef012345678912345678;CalledFrom=app
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AWSALBLogUtils {
    private static final String ROOT_KEY = "Root";
    private static final String SELF_KEY = "Self";
    private static final String SEMICOLON = ";";
    private static final String EQUAL_SIGN = "=";

    private static String[] splitField(String traceFull) {
        return traceFull.split(SEMICOLON);
    }

    public static String getRootTraceId(String traceFull) {
        List<String> keyValueList = Arrays.asList(splitField(traceFull));
        String keyValueString = keyValueList.stream().filter(s -> s.contains(ROOT_KEY)).findAny()
                .orElseThrow(() -> new RuntimeException(String.format("Not found field(%s) in %s header", ROOT_KEY, CommonCode.AWS_ALB_LOG_ID)));
        String[] keyValueArray = keyValueString.split(EQUAL_SIGN);
        return keyValueArray[1];
    }

    public static String getSelfTraceId(String traceFull) {
        List<String> keyValueList = Arrays.asList(splitField(traceFull));
        String keyValueString = keyValueList.stream().filter(s -> s.contains(SELF_KEY)).findAny()
                .orElseThrow(() -> new RuntimeException(String.format("Not found field(%s) in %s header", SELF_KEY, CommonCode.AWS_ALB_LOG_ID)));
        String[] keyValueArray = keyValueString.split(EQUAL_SIGN);
        return keyValueArray[1];
    }

    public static String getFieldTraceId(String traceFull, String field) {
        List<String> keyValueList = Arrays.asList(splitField(traceFull));
        String keyValueString = keyValueList.stream().filter(s -> s.contains(field)).findAny()
                .orElseThrow(() -> new RuntimeException(String.format("Not found field(%s) in %s header", field, CommonCode.AWS_ALB_LOG_ID)));
        String[] keyValueArray = keyValueString.split(EQUAL_SIGN);
        return keyValueArray[1];
    }
}
