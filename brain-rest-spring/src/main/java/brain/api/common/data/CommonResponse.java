package brain.api.common.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommonResponse<T> {
    private CommonMsg message;
    private T payload;

    public CommonResponse(CommonMsg message) {
        this.message = message;
    }

    public CommonResponse(CommonMsg message, T payload) {
        this.message = message;
        this.payload = payload;
    }
}
