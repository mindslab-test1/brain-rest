package brain.api.common.data;

import brain.api.common.CommonCode;
import brain.api.common.enums.CommonExceptionCode;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommonMsg implements Serializable {
    private String message;
    private int status;

    public CommonMsg() {
        this.message = CommonCode.COMMON_MSG_SUCCESS;
        this.status = CommonCode.COMMON_SUCCESS;
    }

    public CommonMsg(String message) {
        this.message = message;
    }

    public CommonMsg(int status) {
        CommonExceptionCode exceptionCode = CommonExceptionCode.getCommonExceptionCode(status);
        this.status = exceptionCode.getErrCode();
        this.message = exceptionCode.getMessage();
    }

    public CommonMsg(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return "CommonMsg{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
