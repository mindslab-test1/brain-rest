package brain.api.common.data;

import brain.api.common.enums.CommonExceptionCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.CommonCode;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseResponse implements Serializable {
    private String message;
    private int status;
    private Object data;

    public BaseResponse(){
        this.message = CommonCode.COMMON_MSG_SUCCESS;
        this.status = CommonCode.COMMON_SUCCESS;
    }

    public BaseResponse(Object data) {
        this.message = CommonCode.COMMON_MSG_SUCCESS;
        this.status = CommonCode.COMMON_SUCCESS;
        this.data = data;
    }

    public BaseResponse(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", data=" + data +
                '}';
    }
}
