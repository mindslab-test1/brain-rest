package brain.api.common.data;

import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ErrorResponse implements Serializable {
    private CommonMsg message;
    private Object data;

    public ErrorResponse() {
        this.message = new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode());
        this.data = message.getMessage();
    }

    public ErrorResponse(int errCode) {
        this.message = new CommonMsg(errCode);
        this.data = message.getMessage();
    }

    public ErrorResponse(MindsRestException e) {
        this.message = new CommonMsg(e.getErrCode());
        this.data = e.getLocalizedMessage();
    }

    public ErrorResponse(int errCode, String msg) {
        this.message = new CommonMsg(errCode);
        this.data = msg;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "message=" + message +
                ", data=" + data +
                '}';
    }
}
