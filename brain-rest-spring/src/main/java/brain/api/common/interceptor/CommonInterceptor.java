package brain.api.common.interceptor;

import brain.api.common.callcount.data.ApiCallCountDTO;
import brain.api.common.callcount.service.ApiCallCountService;
import brain.api.common.callcount.util.CallCountUtil;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;

import static brain.api.common.CommonCode.COMMON_CLASSNAME_INTERCEPTOR;
import static brain.api.common.CommonCode.COMMON_SERVICE_NAME;
import static brain.api.common.callcount.CallCountCommonCode.COMMON_CALL_COUNT_ANONYMOUS;

@Component
@RequiredArgsConstructor
public class CommonInterceptor extends HandlerInterceptorAdapter {
    private static final CloudApiLogger logger = new CloudApiLogger(COMMON_CLASSNAME_INTERCEPTOR, COMMON_SERVICE_NAME);
    private final ApiCallCountService apiCallCountService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException, MindsRestException {
        if (!HttpMethod.GET.matches(request.getMethod())) {
            String apiId = CallCountUtil.extractApiIdInRequest(request);
            apiCallCountService.restoreMemoryCallCountQuota();
            if (!StringUtils.isEmpty(apiId) && apiCallCountService.isBlocked(apiId)) {
                response.getWriter().write(new ObjectMapper().writeValueAsString(
                        new ErrorResponse(CommonExceptionCode.COMMON_ERR_TOO_MANY_REQUEST.getErrCode(),
                                "Your API ID Is Blocked!, Please request " +
                                        TimeUnit.SECONDS.toMinutes(apiCallCountService.getQuota().getTimeQuota())
                                        + " minutes later")));
                response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
                response.setCharacterEncoding(StandardCharsets.UTF_8.name());
                response.setStatus(CommonExceptionCode.COMMON_ERR_TOO_MANY_REQUEST.getStatus().value());
                return false;
            }
        }

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws IOException {
        String apiId = CallCountUtil.extractApiIdInRequest(request);
        String anonymousRequestValue = null;

        if (StringUtils.isEmpty(apiId) && CallCountUtil.isMimeTypeJson(request)) {
            apiId = COMMON_CALL_COUNT_ANONYMOUS;
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            anonymousRequestValue = CallCountUtil.extractRequestBodyInJSON(request);
        }

        if(StringUtils.isEmpty(apiId) && !CallCountUtil.isMimeTypeJson(request)) {
            apiId = COMMON_CALL_COUNT_ANONYMOUS;
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            anonymousRequestValue = CallCountUtil.extractRequestBodyInFormData(request);
        }

        apiCallCountService.putCallCount(new ApiCallCountDTO(apiId, request.getRequestURI(),
                response.getStatus(), LocalDateTime.now(ZoneOffset.UTC), anonymousRequestValue));
    }
}
