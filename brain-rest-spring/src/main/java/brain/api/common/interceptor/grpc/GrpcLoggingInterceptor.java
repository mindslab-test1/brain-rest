package brain.api.common.interceptor.grpc;

import brain.api.common.CommonCode;
import brain.api.common.utils.CommonUtils;
import io.grpc.*;
import org.slf4j.MDC;

public class GrpcLoggingInterceptor implements ClientInterceptor {
    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, Channel channel) {

        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel.newCall(methodDescriptor, callOptions)) {
            @Override
            public void start(Listener<RespT> responseListener, Metadata headers) {
                String traceId = CommonUtils.generateRequestUniqueKey(false);
                MDC.put(CommonCode.TRACE_ID_KEY, traceId);

                headers.put(CommonCode.GRPC_REQUEST_KEY, traceId);

                delegate().start(responseListener, headers);
            }
        };
    }
}
