package brain.api.common.enums;

import brain.api.common.exception.MindsRestException;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

@Getter
public enum CommonExceptionCode {

    /**
     * Define Http Status Code
     */
    COMMON_ERR_BAD_REQUEST(4000, HttpStatus.BAD_REQUEST, "Bad request"),
    COMMON_ERR_NOT_SUPPORTED_LANGUAGE(4000, HttpStatus.BAD_REQUEST,
            "Requested model is not supported for "),
    COMMON_ERR_UNSUPPORTED_MEDIA(4000, HttpStatus.BAD_REQUEST, "Unsupported media exception"),
    COMMON_ERR_UNAUTHORIZED(4001, HttpStatus.UNAUTHORIZED, "Unknown API id / key has been presented"),
    COMMON_ERR_PAYMENT_REQUIRED(4002, HttpStatus.PAYMENT_REQUIRED, "Payment required"),
    COMMON_ERR_FORBIDDEN(4003, HttpStatus.FORBIDDEN, "Request forbidden"),
    COMMON_ERR_NOT_FOUND(4004, HttpStatus.NOT_FOUND, "Not found"),
    COMMON_ERR_TOO_MANY_REQUEST(4029, HttpStatus.TOO_MANY_REQUESTS, "Too many Requests"),
    COMMON_ERR_NOT_ACCEPTABLE(4006, HttpStatus.NOT_ACCEPTABLE, "Not Acceptable"),
    COMMON_ERR_INTERNAL(5000, HttpStatus.INTERNAL_SERVER_ERROR,
            "Internal server error: please contact our organization"),
    COMMON_ERR_NOT_IMPLEMENTED(5001, HttpStatus.NOT_IMPLEMENTED, "Not implemented"),
    COMMON_ERR_SERVICE_UNAVAILABLE(5003, HttpStatus.SERVICE_UNAVAILABLE,
            "Service currently unavailable"),

    /**
     * Define User Custom Code
     */
    COMMON_ERR_UNKNOWN(6000, HttpStatus.INTERNAL_SERVER_ERROR,
            "Unknown: please contact our organization"),
    COMMON_TEXT_STYLE_CUSTOM_ERROR_CODE(7000, HttpStatus.INTERNAL_SERVER_ERROR, "TextStyleTransfer gRPC Error"),
    COMMON_DAP_ERR_TOO_LARGE_FILE(40001, HttpStatus.BAD_REQUEST, "File size is Too Large.");

    private final int errCode;
    private final HttpStatus status;
    private final String message;

    CommonExceptionCode(int errCode, HttpStatus status, String message) {
        this.errCode = errCode;
        this.status = status;
        this.message = message;
    }

    public static HttpStatus getHttpStatusOnException(MindsRestException e) {
        return Arrays.stream(values())
                .filter(enums -> enums.errCode == e.getErrCode()).findFirst()
                .orElse(CommonExceptionCode.COMMON_ERR_INTERNAL).getStatus();
    }

    public static CommonExceptionCode getCommonExceptionCode(int errCode) {
        return Arrays.stream(values())
                .filter(enums -> enums.errCode == errCode).findFirst()
                .orElse(CommonExceptionCode.COMMON_ERR_INTERNAL);
    }

}
