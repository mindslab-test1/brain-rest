package brain.api.common.exception;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;

public class MindsRestException extends Exception{
    private final int errCode;

    public MindsRestException(){
        super(CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage());
        this.errCode = CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode();
    }

    public MindsRestException(String message){
        super(message);
        this.errCode = CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode();
    }

    public MindsRestException(int errCode, String message){
        super(message);
        this.errCode = errCode;
    }

    public MindsRestException(String message, Throwable cause) {
        super(message, cause);
        this.errCode = CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode();
    }

    public MindsRestException(Throwable cause){
        super(cause);
        this.errCode = CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode();
    }

    public MindsRestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errCode = CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode();
    }


    public int getErrCode() {
        return this.errCode;
    }

    public CommonMsg createMessage(){
        return new CommonMsg(
                this.getErrCode(),
                this.getMessage()
        );
    }

}
