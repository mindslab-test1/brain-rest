package brain.api.common.filter;

import brain.api.common.CommonCode;
import brain.api.common.utils.AWSALBLogUtils;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static brain.api.common.CommonCode.AWS_ALB_LOG_ID;

@RequiredArgsConstructor
@Component
public class LoggingFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest httpServletRequest,
                                    @NotNull HttpServletResponse httpServletResponse,
                                    @NotNull FilterChain filterChain) throws ServletException, IOException {
        List<String> headers = Collections.list(httpServletRequest.getHeaderNames());
        String logId = UUID.randomUUID().toString();
        for(String header : headers) {
            switch (header) {
                case AWS_ALB_LOG_ID:
                    String value = httpServletRequest.getHeader(header);
                    logId = AWSALBLogUtils.getRootTraceId(value);
                    break;
                default:
                    break;
            }
        }

        MDC.put(CommonCode.TRACE_ID_KEY, logId);
        httpServletResponse.setHeader(CommonCode.MINDS_LOG_ID, logId);

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
