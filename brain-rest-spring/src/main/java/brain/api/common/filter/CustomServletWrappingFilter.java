package brain.api.common.filter;

import brain.api.common.ReadableRequestBodyWrapper;
import brain.api.common.callcount.util.CallCountUtil;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


@Component
public class CustomServletWrappingFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (httpServletRequest.getContentType() == null) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }
        if (httpServletRequest.getContentType().contains(ContentType.MULTIPART_FORM_DATA.getMimeType()) &&
            !httpServletRequest.getMethod().equalsIgnoreCase(HttpMethod.POST.name())) {
            httpServletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
            httpServletResponse.setStatus(HttpStatus.SC_NOT_ACCEPTABLE);
            httpServletResponse.getWriter().write(new ObjectMapper().writeValueAsString(
                    new ErrorResponse(CommonExceptionCode.COMMON_ERR_NOT_ACCEPTABLE.getErrCode(),
                            "HTTP Method " + httpServletRequest.getMethod() +" AND Content-Type is Not Accept multipart/form-data")));
            return;
        }
        if (CallCountUtil.isMimeTypeJson(httpServletRequest)) {
            ReadableRequestBodyWrapper wrapper = new ReadableRequestBodyWrapper(httpServletRequest);
            filterChain.doFilter(wrapper, httpServletResponse);
        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

}
