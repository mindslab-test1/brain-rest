package brain.api.common;

import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class CommonMultipartFormDataHandler {

    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_SERVICE_NAME, CommonCode.COMMON_CLASSNAME_HANDLER);

    public Map<String, Object> parse(byte[] bytes) throws IOException, MessagingException, MindsRestException {
        InputStream in = new BufferedInputStream(new ByteArrayInputStream(bytes));
        ByteArrayDataSource dataSource = new ByteArrayDataSource(in, MediaType.MULTIPART_FORM_DATA_VALUE);
        MimeMultipart multipart = new MimeMultipart(dataSource);

        Map<String, Object> map = new LinkedHashMap<>();
        String name;

        for (int i = 0; i < multipart.getCount(); i++) {
            BodyPart bodyPart = multipart.getBodyPart(i);

            name = getNameFromHeader(bodyPart);

            map.put(name, extractByContentType(bodyPart));
        }
        logger.debug("Parsing Result: " + map);
        return map;
    }

    private Object extractByContentType(BodyPart bodyPart) throws MessagingException, IOException {
        if (bodyPart.isMimeType(MediaType.TEXT_PLAIN_VALUE)) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream(), StandardCharsets.UTF_8));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            String strResult = result.toString();

            return strResult;
        } else if (bodyPart.isMimeType(MediaType.IMAGE_JPEG_VALUE)) {
            InputStream multiIn = bodyPart.getInputStream();
            byte[] resultImage = IOUtils.toByteArray(multiIn);

            return Base64.getMimeDecoder().decode(resultImage);
        } else {
            return null;
        }
    }

    private String getNameFromHeader(BodyPart bodyPart) {
        String result = "";

        try {
            Enumeration<Header> enumeration = bodyPart.getAllHeaders();
            while (enumeration.hasMoreElements()) {
                Header next = enumeration.nextElement();
                String name = next.getName();
                String value = next.getValue();

                if (HttpHeaders.CONTENT_DISPOSITION.equals(name)) {
                    String[] headers = value.split(" ");
                    for (String header : headers) {
                        if ("name".equals(header.split("=")[0])) {
                            result = header.split("=")[1].replaceAll("\"", "").replaceAll(";", "");
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }

        return result;
    }
}
