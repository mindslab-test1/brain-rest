package brain.api.common.client;

import brain.api.common.CommonCode;
import brain.api.common.data.ResponseUploadedFileInfo;
import brain.api.common.log.CloudApiLogger;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
@Component
public class MediaClient {
    private final RestTemplate restTemplate;

    private static final CloudApiLogger logger = new CloudApiLogger("Media", CommonCode.COMMON_CLASSNAME_CLIENT);

    @Async("mediaThreadExecutor")
    public CompletableFuture<ResponseUploadedFileInfo> uploadFile(String apiId, String model, byte[] data, String filenameWithExtension, String requestUniqueKey) throws IOException {
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        ByteArrayResource fileResource = new ByteArrayResource(data) {
            // 기존 ByteArrayResource의 getFilename 메서드 override
            @Override
            public String getFilename() {
                return requestUniqueKey + "_" + apiId + "_" + model + "_" + filenameWithExtension;
            }
        };

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileResource);
        body.add("uploadDirectory", "/engine-api/stt" + File.separator + model + File.separator + fileResource.getFilename());

        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        ResponseEntity<ResponseUploadedFileInfo> responseEntity = null;
        try {
            responseEntity = restTemplate.exchange(URI.create("https://media.maum.ai/media/file:upload"),
                    HttpMethod.POST,
                    requestMasterHttpEntity,
                    new ParameterizedTypeReference<ResponseUploadedFileInfo>() {});
        } catch (RestClientException e) {
            logger.error(requestUniqueKey + " => " + e.getLocalizedMessage());
        }
        logger.info("Uploaded-File: " + responseEntity.getBody());
        return CompletableFuture.completedFuture(responseEntity.getBody());
    }
}
