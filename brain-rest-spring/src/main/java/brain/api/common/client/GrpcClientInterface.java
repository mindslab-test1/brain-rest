package brain.api.common.client;

import javax.annotation.PostConstruct;

public class GrpcClientInterface {

    protected String destIp;
    protected int destPort;

    @PostConstruct
    private void init(){
        this.destIp = "";
        this.destPort = 0;
    }

    protected boolean checkDestination(String ip, int port){
        if(!destIp.equals(ip) || destPort != port){
            this.destIp = ip;
            this.destPort = port;
            return false;
        } else return true;
    }
}
