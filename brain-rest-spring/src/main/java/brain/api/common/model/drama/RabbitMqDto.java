package brain.api.common.model.drama;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Map;

@Repository
public class RabbitMqDto {
    private String queue_name;
    private String actor_name;
    private Object[] args;
    private Map <String, String> kwargs;
    private Map<String, String> options;
    private String message_id;
    private Long message_timestamp;

    public String getQueue_name() {
        return queue_name;
    }

    public void setQueue_name(String queue_name) {
        this.queue_name = queue_name;
    }

    public String getActor_name() {
        return actor_name;
    }

    public void setActor_name(String actor_name) {
        this.actor_name = actor_name;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Object getKwargs() {
        return kwargs;
    }

    public void setKwargs(Map<String, String> kwargs) {
        this.kwargs = kwargs;
    }

    public Object getOptions() {
        return options;
    }

    public void setOptions(Map<String, String > options) {
        this.options = options;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public Long getMessage_timestamp() {
        return message_timestamp;
    }

    public void setMessage_timestamp(Long message_timestamp) {
        this.message_timestamp = message_timestamp;
    }

    @Override
    public String toString() {
        return "RabbitMqRequestVO{" +
                "queue_name='" + queue_name + '\'' +
                ", actor_name='" + actor_name + '\'' +
                ", args=" + Arrays.toString(args) +
                ", kwargs=" + kwargs +
                ", options=" + options +
                ", message_id='" + message_id + '\'' +
                ", message_timestamp=" + message_timestamp +
                '}';
    }
}
