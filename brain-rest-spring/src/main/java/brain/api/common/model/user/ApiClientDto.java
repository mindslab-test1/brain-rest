package brain.api.common.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiClientDto {
    private String name;
    private String email;
    private String apiId;
    private String apiKey;
    private int grade;

    public ApiClientDto() {

    }

    public ApiClientDto(String apiId, int grade) {
        this.apiId = apiId;
        this.grade = grade;
    }

    public ApiClientDto(String name, String email, String apiId) {
        this.name = name;
        this.email = email;
        this.apiId = apiId;
        this.grade = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "ApiClientDto{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", apiId='" + apiId + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", grade=" + grade +
                '}';
    }
}
