package brain.api.common.model.usage;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SumUsageDto {
    private String service;
    private int sumUsage;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getSumUsage() {
        return sumUsage;
    }

    public void setSumUsage(int sumUsage) {
        this.sumUsage = sumUsage;
    }

    @Override
    public String toString() {
        return "SumUsageDto{" +
                "service='" + service + '\'' +
                ", sumUsage=" + sumUsage +
                '}';
    }
}
