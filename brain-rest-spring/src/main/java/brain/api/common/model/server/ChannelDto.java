package brain.api.common.model.server;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChannelDto {
    private String host;
    private int port;
    private String version;
}
