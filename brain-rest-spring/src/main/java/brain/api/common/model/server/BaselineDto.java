package brain.api.common.model.server;

public class BaselineDto {
    private int id;
    private String service;
    private String ip;
    private int port;
    private String opts;

    public BaselineDto(String service, String opts) {
        this.service = service;
        this.opts = opts;
    }

    public BaselineDto(int id, String service, String ip, int port, String opts) {
        this.id = id;
        this.service = service;
        this.ip = ip;
        this.port = port;
        this.opts = opts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getOpts() {
        return opts;
    }

    public void setOpts(String opts) {
        this.opts = opts;
    }

    @Override
    public String toString() {
        return "BaselineDto{" +
                "id=" + id +
                ", service='" + service + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", opts='" + opts + '\'' +
                '}';
    }
}
