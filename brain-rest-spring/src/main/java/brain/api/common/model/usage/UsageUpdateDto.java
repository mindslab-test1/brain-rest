package brain.api.common.model.usage;

public class UsageUpdateDto {
    private String table;
    private String service;
    private int size;
    private String logId;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    @Override
    public String toString() {
        return "UsageUpdateDto{" +
                "table='" + table + '\'' +
                ", service='" + service + '\'' +
                ", size=" + size +
                ", logId='" + logId + '\'' +
                '}';
    }
}
