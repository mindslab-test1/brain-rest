package brain.api.common.model.usage;

public class UsageTrackDto {
    private int id;
    private String service;
    private int size;
    private String logId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    @Override
    public String toString() {
        return "UsageTrackDto{" +
                "id=" + id +
                ", service='" + service + '\'' +
                ", size=" + size +
                ", logId='" + logId + '\'' +
                '}';
    }
}
