package brain.api.common.model.server;

import brain.api.common.CommonCode;
import brain.api.common.log.CloudApiLogger;
import brain.db.common.BrainRestSqlConfig;
import brain.db.mapper.ModelMapper;
import brain.db.mapper.ServerMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ServerDao {

    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_SERVICE_NAME, CommonCode.COMMON_CLASSNAME_DAO);

    private SqlSessionFactory sessionFactory = BrainRestSqlConfig.getSessionFactory();

    public List<ServerDto> getServerByService(String service){
        try (SqlSession session = sessionFactory.openSession(true)){
            ServerMapper mapper = session.getMapper(ServerMapper.class);
            return mapper.getServerByService(service);
        }
    }

    public BaselineDto getBaselineServer(String service){
        try (SqlSession session = sessionFactory.openSession(true)){
            logger.debug("get baseline server by service");
            ServerMapper mapper = session.getMapper(ServerMapper.class);
            return mapper.getBaseServer(service);
        }
    }

    public BaselineDto getBaselineServer(String service, String options){
        try (SqlSession session = sessionFactory.openSession(true)){
            logger.debug("get baseline server by service and options");
            BaselineDto dto = new BaselineDto(service, options);
            ServerMapper mapper = session.getMapper(ServerMapper.class);
            return mapper.getBaseServerWithOpts(dto);
        }
    }

    public ChannelDto getLipSyncServerInfo(String modelName){
        try (SqlSession session = sessionFactory.openSession(true)){
            Map<String, String> params = new HashMap<>();
            params.put("model", modelName);
            ModelMapper mapper = session.getMapper(ModelMapper.class);
            return mapper.selectLipsyncModelVal(params);
        }
    }
}
