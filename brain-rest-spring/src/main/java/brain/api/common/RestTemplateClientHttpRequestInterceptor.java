package brain.api.common;

import brain.api.common.log.CloudApiLogger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static brain.api.common.CommonCode.NEWLINE;

public class RestTemplateClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {
    private static final CloudApiLogger logger = new CloudApiLogger(CommonCode.COMMON_CLASSNAME_REST, CommonCode.COMMON_SERVICE_NAME);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        loggingRequest(request, body);
        final ClientHttpResponse response = execution.execute(request, body);

        loggingResponse(response);
        return execution.execute(request, body);
    }

    private void loggingRequest(final HttpRequest request, byte[] body) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(NEWLINE);
        stringBuffer.append("==============================[Request]==============================").append(NEWLINE);
        stringBuffer.append("Headers : " + request.getHeaders()).append(NEWLINE);
        stringBuffer.append("Request Method : " + request.getMethod()).append(NEWLINE);
        stringBuffer.append("Request URI : " + request.getURI()).append(NEWLINE);
        stringBuffer.append("Request body : " + (body.length == 0 ? null : new String(body, StandardCharsets.UTF_8))).append(NEWLINE);
        logger.debug(stringBuffer.toString());
    }

    private void loggingResponse(ClientHttpResponse response) throws IOException {
        final String body = getBody(response);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(NEWLINE);
        stringBuffer.append("==============================[Response]==============================").append(NEWLINE);
        stringBuffer.append("Headers : " + response.getHeaders()).append(NEWLINE);
        stringBuffer.append("Response Status : " + response.getRawStatusCode()).append(NEWLINE);
        stringBuffer.append("Request body : " + body).append(NEWLINE);
        logger.debug(stringBuffer.toString());
    }

    private String getBody(@NonNull final ClientHttpResponse response) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(response.getBody()))) {
            StringBuffer stringBuffer = new StringBuffer();
            br.lines().forEach(stringBuffer::append);
            return stringBuffer.toString();
        } catch (IOException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
}
