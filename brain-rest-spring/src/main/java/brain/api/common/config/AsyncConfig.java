package brain.api.common.config;

import brain.api.common.log.LoggingTaskDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync(proxyTargetClass = true)
public class AsyncConfig implements AsyncConfigurer {

    @Bean("mediaThreadExecutor")
    public TaskExecutor executor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        threadPoolTaskExecutor.setMaxPoolSize(Runtime.getRuntime().availableProcessors() * 5);
        threadPoolTaskExecutor.setQueueCapacity(5);
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        threadPoolTaskExecutor.setTaskDecorator(new LoggingTaskDecorator());
        threadPoolTaskExecutor.setThreadNamePrefix("MediaThreadExecutor");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncExceptionHandler();
    }

    static class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
        private static final Logger logger = LoggerFactory.getLogger(AsyncExceptionHandler.class);

        @Override
        public void handleUncaughtException(Throwable throwable, Method method, Object... objects) {
            logger.error(generateThreadDump());
        }

        private String generateThreadDump() {
            StringBuilder dump = new StringBuilder();
            ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
            ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
            Arrays.stream(threadInfos).forEach(threadInfo -> {
                dump.append('"');
                dump.append(threadInfo.getThreadName());
                dump.append("\" ");
                Thread.State state = threadInfo.getThreadState();
                dump.append("\n java.lang.Thread.State: ");
                dump.append(state);
                StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
                Arrays.stream(stackTraceElements).forEach(stackTraceElement -> {
                    dump.append("\n        at ");
                    dump.append(stackTraceElement);
                });
                dump.append("\n\n");
            });
            return dump.toString();
        }
    }
}
