package brain.api.common.config;

import brain.api.common.CommonCode;
import brain.api.common.RedisCode;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.PostConstruct;

import static brain.api.common.RedisCode.REDIS_HOST;
import static brain.api.common.RedisCode.REDIS_PORT;

@Configuration
public class RedisConfig {
    private static final CloudApiLogger logger = new CloudApiLogger(RedisCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONFIG);

    @PostConstruct
    public void setup() throws MindsRestException {
        boolean connect = false;
        try {
            connect = !redisConnectionFactory().getConnection().isClosed();
            logger.info(String.format("%s:%s => Test Connection: %s", REDIS_HOST, REDIS_PORT, connect));
        } catch (Exception e) {
            logger.error(String.format("%s:%s => Test Connection: %s", REDIS_HOST, REDIS_PORT, connect));
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getLocalizedMessage());
        }
    }

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(REDIS_HOST, REDIS_PORT);
        // TODO: security를 위해 password 설정시 필요
//        redisStandaloneConfiguration.setPassword("");
        LettuceConnectionFactory lettuceConnectionFactory = new LettuceConnectionFactory(redisStandaloneConfiguration);
        return lettuceConnectionFactory;
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        redisTemplate.setConnectionFactory(redisConnectionFactory());
        return redisTemplate;
    }
}
