package brain.api.common.log;

import brain.api.common.CommonCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.message.Message;

@Slf4j(topic = "brain.api.logging")
public class CloudApiLogger {
    private final String logPrefix;

    public CloudApiLogger(String serviceName, String className){
        logger.debug(String.format("[%s] {%s} Make logger for: [%s] {%s}",
                CommonCode.COMMON_SERVICE_NAME,
                CommonCode.COMMON_CLASSNAME_UTILS,
                serviceName,
                className
        ));
        this.logPrefix = String.format("[%s] {%s} ", serviceName, className);
    }

    public void debug(String message){
        logger.debug(logPrefix + message);
    }

    public void debug(Message message){
        logger.debug(logPrefix + message);
    }

    public void debug(Object message){
        logger.debug(logPrefix + message);
    }

    public void info(String message){
        logger.info(logPrefix + message);
    }

    public void info(Message message){
        logger.info(logPrefix + message);
    }

    public void info(Object message){
        logger.info(logPrefix + message);
    }

    public void warn(String message){
        logger.warn(logPrefix + message);
    }

    public void warn(Message message){
        logger.warn(logPrefix + message);
    }

    public void warn(Object message){
        logger.warn(logPrefix + message);
    }

    public void error(String message){
        logger.error(logPrefix + message);
    }

    public void error(Message message){
        logger.error(logPrefix + message);
    }

    public void error(Object message){
        logger.error(logPrefix + message);
    }
}
