package brain.api.admin.controller;


import brain.api.admin.AdminCommonCode;
import brain.api.admin.service.AdminService;
import brain.api.common.CommonCode;
import brain.api.common.data.BaseResponse;
import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.user.ApiClientDto;
import brain.api.common.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private static final CloudApiLogger logger = new CloudApiLogger("ADMIN", CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    private AdminService service;

    @RequestMapping(
            value = "getClientInfo",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getClientInfo(@RequestBody Map<String, String> input ,HttpServletRequest request){
        try {
            checkAdmin(request);
            return new ResponseEntity(new BaseResponse(service.getClientInfo(input, request)), HttpStatus.OK);
        } catch (MindsRestException e) {
            return new ResponseEntity(new ErrorResponse(e), HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(
            value = "/addClient",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity addClient(@RequestBody Map <String, String> input, HttpServletRequest request){
        try {
            checkAdmin(request);
        } catch (MindsRestException e) {
            return new ResponseEntity(new ErrorResponse(e), HttpStatus.FORBIDDEN);
        }

        ApiClientDto clientDto = new ApiClientDto(
                input.get("name"),
                input.get("email"),
                input.get("apiId")
        );

        logger.debug(CommonUtils.getIp(request));
        try {
            return new ResponseEntity(service.addClient(clientDto), HttpStatus.OK);
        } catch (MindsRestException e) {
            return new ResponseEntity(new ErrorResponse(e), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(
            value = "/configClient",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public void configClient(){

    }

    @RequestMapping(
            value = "/delClient",
            method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity deleteClient(@RequestBody ApiClientDto dto, HttpServletRequest request){
        try {
            checkAdmin(request);
        } catch (MindsRestException e) {
            return new ResponseEntity(new ErrorResponse(e), HttpStatus.FORBIDDEN);
        }
        logger.debug(dto);
        if (service.deleteClient(dto.getApiId()) < 0) return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity(new BaseResponse(), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/resumeClient",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity resumeClient(@RequestBody ApiClientDto dto, HttpServletRequest request){
        try {
            checkAdmin(request);
        } catch (MindsRestException e) {
            return new ResponseEntity(new ErrorResponse(e), HttpStatus.FORBIDDEN);
        }
        logger.debug(dto);
        if (service.resumeClient(dto.getApiId()) < 0) return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);

        return new ResponseEntity(new BaseResponse(), HttpStatus.OK);
    }

    @RequestMapping(
            value = "/selectUsage",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity selectUsage(@RequestBody Map <String, String> input, HttpServletRequest request) {
        try {
            logger.debug(input);

            try {
                checkAdmin(request);
            } catch (MindsRestException e) {
                return new ResponseEntity(new ErrorResponse(e), HttpStatus.FORBIDDEN);
            }

            return new ResponseEntity(
                service.selectUsage(
                    input
                    , request
                ),
                HttpStatus.OK
            );
        } catch(MindsRestException e) {
            logger.warn(e.getMessage());
            // 임시로 500 ERR 출력
            return new ResponseEntity(new ErrorResponse(e), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void checkAdmin(HttpServletRequest request) throws MindsRestException {
        String ip = CommonUtils.getIp(request);

        switch(ip) {
            case "0:0:0:0:0:0:0:1":         // localhost
            case "125.132.250.204" :        // MindsLab 사내 공인 IP
            case "52.79.81.3":              // MindsLab 사내 공인 IP2
            case "54.180.67.236" :          // service.maum.ai
            case "13.125.81.216" :         // dev-common.maum.ai
            case "13.124.208.0" :           // stg-common.maum.ai
                break;
            default :
                throw new MindsRestException(AdminCommonCode.ADMIN_ERR_FORBIDDEN, AdminCommonCode.ADMIN_MSG_FORBIDDEN);
        }

    }

}
