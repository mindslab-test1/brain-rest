package brain.api.admin;

public class AdminCommonCode {
    private AdminCommonCode(){}

    public static final String SERVICE_NAME = "Admin";

    // Communication related error
    public static final int ADMIN_ERR_FORBIDDEN = 4003;
    public static final String ADMIN_MSG_FORBIDDEN = "Request forbidden user";

}
