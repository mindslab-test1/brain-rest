package brain.api.admin.service.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.model.server.ServerDto;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ServerListResponse implements Serializable {
    private List<ServerDto> grpcList;
    private int size;

    public ServerListResponse() {
    }

    public ServerListResponse(List<ServerDto> grpcList) {
        this.grpcList = grpcList;
        this.size = grpcList.size();
    }

    public List<ServerDto> getGrpcList() {
        return grpcList;
    }

    public void setGrpcList(List<ServerDto> grpcList) {
        this.grpcList = grpcList;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ServerListResponse{" +
                "grpcList=" + grpcList +
                ", size=" + size +
                '}';
    }
}
