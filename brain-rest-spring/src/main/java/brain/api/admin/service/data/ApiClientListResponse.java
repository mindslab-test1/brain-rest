package brain.api.admin.service.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.model.user.ApiClientDto;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiClientListResponse implements Serializable {
    private List<ApiClientDto> clientList;
    private int size;

    public ApiClientListResponse(){

    }

    public ApiClientListResponse(List<ApiClientDto> clientList) {
        this.clientList = clientList;
        this.size = clientList.size();
    }

    public List<ApiClientDto> getClientList() {
        return clientList;
    }

    public void setClientList(List<ApiClientDto> clientList) {
        this.clientList = clientList;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ApiClientListResponse{" +
                "clientList=" + clientList +
                ", size=" + size +
                '}';
    }
}
