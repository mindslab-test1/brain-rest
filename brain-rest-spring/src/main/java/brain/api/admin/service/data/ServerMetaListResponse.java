package brain.api.admin.service.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ServerMetaListResponse implements Serializable {
    private List<?> serverMetaList;
    private int size;

    public ServerMetaListResponse() {
    }

    public ServerMetaListResponse(List<?> serverMetaList) {
        this.serverMetaList = serverMetaList;
    }

    public List<?> getServerMetaList() {
        return serverMetaList;
    }

    public void setServerMetaList(List<?> serverMetaList) {
        this.serverMetaList = serverMetaList;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ServerMetaListResponse{" +
                "serverMetaList=" + serverMetaList +
                ", size=" + size +
                '}';
    }
}
