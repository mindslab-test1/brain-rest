package brain.api.aps.model;

public class ApsResponse {
    int state;
    String message;
    float errorRate;

    public ApsResponse(int state, String message) {
        this.message = message;
        this.state = state;
        this.errorRate = 100;
    }
    public ApsResponse(float errorRate) {
        this.state = 200;
        this.message = "sucess";
        this.errorRate = errorRate;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public float getErrorRate() {
        return errorRate;
    }

    public void setErrorRate(float errorRate) {
        this.errorRate = errorRate;
    }
}
