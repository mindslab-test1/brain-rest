package brain.api.aps.service;

import brain.api.aps.ApsCommonCode;
import brain.api.aps.client.ApsClient;
import brain.api.aps.model.ApsResponse;
import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Service
public class ApsService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(ApsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);

    @Autowired
    ApsClient apsClient;

    public ApsResponse pron(String apiId, String apiKey, MultipartFile file, String text, HttpServletRequest httpRequest) throws MindsRestException {
        validate(apiId, apiKey, ApsCommonCode.SERVICE_NAME, 1, httpRequest);
        apsClient.setDestination(ApsCommonCode.SERVER_IP, ApsCommonCode.SERVER_PORT);
        ApsResponse response;
        try {
           response = new ApsResponse(apsClient.doPron(file, text));
        } catch (IOException | MindsRestException e) {
            logger.error(e.getMessage());
            response = new ApsResponse(500, "Please Contact Us.");
        }
        return response;
    }
}
