package brain.api.aps.controller;

import brain.api.aps.ApsCommonCode;
import brain.api.aps.model.ApsResponse;
import brain.api.aps.service.ApsService;
import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/aps")
public class ApsController {

    private static final CloudApiLogger logger = new CloudApiLogger(ApsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    ApsService apsService;

    @PostMapping(
            value = "/pron",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    ) public ResponseEntity pron(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            @RequestParam("text") String text,
            HttpServletRequest httpRequest
    ) {
        ApsResponse result;
        try {
            result = apsService.pron(apiId, apiKey, file, text, httpRequest);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            int resultCode;
            resultCode = (e.getErrCode() / 10) + (e.getErrCode() % 10);
            return ResponseEntity.status(resultCode).body(new ApsResponse(resultCode, e.getMessage()));
        }
        return ResponseEntity.ok(result);
    }
}
