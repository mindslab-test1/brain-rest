package brain.api.aps;

public class ApsCommonCode {

    private ApsCommonCode(){}

    public static final String SERVICE_NAME = "APS";
    public static final String SERVER_IP = "182.162.19.23";
    public static final int SERVER_PORT = 21001;
}
