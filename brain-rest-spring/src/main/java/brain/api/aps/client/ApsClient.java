package brain.api.aps.client;

import brain.api.aps.ApsCommonCode;
import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import maum.brain.aps.Aps;
import maum.brain.aps.ApsServiceGrpc;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class ApsClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(ApsCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private ApsServiceGrpc.ApsServiceStub apsStub;

    public CommonMsg setDestination(String ip, int port) {
        try {
            if (super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.apsStub = ApsServiceGrpc.newStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

            return new CommonMsg();
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public float doPron(MultipartFile file, String text) throws IOException, MindsRestException {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        List<Float> resultArray = new ArrayList<>();
        List<String> errorList = new ArrayList<>();

        StreamObserver<Aps.ApsResponse> responseObserver = new StreamObserver<Aps.ApsResponse>() {
            @Override
            public void onNext(Aps.ApsResponse apsResponse) {
                resultArray.add(apsResponse.getErrorRate());
            }

            @Override
            public void onError(Throwable throwable) {
                logger.info("Error: Response onError from APS doPron");
                logger.error(throwable.getMessage());
                finishLatch.countDown();
                errorList.add(throwable.getMessage());
            }

            @Override
            public void onCompleted() {
                logger.info("Finished from responseObserver in APS doPron");
                finishLatch.countDown();
            }
        };

        StreamObserver<Aps.ApsRequest> requestObserver = apsStub.score(responseObserver);
        Aps.ApsRequest.Builder apsReqBuilder = Aps.ApsRequest.newBuilder();

        ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
        int len = 0;
        byte[] buffer = new byte[1024 * 512];
        while((len = bis.read(buffer)) > 0) {
            if (len < buffer.length) buffer = Arrays.copyOfRange(buffer, 0, len);
            ByteString inputByteString = ByteString.copyFrom(buffer);
            requestObserver.onNext(apsReqBuilder.setSpeech(inputByteString).setTranscription(text).build());
        }
        requestObserver.onCompleted();

        try {
            finishLatch.await(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw new InternalException(e.getMessage());
        }

        if (errorList.size() != 0) {
            StringBuilder stringBuilder = new StringBuilder();

            for (String errorReason : errorList) {
                stringBuilder.append(errorReason);
                stringBuilder.append("\n");
            }

            throw new MindsRestException(stringBuilder.toString());
        }

        Float result = resultArray.get(0);
        logger.info(result);

        return result;
    }
}
