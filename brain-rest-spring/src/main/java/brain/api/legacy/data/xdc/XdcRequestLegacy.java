package brain.api.legacy.data.xdc;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.CommonCode;
import brain.api.common.data.BaseRequest;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class XdcRequestLegacy extends BaseRequest implements Serializable {
    private String model;
    private String command;
    private String text;
    private CommonCode.Lang lang;


    public XdcRequestLegacy(){

    }

    public XdcRequestLegacy(
            String apiId, String apiKey,
            String model,
            String command,
            String text,
            CommonCode.Lang lang
    ){
        super.setApiIdKey(apiId, apiKey);
        this.model = model;
        this.command = command;
        this.text = text;
        this.lang = lang;
    }

    public void setModel(String model){
        this.model = model;
    }

    public String getModel(){
        return this.model;
    }

    public void setCommand(String command){
        this.command = command;
    }

    public String getCommand(){
        return this.command;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return this.text;
    }

    public void setLang(CommonCode.Lang lang){
        this.lang = lang;
    }

    public CommonCode.Lang getLang(){
        return this.lang;
    }
}
