package brain.api.legacy.data.mrc;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MrcAnswerLegacy implements Serializable {
    private String answer;
    private float prob;

    public MrcAnswerLegacy() {

    }

    public MrcAnswerLegacy(String answer, float prob) {
        this.answer = answer;
        this.prob = prob;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public float getProb() {
        return prob;
    }

    public void setProb(float prob) {
        this.prob = prob;
    }

    @Override
    public String toString() {
        return "MrcAnswerLegacy{" +
                "answer='" + answer + '\'' +
                ", prob=" + prob +
                '}';
    }
}
