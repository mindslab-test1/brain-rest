package brain.api.legacy.data.mrc;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MrcResponseLegacy implements Serializable {
    private List<MrcAnswersLegacy> data;
    private String status;
    private String extra_data;

    public MrcResponseLegacy() {

    }

    public MrcResponseLegacy(List<MrcAnswersLegacy> data, String status, String extra_data) {
        this.data = data;
        this.status = status;
        this.extra_data = extra_data;
    }

    public List<MrcAnswersLegacy> getData() {
        return data;
    }

    public void setData(List<MrcAnswersLegacy> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExtra_data() {
        return extra_data;
    }

    public void setExtra_data(String extra_data) {
        this.extra_data = extra_data;
    }

    @Override
    public String toString() {
        return "MrcResponseLegacy{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", extra_data='" + extra_data + '\'' +
                '}';
    }
}
