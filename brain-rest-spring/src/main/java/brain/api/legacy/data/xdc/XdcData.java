package brain.api.legacy.data.xdc;

import java.io.Serializable;
import java.util.List;

public class XdcData implements Serializable {
    private List<SentenceAttention> sent_attns;
    private List<List<Integer>> top_word_indices_lists;
    private List<Classification> cls;
    private List<Integer> top_sent_indices;
    private List<String> sentences;

    public XdcData() {
    }

    public XdcData(
            List<SentenceAttention> sent_attns,
            List<List<Integer>> top_word_indices_lists,
            List<Classification> cls,
            List<Integer> top_sent_indices,
            List<String> sentences
    ) {
        this.sent_attns = sent_attns;
        this.top_word_indices_lists = top_word_indices_lists;
        this.cls = cls;
        this.top_sent_indices = top_sent_indices;
        this.sentences = sentences;
    }

    public List<SentenceAttention> getSent_attns() {
        return this.sent_attns;
    }

    public void setSent_attns(List<SentenceAttention> sent_attns) {
        this.sent_attns = sent_attns;
    }

    public List<List<Integer>> getTop_word_indices_lists() {
        return this.top_word_indices_lists;
    }

    public void setTop_word_indices_lists(List<List<Integer>> top_word_indices_lists) {
        this.top_word_indices_lists = top_word_indices_lists;
    }

    public List<Classification> getCls() {
        return this.cls;
    }

    public void setCls(List<Classification> cls) {
        this.cls = cls;
    }

    public List<Integer> getTop_sent_indices() {
        return this.top_sent_indices;
    }

    public void setTop_sent_indices(List<Integer> top_sent_indices) {
        this.top_sent_indices = top_sent_indices;
    }

    public List<String> getSentences() {
        return this.sentences;
    }

    public void setSentences(List<String> sentences) {
        this.sentences = sentences;
    }

    @Override
    public String toString() {
        return "XdcData{" +
                "sent_attns=" + sent_attns +
                ", top_word_indices_lists=" + top_word_indices_lists +
                ", cls=" + cls +
                ", top_sent_indices=" + top_sent_indices +
                ", sentences=" + sentences +
                '}';
    }
}
