package brain.api.legacy.data.xdc;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class XdcResponseLegacy implements Serializable {
    private XdcData data;
    private String status;

    public XdcResponseLegacy() {
    }

    public XdcResponseLegacy(XdcData data, String status) {
        this.data = data;
        this.status = status;
    }

    public XdcData getData() {
        return this.data;
    }

    public void setData(XdcData data) {
        this.data = data;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "XdcResponseLegacy{" +
                "data=" + data +
                ", status='" + status + '\'' +
                '}';
    }
}
