package brain.api.legacy.data.xdc;

import java.io.Serializable;

public class WordAttention implements Serializable {
    private int start;
    private int end;
    private float weight;

    public WordAttention() {
    }

    public WordAttention(int start, int end, float weight) {
        this.start = start;
        this.end = end;
        this.weight = weight;
    }

    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return this.end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "WordAttention{" +
                "start=" + start +
                ", end=" + end +
                ", weight=" + weight +
                '}';
    }
}