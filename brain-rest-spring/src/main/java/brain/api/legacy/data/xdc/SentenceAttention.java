package brain.api.legacy.data.xdc;

import java.io.Serializable;
import java.util.List;

public class SentenceAttention implements Serializable {
    private float weight;
    private List<WordAttention> word_attns;

    public SentenceAttention() {
    }

    public SentenceAttention(float weight, List<WordAttention> word_attns) {
        this.weight = weight;
        this.word_attns = word_attns;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public List<WordAttention> getWord_attns() {
        return this.word_attns;
    }

    public void setWord_attns(List<WordAttention> word_attns) {
        this.word_attns = word_attns;
    }

    @Override
    public String toString() {
        return "SentenceAttention{" +
                "weight=" + weight +
                ", word_attns=" + word_attns +
                '}';
    }
}
