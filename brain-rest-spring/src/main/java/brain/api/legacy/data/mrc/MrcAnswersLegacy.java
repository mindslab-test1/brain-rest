package brain.api.legacy.data.mrc;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MrcAnswersLegacy implements Serializable {
    private List<MrcAnswerLegacy> answers;
    private String original;

    public MrcAnswersLegacy() {
    }

    public MrcAnswersLegacy(List<MrcAnswerLegacy> answers, String original) {
        this.answers = answers;
        this.original = original;
    }

    public List<MrcAnswerLegacy> getAnswers() {
        return answers;
    }

    public void setAnswers(List<MrcAnswerLegacy> answers) {
        this.answers = answers;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    @Override
    public String toString() {
        return "MrcAnswersLegacy{" +
                "answers=" + answers +
                ", original='" + original + '\'' +
                '}';
    }
}
