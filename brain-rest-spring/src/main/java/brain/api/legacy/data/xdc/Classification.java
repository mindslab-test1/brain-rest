package brain.api.legacy.data.xdc;

import java.io.Serializable;

public class Classification implements Serializable {
    private String label;
    private float probability;

    public Classification() {
    }

    public Classification(String label, float probability) {
        this.label = label;
        this.probability = probability;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getProbability() {
        return this.probability;
    }

    public void setProbability(float probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "Classification{" +
                "label='" + label + '\'' +
                ", probability=" + probability +
                '}';
    }
}
