package brain.api.legacy.data.tts;

import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.CommonCode;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TtsRequestLegacy implements Serializable {
    private String id;
    private String key;
    private CommonCode.Lang lang;
    private String text;

    public TtsRequestLegacy(){
    }

    public TtsRequestLegacy(
            String id, String key,
            CommonCode.Lang lang,
            String text
    ){
        this.id = id;
        this.key = key;
        this.lang = lang;
        this.text = text;
    }

    public void setLang(CommonCode.Lang lang) {
        this.lang = lang;
    }

    public CommonCode.Lang getLang(){
        return this.lang;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return this.text;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setKey(String key){
        this.key = key;
    }

    public String getKey(){
        return this.key;
    }

    @Override
    public String toString(){
        return "TtsRequestLegacy{" +
                "lang=" + this.lang +
                ", text=" + this.text +
                ", Id=" + this.getId()+
                ", Key=" + this.getKey() +
                "}";
    }


}
