package brain.api.legacy.service;

import brain.api.avr.AvrCommonCode;
import brain.api.avr.client.AvrFlaskClient;
import brain.api.avr.data.AvrBaseResponse;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.model.server.BaselineDto;
import brain.api.common.service.BaseService;
import brain.api.common.utils.CommonUtils;
import brain.api.dap.DapCommonCode;
import brain.api.dap.client.DapDramaClient;
import brain.api.dap.data.DvectorizeBaseResponse;
import brain.api.dap.data.GhostvladBaseResponse;
import brain.api.esr.EsrCommonCode;
import brain.api.esr.client.EsrGrpcClient;
import brain.api.gpt.GptCommonCode;
import brain.api.gpt.client.GptGrpcClient;
import brain.api.gpt.data.GptBaseResponse;
import brain.api.gpt.model.GptRequestParams;
import brain.api.hmd.HmdCommonCode;
import brain.api.hmd.client.HmdGrpcClient;
import brain.api.hmd.data.HmdBaseResponse;
import brain.api.idr.IdrCommonCode;
import brain.api.idr.client.IdrFlaskClient;
import brain.api.idr.data.IdrBaseResponse;
import brain.api.insight.InsightCommonCode;
import brain.api.insight.client.InsightGrpcClient;
import brain.api.insight.data.InsightBaseResponse;
import brain.api.itf.ItfCommonCode;
import brain.api.itf.client.ItfBertGrpcClient;
import brain.api.itf.client.ItfGrpcClient;
import brain.api.itf.data.ItfBaseResponse;
import brain.api.itf.model.ItfRequestParams;
import brain.api.legacy.data.xdc.XdcResponseLegacy;
import brain.api.mrc.MrcCommonCode;
import brain.api.mrc.client.MrcGrpcClient;
import brain.api.mrc.data.MrcBaseResponse;
import brain.api.ner.client.NerGrpcClient;
import brain.api.ner.data.NerBaseResponse;
import brain.api.ner.model.NerRequestParams;
import brain.api.nlp.NlpCommonCode;
import brain.api.nlp.client.NlpGrpcClient;
import brain.api.nlp.data.NlpBaseResponse;
import brain.api.stt.SttCommonCode;
import brain.api.stt.client.SttClient;
import brain.api.stt.data.SttRes;
import brain.api.stt.data.SttResDetail;
import brain.api.tti.TtiCommonCode;
import brain.api.tti.client.TtiGrpcClient;
import brain.api.tti.data.TtiBaseResponse;
import brain.api.tts.TtsCommonCode;
import brain.api.tts.client.TtsGrpcClient;
import brain.api.txr.TxrCommonCode;
import brain.api.txr.client.TxrGrpcClient;
import brain.api.txr.data.TxrBaseResponse;
import brain.api.xdc.XdcCommonCode;
import brain.api.xdc.client.XdcClientInterface;
import brain.api.xdc.client.XdcGrpcClient;
import brain.api.xdc.data.XdcBaseResponse;
import com.google.protobuf.ByteString;
import lombok.RequiredArgsConstructor;
import maum.brain.tts.TtsMediaResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class LegacyService extends BaseService {

    private static final CloudApiLogger logger = new CloudApiLogger("LEGACY", CommonCode.COMMON_CLASSNAME_SERVICE);

    private final MrcGrpcClient mrcGrpcClient;
    private final HmdGrpcClient hmdGrpcClient;
    private final NlpGrpcClient nlpGrpcClient;
    private final XdcGrpcClient xdcGrpcClient;
    private final XdcClientInterface xdcClient;
    private final GptGrpcClient gptClient;
    private final TtiGrpcClient ttiClient;
    private final DapDramaClient dapClient;
    private final TxrGrpcClient txrClient;
    private final AvrFlaskClient avrFlaskClient;
    private final IdrFlaskClient idrFlaskClient;
    private final InsightGrpcClient insightClient;
    private final TtsGrpcClient ttsClient;
    private final EsrGrpcClient esrClient;
    private final ItfGrpcClient itfClient;
    private final ItfBertGrpcClient itfBertClient;
    private final NerGrpcClient nerClient;
    private final SttClient sttClient;

    public XdcResponseLegacy doXdcLegacy(
            String apiId,
            String apiKey,
            String cmd,
            String content
    ) throws MindsRestException {
        CommonMsg msg = validateLegacy(apiId, apiKey);
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS) throw new MindsRestException(
                msg.getStatus(),
                msg.getMessage()
        );
        if (!cmd.equals("runXDC"))
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getMessage()
            );

        logger.info(String.format("[Legacy Xdc] apiId=%s, cmd=%s, content=%s", apiId, cmd, content));

        return xdcGrpcClient.xdcGetClassLegacy(content);
    }

    public MrcBaseResponse doMrcBert(
            String apiId, String apiKey, String lang, String context, String question, HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, MrcCommonCode.SERVICE_NAME, 1, request);
        CommonMsg msg;
        logger.info(String.format("[Legacy Bert Mrc] apiId=%s, lang=%s, context=%s, question=%s", apiId, lang, context, question));

        if (lang.equals("kor") || lang.equals("ko_KR")) {
            BaselineDto dto = serverDao.getBaselineServer(MrcCommonCode.SERVICE_NAME, "kor");
//            msg = mrcGrpcClient.setDestination("125.132.250.243", 35002);          //84 외부 서버 IP
//            msg = mrcGrpcClient.setDestination("182.162.19.15", 35002);
            msg = mrcGrpcClient.setDestination(dto.getIp(), dto.getPort());     // 구)41 서버 (평촌 IDC)
        }
//            msg = mrcGrpcClient.setDestination("182.162.19.15", 35005);     // 구)41 서버 (평촌 IDC) 2.0 Ver
        else {
            BaselineDto dto = serverDao.getBaselineServer(MrcCommonCode.SERVICE_NAME, "eng");
            msg = mrcGrpcClient.setDestination(dto.getIp(), dto.getPort());     // 구)41 서버 (평촌 IDC)
        }
//            msg = mrcGrpcClient.setDestination("125.132.250.243", 35001);          //84 외부 서버 IP
//            msg = mrcGrpcClient.setDestination("182.162.19.15", 35004);     // 구)41 서버 (평촌 IDC) 2.0 Ver
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS)
            throw new MindsRestException(msg.getStatus(), msg.getMessage());
        try {
            logger.debug("start bertSquardMrc");

            return mrcGrpcClient.bertSquadMrc(context, question);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new MrcBaseResponse(
                    new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode())
            );
        }
    }

    public XdcBaseResponse doXdcBert(String apiId, String apiKey, String content, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, XdcCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Bert Xdc] apiId=%s, content=%s", apiId, content));

//        CommonMsg msg = xdcClient.setDestination("125.132.250.243", 35003);     //84 외부 서버 IP
        CommonMsg msg = xdcClient.setDestination("182.162.19.15", 35003);     // 구)41 서버 (평촌 IDC)
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS)
            throw new MindsRestException(msg.getStatus(), msg.getMessage());
        try {
            XdcBaseResponse response = xdcClient.bertXdc(content);

            return response;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new XdcBaseResponse(
                    new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()),
                    null,
                    null,
                    null,
                    null
            );
        }
    }

    public TtiBaseResponse doTti(String apiId, String apiKey, String reqText, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, TtiCommonCode.SERVICE_NAME, 1, request);

        logger.info(String.format("[Legacy Tti] apiId=%s, reqText=%s", apiId, reqText));

        CommonMsg msg = ttiClient.setDestination("125.132.250.243", 37001);
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS)
            throw new MindsRestException(msg.getStatus(), msg.getMessage());

        try {
            return ttiClient.imageGen(reqText);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new TtiBaseResponse(
                    null,
                    new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode())
            );
        }
    }

    public ItfBaseResponse doBertItf(ItfRequestParams param, HttpServletRequest request) throws MindsRestException {
        validate(param.getApiId(), param.getApiKey(), ItfCommonCode.SERVICE_NAME, 1, request);

        logger.info(String.format("[Legacy Bert Itf] %s", param));

        CommonMsg msg = itfBertClient.setDestination("182.162.19.8", 35015);
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS)
            throw new MindsRestException(msg.getStatus(), msg.getMessage());

        return itfBertClient.getItf(param.getContext());
    }


    public NerBaseResponse doNer(NerRequestParams param, HttpServletRequest request) throws MindsRestException {
        logger.debug("[doNer] - " + param);
        validate(param.getApiId(), param.getApiKey(), ItfCommonCode.SERVICE_NAME, 1, request);
        logger.debug("[doNer] - validate is okay");

        CommonMsg msg = nerClient.setDestination("182.162.19.8", 35005);
        logger.info("[Legacy Ner] msg - " + msg);
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS)
            throw new MindsRestException(msg.getStatus(), msg.getMessage());

        return nerClient.getNer(param.getContext());
    }

    public GptBaseResponse doGpt(GptRequestParams param, HttpServletRequest request) throws MindsRestException {
        validate(param.getApiId(), param.getApiKey(), GptCommonCode.SERVICE_NAME, 1, request);

        logger.info(String.format("[Legacy Gpt] %s", param));

        CommonMsg msg;
        if (param.getLang().equals("kor"))
//            msg = gptClient.setDestination("125.132.250.243", 36002);     //84 외부 서버 IP
            msg = gptClient.setDestination("182.162.19.15", 36002);     // 구)41 서버 (평촌 IDC)
        else if (param.getLang().equals("eng"))
//            msg = gptClient.setDestination("125.132.250.243", 36001);     //84 외부 서버 IP
            msg = gptClient.setDestination("182.162.19.15", 36001);     // 구)41 서버 (평촌 IDC)
        else
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_NOT_SUPPORTED_LANGUAGE.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_NOT_SUPPORTED_LANGUAGE.getMessage()
            );
        if (msg.getStatus() != CommonCode.COMMON_SUCCESS)
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_INTERNAL.getMessage()
            );

        return gptClient.textGenerate(param.getContext());
    }

    public ResponseEntity doVoiceFilter(
            String apiId,
            String apiKey,
            MultipartFile mixedVoice,
            MultipartFile reqVoice,
            HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);

        logger.info(String.format("[Legacy VoiceFilter] apiId=%s, mixedVoice={%s, %s}, reqVoice={%s, %s}",
                apiId,
                mixedVoice.getSize(), mixedVoice.getOriginalFilename(),
                reqVoice.getSize(), reqVoice.getOriginalFilename()));

        return dapClient.doVoiceFilter(reqVoice, mixedVoice);
    }


    public TtiBaseResponse doTextToImage(
            Map<String, String> request, HttpServletRequest servletRequest
    ) throws MindsRestException {
        String apiId = request.get("apiId");
        String apiKey = request.get("apiKey");
        String reqText = request.get("reqText");
        validate(apiId, apiKey, TtiCommonCode.SERVICE_NAME, 1, servletRequest);

        logger.info(String.format("[Legacy TextToImage] apiId=%s, reqText=%s", apiId, reqText));

//        ttiClient.setDestination("10.122.64.65", 36001);      // 65번 서버 (사내)
        ttiClient.setDestination("125.132.250.244", 36001);      // 65번 서버

        return ttiClient.imageGen(reqText);
    }

    public TxrBaseResponse doTextRemoval(
            Map<String, String> request,
            MultipartFile imgFile,
            HttpServletRequest servletRequest
    ) throws MindsRestException {
        String apiId = request.get("apiId");
        String apiKey = request.get("apiKey");

        validate(apiId, apiKey, TxrCommonCode.SERVICE_NAME, 1, servletRequest);

        logger.info(String.format("[Legacy TextRemoval] apiId=%s, imgFile={%s, %s}", apiId, imgFile.getSize(), imgFile.getOriginalFilename()));

        txrClient.setDestination("182.162.19.10", 56000);
        return txrClient.textRemoval(imgFile);
    }

    public ResponseEntity doCnnNoise(String apiId, String apiKey, MultipartFile inputWav, HttpServletRequest request) throws MindsRestException {
        logger.debug("welcome denoise (cnnoise) " + apiId);

        try {
            int usage = (int) (inputWav.getSize() / 16000);
            validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, usage, request);
            logger.info(String.format("[Legacy CnnNoise] apiId=%s, inputWav={%s, %s}", apiId, inputWav.getSize(), inputWav.getOriginalFilename()));

            String messageKey = dapClient.cnnoiseRabbitMqEnqueue(inputWav);
            logger.info(messageKey);

            return dapClient.cnnoiseRedisGetWave(messageKey);

        } catch (Exception e) {
            throw new MindsRestException(e.getMessage());
        }

    }

//    public void doDramaMrc(String context, String question, String apiId, String apiKey, HttpServletRequest request) throws MindsRestException {
//        validate(apiId, apiKey, MrcCommonCode.SERVICE_NAME, 1, request);
//
//        String messageKey = mrcDramaClient.mrcRabbitMqEnqueue(context, question);
//        mrcDramaClient.mrcRedisGetString(messageKey);
//    }
//
//    public void doDramaXdc(String context, String apiId, String apiKey) throws MindsRestException {
//        validate(apiId, apiKey, XdcCommonCode.SERVICE_NAME, 1);
//
//        xdcDramaClient.doXdcDrama(context);
//    }

    public ResponseEntity doDiarize(String apiId, String apiKey, MultipartFile reqVoice, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);

        logger.info(String.format("[Legacy Diarize] apiId=%s, reqVoice={%s, %s}", apiId, reqVoice.getSize(), reqVoice.getOriginalFilename()));

        return dapClient.doDiarize(reqVoice);
    }


    public HmdBaseResponse doHmd(Map<String, String> request, HttpServletRequest servletRequest) throws MindsRestException {
        String apiId = request.get("apiId");
        String apiKey = request.get("apiKey");
        String lang = request.get("lang");
        String reqText = request.get("reqText");
        validate(apiId, apiKey, HmdCommonCode.SERVICE_NAME, 1, servletRequest);

        logger.info(String.format("[Legacy Hmd] apiId=%s, lang=%s, reqText=%s", apiId, lang, reqText));

        return hmdGrpcClient.hmdLegacy(reqText, lang);
    }

    public NlpBaseResponse doNlp(Map<String, String> request, HttpServletRequest servletRequest) throws MindsRestException {
        String apiId = request.get("apiId");
        String apiKey = request.get("apiKey");
        String lang = request.get("lang");
        String reqText = request.get("reqText");
        validate(apiId, apiKey, NlpCommonCode.SERVICE_NAME, 1, servletRequest);

        logger.info(String.format("[Legacy Nlp] apiId=%s, lang=%s, reqText=%s", apiId, lang, reqText));

        return nlpGrpcClient.nlpLegacy(reqText, lang);
    }

    public AvrBaseResponse doAvr(String apiId, String apiKey, String option, MultipartFile reqImage, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, AvrCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Avr] apiId=%s, option=%s, reqImage={%s, %s}", apiId, option, reqImage.getSize(), reqImage.getOriginalFilename()));

        if (!(option.equals("window") || option.equals("face") || option.equals("plate")))
            throw new MindsRestException(
                    CommonExceptionCode.COMMON_ERR_NOT_IMPLEMENTED.getErrCode(),
                    CommonExceptionCode.COMMON_ERR_NOT_IMPLEMENTED.getMessage()
            );

        return avrFlaskClient.doAvr(reqImage, option);
    }

    public IdrBaseResponse doIdr(String apiId, String apiKey, MultipartFile reqImage, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, IdrCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Idr] apiId=%s, reqImage={%s, %s}", apiId, reqImage.getSize(), reqImage.getOriginalFilename()));

        return idrFlaskClient.doIdr(reqImage);
    }

    public DvectorizeBaseResponse doDvectorize(String apiId, String apiKey, MultipartFile reqVoice, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Dvectorize] apiId=%s, reqVoice={%s, %s}", apiId, reqVoice.getSize(), reqVoice.getOriginalFilename()));

        return dapClient.doDvecotrize(reqVoice);
    }

    public GhostvladBaseResponse doGhostvlad(String apiId, String apiKey, MultipartFile reqVoice, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, DapCommonCode.SERVICE_NAME, 1, request);

        try {
            logger.info("fileSave : " + waveDataSave(reqVoice));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        logger.info(String.format("[Legacy GhostVlad] apiId=%s, reqVoice={%s, %s}", apiId, reqVoice.getSize(), reqVoice.getOriginalFilename()));

        return dapClient.doGhostvlad(reqVoice);
    }

    public InsightBaseResponse doInsight(String apiId, String apiKey, MultipartFile reqImage, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, InsightCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Insight] apiId=%s, reqImage={%s, %s}", apiId, reqImage.getSize(), reqImage.getOriginalFilename()));

        insightClient.setDestination("114.108.173.112", 56101);

        return insightClient.insightFace(reqImage);
    }

    private ResponseEntity<StreamingResponseBody> doTts(
            String apiId, String apiKey, String ip, int port, String text, String lang, int speaker, int samplerate, HttpServletRequest request
    ) throws MindsRestException {
        validate(apiId, apiKey, TtsCommonCode.SERVICE_NAME, text.length(), request);

        logger.info(String.format("[Legacy Tts] apiId=%s, lang=%s, text=%s", apiId, lang, text));

        List<String> ipPool = new LinkedList<>();
        if (ip.split(",").length > 1) {
            ipPool = ttsClient.setIpPool(ip);
        } else {
            ipPool.add(ip);
        }

        ttsClient.setDestination(ipPool.get(0), port);

        Iterator<TtsMediaResponse> iter = ttsClient.doTts(text, lang, speaker, samplerate, ipPool, port);

        StreamingResponseBody responseBody = out -> {
            WritableByteChannel och = Channels.newChannel(out);
            try {
                TtsMediaResponse ttsResp = null;
                while (iter.hasNext()) {
                    ttsResp = iter.next();
                    ByteString data = ttsResp.getMediaData();
                    ByteBuffer buff = data.asReadOnlyByteBuffer();
                    och.write(buff);
                }
            } finally {
                logger.info("tts/block/speak/close");
                och.close();
                out.close();
            }
        };

        return ResponseEntity.ok().header("Content-Type", "audio/x-wav").body(responseBody);
    }

    public ResponseEntity<StreamingResponseBody> runTts(Map<String, String> params, String ip, int port, int speaker, HttpServletRequest request) {

        try {

            String apiId = params.get("ID");
            String apiKey = params.get("key");
            String text = params.get("text");
            String lang = params.get("lang");

            logger.info(String.format("[Legacy RunTts] apiId=%s, lang=%s, text=%s", apiId, lang, text));

            return this.doTts(
                    apiId
                    , apiKey
                    , ip
                    , port
                    , text
                    , lang
                    , speaker
                    , 16000
                    , request
            );

        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return ResponseEntity.status(500).build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }

    public byte[] doEsr(String apiId, String apiKey, MultipartFile img, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, EsrCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Esr] apiId=%s, img={%s, %s}", apiId, img.getSize(), img.getOriginalFilename()));

        esrClient.setDestination("182.162.19.15", 47000);

        return esrClient.superResolutaion(img);
    }

    public Map<String, String> parseJson(Map<String, String> params, HttpServletRequest request) throws IOException {
        if (params.size() == 0) {
            try {
                params = CommonUtils.parsingJson(request);
                logger.debug(params.toString());
            } catch (IOException e) {
                logger.error(e.getMessage());
                throw e;
            }
        }

        return params;
    }

    public String doItf(String apiId, String apiKey, String utter, String inputType, String lang, String chatbot, String model, HttpServletRequest request) throws MindsRestException {
        validate(apiId, apiKey, ItfCommonCode.SERVICE_NAME, 1, request);

        logger.info(String.format("[Legacy Itf] apiId=%s, utter=%s, inputType=%s, lang=%s, chatbot=%s, model=%s", apiId, utter, inputType, lang, chatbot, model));

        String ip = "54.180.19.55";
        int port = 41157;

        if (model.equalsIgnoreCase("ECA")) {
            port = 45005;
        }


        try {
//            msg = itfClient.setDestination("13.209.150.189", 45005);     // 구)41 서버 (평촌 IDC)
            itfClient.setDestination(ip, port);     // 구)41 서버 (평촌 IDC)
            return itfClient.findIntent(utter, inputType, lang, chatbot);

        } catch (Exception e) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(),
                    "Cannot Connection" + ip + ":" + port);
        }

    }

    private boolean waveDataSave(MultipartFile file) throws IOException {
        String path = "/home/ubuntu/maum/ghostvlad";
        boolean result = false;
        FileOutputStream fileOutputStream = null;
        try {
            File saveDir = new File(path);

            if (!saveDir.exists()) {
                saveDir.mkdirs();
            }

            int fileIndex = saveDir.listFiles().length;

            File saveFile = new File(path + "/kioskWav" + fileIndex + ".wav");

            byte[] data = file.getBytes();
            fileOutputStream = new FileOutputStream(path + "/kioskWav" + fileIndex + ".wav");
            fileOutputStream.write(data);
            result = true;

        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
        return result;
    }

    public SttRes simpleRecog(String apiId, String apiKey, String cmd, String lang, Integer sampling, String model, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        validate(apiId, apiKey, SttCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy Stt] apiId=%s, lang=%s, sampling=%s, model=%s, file={%s, %s}", apiId, lang, sampling, model, file.getSize(), file.getOriginalFilename()));

        sttClient.setDestination("182.162.19.10", 9801);

        SttRes response = sttClient.doSimpleRecog(file, lang, sampling, model);

        return response;
    }

    public SttResDetail detailRecog(String apiId, String apiKey, String cmd, String lang, Integer sampling, String model, MultipartFile file, HttpServletRequest request) throws MindsRestException, IOException {
        validate(apiId, apiKey, SttCommonCode.SERVICE_NAME, 1, request);
        logger.info(String.format("[Legacy SttDetail] apiId=%s, lang=%s, sampling=%s, model=%s, file={%s, %s}", apiId, lang, sampling, model, file.getSize(), file.getOriginalFilename()));

        sttClient.setDestination("182.162.19.10", 9801);

        SttResDetail response = sttClient.doDetailRecog(file, lang, sampling, model);

        return response;
    }

    public boolean dapFileSizeCheck(MultipartFile file) {
        if (file.getSize() > 2097152) {
            return false;
        }
        return true;
    }
}
