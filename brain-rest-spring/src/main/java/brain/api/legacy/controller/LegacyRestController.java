package brain.api.legacy.controller;

import brain.api.avr.data.AvrBaseResponse;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.ErrorResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.dap.data.DvectorizeBaseResponse;
import brain.api.dap.data.GhostvladBaseResponse;
import brain.api.esr.EsrCommonCode;
import brain.api.gpt.data.GptBaseResponse;
import brain.api.gpt.model.GptRequestParams;
import brain.api.hmd.data.HmdBaseResponse;
import brain.api.idr.data.IdrBaseResponse;
import brain.api.insight.data.InsightBaseResponse;
import brain.api.itf.data.ItfBaseResponse;
import brain.api.itf.model.ItfRequestParams;
import brain.api.legacy.data.mrc.MrcAnswerLegacy;
import brain.api.legacy.data.mrc.MrcAnswersLegacy;
import brain.api.legacy.data.mrc.MrcRequestLegacy;
import brain.api.legacy.data.mrc.MrcResponseLegacy;
import brain.api.legacy.data.xdc.XdcResponseLegacy;
import brain.api.legacy.service.LegacyService;
import brain.api.mrc.MrcCommonCode;
import brain.api.mrc.client.MrcGrpcClient;
import brain.api.mrc.data.MrcBaseResponse;
import brain.api.mrc.model.MrcRequestParams;
import brain.api.ner.data.NerBaseResponse;
import brain.api.ner.model.NerRequestParams;
import brain.api.nlp.data.NlpBaseResponse;
import brain.api.stt.data.SttRes;
import brain.api.stt.data.SttResDetail;
import brain.api.tti.data.TtiBaseResponse;
import brain.api.tts.client.TtsGrpcClient;
import brain.api.txr.data.TxrBaseResponse;
import brain.api.xdc.data.XdcBaseResponse;
import brain.api.xdc.model.XdcRequestParams;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class LegacyRestController {
    private static final CloudApiLogger logger = new CloudApiLogger("LEGACY", CommonCode.COMMON_CLASSNAME_CONTROLLER);

    @Autowired
    private LegacyService service;

    @Autowired
    private TtsGrpcClient ttsClient;

    @Autowired
    private MrcGrpcClient mrcClient;

    private Gson gson = new Gson();

    // TODO
    @RequestMapping(
            value = "/mrc",
            method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MrcResponseLegacy mrcLegacy(
            @RequestBody MrcRequestLegacy request,
            HttpServletRequest servletRequest,
            HttpServletResponse response
    )
    {
        logger.info("[Legacy MRC] - request : " + request.toString());
        logger.debug("legacy mrc");
        final String mrcAddress = "182.162.19.15";
        try {
            service.validate(request.getID(), request.getKey(), MrcCommonCode.SERVICE_NAME, 1, servletRequest);
        } catch (MindsRestException e){
            response.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new MrcResponseLegacy();
        }
        try {
            if (request.getLang().equals("kor") || request.getLang().equals("ko_KR")) {
                mrcClient.setDestination(mrcAddress, 35002);
            } else
                mrcClient.setDestination(mrcAddress, 35001);
        } catch (NullPointerException e){
            mrcClient.setDestination(mrcAddress, 35002);
        }
        MrcBaseResponse clientResponse = mrcClient.bertSquadMrc(request.getSentence(), request.getQuestion());
        ArrayList<MrcAnswersLegacy> answersLegacies = new ArrayList<>();
        ArrayList<MrcAnswerLegacy> answerLegacies = new ArrayList<>();
        answerLegacies.add(
                new MrcAnswerLegacy(
                        clientResponse.getAnswer(),
                        clientResponse.getProb()
                )
        );
        answersLegacies.add(
                new MrcAnswersLegacy(
                        answerLegacies,
                        request.getSentence()
                )
        );

        return new MrcResponseLegacy(
                answersLegacies,
                "Success",
                "None"
        );
    }

    // TODO
    @RequestMapping(
            value = "/xdc",
            method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public XdcResponseLegacy xdcLegacy(
            @RequestBody Map<String, String> input,
            HttpServletResponse response
    )
    {
        logger.info("[Legacy XDC] - input : " + input);
        logger.debug("legacy xdc");

        try {
            return service.doXdcLegacy(
                    input.get("ID"),
                    input.get("key"),
                    input.get("cmd"),
                    input.get("text")
            );
        } catch (MindsRestException e){
            response.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return null;
        }
    }

    @RequestMapping(
            value = "/bert.itf",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody
    ItfBaseResponse bertItf(
            @RequestBody ItfRequestParams params,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse
    ){
        try {
            return service.doBertItf(params, httpRequest);
        } catch (MindsRestException e) {
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new ItfBaseResponse(new CommonMsg(5000));
        }
    }

    @RequestMapping(
            value = "/bert.ner",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody
    NerBaseResponse bertNer(
            @RequestBody NerRequestParams params,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse
    ){
        try{
            logger.info("[Bert NER] - params : " + params.toString());
            return service.doNer(params, httpRequest);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new NerBaseResponse(new CommonMsg(5000));
        }
    }

    @RequestMapping(
            value = "/gpt",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody GptBaseResponse gptTest(
            @RequestBody GptRequestParams params,
            HttpServletRequest request,
            HttpServletResponse httpResponse
    ){
        try{
            logger.info("[Legacy GPT] - parmas : " + params.toString());

            return service.doGpt(params, request);
        } catch (MindsRestException e){
            logger.warn(e.getMessage());
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new GptBaseResponse(
                    new CommonMsg(e.getErrCode())
            );
        } catch (Exception e) {
            logger.error(e.getMessage());
            httpResponse.setStatus(500);
            return new GptBaseResponse(
                    new CommonMsg(5000)
            );
        }
    }

    @RequestMapping(
            value = "/bert.mrc",
            method = RequestMethod.POST
    )
    public ResponseEntity<String> bertMrc(
            @RequestBody MrcRequestParams params,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        logger.info("[Legacy MRC] - parmas : " + params.toString());
        logger.debug("bsproto");

        try {
            MrcBaseResponse mrcResponse = service.doMrcBert(params.getApiId(), params.getApiKey(), params.getLang(), params.getContext(), params.getQuestion(), request);
            logger.debug(response);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(new MediaType("application", "json"));

            return new ResponseEntity<>(gson.toJson(mrcResponse), httpHeaders, HttpStatus.OK);
        } catch (MindsRestException e){
            logger.error(e.getMessage());
            response.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new ResponseEntity<>(
                    gson.toJson(
                            new MrcBaseResponse(
                                    new CommonMsg(
                                            e.getErrCode()
                                    )
                            )
                    ),
                    HttpStatus.valueOf(CommonExceptionCode.getHttpStatusOnException(e).value())
            );
        }
    }

    // TODO
    @RequestMapping(
            value = "/bert.xdc",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody XdcBaseResponse bxPrototype(
            @RequestBody XdcRequestParams params,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse
    ){
        logger.info("[Legacy Bert.XDC] - parmas : " + params.toString());

        try {
            XdcBaseResponse response = service.doXdcBert(
                    params.getApiId(), params.getApiKey(), params.getContext(), httpRequest
            );
            logger.debug(response);
            return response;
        } catch (MindsRestException e){
            logger.error(e.getMessage());
            httpResponse.setStatus(CommonExceptionCode.getHttpStatusOnException(e).value());
            return new XdcBaseResponse(
                    new CommonMsg(e.getErrCode()),
                    null,
                    null,
                    null,
                    null
            );
        }
    }

    // TODO
    @RequestMapping(
            value = "/tti",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public TtiBaseResponse ttiTest(
            @RequestBody Map<String, String> request,
            HttpServletRequest httpRequest
    ){
        logger.info("[Legacy TTI] - request : " + request.toString());

        try {
            return service.doTextToImage(request, httpRequest);
        } catch (MindsRestException e){
            logger.error(e.getMessage());
            return new TtiBaseResponse();
        }
    }

    @RequestMapping(
            value = "/tti_image",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = {MediaType.IMAGE_JPEG_VALUE}
    )
    public @ResponseBody ResponseEntity<?> ttiAsImageFile(
            @RequestBody Map<String, String> request,
            HttpServletRequest httpRequest
    ){
        try {
            logger.info("[Legacy TTI_Image] - request : " + request.toString());
            logger.debug("welcome tti");
            TtiBaseResponse response = service.doTextToImage(request, httpRequest);
            if(response.getImageAsBytes() == null)
                throw new MindsRestException(
                        response.getMessage().getStatus(),
                        response.getMessage().getMessage()
                );

            return new ResponseEntity<>(IOUtils.toByteArray(
                    new ByteArrayInputStream(response.getImageAsBytes())), HttpStatus.OK
            );
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseEntity<>(new TtiBaseResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value ="/dap/denoise",
            method = RequestMethod.POST
    )
    public ResponseEntity cnnnoiseTest(
            @RequestParam("file") MultipartFile file,
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            HttpServletRequest request
    ) throws Exception {
        logger.info("[Legacy Denoise] - apiId : " + apiId + ", fileSize : " + file.getSize());
        if (!service.dapFileSizeCheck(file)) {
            logger.error("[Denoise] :  File Too Large : " + file.getSize());
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/json;");
            return new ResponseEntity(new ErrorResponse(CommonExceptionCode.COMMON_DAP_ERR_TOO_LARGE_FILE.getErrCode()),
                    httpHeaders, HttpStatus.BAD_REQUEST);
        }

        return service.doCnnNoise(apiId, apiKey, file, request);
    }

    @RequestMapping(
            value = "/txtr",
            method = RequestMethod.POST,
            produces = {MediaType.IMAGE_PNG_VALUE}
    )
    public @ResponseBody ResponseEntity<?> textRemoval(
            @RequestParam("file") MultipartFile reqImage,
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            HttpServletRequest httpRequest
    ){
        logger.info("[Legacy Txtr] - apiId : " + apiId + ", fileSize : " + reqImage.getSize());

        try {
            Map <String, String> request = new HashMap <>();
            request.put("apiId", apiId);
            request.put("apiKey", apiKey);
            TxrBaseResponse response = service.doTextRemoval(
                    request,
                    reqImage,
                    httpRequest
            );

            return new ResponseEntity <>(
                    IOUtils.toByteArray(
                            new ByteArrayInputStream(response.getImageAsBytes())
                    ), HttpStatus.OK
            );
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseEntity<>(new TtiBaseResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/dap/voiceFilter",
            method = RequestMethod.POST
    )
    public ResponseEntity voiceFilter(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("mixedVoice") MultipartFile mixedVoice,
            @RequestParam("reqVoice") MultipartFile reqVoice,
            HttpServletRequest request
    ){
        logger.info("[Legacy VoiceFilter] - apiId : " + apiId
                + ", mixedVoiceSize : " + mixedVoice.getSize()
                + ", reqVoiceSize : " + reqVoice.getSize());
        logger.debug("Welcome VoiceFilter");

        if (!service.dapFileSizeCheck(mixedVoice) || !service.dapFileSizeCheck(reqVoice)) {
            logger.error("[VoiceFilter] :  File Too Large : " + mixedVoice.getSize());
            logger.error("[VoiceFilter] :  File Too Large : " + reqVoice.getSize());
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/json;");
            return new ResponseEntity(new ErrorResponse(CommonExceptionCode.COMMON_DAP_ERR_TOO_LARGE_FILE.getErrCode()),
                    HttpStatus.BAD_REQUEST);
        }

        try {
            return service.doVoiceFilter(
                    apiId,
                    apiKey,
                    mixedVoice,
                    reqVoice,
                    request
            );
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseEntity<>(new TtiBaseResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(
            value = "/dap/diarize",
            method = RequestMethod.POST
    )
    public ResponseEntity diarize(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("reqVoice") MultipartFile reqVoice,
            HttpServletRequest request
    ){
        logger.info("[Legacy Diarize] - apiId : " + apiId + ", reqVoiceSize : " + reqVoice.getSize());
        logger.debug("Welcome VoiceFilter");

        if (!service.dapFileSizeCheck(reqVoice)) {
            logger.error("[Diarize] :  File Too Large : " + reqVoice.getSize());
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/json;");
            return new ResponseEntity(new ErrorResponse(CommonExceptionCode.COMMON_DAP_ERR_TOO_LARGE_FILE.getErrCode()), HttpStatus.BAD_REQUEST);
        }

        try{
            return service.doDiarize(
                    apiId,
                    apiKey,
                    reqVoice,
                    request
            );
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseEntity<>(new TtiBaseResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(
            value = "/hmd",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public HmdBaseResponse hmd(
            @RequestBody Map<String, String> request,
            HttpServletRequest httpRequest
    ) {
        try {
            return service.doHmd(request, httpRequest);
        } catch (MindsRestException e){
            logger.debug(e.getMessage());
            return new HmdBaseResponse();
        }
    }

    @RequestMapping(
            value = "/nlu",
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public NlpBaseResponse nlu(
            @RequestBody Map<String, String> request,
            HttpServletRequest httpRequest
    ){
        try {
            return service.doNlp(request, httpRequest);
        } catch (MindsRestException e){
            logger.debug(e);
            return new NlpBaseResponse();
        } catch (Exception e){
            logger.debug(e.getMessage());
            return new NlpBaseResponse(
                    new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode(), e.getMessage()),
                    null
            );
        }
    }

    @RequestMapping(
            value = "/avr",
            method = RequestMethod.POST,
            produces = {MediaType.IMAGE_JPEG_VALUE}
    )
    public @ResponseBody ResponseEntity<?> objectDetection(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile reqImage,
            @RequestParam("option") String option,
            HttpServletRequest request
    ){
        logger.info("[Legacy avr] - apiId : " + apiId + ", fileSize : " + reqImage.getSize());

        try {
            AvrBaseResponse response = service.doAvr(apiId, apiKey, option, reqImage, request);

            return new ResponseEntity <>(
                    IOUtils.toByteArray(
                            new ByteArrayInputStream(response.getData().getImageAsBytes())
                    ), HttpStatus.OK
            );
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseEntity<>(new TtiBaseResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/idr",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}
    )
    public @ResponseBody IdrBaseResponse imageDocumentRecog(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile reqImage,
            HttpServletRequest request
    ){
        logger.info("[Legacy idr] - apiId : " + apiId + ", fileSize : " + reqImage.getSize());

        try {
            return service.doIdr(apiId, apiKey, reqImage, request);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new IdrBaseResponse(
                    new CommonMsg(
                            e.getErrCode()
                    )
            );
        } catch (Exception e){
            logger.error(e.getMessage());
            return new IdrBaseResponse(
                    new CommonMsg(
                            CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode()
                    )
            );
        }
    }

    @RequestMapping(
            value = "/dap/dvectorize"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody DvectorizeBaseResponse dvectorize(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam("reqVoice") MultipartFile reqVoice
            , HttpServletRequest request
    ) {
        logger.info("[Legacy Dvectorize] - apiId : " + apiId + ", reqVoiceSize : " + reqVoice.getSize());

        if (!service.dapFileSizeCheck(reqVoice)) {
            logger.error("[Dvectorize] :  File Too Large : " + reqVoice.getSize());
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/json;");
            return new DvectorizeBaseResponse(CommonExceptionCode.COMMON_DAP_ERR_TOO_LARGE_FILE.getErrCode());
        }

        try {
            return service.doDvectorize(apiId, apiKey, reqVoice, request);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new DvectorizeBaseResponse(e.getErrCode());
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new DvectorizeBaseResponse(5000);
        }
    }

    @RequestMapping(
            value = "/dap/ghostvlad"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody GhostvladBaseResponse ghostvlad(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam("reqVoice") MultipartFile reqVoice
            , HttpServletRequest request
    ) {
        logger.info("[Legacy Ghostvlad] - apiId : " + apiId + ", reqVoiceSize : " + reqVoice.getSize());

        if (!service.dapFileSizeCheck(reqVoice)) {
            logger.error("[Ghostvlad] :  File Too Large : " + reqVoice.getSize());
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", "application/json;");
            return new GhostvladBaseResponse(CommonExceptionCode.COMMON_DAP_ERR_TOO_LARGE_FILE.getErrCode());
        }
        try {
            return service.doGhostvlad(apiId, apiKey, reqVoice, request);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new GhostvladBaseResponse(e.getErrCode());
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new GhostvladBaseResponse(5000);
        }
    }

    @RequestMapping(
            value = "/insight"
            , method = RequestMethod.POST
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody InsightBaseResponse insight(
            @RequestParam("apiId") String apiId
            , @RequestParam("apiKey") String apiKey
            , @RequestParam("file") MultipartFile file
            , HttpServletRequest request
    ) {
        logger.info("[Legacy Insight] - apiId : " + apiId + ", fileName : " + file.getOriginalFilename());

        try {
            return service.doInsight(apiId, apiKey, file, request);
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new InsightBaseResponse();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new InsightBaseResponse();
        }
    }

    @RequestMapping(
            value = "/tts"
            , method = RequestMethod.POST
    )
    public ResponseEntity<StreamingResponseBody> tts(
            @RequestParam Map<String, String> params,
            HttpServletRequest request
    ) {
        try {
            params = service.parseJson(params, request);
            String apiId = params.get("ID");

        } catch (IOException e) {
            return ResponseEntity.status(500).build();
        }

        return service.runTts(params, "182.162.19.27", 9999, 0, request);
    }

    @RequestMapping(
            value = "/voice1"
            , method = RequestMethod.POST
    )
    public ResponseEntity<StreamingResponseBody> ttsVoice1(
            @RequestParam Map<String, String> params,
            HttpServletRequest request
    ) {
        try {
            params = service.parseJson(params, request);
            String apiId = params.get("ID");
            logger.info("[TTS Voice1] : " + params);

            if (!apiId.equals("minds-api-client-15")) {
                logger.error("[TTS Voice1] Not Allowed User : " + apiId);
                return ResponseEntity.status(403).build();
            }

        } catch (IOException e) {
            return ResponseEntity.status(500).build();
        }

        return service.runTts(params, "182.162.19.10", 9999, 0, request);
    }


    @RequestMapping(
            value = "/voice2"
            , method = RequestMethod.POST
    )
    public ResponseEntity<StreamingResponseBody> ttsVoice2(
            @RequestParam Map<String, String> params,
            HttpServletRequest request
    ) {

        int speaker = -1;
        try {
            params = service.parseJson(params, request);
            String apiId = params.get("ID");
            logger.info("[TTS Voice1] : " + params);
            switch (apiId) {
                case "minds-api-client-16" :
                case "minds-api-test" : speaker = 0; break;
                case "minds-api-client-17" : speaker = 1; break;
                default:
                    logger.error("[TTS Voice2] Not Allowed User : " + apiId);
                    return ResponseEntity.status(500).build();
            }
        } catch (IOException e) {
            return ResponseEntity.status(500).build();
        }

        return service.runTts(params, "182.162.19.27", 9995, speaker, request);

    }

    @RequestMapping(
            value = "/voice3"
            , method = RequestMethod.POST
    )
    public ResponseEntity<StreamingResponseBody> ttsVoice3(
            @RequestParam Map<String, String> params,
            HttpServletRequest request
    ) {
        try {
            params = service.parseJson(params, request);
            String apiId = params.get("ID");
            logger.info("[TTS Voice3] : " + params);
            if (!apiId.equals("minds-api-client-18")) {
                logger.error("[TTS Voice3] Not Allowed User : " + apiId);
                return ResponseEntity.status(500).build();
            }
        } catch (IOException e) {
            return ResponseEntity.status(500).build();
        }

        return service.runTts(params, "182.162.19.27", 9969, 0, request);
    }

    @RequestMapping(
            value = "/voice4"
            , method = RequestMethod.POST
    )
    public ResponseEntity<StreamingResponseBody> ttsVoice4(
            @RequestParam Map<String, String> params,
            HttpServletRequest request
    ) {
        try {
            params = service.parseJson(params, request);
            String apiId = params.get("ID");
            logger.info("[TTS Voice4] : " + params);
            if (!apiId.equals("i4vine-dev-api-id")) {
                logger.error("[TTS Voice4] Not Allowed User : " + apiId);
                return ResponseEntity.status(500).build();
            }
        } catch (IOException e) {
            return ResponseEntity.status(500).build();
        }

        return service.runTts(params, "182.162.19.27", 9999, 0, request);
    }

    @RequestMapping(
            value = "/esr",
            method = RequestMethod.POST,
            produces = {MediaType.IMAGE_PNG_VALUE}
    )
    public @ResponseBody ResponseEntity superResolutaion(
            @RequestParam("file") MultipartFile reqImage,
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            HttpServletRequest request
    ){
        logger.info("[Legacy ESR] - apiId : " + apiId + ", fileSize : " + reqImage.getSize());

        if (reqImage.getSize() > 307200) {
            logger.warn(EsrCommonCode.TOO_LARGE_FILE);
            return new ResponseEntity(new ErrorResponse(), HttpStatus.BAD_REQUEST);
        }

        try {
            return new ResponseEntity <>(
                    IOUtils.toByteArray(
                            new ByteArrayInputStream(service.doEsr(apiId, apiKey, reqImage, request))
                    ), HttpStatus.OK
            );
        } catch (MindsRestException e) {
            logger.warn(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseEntity<>(new ErrorResponse().toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/itf",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE}
    )
    public @ResponseBody ResponseEntity itfLegacy(
            @RequestBody Map<String, String> request,
            HttpServletRequest httpRequest
    ){
        try {
            String model = "default";
            if (request.containsKey("model")) {
                model = request.get("model");
            }

            logger.info("[ITF request] : " + request.toString());
            String result = service.doItf(request.get("apiId"),
                    request.get("apiKey"),
                    request.get("utter"),
                    "keyboard",
                    request.get("lang"),
                    "",
                    model,
                    httpRequest
            );
            return new ResponseEntity<String>(result, HttpStatus.OK);
        } catch (MindsRestException e) {
            logger.error(e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
            value = "/stt",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody SttRes stt(
            @RequestParam("ID") String apiId,
            @RequestParam("key") String apiKey,
            @RequestParam("cmd") String cmd,
            @RequestParam("lang") String lang,
            @RequestParam("sampling") Integer sampling,
            @RequestParam("level") String level,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ) {
        try {
            return service.simpleRecog(apiId, apiKey, cmd, lang, sampling, level, file, httpRequest);
        } catch (MindsRestException | IOException e) {
            logger.error(e.getMessage());
            return new SttRes(e.toString());
        }
    }

    @RequestMapping(
            value = "/detail_stt",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public @ResponseBody SttResDetail sttDetail(
            @RequestParam("ID") String apiId,
            @RequestParam("key") String apiKey,
            @RequestParam("cmd") String cmd,
            @RequestParam("lang") String lang,
            @RequestParam("sampling") Integer sampling,
            @RequestParam("level") String level,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ) {
        try {
            return service.detailRecog(apiId, apiKey, cmd, lang, sampling, level, file, httpRequest);
        } catch (MindsRestException | IOException e) {
            logger.error(e.getMessage());
            return new SttResDetail(e.toString());
        }
    }
}
