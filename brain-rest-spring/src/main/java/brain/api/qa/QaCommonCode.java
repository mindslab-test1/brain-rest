package brain.api.qa;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class QaCommonCode {
    public static final String SERVICE_NAME = "QA";

    // News-Wiki Bot
    public static final String CONTENT_HOST = "http://114.108.173.123:5050/";
    public static final String CONTENT_PATH = "ai_human_qa";
    public static final int CONTENT_NTOP = 5;

    // KBQA Bot
    // TODO: KBQA 서버가 마련되면 변경 필요
    public static final String KBQA_HOST = "http://<>:51001/";
    public static final String KBQA_PATH = "kbqa";
    public static final int KBQA_NTOP = 1;

    public static final int QUESTION_TEXT_LIMIT_SIZE = 50;

}
