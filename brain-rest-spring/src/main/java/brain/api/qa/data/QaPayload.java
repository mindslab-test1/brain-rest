package brain.api.qa.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class QaPayload extends QaCommonPayload {
    private int frequency;
    private float prob;
    private List<ContentInfo> newsList;
}
