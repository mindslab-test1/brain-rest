package brain.api.qa.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ContentInfo {
    private String id;
    private String title;
    private String sentence;
    private String context;
    private int frequency;
    private float answerProb;
    private float searchProb;
    private float sentenceProb;
}
