package brain.api.qa.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class QaResponse<T> {
    private List<T> outputs;
}
