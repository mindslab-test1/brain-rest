package brain.api.qa.data.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class QuestionDto {
    @NotBlank
    private String apiId;
    @NotBlank
    private String apiKey;
    @NotBlank
    private String question;
    @NotNull
    @Min(value = 1)
    private Integer ntop;
    @NotNull
    private String domain;
    @NotBlank
    private String lang;
}
