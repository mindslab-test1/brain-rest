package brain.api.qa.data;

import lombok.*;

@Builder
@AllArgsConstructor
@Getter
@Setter
@ToString
public class QaRequest {
    private String question;
    private int ntop;
    private String domain;
}
