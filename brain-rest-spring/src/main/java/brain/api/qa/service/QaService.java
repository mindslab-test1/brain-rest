package brain.api.qa.service;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.CommonResponse;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import brain.api.qa.QaCommonCode;
import brain.api.qa.client.QaClient;
import brain.api.qa.data.QaResponse;
import brain.api.qa.data.dto.QuestionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@Service
public class QaService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(QaCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_SERVICE);
    private final QaClient qaClient;

    public CommonResponse<QaResponse<?>> getAnswersByQuestion(QuestionDto dto, HttpServletRequest request) throws MindsRestException {
        logger.info(dto);
        validate(dto.getApiId(), dto.getApiKey(),
                String.format("%s-%s",QaCommonCode.SERVICE_NAME, dto.getDomain().toUpperCase()), 1, request);

        if (dto.getQuestion().length() > QaCommonCode.QUESTION_TEXT_LIMIT_SIZE) {
            throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    String.format("The length of the requested question exceeds the limit size(%s).", QaCommonCode.QUESTION_TEXT_LIMIT_SIZE));
        }

        switch (dto.getDomain().toLowerCase()) {
            case "kbqa":
                if (dto.getNtop() != QaCommonCode.KBQA_NTOP) {
                    throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                            String.format("The %s-ntop exceeds the limit size(%s)", dto.getDomain(), QaCommonCode.KBQA_NTOP));
                }
                return new CommonResponse<>(new CommonMsg(), qaClient.getAnswersForKBQA(dto));
            default:
                if (dto.getNtop() > QaCommonCode.CONTENT_NTOP) {
                    throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                            String.format("The %s-ntop exceeds the limit size(%s)", dto.getDomain(), QaCommonCode.CONTENT_NTOP));
                }
                return new CommonResponse<>(new CommonMsg(), qaClient.getAnswersForContents(dto));
        }
    }
}
