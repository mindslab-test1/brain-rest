package brain.api.qa.client;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.qa.QaCommonCode;
import brain.api.qa.data.QaCommonPayload;
import brain.api.qa.data.QaPayload;
import brain.api.qa.data.QaRequest;
import brain.api.qa.data.QaResponse;
import brain.api.qa.data.dto.QuestionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Component
public class QaClient {
    public static final CloudApiLogger logger = new CloudApiLogger(QaCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private final RestTemplate restTemplate;

    private HttpHeaders defaultRequestHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        return requestHeaders;
    }

    public QaResponse<QaCommonPayload> getAnswersForKBQA(QuestionDto dto) throws MindsRestException {
        QaRequest qaRequest = QaRequest.builder()
                .question(dto.getQuestion())
                .ntop(dto.getNtop())
                .domain(dto.getDomain().toLowerCase())
                .build();
        HttpHeaders headers = defaultRequestHeaders();
        HttpEntity<QaRequest> httpEntity = new HttpEntity<>(qaRequest, headers);

        String url = QaCommonCode.KBQA_HOST + QaCommonCode.KBQA_PATH;
        try {
            ResponseEntity<QaResponse<QaCommonPayload>> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, new ParameterizedTypeReference<QaResponse<QaCommonPayload>>() {});
            QaResponse<QaCommonPayload> qaResponse = response.getBody();
            logger.info(String.format("Result: %s", qaResponse));
            return qaResponse;
        } catch (RestClientException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e.getMessage());
        }
    }

    public QaResponse<QaPayload> getAnswersForContents(QuestionDto dto) throws MindsRestException {
        QaRequest qaRequest = QaRequest.builder()
                .question(dto.getQuestion())
                .ntop(dto.getNtop())
                .domain(dto.getDomain().toLowerCase())
                .build();
        HttpHeaders headers = defaultRequestHeaders();
        HttpEntity<QaRequest> httpEntity = new HttpEntity<>(qaRequest, headers);

        String url = QaCommonCode.CONTENT_HOST + QaCommonCode.CONTENT_PATH;
        try {
            ResponseEntity<QaResponse<QaPayload>> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, new ParameterizedTypeReference<QaResponse<QaPayload>>() {});
            QaResponse<QaPayload> qaResponse = response.getBody();
            logger.info(String.format("Result: %s", qaResponse));
            return qaResponse;
        } catch (RestClientException e) {
            logger.error(e.getLocalizedMessage());
            throw new MindsRestException(e.getMessage());
        }
    }
}
