package brain.api.itf.data;

import brain.api.common.data.CommonMsg;

public class ItfBaseResponse {
    private CommonMsg message;
    private String result;

    public ItfBaseResponse(String result){
        this.result = result;
        this.message = new CommonMsg();
    }

    public ItfBaseResponse(CommonMsg message) {
        this.result = null;
        this.message = message;
    }

    public CommonMsg getMessage() {
        return message;
    }
    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ItfResponse{" +
                "message=" + message +
                ", result='" + result + '\'' +
                '}';
    }
}
