package brain.api.itf.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.itf.data.ItfBaseResponse;
import brain.api.itf.model.ItfIntent;
import brain.api.mrc.MrcCommonCode;
import maum.brain.bert.intent.BertIntentFinderGrpc;
import maum.brain.bert.intent.Bi;
import org.springframework.stereotype.Component;

@Component
public class ItfBertGrpcClient extends GrpcClientInterface {
    private static final CloudApiLogger logger = new CloudApiLogger(MrcCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private BertIntentFinderGrpc.BertIntentFinderBlockingStub bertItfBlockingStub;

    public CommonMsg setDestination(String ip, int port){
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.bertItfBlockingStub = BertIntentFinderGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e){
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public ItfBaseResponse getItf(String context){
        logger.info("[getItf] - " + context);
        Bi.Utterance.Builder utterBuilder = Bi.Utterance.newBuilder();

        Bi.Utterance utter = utterBuilder.setUtter(context).build();
        Bi.Intent output = this.bertItfBlockingStub.getIntentSimple(utter);

        ItfIntent json = new ItfIntent(output.getIntent());

        ItfBaseResponse response = new ItfBaseResponse(json.getIntent());

        return response;
    }
}
