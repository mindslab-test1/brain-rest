package brain.api.itf.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.itf.ItfCommonCode;
import com.google.gson.Gson;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import maum.m2u.common.Dialog;
import maum.m2u.router.v3.IntentFinderGrpc;
import maum.m2u.router.v3.Intentfinder;
import org.springframework.stereotype.Component;

@Component
public class ItfGrpcClient extends GrpcClientInterface {

    private static final CloudApiLogger logger = new CloudApiLogger(ItfCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private IntentFinderGrpc.IntentFinderBlockingStub itfStub;

    public CommonMsg setDestination (String ip, int port) {
        try {
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.itfStub = MetadataUtils.attachHeaders(
                    IntentFinderGrpc.newBlockingStub(
                            CommonUtils.getManagedChannel(ip, port)
                    ), setServerMeta()
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch(Exception e) {
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    public String findIntent(String utter, String inputType, String lang, String chatbot) throws MindsRestException {
        Gson gson = new Gson();

        logger.debug("ITF : " + utter + ", " + inputType + ", " + lang + ", " + chatbot);

        Intentfinder.FindIntentRequest request = buildFineIntnetReq(utter, inputType, lang, chatbot);
        Intentfinder.FindIntentResponse response;
        response = this.itfStub.findIntent(request);
        logger.info("[ITF] Success : " + response.getSkill().getSkill());
        return gson.toJson(response);
    }

    private Intentfinder.FindIntentRequest buildFineIntnetReq(
            String utter, String inputType, String lang, String chatbot
    ) throws MindsRestException
    {
        Intentfinder.FindIntentRequest.Builder builder = Intentfinder.FindIntentRequest.newBuilder();

        //1. Utter utter
        builder.setUtter(buildUtter(utter, inputType, lang));

        //2. String chatbot
        builder.setChatbot(chatbot);

        return builder.build();
    }

    private Dialog.Utter buildUtter(String utter, String inputType, String lang) throws MindsRestException {
        Dialog.Utter.Builder builder = Dialog.Utter.newBuilder();

        //1. String utter
        builder.setUtter(utter);

        //2. Enum InputType
        int inputTypeVal;
        switch (inputType.toLowerCase()) {
            case "speech" : inputTypeVal = 0; break;
            case "keyboard" : inputTypeVal = 1; break;
            case "touch" : inputTypeVal = 2; break;
            case "image" : inputTypeVal = 3; break;
            case "image_document" : inputTypeVal = 4; break;
            case "video" : inputTypeVal = 5; break;
            case "open_event" : inputTypeVal = 100; break;
            default : throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "ITF inputType : "+ inputType +" is not existence.");
        }

        builder.setInputTypeValue(inputTypeVal);

        //3. maum.common.Lang lang
        int langVal;
        switch(lang.toLowerCase()) {
            case "ko_kr" :
            case "kor" :
            case "ko" : langVal = 0; break;
            case "en_us" :
            case "eng":
            case "en" : langVal = 1; break;
            default : throw new MindsRestException(CommonExceptionCode.COMMON_ERR_BAD_REQUEST.getErrCode(),
                    "ITF lang : "+ lang +" is not existence.");
        }
        builder.setLangValue(langVal);

        return builder.build();
    }

    private Metadata setServerMeta() {
        Metadata serverMeta = new Metadata();
        Metadata.Key<String> OperationSyncId = Metadata.Key.of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);

        serverMeta.put(OperationSyncId, "bbe14c2b-c122-4406-a144-15611c8d83dd");
        return serverMeta;
    }

}
