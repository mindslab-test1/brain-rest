package brain.api.itf.model;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItfIntent implements Serializable {
    private String intent;

    public ItfIntent(String intetnt){
        this.intent = intetnt;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getIntent() {
        return intent;
    }

    @Override
    public String toString() {
        return "itfIntent{" +
                this.intent + '\'' +
                '}';
    }
}
