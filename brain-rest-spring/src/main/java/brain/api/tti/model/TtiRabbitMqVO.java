package brain.api.tti.model;

import brain.api.common.model.drama.RabbitMqDto;

public class TtiRabbitMqVO extends RabbitMqDto {

    public TtiRabbitMqVO() {
        setQueue_name("tti_actor");
        setActor_name("tti_actor");
    }
}
