package brain.api.tti.data;

import brain.api.common.enums.CommonExceptionCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TtiBaseResponse {
    private byte[] imageAsBytes;
    private CommonMsg message;

    public TtiBaseResponse() {
        this.message = new CommonMsg(CommonExceptionCode.COMMON_ERR_INTERNAL.getErrCode());
    }

    public TtiBaseResponse(byte[] imageAsBytes, CommonMsg message) {
        this.imageAsBytes = imageAsBytes;
        this.message = message;
    }

    public byte[] getImageAsBytes() {
        return imageAsBytes;
    }

    public void setImageAsBytes(byte[] imageAsBytes) {
        this.imageAsBytes = imageAsBytes;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TtiBaseResponse{" +
                "message=" + message +
                '}';
    }
}
