package brain.api.tti.client;

import brain.api.common.data.CommonMsg;
import brain.api.tti.data.TtiBaseResponse;

public interface TtiClientInterface {
    public TtiBaseResponse imageGen(String reqText);
    public CommonMsg setDestination(String ip, int port);
}
