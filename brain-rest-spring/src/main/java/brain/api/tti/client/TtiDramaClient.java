package brain.api.tti.client;

import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.tti.TtiCommonCode;
import brain.api.tti.model.TtiRabbitMqVO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.exceptions.JedisConnectionException;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
@JsonIgnoreProperties(ignoreUnknown=true)
public class TtiDramaClient {

    private static final CloudApiLogger logger = new CloudApiLogger(TtiCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    private final String RABBITMQ_HOSTADDR = "10.122.64.85";
    private static final String REDIS_HOST = "10.122.64.85";
    private static final String REDIS_PASS = "msl1234~";
    private final int TIME_OUT = 1000;

    /**
     * tti RabbitMQ Enqueue
     * 2019.6.28 Byeonguk.yu
     */
    public String ttiRabbitMqEnqueue(String reqText) throws MindsRestException {
        final UUID UNIQUE_ID = UUID.randomUUID();
        TtiRabbitMqVO rabbitMqVO = new TtiRabbitMqVO();
        String messageKey = "";

        ConnectionFactory factory = new ConnectionFactory();
        Gson gson = new Gson();

        Map <String, String> kwargsMap = new HashMap <>();
        Map <String, String> optionsMap = new HashMap <>();
        optionsMap.put("store_results", "True");

        rabbitMqVO.setMessage_id(UNIQUE_ID.toString());
        rabbitMqVO.setMessage_timestamp(Calendar.getInstance().getTimeInMillis());
        rabbitMqVO.setKwargs(kwargsMap);
        rabbitMqVO.setOptions(optionsMap);

        Object[] args = new Object[1];
        args[0] = reqText;
        rabbitMqVO.setArgs(args);

        String objJson = gson.toJson(rabbitMqVO);

        factory.setHost(RABBITMQ_HOSTADDR);

        Map <String, Object> declareArgs = new HashMap <>();
        declareArgs.put("x-dead-letter-exchange", "");
        declareArgs.put("x-dead-letter-routing-key", rabbitMqVO.getQueue_name() + ".XQ");

        try (
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            messageKey = CommonUtils.dramaBuildMessageKey(rabbitMqVO);
            channel.queueDeclare(rabbitMqVO.getQueue_name(), true, false, false, declareArgs);
            channel.basicPublish("", rabbitMqVO.getQueue_name(), null, objJson.getBytes("UTF-8"));
        } catch(Exception e) {
            logger.debug(e.getMessage());
            throw new MindsRestException(500, "TTI Connect Err");
        }

        return messageKey;

    }

    private JedisPool redisPool = new JedisPool(
        new JedisPoolConfig(),              // config
        REDIS_HOST,                         // host
        6379,                         // port
        Protocol.DEFAULT_TIMEOUT,           // timeout
        REDIS_PASS                          // password
    );


    public ResponseEntity ttiRedisGetImage(String messagekey) throws IOException {
        Gson gson = new Gson();
        Jedis redis = null;
        boolean isLast = false;
        String result = "";

        try {
            redis = redisPool.getResource();
            List<String> resultList = redis.blpop(TIME_OUT, messagekey);

            for (String tempResult : resultList) {
                result = tempResult;
            }

        }
        catch(JedisConnectionException e) {
            if (redis != null) {
                redis.close();
                redis = null;
            }
            throw e;
        }
        finally {
            if (redis != null) {
                redis.close();
            }
        }

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = (JsonArray) jsonParser.parse(result);

        String base64Img = jsonArray.get(1).getAsString();

        //TODO jsonArray(BASE64 String) >> IMG 변환 코드 필요
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] imageByte = decoder.decodeBuffer(base64Img);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        BufferedImage image = ImageIO.read(bis);
        bis.close();

        File outputFile = new File("C:/maum/api/tti/tti_" + Calendar.getInstance().getTimeInMillis() + ".jpeg");
        if (!outputFile.exists()) {
            outputFile.mkdirs();
        }
        ImageIO.write(image, "jpeg", outputFile);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(new MediaType("image", "jpg"));
        httpHeaders.setCacheControl(CacheControl.noCache().getHeaderValue());

        return new ResponseEntity(imageByte, httpHeaders, HttpStatus.OK);
    }

    public ResponseEntity<?> doTtiDrama(String reqText) throws IOException, MindsRestException {
        return ttiRedisGetImage(ttiRabbitMqEnqueue(reqText));
    }
}
