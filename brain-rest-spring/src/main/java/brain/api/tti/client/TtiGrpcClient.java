package brain.api.tti.client;

import brain.api.common.CommonCode;
import brain.api.common.client.GrpcClientInterface;
import brain.api.common.data.CommonMsg;
import brain.api.common.enums.CommonExceptionCode;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.tti.TtiCommonCode;
import brain.api.tti.data.TtiBaseResponse;
import com.google.protobuf.ByteString;
import maum.brain.tti.TTIGrpc;
import maum.brain.tti.Tti;
import org.springframework.stereotype.Component;

// TODO TTI service reference 장인호 매니저님
@Component
public class TtiGrpcClient extends GrpcClientInterface implements TtiClientInterface{
    private static final CloudApiLogger logger = new CloudApiLogger(TtiCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);
    private TTIGrpc.TTIBlockingStub ttiBlockingStub;

    @Override
    public CommonMsg setDestination(String ip, int port){
        try{
            if(super.checkDestination(ip, port)) return new CommonMsg();
            logger.debug(String.format("set destination %s, %d", ip, port));
            this.ttiBlockingStub = TTIGrpc.newBlockingStub(
                    CommonUtils.getManagedChannel(ip, port)
            ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());
            return new CommonMsg();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new CommonMsg(CommonExceptionCode.COMMON_ERR_NOT_FOUND.getErrCode());
        }
    }

    @Override
    public TtiBaseResponse imageGen(String reqText){
        Tti.InputText inputText = Tti.InputText.newBuilder()
                .setCaption(reqText)
                .build();

        Tti.OutputImg outputImg = ttiBlockingStub.genImg(inputText);
        ByteString outputImgRaw = outputImg.getByteimg();

        try{
            TtiBaseResponse response = new TtiBaseResponse(
                    outputImgRaw.toByteArray(),
                    new CommonMsg()
            );
            logger.info("[Legacy TTI] - responseMessage : " + response.getMessage() + ", responseByteLength : " + response.getImageAsBytes().length);

            return response;
        } catch (Exception e){
            logger.error(e.getMessage());
            return new TtiBaseResponse();
        }
    }

}
