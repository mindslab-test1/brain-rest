package brain.api.hmd.client;

import brain.api.common.CommonCode;
import brain.api.common.data.CommonMsg;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.utils.CommonUtils;
import brain.api.hmd.HmdCommonCode;
import brain.api.hmd.data.HmdBaseResponse;
import brain.api.hmd.data.HmdClassified;
import brain.api.nlp.data.NlpDocument;
import maum.brain.hmd.Hmd;
import maum.brain.hmd.HmdClassifierGrpc;
import maum.common.LangOuterClass;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class HmdGrpcClient {
    private static final CloudApiLogger logger = new CloudApiLogger(HmdCommonCode.SERVICE_NAME, CommonCode.COMMON_CLASSNAME_CLIENT);

    public HmdBaseResponse hmdLegacy(String requestText, String lang){
        logger.debug("hmd legacy kor");
        HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(
                CommonUtils.getManagedChannel("54.180.179.241", 9816)
        ).withExecutor(CommonUtils.gRPCThreadPoolExecutor());

        logger.debug("set channel");
        Hmd.HmdInputText text;
        if(lang.equals("kor") || lang.equals("ko_KR"))
             text = Hmd.HmdInputText.newBuilder()
                    .setText(requestText)
                    .setModel("sentiment")
                    .setLang(LangOuterClass.LangCode.kor)
                    .build();
        else
            text = Hmd.HmdInputText.newBuilder()
                    .setText(requestText)
                    .setModel("sentiment")
                    .setLang(LangOuterClass.LangCode.eng)
                    .build();

        logger.debug("set text");
        Hmd.HmdResultDocument res = stub.getClassByText(text);
        logger.debug("get class");
        NlpDocument document = CommonUtils.nlpDocumentBuilder(res.getDocument());

        ArrayList<HmdClassified> clsList = new ArrayList<>();
        for(Hmd.HmdClassified classified: res.getClsList())
            clsList.add(
                    new HmdClassified(
                            classified.getSentSeq(),
                            classified.getCategory(),
                            classified.getPattern(),
                            classified.getSearchKey(),
                            classified.getSentence()
                    )
            );

        HmdBaseResponse hmdBaseResponse = new HmdBaseResponse(
                new CommonMsg(),
                document,
                clsList
        );
        logger.info(String.format("Result: %s", hmdBaseResponse));

        return hmdBaseResponse;
    }
}
