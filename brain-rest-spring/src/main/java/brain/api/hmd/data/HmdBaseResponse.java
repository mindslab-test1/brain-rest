package brain.api.hmd.data;

import brain.api.common.data.CommonMsg;
import brain.api.nlp.data.NlpDocument;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HmdBaseResponse implements Serializable {
    private CommonMsg message;
    private NlpDocument document;
    private List<HmdClassified> cls;

    public HmdBaseResponse() {
    }

    public HmdBaseResponse(CommonMsg message, NlpDocument document, List<HmdClassified> cls) {
        this.message = message;
        this.document = document;
        this.cls = cls;
    }

    public CommonMsg getMessage() {
        return message;
    }

    public void setMessage(CommonMsg message) {
        this.message = message;
    }

    public NlpDocument getDocument() {
        return document;
    }

    public void setDocument(NlpDocument document) {
        this.document = document;
    }

    public List<HmdClassified> getCls() {
        return cls;
    }

    public void setCls(List<HmdClassified> cls) {
        this.cls = cls;
    }

    @Override
    public String toString() {
        return "HmdBaseResponse{" +
                "message=" + message +
                ", document=" + document +
                ", cls=" + cls +
                '}';
    }
}
