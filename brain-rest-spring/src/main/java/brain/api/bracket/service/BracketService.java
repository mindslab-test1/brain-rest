package brain.api.bracket.service;

import brain.api.bracket.BracketCommonCode;
import brain.api.bracket.client.BracketClient;
import brain.api.common.CommonCode;
import brain.api.common.exception.MindsRestException;
import brain.api.common.log.CloudApiLogger;
import brain.api.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class BracketService extends BaseService {
    private static final CloudApiLogger logger = new CloudApiLogger(BracketCommonCode.SERVICE_NAME, CommonCode.COMMON_SERVICE_NAME);

    @Autowired
    BracketClient bracketClient;

    public ByteArrayOutputStream downloadZip(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException, InterruptedException, IOException {
        validate(apiId, apiKey, BracketCommonCode.SERVICE_NAME, 1, request);
        bracketClient.setDestination("182.162.19.12", 1444);
        String processId = bracketClient.processing(file);
        return bracketClient.downloadZip(processId);
    }

    public ByteArrayOutputStream downloadAb(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException, InterruptedException, IOException {
        validate(apiId, apiKey, BracketCommonCode.SERVICE_NAME, 1, request);
        bracketClient.setDestination("182.162.19.12", 1444);
        String processId = bracketClient.processing(file);
        return bracketClient.downloadAb(processId);
    }

    public ByteArrayOutputStream downloadRegion(String apiId, String apiKey, MultipartFile file, HttpServletRequest request) throws MindsRestException, InterruptedException, IOException {
        validate(apiId, apiKey, BracketCommonCode.SERVICE_NAME, 1, request);
        bracketClient.setDestination("182.162.19.12", 1444);
        String processId = bracketClient.processing(file);
        return bracketClient.downloadRegion(processId);
    }

}
