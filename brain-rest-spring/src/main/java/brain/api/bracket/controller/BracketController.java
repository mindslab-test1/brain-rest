package brain.api.bracket.controller;

import brain.api.bracket.service.BracketService;
import brain.api.common.data.CommonMsg;
import brain.api.common.data.ErrorResponse;
import brain.api.common.exception.MindsRestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.annotation.Resource;
import javax.mail.Multipart;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;

@RestController
@RequestMapping("/bracket")
public class BracketController {

    @Autowired
    private BracketService bracketService;

    @RequestMapping(
        value = "/downloadZip",
        method = RequestMethod.POST
    ) public ResponseEntity downloadZip(
        @RequestParam("apiId") String apiId,
        @RequestParam("apiKey") String apiKey,
        @RequestParam("file") MultipartFile file,
        HttpServletRequest httpRequest
    ){
        try {
            ByteArrayOutputStream buffer = bracketService.downloadZip(apiId, apiKey, file, httpRequest);
            String orgFilename = file.getOriginalFilename();
            String filename = orgFilename.substring(0, orgFilename.lastIndexOf('.'));
            String extension = ".zip";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=bracket-"+filename+extension)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (Exception e) {
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
        value = "/downloadAb",
        method = RequestMethod.POST
    ) public ResponseEntity downloadAb(
        @RequestParam("apiId") String apiId,
        @RequestParam("apiKey") String apiKey,
        @RequestParam("file") MultipartFile file,
        HttpServletRequest httpRequest
    ){
        try {
            ByteArrayOutputStream buffer = bracketService.downloadAb(apiId, apiKey, file, httpRequest);
            String orgFilename = file.getOriginalFilename();
            String filename = orgFilename.substring(0, orgFilename.lastIndexOf('.'));
            String extension = ".obj";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=bracket-ab-"+filename+extension)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (Exception e) {
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(
        value = "downloadRegion",
        method = RequestMethod.POST
    ) public ResponseEntity downloadRegion(
            @RequestParam("apiId") String apiId,
            @RequestParam("apiKey") String apiKey,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest httpRequest
    ){
        try {
            ByteArrayOutputStream buffer = bracketService.downloadRegion(apiId, apiKey, file, httpRequest);
            String orgFilename = file.getOriginalFilename();
            String filename = orgFilename.substring(0, orgFilename.lastIndexOf('.'));
            String extension = ".obj";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=bracket-region-"+filename+extension)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(buffer.toByteArray());
        } catch (Exception e) {
            return new ResponseEntity(new ErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
